# combo - content management system
# Copyright (C) 2014  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Django settings file; it loads the default settings, and local settings
(from a local_settings.py file, or a configuration file set in the
COMBO_SETTINGS_FILE environment variable).

The local settings file should exist, at least to set a suitable SECRET_KEY,
and to disable DEBUG mode in production.
"""

import copy
import os

from django.conf import global_settings
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r^(w+o4*txe1=t+0w*w3*9%idij!yeq1#axpsi4%5*u#3u&)1t'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LIVERELOAD_ENABLED = False

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'rest_framework',
    'ckeditor',
    'eopayment',
    'gadjo',
    'sorl.thumbnail',
    'combo.data',
    'combo.locales',
    'combo.profile',
    'combo.manager',
    'combo.public',
    'combo.apps.assets',
    'combo.apps.dashboard',
    'combo.apps.wcs',
    'combo.apps.publik',
    'combo.apps.export_import',
    'combo.apps.family',
    'combo.apps.dataviz',
    'combo.apps.lingo',
    'combo.apps.fargo',
    'combo.apps.notifications',
    'combo.apps.search',
    'combo.apps.maps',
    'combo.apps.pwa',
    'combo.apps.gallery',
    'combo.apps.kb',
    'xstatic.pkg.leaflet',
    'xstatic.pkg.opensans',
    'xstatic.pkg.roboto_fontface',
    'xstatic.pkg.leaflet_markercluster',
    'xstatic.pkg.leaflet_gesturehandling',
    'publik_django_templatetags',
    'xstatic.pkg.select2',
)

MIDDLEWARE = (
    'combo.middleware.GlobalRequestMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# Serve xstatic files, required for gadjo
STATICFILES_FINDERS = list(global_settings.STATICFILES_FINDERS) + ['gadjo.finders.XStaticFinder']

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'combo.context_processors.template_vars',
                'publik_django_templatetags.wcs.context_processors.wcs_objects',
            ],
            'builtins': [
                'combo.public.templatetags.combo',
                'publik_django_templatetags.publik.templatetags.publik',
                'django.contrib.humanize.templatetags.humanize',
                'combo.apps.wcs.templatetags.wcs',
                'publik_django_templatetags.wcs.templatetags.wcs',
            ],
        },
    },
]

ROOT_URLCONF = 'combo.urls'

WSGI_APPLICATION = 'combo.wsgi.application'

# custom date formats
FORMAT_MODULE_PATH = 'combo.formats'

# Database
# https://docs.djangoproject.com/en/2.2/ref/databases/#postgresql-notes

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'UTC'

USE_I18N = True


USE_TZ = True

LOCALE_PATHS = (os.path.join(BASE_DIR, 'combo', 'locales', 'locale'),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Allow embedding in <iframe>, this is required for the PWA preview mode
X_FRAME_OPTIONS = 'SAMEORIGIN'

# mode for newly updated files
FILE_UPLOAD_PERMISSIONS = 0o644

CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_IMAGE_BACKEND = 'pillow'

CKEDITOR_CONFIGS = {
    'default': {
        'allowedContent': True,
        'removePlugins': 'stylesheetparser',
        'toolbar_Own': [
            ['Source', 'Format', '-', 'Bold', 'Italic'],
            ['NumberedList', 'BulletedList'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['Image', '-', 'HorizontalRule'],
            [
                'RemoveFormat',
            ],
            ['Maximize'],
        ],
        'toolbar': 'Own',
        'resize_enabled': False,
        'width': '100%',
    },
}

CKEDITOR_CONFIGS['small'] = copy.copy(CKEDITOR_CONFIGS['default'])
CKEDITOR_CONFIGS['small']['height'] = 150

# from solr.thumbnail -- https://sorl-thumbnail.readthedocs.io/en/latest/reference/settings.html
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_FORCE_OVERWRITE = False

COMBO_DEFAULT_PUBLIC_TEMPLATE = 'standard'
COMBO_PUBLIC_TEMPLATES = {
    'standard': {
        'name': _('One column'),
        'template': 'combo/page_template.html',
        # get those labels into gettext catalog
        '_': (_('Top of content'), _('Bottom of content')),
    },
    'standard-sidebar': {
        'name': _('One column + sidebar'),
        'template': 'combo/page_template_sidebar.html',
    },
    'two-columns': {
        'name': _('Two columns'),
        'template': 'combo/page_template_2cols.html',
    },
    'two-columns-sidebar': {
        'name': _('Two columns + sidebar'),
        'template': 'combo/page_template_2cols_sidebar.html',
        # get those labels into gettext catalog
        '_': (_('Left column'), _('Right column')),
    },
    'homepage': {
        'name': _('Home page'),
        'template': 'combo/page_template_homepage.html',
        # get those labels into gettext catalog
        '_': (_('Top banner'), _('Middle column')),
    },
}
COMBO_PUBLIC_TEMPLATES_ROOT_PAGE = 'modeles'

# extra variables for templates
TEMPLATE_VARS = {}

# Authentication settings
try:
    import mellon
except ImportError:
    mellon = None

if mellon is not None:
    INSTALLED_APPS += ('mellon',)
    AUTHENTICATION_BACKENDS = (
        'mellon.backends.SAMLBackend',
        'django.contrib.auth.backends.ModelBackend',
    )

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []

# search services
COMBO_SEARCH_SERVICES = {}
POSTGRESQL_FTS_SEARCH_CONFIG = 'french_unaccent'

# mapping of payment modes
LINGO_NO_ONLINE_PAYMENT_REASONS = {}

JSON_CELL_TYPES = {}

# dashboard support
COMBO_DASHBOARD_ENABLED = False

COMBO_DASHBOARD_NEW_TILE_POSITION = 'last'

# collapse pages on combo home
COMBO_MANAGE_HOME_COLLAPSE_PAGES = False

# default position on maps
COMBO_MAP_DEFAULT_POSITION = {'lat': '48.83369263315934', 'lng': '2.3233688436448574'}

# default map bounds
COMBO_MAP_MAX_BOUNDS = {'corner1': {'lat': None, 'lng': None}, 'corner2': {'lat': None, 'lng': None}}

# default map tiles url
COMBO_MAP_TILE_URLTEMPLATE = 'https://tiles.entrouvert.org/hdm/{z}/{x}/{y}.png'

# default combo map attribution
COMBO_MAP_ATTRIBUTION = _(
    'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
)

# geocoding service
COMBO_MAP_GEOCODING_URL = 'https://nominatim.entrouvert.org/search'

# send notifications about new invoices
LINGO_INVOICE_NOTIFICATIONS_ENABLED = True

# default delta, in days, for invoice remind notifications
LINGO_NEW_INVOICES_REMIND_DELTA = 7

# allow external payment URL
LINGO_ALLOW_EXTERNAL_PAYMENT_URL = True

# default site
SITE_BASE_URL = 'http://localhost'

# timeout used in python-requests call, in seconds
# we use 28s by default: timeout just before web server, which is usually 30s
REQUESTS_TIMEOUT = 28

# default duration of notifications (in days)
COMBO_DEFAULT_NOTIFICATION_DURATION = 3

# tracking code throttling
WCS_TRACKING_CODE_RATE_LIMIT = '3/s 1500/d'

# predefined slots for assets
# example: {'banner': {'label': 'Banner image'}}
COMBO_ASSET_SLOTS = {}

# dynamic slots created for wcs category/form cells
# example: {'picture': {'prefix': 'Picture'}}
# XXX deprecated
WCS_CATEGORY_ASSET_SLOTS = {}

# maximum number of forms retrieved in user forms cell
WCS_USER_FORMS_LIMIT = 100

# XXX deprecated
WCS_FORM_ASSET_SLOTS = {}

COMBO_ASSET_IMAGE_MAX_WIDTH = 1200

COMBO_CELL_ASSET_SLOTS = {
    'data_feedcell': {
        'picture': {
            'prefix': _('Picture'),
        },
    },
    'data_linklistcell': {
        'picture': {
            'prefix': _('Picture'),
        },
    },
    'wcs_wcsformsofcategorycell': {
        'picture': {
            'prefix': _('Picture'),
        },
    },
}

# serve assets using nginx X-Accel
COMBO_X_ACCEL_ASSETS = False

COMBO_CELL_TEMPLATES = {}

COMBO_MAP_LAYER_ASSET_SLOTS = {}

# known services
KNOWN_SERVICES = {}

# services known to expose statistics
STATISTICS_PROVIDERS = ['bijoe', 'authentic', 'wcs', 'chrono', 'passerelle', 'lingo']

# PWA Settings
PWA_VAPID_PUBLIK_KEY = None
PWA_VAPID_PRIVATE_KEY = None
PWA_VAPID_CLAIMS = None
PWA_NOTIFICATION_BADGE_URL = None
PWA_NOTIFICATION_ICON_URL = None


# hide work-in-progress/experimental/broken/legacy/whatever cells for now
BOOKING_CALENDAR_CELL_ENABLED = False
LEGACY_CHART_CELL_ENABLED = False
PUBLIK_FAMILY_CELL_ENABLED = False
CATEGORIES_CELL_ENABLED = False
CATEGORY_CELL_ENABLED = False


# and enable others
CHART_FILTERS_CELL_ENABLED = True

# default country code for phonenumbers' user phone parsing
DEFAULT_COUNTRY_CODE = '33'

REST_FRAMEWORK = {'EXCEPTION_HANDLER': 'combo.utils.api.exception_handler'}


def debug_show_toolbar(request):
    from debug_toolbar.middleware import show_toolbar as dt_show_toolbar  # pylint: disable=import-error

    return dt_show_toolbar(request) and not request.path.startswith('/__skeleton__/')


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': debug_show_toolbar}


local_settings_file = os.environ.get(
    'COMBO_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as fd:
        exec(fd.read())
