# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.conf import settings
from django.utils.functional import SimpleLazyObject

from combo.apps.pwa.models import PwaSettings
from combo.utils.cache import cache_during_request


def template_vars(request):
    context_extras = {
        'debug': settings.DEBUG,
        'livereload_enabled': settings.LIVERELOAD_ENABLED,
        'pwa_settings': cache_during_request(PwaSettings.singleton),
        'true': True,
        'false': False,
        'null': None,
        'now': SimpleLazyObject(datetime.datetime.now),
        'today': SimpleLazyObject(datetime.date.today),
    }
    context_extras.update(settings.TEMPLATE_VARS)
    return context_extras
