#
# combo - content management system
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django import forms
from django.core import validators


def get_available_image_extensions():
    return validators.get_available_image_extensions() + ['svg']


validate_image_file_extension = validators.FileExtensionValidator(
    allowed_extensions=get_available_image_extensions(),
)


class ImageIncludingSvgField(forms.ImageField):
    default_validators = [validate_image_file_extension]

    def to_python(self, data):
        if data.name and data.name.endswith('.svg'):
            # bypass image field Pillow check
            return super(forms.ImageField, self).to_python(data)
        return super().to_python(data)
