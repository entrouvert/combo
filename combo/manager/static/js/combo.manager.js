function multisort(element)
{
  var $category_selector = $(element).parents('.cell').find('select.category-select');
  var category_value = null;
  if ($category_selector.length) {
    category_value = $category_selector.val();
    $category_selector.off('change').on('change', function() {
      multisort(element);
    });
  }
  var $ordering_selector = $(element).parents('.cell').find('select.ordering-select');
  if ($ordering_selector.length) {
    $ordering_selector.off('change').on('change', function() {
      var val = $(this).val();
      if (val == 'manual') {
        $(element).prev().show();
        $(element).show();
      } else {
        $(element).prev().hide();
        $(element).hide();
      }
    }).trigger('change');
  }

  $(element).find('ul').remove();
  $(element).find('select').hide();

  var $ul = $('<ul class="multisort"></ul>');

  var checkboxes = $(element).data('checkboxes');

  $(element).find('option').each(function(i, x) {
    if (category_value && $(x).val().indexOf(category_value + ':') != 0) {
      return;
    }
    var checkbox = '';
    if (checkboxes) {
      if ($(x).attr('selected')) {
        checkbox = '<input type="checkbox" checked/>'
      } else {
        checkbox = '<input type="checkbox"/>'
      }
    }
    $('<li data-value="' + $(x).val() + '"><span class="handle">⣿</span><label>'+ checkbox + $(x).text() + '</label></li>').appendTo($ul);
  });
  $ul.appendTo(element);

  function multisort_sync() {
    var $select = $(element).find('select');
    var options = Array();
    $ul.find('li').each(function(i, x) {
      var selected = true;
      if (checkboxes && $(x).find('input[type=checkbox]:checked').length == 0) {
        selected = false;
      }
      var value = $(x).data('value');
      var $option = $select.find('[value="' + value + '"]');
      if (selected) {
        $option.prop('selected', 'selected');
      } else {
        $option.prop('selected', null);
      }
      $option.detach();
      options.push($option);
    });
    while (options.length) {
      $select.prepend(options.pop());
    }
  }

  $ul.find('input[type=checkbox]').on('change', function() {
    multisort_sync();
  });
  $ul.sortable({
    handle: '.handle',
    update: function(event, ui) {
      multisort_sync();
    }
  });
}

function get_page_children($page) {
  var children = [];
  $page.nextAll().each(function() {
    var level = parseInt($(this).data('level'), 10);
    if (level <= parseInt($page.data('level'), 10)) {
      return false;
    }
    children.push($(this));
  });
  return children;
}

function show_page_children($page) {
  $.each(get_page_children($page), function() {
    $child = $(this);
    var level = parseInt($child.data('level'), 10);
    if (level == parseInt($page.data('level'), 10) + 1) {
      $child.show();
      if ($child.hasClass('toggled')) {
        show_page_children($child);
      }
    }
  });
}

function hide_page_children($page) {
  $.each(get_page_children($page), function() {
    $(this).hide();
  });
}

function init_pages_toggle() {
  $('.page').each(function() {
    if (!get_page_children($(this)).length) {
      $(this).removeClass('toggled').removeClass('untoggled').find('.togglable').remove();
    }
  });
  $('div.page.untoggled').each(function() {
    hide_page_children($(this));
  });
  $('div.page.toggled[level=1]').each(function() {
    show_page_children($(this));
  });
}

function init_pages_list(toggle_state)
{
  if ($('#pages-list').length == 0)
    return;
  init_pages_toggle();
  // if toggle_state is defined, restore toggled pages
  if (toggle_state) {
    $('div.page').each(function() {
      if (toggle_state.includes($(this).data('page-id'))) {
        $('span.togglable', $(this)).trigger('click');
      }
    });
  }
  var list_offset = $('#pages-list').offset().left;
  $('#pages-list').sortable({
     handle: '.handle',
     stop: function(event, ui) {
       var moved_page_id = ui.item.data('page-id');
       /* 25 is the per-level margin-left applied to pages, it needs to be kept
        * in sync with the css file */
       var item_offset = ui.offset.left - list_offset + parseInt($(ui.item).data('level'))*25;
       var new_level = Math.abs(Math.round(item_offset / 25));
       if (new_level <= 0) {
         new_level = 0;
       } else {
         var previous_page = $('#pages-list div[data-page-id=' + moved_page_id + ']').prev();
         var previous_page_level = parseInt($(previous_page).data('level'));
         if (new_level > previous_page_level+1) {
           new_level = previous_page_level+1;
         }
       }

       var new_parent = null;
       if (new_level != 0) {
          new_parent = $($(ui.item).prevAll('[data-level='+(new_level-1)+']')[0]).data('page-id');
       }
       /* remove classes and add new values */
       $(ui.item).removeClass('level-0').removeClass('level-1').removeClass('level-2');
       $(ui.item).addClass('level-' + new_level);
       $(ui.item).data('level', new_level).attr('data-level', new_level);

       var new_order = $('#pages-list div').map(function() { return $(this).data('page-id'); }).get().join();
       // keep actuel toggled pages + new parent to restore display on init_pages_list
       var old_toggle_state = [];
       $('div.page.toggled').each(function() {
         old_toggle_state.push($(this).data('page-id'));
       });
       if (!old_toggle_state.includes(new_parent)) {
         old_toggle_state.push(new_parent);
       }

       $.ajax({
           url: $('#pages-list').data('page-order-url'),
           type: 'POST',
           data: {'new-order': new_order,
                  'moved-page-id': moved_page_id,
                  'moved-page-new-parent': new_parent
           },
           success: function(data, status) {
             $('#pages-list').replaceWith($(data).find('#pages-list'));
             init_pages_list(old_toggle_state);
           }
       });
     }
  });
}

function escape_html(value) {
  // Empty node used to escape templates rendering
  let escaper = document.createElement('div');
  escaper.textContent = value;
  value = escaper.innerHTML;
  delete(escaper);
  return value;
}


$(function() {
  $('div.cell-list h3').on('click', function() {
    $(this).parent().toggleClass('toggled').toggleClass('untoggled');
  });
  $('div.cell-list a.close-button').on('click', function() {
    $(this).parents('.toggled').toggleClass('toggled').toggleClass('untoggled');
    return false;
  });
  init_pages_list();
  $(document).on('click', 'div.page.untoggled span.togglable', function() {
    $(this).parent().toggleClass('toggled').toggleClass('untoggled');
    show_page_children($(this).parent());
  });
  $(document).on('click', 'div.page.toggled span.togglable', function() {
    $(this).parent().toggleClass('toggled').toggleClass('untoggled');
    hide_page_children($(this).parent());
  });

  function init_cells_lists() {
    $('.cell-list').each(function() {
      if ($(this).find('div.cell').length == 0) {
        $(this).addClass('empty-cell-list');
      } else {
        $(this).removeClass('empty-cell-list');
      }
    });
  }
  init_cells_lists();

  $('.cell-list').sortable({
     handle: '.handle',
     connectWith: '.cell-list',
     helper: 'clone',
     placeholder: 'empty-cell',
     tolerance: 'pointer',
     start: function(event, ui) {
             if (ui.helper.parent().find('div.cell').length <= 2) {
                     /* cell + placeholder */
                     ui.helper.parent().addClass('empty-cell-list');
             }
             $('body').addClass('dragging-cell');
             ui.helper.removeClass('toggled').addClass('untoggled').css('height',
                             ui.helper.find('h3').outerHeight());
     },
     stop: function(event, ui) {
             $('body').removeClass('dragging-cell');
             init_cells_lists();
     },
     update: function(event, ui) {
       var new_order = Object();
       $('.cell').each(function(i, x) {
           var cell_suffix = $(x).data('cell-reference');
           new_order['pos_' + cell_suffix] = i;
           new_placeholder = $(x).closest('.placeholder').data('placeholder-key');
           new_order['ph_' + cell_suffix] = new_placeholder;
       });
       $.ajax({
           url: $('#placeholders').data('cell-order-url'),
           data: new_order
       });
     }
  });
  $('#sidebar button').on('click', function() {
    window.location = $(this).data('add-url');
    return false;
  });

  $('div.cell button.save').on('click', function(event) {
     var $button = $(this);
     var $form = $(this).closest('form');
     var $cell = $(this).closest('div.cell');
     var button_label = $button.text();
     for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
     }
     $.ajax({
         url: $form.attr('action'),
         data: $form.serialize(),
         type: 'POST',
         beforeSend: function() { $button.attr('disabled', 'disabled'); },
         success: function(data) {
            $button.attr('disabled', null);
            for (const tab_slug in data.tabs) {
              var $tab_content = $form.find('[data-tab-slug="' + tab_slug + '"]');
              if (data.tabs[tab_slug].form.indexOf('ckeditortype') == -1) {
                /* update form with new content, unless it has a ckeditor, as
                 * this causes an unpleasant flickering */
                $tab_content.html(data.tabs[tab_slug].form);
              } else {
                for (const error in data.errorlist[tab_slug]) {
                  var $widget_p = $('[name="' + data.prefix + '-' + error).closest('p');
                  var $widget_ul = $('<ul class="errorlist"></ul>');
                  for (idx in data.errorlist[tab_slug][error]) {
                    $widget_ul.append($('<li>', {text: data.errorlist[tab_slug][error][idx]}));
                  }
                  $widget_p.before($widget_ul);
                }
              }
              if (data.tabs[tab_slug].is_not_default) {
                $('#tab-' + $cell.data('cell-reference') + '-' + tab_slug).addClass('pk-tabs--button-marker');
              } else {
                $('#tab-' + $cell.data('cell-reference') + '-' + tab_slug).removeClass('pk-tabs--button-marker');
              }
            }
            // update title labels
            $cell.find('.visibility-summary').removeClass(
              ).addClass('visibility-summary').addClass(data.visibility_css_class
              ).text(data.visibility_content);
            function wrap(label, sign) {
              if (! label) return '';
              if (sign == '(') return '(' + label + ')';
              if (sign == '[') return '[' + label + ']';
              return label;
            }
            $cell.find('h3 .cell-slug').text(wrap(data.slug, '['));
            $cell.find('h3 .cell-template-label').text(wrap(data.template_label, '('));
            $cell.find('h3 .extra-css-class').text(wrap(data.extra_css_class, '['));
            $cell.find('h3 .additional-label i').text(wrap(data.additional_label));
            if (data['invalid_reason']) {
              if (! $cell.find('.invalid').length) {
                $('<span class="invalid"></span>').insertAfter($cell.find('.additional-label'));
              }
              $cell.find('.invalid').text(data['invalid_reason']);
            } else {
              $cell.find('.invalid').remove();
            }

            $form.parents('div.cell').trigger('combo:cellform-reloaded');

            // select first panel with errors (if any)
            const $current_tab = $form.find('[role=tabpanel]:not([hidden])');
            if ($current_tab.find('.errorlist').length == 0) {
              const $panel_with_error = $form.find('[role=tabpanel] .errorlist').first().parents('[role=tabpanel]').first();
              if ($panel_with_error.length) {
                const $tab_button = $('[role=tab][aria-controls="' + $panel_with_error.attr('id') + '"]');
                $current_tab.closest('.pk-tabs')[0].tabs.selectTab($tab_button[0]);
              }
            }
         }
     });
     return false;
  });

  function handle_tipi_form(element) {
    var prefix_components = element.attr('name').split('-');
    /* remove field name and keep only prefix */
    prefix_components.pop();
    var prefix = prefix_components.join('-');
    function hide_fields(fields) {
        fields.forEach(function(f) {
            $("[name="+prefix+"-"+f+"]").parent().hide();
        });
    }
    function show_fields(fields) {
        fields.forEach(function(f) {
            $("[name="+prefix+"-"+f+"]").parent().show();
        })
    }
    if (element.val() == 'pesv2') {
        show_fields(['idpce', 'idligne']);
        hide_fields(['rolrec', 'roldeb', 'roldet']);
    } else {
        show_fields(['rolrec', 'roldeb', 'roldet']);
        hide_fields(['idpce', 'idligne']);
    }
  }

  $('.cell.tipi-payment-form-cell select').on('change', function() {
      handle_tipi_form($(this));
  });
  $('.cell.tipi-payment-form-cell select').trigger('change');

  $('.cell.tipi-payment-form-cell').on('combo:cellform-reloaded', function() {
      var $select = $(this).find('select');
      $select.on('change', function() {
          handle_tipi_form($select);
      });
      $select.trigger('change');
  });

  $('div.cell').each(function(i, x) {
    const $cell = $(this);
    if (window.location.hash == '#' + $cell.attr('id') || (
            window.location.hash &&
            window.location.hash.indexOf('#open:') == 0 &&
            $cell.find('button#tab-' + window.location.hash.substring(6)).length
    )) {
      $cell.addClass('toggled');
    } else {
      $cell.addClass('untoggled');
    }

    const $animated = $cell.find('h3 + div');
    $animated.on('transitionend', function(el){
      if ($cell.is(".toggled")) {
        $cell.trigger("cell:open");
      }
      if ($cell.is(".untoggled")) {
        $cell.trigger("cell:close");
      }
    })
  });

  function show_asset($elem) {
    var $img = $elem.find('img');
    if ($img.data('href')) {
      $('#asset-preview').empty().append($('<img src="' + $img.data('href') + '"/>'));
    } else {
      $('#asset-preview').empty();
    }
    return true;
  }
  if ($('#assets-browser.assets-ckeditor').length) {
    $('#assets-browser table tr').on('click', function() {
      $('#assets-browser table tr').removeClass('active');
      $(this).addClass('active');
      show_asset($(this));
      $('#asset-ckeditor-embed').attr('href', $(this).data('href')).prop('disabled', false);
    });
  } else {
    $('#assets-browser table tr').on('hover mouseenter', function() {
      show_asset($(this));
    }).on('mouseleave', function() {
      $('#asset-preview').empty();
    });
  }
  $('.manager-add-new-cell a').on('click', function() {
    $(this).next().toggle();
    return false;
  });
  $('.manager-add-new-cell button').on('click', function() {
    window.location = $(this).parent('div').find('option:selected').data('add-url');
    return false;
  });

  function prepare_dynamic_fields() {
    $('[data-dynamic-display-parent]').off('change input').on('change input', function() {
      var sel1 = '[data-dynamic-display-child-of="' + $(this).attr('name') + '"]';
      var sel2 = '[data-dynamic-display-value="' + $(this).val() + '"]';
      var sel3 = '[data-dynamic-display-value-in*=" ' + $(this).val() + ' "]';
      var sel4 = '[data-dynamic-display-checked="' + $(this).prop('checked') + '"]';
      $(sel1).addClass('field-hidden').hide();
      $(sel1 + sel2).removeClass('field-hidden').show();
      $(sel1 + sel3).removeClass('field-hidden').show();
      $(sel1 + sel4).removeClass('field-hidden').show();
      $(sel1).trigger('change');
    });
    $('[data-dynamic-display-child-of]').addClass('field-hidden').hide();
    $('select[data-dynamic-display-parent]').trigger('change');
    $('[data-dynamic-display-parent]:checked').trigger('change');
  }
  prepare_dynamic_fields();
  $(document).on('combo:dialog-loaded', prepare_dynamic_fields);

  $(document).on('click', '#add-page-property-form', function() {
    if (typeof property_forms === "undefined") {var property_forms = $('.page-property-form');}
    if (typeof total_forms === "undefined") {var total_form = $('#id_form-TOTAL_FORMS');}
    if (typeof form_num === "undefined") {var form_num = property_forms.length - 1;}
    var new_form = $(property_forms[0]).clone();
    var form_regex = RegExp(`form-(\\d){1}-`,'g');
    form_num++;
    new_form.html(new_form.html().replace(form_regex, `form-${form_num}-`));
    new_form.appendTo('#page-property-forms tbody');
    $('#id_form-' + form_num + '-key').val('');
    $('#id_form-' + form_num + '-value').val('');
    total_form.val(form_num + 1);
  })

  $(document).on('change', '#linked-card-form #id_carddef_reference', function () {
    $('#linked-card-variable span').hide();
    if ($(this).val()) {
      $('#linked-card-variable span[data-key="' + $(this).val() + '"]').show();
    }
  });

  $('.cell').on('combo:cellform-reloaded', function() {
    multiSelectWidget.init(this);
  });

  function init_select2() {
    $('select[data-combo-autocomplete]').each(function(idx, elem) {
      $(elem).select2({
        ajax: {
          url: $(elem).data('select2-url'),
          dataType: 'json'
        }
      });
    });
  }
  init_select2();
  $('.cell').on('combo:cellform-reloaded', init_select2);
  $(document).on('gadjo:dialog-loaded', init_select2);
});


// UI to customize the layout of the content of a wcs-card-cell
const Card_cell_custom = function(cell) {
  this.cell = cell;
  this.init();
}

Card_cell_custom.prototype = {
  parse_tpl: function(template) {
    return document.importNode(template.content, true).firstElementChild;
  },
  field_with_varname: function(varname) {
    if (varname.startsWith('user:')) {
      const field = this.cardSchema.user.fields.filter(function(i) {
        return i.varname === varname.substring(5);
      })
      return field[0];
    } else if (varname.startsWith('info:')) {
      const field = this.cardSchema.info.fields.filter(function(i) {
        return i.varname === varname;
      })
      return field[0];
    }
    const field = this.cardSchema.fields.filter(function(i) {
      return i.varname === varname;
    })
    return field[0];
  },
  //  Grid methods
  grid__form_dialog: function(callback){
    const _self = this;
    $(this.grid_form).dialog({
      modal: true,
      width: 'auto',
      open: function( event, ui ) {
        if (_self.display_mode == 'card') {
          $(_self.grid_form[0]).val(_self.gridSchema.grid_class || 'fx-grid--auto');
        } else if (_self.display_mode == 'table') {
          $(_self.grid_form[0]).prop('checked', _self.gridSchema.grid_headers || false);
        }
      },
      buttons: [
        {
          text: gettext('Cancel'),
          class:"cancel-button",
          click: function() {
              $(this).dialog("close");
          }
        },{
          text: gettext('Edit'),
          class: "submit-button",
          click: function() {
            const form_datas = {};
            if (_self.display_mode == 'card') {
              const select_layout = _self.grid_form[0];
              form_datas.grid_class = select_layout.value;
            } else if (_self.display_mode == 'table') {
              const with_headers = _self.grid_form[0];
              form_datas.grid_headers = with_headers.checked;
            }
            callback(form_datas);
            $(this).dialog("close");
          }
        }
      ]
    });
  },
  grid__set_schema: function(form_datas){
    if (this.display_mode == 'card') {
      this.gridSchema.grid_class = form_datas.grid_class;
    } else if (this.display_mode == 'table') {
      this.gridSchema.grid_headers = form_datas.grid_headers;
    }
    this.grid__set_layout();
    this.grid__store_schema();
  },
  grid__store_schema: function(){
    this.store.value = JSON.stringify(this.gridSchema);
  },
  grid__set_layout: function() {
    if (this.display_mode == 'card') {
      this.grid_layout_label.textContent = $(this.grid_form).find('select[name="grid-layout"] option[value="' + (this.gridSchema.grid_class || 'fx-grid--auto') + '"]').text();
      if (this.grid_wrapper.dataset.grid_layout) {
        this.grid_wrapper.classList.remove(this.grid_wrapper.dataset.grid_layout); }
      this.grid_wrapper.classList.add(this.gridSchema.grid_class);
      this.grid_wrapper.dataset.grid_layout = this.gridSchema.grid_class;
    } else if (this.display_mode == 'table') {
      this.grid_layout_label.textContent = this.gridSchema.grid_headers ? gettext('yes') : gettext('no');
    }
  },
  grid_toggle: function() {
    if (this.toggleBtn.checked) {
      this.grids.forEach( (el) => {
        el.hidden = (el == this.grid) ? false : true
      })
    } else {
      this.grids.forEach( (el) => {
        el.hidden = true;
      })
    }
  },
  //  Grid cell methods
  grid_cell__init_form: function() {
    const varname_select = this.grid_cell_form.elements.field_varname;
    const user_varname_select = this.grid_cell_form.elements.user_field_varname;
    const link_select = this.grid_cell_form.elements.link_page;
    this.cardSchema.fields.forEach(function(el, id) {
      if (el.varname && el.type !== "page") {
        $('<option />')
          .attr('value', el.varname)
          .text(el.label)
          .appendTo(varname_select);
        if (el.type == 'file') {
            $('<option />')
              .attr('value', 'field:' + el.varname)
              .text(el.label + ' (' + gettext('File') + ')')
              .appendTo(link_select);
        }
      }
    });
    this.cardSchema.user.fields.forEach(function(el, id) {
      if (el.varname) {
        $('<option />')
          .attr('value', 'user:' + el.varname)
          .text(el.label)
          .appendTo(user_varname_select);
      }
    });
  },
  grid_cell__form_dialog: function(button_label, callback){
    const _self = this;
    $(this.grid_cell_form).dialog({
      modal: true,
      width: 'auto',
      open: function( event, ui ) {
        $(document).trigger('combo:dialog-loaded');
        $(_self.grid_cell_form.elements.field_content).change();
        $(_self.grid_cell_form.elements.field_empty_display_mode).change();
        $(_self.grid_cell_form.elements.link_page).change();
      },
      buttons: [
        {
          text: gettext('Cancel'),
          class:"cancel-button",
          click: function() {
              $(this).dialog("close");
          }
        },{
          text: button_label,
          class: "submit-button",
          click: function() {
            const form_datas = {};
            $(_self.grid_cell_form).serializeArray().forEach(function(el){
              form_datas[el.name] = el.value;
            });
            callback(form_datas);
            $(this).dialog("close");
          }
        }
      ]
    });
  },
  grid_cell__new: function() {
    const el = this.grid_cell_tpl.cloneNode(true);
    el.deletBtn = el.querySelector(this.deletBtn_selector);
    el.editBtn = el.querySelector(this.editBtn_selector);
    el.contentEl = el.querySelector(this.contentEl_selector);
    return el;
  },
  grid_cell__set: function(grid_cell, schema_cell) {
    // Set size css class
    if (!grid_cell.classList.contains(schema_cell.cell_size)) {
      if (grid_cell.dataset.cell_size) grid_cell.classList.remove(grid_cell.dataset.cell_size);
      if (schema_cell.cell_size) grid_cell.classList.add(schema_cell.cell_size);
    }
    // Store Schema in el data
    for (const key in schema_cell) {
      grid_cell.dataset[key] = schema_cell[key];
    }
    // set cell text
    let schema_field = this.field_with_varname(schema_cell.varname);
    let cell_text = "";
    if (schema_field || ['@custom@', '@link@', '@action@'].includes(schema_cell.varname)) {
      let cell_content = "";
      let cell_display_mode_label = '';
      switch (schema_cell.varname) {
        case '@action@':
          cell_content += $(this.grid_cell_form).find('select[name="trigger_id"] option[value="' + schema_cell.trigger_id + '"]').text();
          cell_content += ' (' + gettext('Action Button') + ')';
          cell_display_mode_label = undefined
          break;
        case '@custom@':
          cell_content = escape_html(schema_cell.template || '') + ' (' + gettext('Custom') + ')';
          cell_display_mode_label = $(this.grid_cell_form).find('select[name="custom_display_mode"] option[value="' + (schema_cell.display_mode || 'label') + '"]').text();
          break;
        case '@link@':
          cell_content = escape_html(schema_cell.template || '') + ': ';
          if (schema_cell.page) {
            cell_content += $(this.grid_cell_form).find('select[name="link_page"] option[value="' + schema_cell.page + '"]').text();
          } else if (schema_cell.link_field) {
            let link_field = this.field_with_varname(schema_cell.link_field)
            if (link_field) {
              cell_content += link_field.label + ' (' + gettext('File') + ')';
            }
          } else {
            cell_content += escape_html(schema_cell.url_template || '');
          }
          cell_content += ' (' + gettext('Link') + ')';
          cell_display_mode_label = $(this.grid_cell_form).find('select[name="link_display_mode"] option[value="' + schema_cell.display_mode + '"]').text();
          break;
        default:
          cell_content = schema_field.label;
          if (schema_cell.varname.startsWith('user:')) {
            cell_content += ' (' + gettext('User field') + ')';
          } else if (schema_cell.varname.startsWith('info:')) {
            cell_content += ' (' + gettext('Card information field') + ')';
          }
          cell_display_mode_label = $(this.grid_cell_form).find('select[name="field_content"] option[value="' + (schema_cell.field_content || 'label-and-value') + '"]').text();
          if ((schema_cell.field_content || 'label-and-value') != 'label-and-value') {
            cell_display_mode_label += ' - ';
            cell_display_mode_label += $(this.grid_cell_form).find('select[name="field_display_mode"] option[value="' + (schema_cell.display_mode || 'text') + '"]').text();
          }
          if (schema_field && schema_field.type == 'file') {
            cell_display_mode_label += ' (' + $(this.grid_cell_form).find('select[name="file_field_display_mode"] option[value="' + (schema_cell.file_display_mode || 'link') + '"]').text() + ')';
          }
      }

      cell_text += cell_content;
      cell_text += '<span class="cell-meta">';
      if (cell_display_mode_label) {
        cell_text += '<span class="cell-display-mode-label">' + cell_display_mode_label + '</span>';
      }
      if (this.display_mode === 'table') {
        if (['@link@', '@custom@'].includes(schema_cell.varname)) {
          cell_text += '<span class="cell-header">' + gettext('Header:') + ' ' + (schema_cell.header || '') + '</span>';
        }
        if (schema_cell.col_width) {
          cell_text += '<span class="col-width">' + gettext('Size:') + ' ' + schema_cell.col_width + '</span>';
        }
      }
      if (this.display_mode == 'card') {
        let cell_size_label = $(this.grid_cell_form).find('select[name="cell_size"] option[value="' + (schema_cell.cell_size || '') + '"]').text();
        cell_text += '<span class="cell-size-label">' + gettext('Size:') + ' ' + cell_size_label + '</span>';
      }
      cell_text += '</span>';
    } else {
      cell_text += '<span class="warning">' + gettext('Deleted field:') + ' ' + schema_cell.varname + '</span>';
    }
    grid_cell.contentEl.innerHTML = cell_text;
  },
  grid_cell__add: function(schema_cell) {
    const new_grid_cell = this.grid_cell__new();
    this.grid_cell__set(new_grid_cell, schema_cell);
    if (new_grid_cell.deletBtn)
      new_grid_cell.deletBtn.addEventListener('click', () => {this.grid_cell__delete(new_grid_cell)});
    if (new_grid_cell.editBtn)
      new_grid_cell.editBtn.addEventListener('click', () => {this.grid_cell__edit(new_grid_cell)});
    this.grid_wrapper.append(new_grid_cell);
  },
  grid_cell__delete: function(grid_cell) {
    const cell_id = $(grid_cell).index();
    this.gridSchema.cells = this.gridSchema.cells.filter(function(el, i){
      return i !== cell_id;
    });
    $(grid_cell).remove();
    this.grid__store_schema();
  },
  grid_cell__edit_set_fields: function(grid_cell, schema_cell) {
    this.grid_cell_form.reset();

    const sc = schema_cell;
    const fe = this.grid_cell_form.elements;

    if (this.display_mode === 'card') {
      fe.cell_size.value = sc.cell_size || "";
      switch (sc.varname) {
        case '@action@':
          fe.entry_type.value = '@action@';
          fe.action_label.value = sc.action_label || "";
          fe.trigger_id.value = sc.trigger_id || "";
          fe.action_ask_confirmation.value = 'true';
          fe.action_ask_confirmation.checked = sc.action_ask_confirmation == true;
          fe.action_confirmation_template.value = sc.action_confirmation_template || "";
          fe.unavailable_action_mode.value = sc.unavailable_action_mode || "";
          break;

        case '@custom@':
          fe.entry_type.value = '@custom@';
          fe.custom_template.value = sc.template || "";
          fe.custom_display_mode.value = sc.display_mode || 'label';
          break;

        case '@link@':
          fe.entry_type.value = '@link@';
          fe.link_label_template.value = sc.template || "";
          fe.link_display_mode.value = sc.display_mode;

          if (sc.link_field) {
            fe.link_page.value = 'field:' + sc.link_field;
          } else {
            fe.link_page.value = sc.page || "";
            fe.link_url_template.value = sc.url_template || "";
          }
          break;

        default:
          fe.field_content.value = sc.field_content || 'label-and-value';
          fe.field_display_mode.value = ((sc.field_content || 'label-and-value') == 'label-and-value')
            ? 'text'
            : sc.display_mode || 'text';

          let schema_field = this.field_with_varname(sc.varname);
          fe.file_field_display_mode.value = sc.file_display_mode || 'link';

          if (['@skip@', '@empty@'].includes(sc.empty_value)) {
            fe.field_empty_display_mode.value = sc.empty_value;
          } else {
            fe.field_empty_display_mode.value = '@custom@';
            fe.field_empty_text.value = sc.empty_value || "";
          }

          if (sc.varname.startsWith('user:')) {
            fe.entry_type.value = '@user-field@';
            fe.user_field_varname.value = sc.varname;
          } else if (sc.varname.startsWith('info:')) {
            fe.entry_type.value = '@info-field@';
            fe.info_field_varname.value = sc.varname;
          } else {
            fe.entry_type.value = '@field@';
            fe.field_varname.value = sc.varname;
          }
      }
    }

    if (this.display_mode === 'table') {
      switch (sc.varname) {
        case '@action@':
          fe.entry_type.value = '@action@';
          fe.action_header.value = sc.header || "";
          fe.action_label.value = sc.action_label || "";
          fe.trigger_id.value = sc.trigger_id || "";
          fe.action_ask_confirmation.value = 'true';
          fe.action_ask_confirmation.checked = sc.action_ask_confirmation == 'true';
          fe.action_confirmation_template.value = sc.action_confirmation_template || "";
          fe.unavailable_action_mode.value = sc.unavailable_action_mode || "";
          break;

        case '@custom@':
          fe.entry_type.value = '@custom@';
          fe.custom_header.value = sc.header || "";
          fe.custom_template.value = sc.template || "";
          break;

        case '@link@':
          fe.entry_type.value = '@link@';
          fe.link_header.value = sc.header || "";
          fe.link_label_template.value = sc.template || "";
          fe.link_display_mode.value = sc.display_mode;

          if (sc.link_field) {
            fe.link_page.value = 'field:' + sc.link_field;
          } else {
            fe.link_page.value = sc.page || "";
            fe.link_url_template.value = sc.url_template || "";
          }
          break;

        default:
          let schema_field = this.field_with_varname(sc.varname);
          fe.file_field_display_mode.value = sc.file_display_mode || 'link';
          fe.field_empty_text.value = sc.empty_value || "";

          if (['@skip@', '@empty@'].includes(fe.field_empty_text.value)) {
            fe.field_empty_text.value = '';
          }

          if (sc.varname.startsWith('user:')) {
            fe.entry_type.value = '@user-field@';
            fe.user_field_varname.value = sc.varname;
          } else if (sc.varname.startsWith('info:')) {
            fe.entry_type.value = '@info-field@';
            fe.info_field_varname.value = sc.varname;
          } else {
            fe.entry_type.value = '@field@';
            fe.field_varname.value = sc.varname;
          }
      }

      fe.col_width.value = sc.col_width || "";
    }

    if (this.display_mode === 'list') {
      fe.entry_type.value = '@link@';
      fe.link_label_template.value = sc.template || "";
      fe.link_url_template.value = sc.url_template || "";
    }
  },
  grid_cell__add_set_fields: function(grid_cell) {
    // reset values
    $(this.grid_cell_form).find('select').each(function(i, el) {
      $(el).find('option:selected').prop("selected", false);
      $(el).find('option:first').prop("selected", true);
      $(el).change();
    });
    $(this.grid_cell_form).find('input, textarea').each(function(i, el) {
      $(el).val('');
      $(el).change();
    });
  },
  grid_cell__edit: function(grid_cell) {
    const cell_id = $(grid_cell).index();
    const grid_cell_schema = this.gridSchema.cells[cell_id];
    this.grid_cell__edit_set_fields(grid_cell, grid_cell_schema);
    this.grid_cell__form_dialog(gettext('Edit'), (form_datas) => {
      const grid_cell_schema_mod = this.grid_cell__set_schema(form_datas, grid_cell_schema);
      this.grid_cell__set(grid_cell, grid_cell_schema_mod);
      this.grid__store_schema();
    });
  },
  grid_cell__set_schema: function(form_datas, schema_cell) {
    for (var data in schema_cell)
      delete schema_cell[data];

    const fd = form_datas;
    let sc = schema_cell;

    if (this.display_mode == 'card') {
      sc.cell_size = fd.cell_size;
      switch (fd.entry_type) {
        case '@action@':
          sc.varname = '@action@';
          sc.trigger_id = fd.trigger_id;
          sc.action_label = fd.action_label;
          sc.action_ask_confirmation = fd.action_ask_confirmation == 'true';
          sc.action_confirmation_template = fd.action_confirmation_template;
          sc.unavailable_action_mode = fd.unavailable_action_mode;
          break;

        case '@custom@':
          sc.varname = "@custom@";
          sc.display_mode = fd.custom_display_mode;
          sc.template = fd.custom_template;
          break;

        case '@link@':
          sc.varname = "@link@";
          sc.display_mode = fd.link_display_mode;
          sc.template = fd.link_label_template;
          sc.page = fd.link_page;
          sc.url_template = (fd.link_page)
            ? ''
            : form_datas.link_url_template;

          if (fd.link_page.startsWith('field:')) {
            delete sc.page;
            delete sc.url_template;
            sc.link_field = fd.link_page.substring(6);
          }
          break;

        default:
          sc.varname = fd.field_varname;
          sc.field_content = fd.field_content;
          sc.display_mode = (sc.field_content === "label-and-value")
            ? "text"
            : fd.field_display_mode;
          sc.empty_value = (fd.field_empty_display_mode === '@custom@')
            ? fd.field_empty_text
            : fd.field_empty_display_mode;

          let schema_field = this.field_with_varname(schema_cell.varname);
          if (schema_field && schema_field.type == 'file') {
            sc.file_display_mode = fd.file_field_display_mode;
          }

          if (fd.entry_type === '@user-field@')
            sc.varname = fd.user_field_varname;

          if (fd.entry_type === '@info-field@')
            sc.varname = fd.info_field_varname;
      }
    }

    if (this.display_mode === 'table') {
      switch (fd.entry_type) {
        case '@action@':
          sc.varname = '@action@';
          sc.header = fd.action_header;
          sc.trigger_id = fd.trigger_id;
          sc.action_label = fd.action_label;
          sc.action_ask_confirmation = fd.action_ask_confirmation == 'true';
          sc.action_confirmation_template = fd.action_confirmation_template;
          sc.unavailable_action_mode = fd.unavailable_action_mode;
          break;

        case '@custom@':
          sc.varname = '@custom@';
          sc.header = fd.custom_header;
          sc.template = fd.custom_template;
          break;

        case '@link@':
          sc.varname = "@link@";
          sc.header = fd.link_header;
          sc.display_mode = fd.link_display_mode;
          sc.template = fd.link_label_template;

          sc.page = fd.link_page;
          sc.url_template = (fd.link_page)
            ? ''
            : form_datas.link_url_template;

          if (fd.link_page.startsWith('field:')) {
            delete sc.page;
            delete sc.url_template;
            sc.link_field = fd.link_page.substring(6);
          }
          break;

        default:
          sc.varname = fd.field_varname;
          sc.empty_value = fd.field_empty_text;

          let schema_field = this.field_with_varname(schema_cell.varname);
          if (schema_field && schema_field.type == 'file') {
            sc.file_display_mode = fd.file_field_display_mode;
          }

          if (fd.entry_type === '@user-field@')
            sc.varname = fd.user_field_varname;

          if (fd.entry_type === '@info-field@')
            sc.varname = fd.info_field_varname;
      }

      sc.col_width = fd.col_width;
    }

    if (this.display_mode === 'list') {
      sc.varname = '@link@';
      sc.template = fd.link_label_template;
      sc.url_template = fd.link_url_template;
    }

    return sc;
  },

  grid_cell__add_schema: function(form_datas) {
    let schema_cell = {};
    schema_cell = this.grid_cell__set_schema(form_datas, schema_cell);

    this.gridSchema.cells.push(schema_cell);

    this.grid_cell__add(schema_cell);
    this.grid__store_schema();
  },

  grid_cell__init: function() {
    if (!this.gridSchema.cells.length) return;

    if (this.grid_wrapper.childElementCount) {
      while (this.grid_wrapper.lastElementChild) {
        this.grid_wrapper.removeChild(this.grid_wrapper.lastElementChild);
      }
    }
    this.gridSchema.cells.forEach((el) => {
      this.grid_cell__add(el);
    });
  },
  // Init methods
  on: function() {
    if (!(this.toggleBtn.checked && this.displayModeSelect.value == this.display_mode)) {
      return;
    }

    this.store = this.cell.querySelector('input[id$="-custom_schema"]');
    try {
      const storeSchema = JSON.parse(this.store.value);
      this.gridSchema_existing = (Object.keys(storeSchema).length) ? storeSchema : undefined;
    } catch(e) {
      console.error(e);
      this.gridSchema_existing = undefined;
    }

    this.allGridSchemas = Object.assign({}, this.gridSchema_default);
    if (this.gridSchema_existing) {
      if (this.gridSchema_existing.display_mode)
        Object.assign(this.allGridSchemas[this.gridSchema_existing.display_mode], this.gridSchema_existing);
      else
        Object.assign(this.allGridSchemas[this.display_mode], this.gridSchema_existing);
    }
    this.gridSchema = this.allGridSchemas[this.display_mode];

    this.grid__set_layout();
    this.grid_cell__init();

    // Edit grid layout btn click
    $(this.edit_grid_btn).on('click', () => {
      this.grid__form_dialog(this.grid__set_schema.bind(this));
    })
    // Add grid cell Btn click
    $(this.add_grid_cell_btn).on('click', () => {
      this.grid_cell__add_set_fields();
      this.grid_cell__form_dialog(gettext('Add'), this.grid_cell__add_schema.bind(this));
    });
    // Grid cells sortable
    const _self = this;
    $(_self.grid_wrapper).sortable({
      items: "> " + this.grid_cell_selector,
      placeholder: this.grid_cell_placeholder_selector,
      update: function(event, ui) {
        ui.item.data('update_index', ui.item.index());
        const moved_cell_schema = _self.gridSchema.cells[ui.item.data('start_index')];
        _self.gridSchema.cells.splice(ui.item.data('start_index'), 1);
        _self.gridSchema.cells.splice(ui.item.data('update_index'), 0, moved_cell_schema);
        _self.grid__store_schema();
      },
      start: function(event, ui) {
        ui.item.data("start_index", ui.item.index());
        ui.placeholder.addClass(ui.item[0].dataset.cell_size).css('height', ui.item[0].offsetHeight);
      }
    });

    this.is_on = true;
  },
  init_elements: function() {
    var selector;
    if (this.display_mode == 'card') {
        selector = '.as-card';
    } else if (this.display_mode == 'table') {
        selector = '.as-table';
    } else {
        selector = '.as-list';
    }

    this.deletBtn_selector = selector + '.wcs-cards-cell--grid-cell-delete';
    this.editBtn_selector = selector + '.wcs-cards-cell--grid-cell-edit';
    this.contentEl_selector = selector + '.wcs-cards-cell--grid-cell-content';
    this.grid_cell_selector = selector + '.wcs-cards-cell--grid-cell';
    this.grid_cell_placeholder_selector = selector + '.wcs-cards-cell--grid-cell-placeholder';

    this.grids = this.cell.querySelectorAll('.wcs-cards-cell--grid');
    this.grid = this.cell.querySelector(selector + '.wcs-cards-cell--grid');

    if (this.display_mode == 'card') {
      this.edit_grid_btn = this.cell.querySelector(selector + '.wcs-cards-cell--grid-layout-btn');
      this.grid_layout_label = this.cell.querySelector(selector + '.wcs-cards-cell--grid-layout-mode');
    } else if (this.display_mode == 'table') {
      this.edit_grid_btn = this.cell.querySelector(selector + '.wcs-cards-cell--grid-headers-btn');
      this.grid_layout_label = this.cell.querySelector(selector + '.wcs-cards-cell--grid-headers-mode');
    }

    if (this.display_mode != 'list') {
      const grid_form_tpl = this.cell.querySelector(selector + '.wcs-cards-cell--grid-form-tpl');
      this.grid_form = this.parse_tpl(grid_form_tpl);
    }

    this.add_grid_cell_btn = this.cell.querySelector(selector + '.wcs-cards-cell--add-grid-cell-btn');

    const grid_cell_form_tpl = this.cell.querySelector(selector + '.wcs-cards-cell--grid-cell-form-tpl');
    this.grid_cell_form = this.parse_tpl(grid_cell_form_tpl);

    const grid_cell_tpl = this.cell.querySelector(selector + '.wcs-cards-cell--grid-cell-tpl');
    this.grid_cell_tpl = this.parse_tpl(grid_cell_tpl);

    this.grid_wrapper = this.cell.querySelector(selector + '.wcs-cards-cell--grid-cells');
  },
  init: function() {
    const cardSchema_el = this.cell.querySelector('[id*="card-schema-"]');
    this.cardSchema = cardSchema_el ? JSON.parse(cardSchema_el.innerText) : undefined;

    if (!this.cardSchema) {
      return;
    }

    this.gridSchema_default = {
      "card": {
        "display_mode": "card",
        "grid_class": "fx-grid--auto",
        "cells": []
      },
      "table": {
        "display_mode": "table",
        "grid_headers": false,
        "cells": []
      },
      "list": {
        "display_mode": "list",
        "cells": [
          {
            varname: "@link@",
            template: "{{ card.text }}",
            url_template: "{% if card_page_base_url %}{{ card_page_base_url }}{{ card.id }}/{% else %}{{ card.url }}{% endif %}"
          }
        ]
      }
    }

    this.is_on = false;

    this.toggleBtn = this.cell.querySelector('input[id$="-customize_display"]');
    this.displayModeSelect = this.cell.querySelector('select[id$="-display_mode"]');

    $(this.displayModeSelect).on('change', (e) => {
        this.display_mode = e.target.value;
        this.init_elements();
        this.on();
        this.grid_cell__init_form();
        this.grid_toggle();
    }).change();
    $(this.toggleBtn).on('change', (e) => {
        this.on();
        this.grid_toggle();
    }).change();
  }
}

// Active custom card UI for each card cell
$(function() {
  $('.wcs-card-cell').each(function(i, el) {
    const custom_card_as_card = new Card_cell_custom(el);
    $(el).on('combo:cellform-reloaded', function() {
      custom_card_as_card.init();
    });
  })
});
