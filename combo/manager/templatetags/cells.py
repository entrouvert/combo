# combo - content management system
# Copyright (C) 2014  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms, template
from django.urls import reverse

from combo.manager.forms import build_tab_is_not_default

register = template.Library()


@register.simple_tag(takes_context=True)
def cell_form(context, cell):
    context['url'] = reverse(
        'combo-manager-page-edit-cell',
        kwargs={'page_pk': cell.page_id, 'cell_reference': cell.get_reference()},
    )
    context['manager_tabs'] = cell.get_manager_tabs()
    for tab in context['manager_tabs']:
        if tab['slug'] == 'general':
            form_name = 'form'
        else:
            form_name = '%s_form' % tab['slug']
        if tab.get('fields'):
            tab['form'] = forms.models.modelform_factory(cell.__class__, fields=tab['fields'])
        tab['form_instance'] = tab['form'](initial={}, instance=cell, prefix='c%s' % cell.get_reference())
        tab['is_not_default'] = build_tab_is_not_default(tab['form_instance'])
        context[form_name] = tab['form_instance']
    context['cell'] = cell
    cell_form_template = template.loader.get_template('combo/manager_edit_cell_block.html')
    with context.push():
        context = context.flatten()
        context.update(cell.get_extra_manager_context())
        return cell_form_template.render(context)
