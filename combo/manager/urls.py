# combo - content management system
# Copyright (C) 2014  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ckeditor.views as ckeditor_views
from django.urls import path, re_path
from django.views.decorators.cache import never_cache
from django.views.decorators.clickjacking import xframe_options_sameorigin

from combo.apps.assets import views as assets_views
from combo.urls_utils import pages_admin_required

from .. import plugins
from . import views

urlpatterns = [
    path('', views.homepage, name='combo-manager-homepage'),
    re_path(r'^menu.json$', views.menu_json),
    path('site-export', pages_admin_required(views.site_export), name='combo-manager-site-export'),
    path('site-import', pages_admin_required(views.site_import), name='combo-manager-site-import'),
    path('site-settings', views.site_settings, name='combo-manager-site-settings'),
    path('models/<model_name>/ajax-choices', views.select2_choices, name='combo-manager-select2-choices'),
    path(
        'cells/invalid-report/',
        pages_admin_required(views.invalid_cell_report),
        name='combo-manager-invalid-cell-report',
    ),
    path('pages/add/', views.page_add, name='combo-manager-page-add'),
    path('pages/<int:pk>/', views.page_view, name='combo-manager-page-view'),
    path('pages/<int:pk>/template', views.page_select_template, name='combo-manager-page-select-template'),
    path('pages/<int:pk>/visibility', views.page_visibility, name='combo-manager-page-visibility'),
    path(
        'pages/<int:pk>/redirection',
        views.page_edit_redirection,
        name='combo-manager-page-edit-redirection',
    ),
    path(
        'pages/<int:pk>/include-in-navigation',
        views.page_edit_include_in_navigation,
        name='combo-manager-page-edit-include-in-navigation',
    ),
    path('pages/<int:pk>/slug', views.page_edit_slug, name='combo-manager-page-edit-slug'),
    path(
        'pages/<int:pk>/linked-card',
        views.page_edit_linked_card,
        name='combo-manager-page-edit-linked-card',
    ),
    path('pages/<int:pk>/title', views.page_edit_title, name='combo-manager-page-edit-title'),
    path(
        'pages/<int:pk>/description',
        views.page_edit_description,
        name='combo-manager-page-edit-description',
    ),
    path('pages/<int:pk>/picture/', views.page_edit_picture, name='combo-manager-page-edit-picture'),
    path(
        'pages/<int:pk>/remove-picture/',
        views.page_remove_picture,
        name='combo-manager-page-remove-picture',
    ),
    path(
        'pages/<int:pk>/extra-variables/',
        views.page_edit_extra_variables,
        name='combo-manager-page-edit-extra-variables',
    ),
    path(
        'pages/<int:pk>/delete',
        pages_admin_required(views.page_delete),
        name='combo-manager-page-delete',
    ),
    path('pages/<int:pk>/export', views.page_export, name='combo-manager-page-export'),
    path(
        'pages/<int:pk>/add/',
        pages_admin_required(views.page_add_child),
        name='combo-manager-page-add-child',
    ),
    path(
        'pages/<int:pk>/duplicate',
        pages_admin_required(views.page_duplicate),
        name='combo-manager-page-duplicate',
    ),
    re_path(
        r'^pages/(?P<pk>\d+)/save',
        pages_admin_required(views.snapshot_save),
        name='combo-manager-page-save',
    ),
    path(
        'pages/<int:pk>/edit-roles/',
        pages_admin_required(views.page_edit_roles),
        name='combo-manager-page-edit-roles',
    ),
    path('pages/<int:pk>/inspect/', views.page_inspect, name='combo-manager-page-inspect'),
    path('pages/<int:pk>/history', views.page_history, name='combo-manager-page-history'),
    path(
        'pages/<int:page_pk>/history/<int:pk>/',
        pages_admin_required(views.snapshot_restore),
        name='combo-manager-snapshot-restore',
    ),
    path(
        'pages/<int:page_pk>/history/<int:pk>/view/',
        views.snapshot_view,
        name='combo-manager-snapshot-view',
    ),
    path(
        'pages/<int:page_pk>/history/<int:pk>/inspect/',
        views.snapshot_inspect,
        name='combo-manager-snapshot-inspect',
    ),
    path(
        'pages/<int:page_pk>/history/<int:pk>/export',
        pages_admin_required(views.snapshot_export),
        name='combo-manager-snapshot-export',
    ),
    path(
        'pages/<int:pk>/history/compare/',
        views.page_history_compare,
        name='combo-manager-page-history-compare',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/add-cell-to-(?P<ph_key>[\w_-]+)/(?P<cell_type>\w+)/(?P<variant>[\w-]+)/$',
        views.page_add_cell,
        name='combo-manager-page-add-cell',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/$',
        views.page_edit_cell,
        name='combo-manager-page-edit-cell',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/delete$',
        views.page_delete_cell,
        name='combo-manager-page-delete-cell',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/duplicate$',
        views.page_duplicate_cell,
        name='combo-manager-page-duplicate-cell',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/attribute/(?P<attribute>[\w_]+)/$',
        views.page_edit_cell_attribute,
        name='combo-manager-page-edit-cell-attribute',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/add-link/(?P<link_code>[\w-]+)$',
        views.page_list_cell_add_link,
        name='combo-manager-page-list-cell-add-link',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/link/(?P<link_cell_reference>[\w_-]+)/$',
        views.page_list_cell_edit_link,
        name='combo-manager-page-list-cell-edit-link',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/link/(?P<link_cell_reference>[\w_-]+)/delete$',
        views.page_list_cell_delete_link,
        name='combo-manager-page-list-cell-delete-link',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/order$',
        views.link_list_order,
        name='combo-manager-link-list-order',
    ),
    path('pages/<int:page_pk>/order', views.cell_order, name='combo-manager-cell-order'),
    re_path(
        r'^pages/(?P<page_pk>\d+)/placeholder/(?P<placeholder>[\w_-]+)/options$',
        views.placeholder_options,
        name='combo-manage-placeholder-options',
    ),
    path('pages/order', views.page_order, name='combo-manager-page-order'),
    re_path(
        r'^pages/(?P<page_path>[\w/_-]+)/$',
        views.page_redirect_to_edit,
        name='combo-manager-page-redirect-to-edit',
    ),
    path('test-tool/', pages_admin_required(views.test_tool), name='combo-manager-test-tool'),
    path(
        'test-toolt/test-template/',
        pages_admin_required(views.test_tool_test_template),
        name='combo-manager-test-tool-test-template',
    ),
    re_path(r'^ckeditor/upload/', xframe_options_sameorigin(ckeditor_views.upload), name='ckeditor_upload'),
    re_path(r'^ckeditor/browse/', never_cache(assets_views.browse), name='ckeditor_browse'),
]

urlpatterns = plugins.register_plugins_manager_urls(urlpatterns)
