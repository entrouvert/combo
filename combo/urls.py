# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from django.views.i18n import JavaScriptCatalog

from . import plugins
from .manager.urls import urlpatterns as combo_manager_urls
from .public.views import error403, error404, login, logout, mellon_page_hook
from .urls_utils import decorated_includes, manager_required

urlpatterns = [
    re_path(r'^manage/', decorated_includes(manager_required, include(combo_manager_urls))),
    path('logout/', logout, name='auth_logout'),
    path('login/', login, name='auth_login'),
    path('403', error403),
    path('404', error404),
    path('jsi18n', JavaScriptCatalog.as_view(), name='javascript-catalog'),
]

handler403 = error403
handler404 = error404

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(
        path(
            'accounts/mellon/',
            include('mellon.urls'),
            kwargs={
                'template_base': 'combo/mellon_base_template.html',
                'context_hook': mellon_page_hook,
            },
        )
    )

# static and media files
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar  # pylint: disable=import-error

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

urlpatterns = plugins.register_plugins_urls(urlpatterns)

# other URLs are handled as public URLs
urlpatterns.append(path('', include('combo.public.urls')))
