# combo - content management system
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import threading

from django.utils.deprecation import MiddlewareMixin

_requests = {}


def get_request():
    return _requests.get(threading.current_thread())


class GlobalRequestMiddleware(MiddlewareMixin):
    def process_request(self, request):
        _requests[threading.current_thread()] = request
        request.cache = {}

    def process_response(self, request, response):
        if threading.current_thread() in _requests:
            del _requests[threading.current_thread()]
        return response
