from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0009_agenda_references_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='agenda_references_template',
            field=models.CharField(blank=True, max_length=2000, verbose_name='Agenda references template'),
        ),
    ]
