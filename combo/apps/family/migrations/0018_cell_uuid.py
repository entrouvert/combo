import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in ['WeeklyAgendaCell']:
        klass = apps.get_model('family', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0017_booking_not_paid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
