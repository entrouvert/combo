from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0012_booking_form_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
