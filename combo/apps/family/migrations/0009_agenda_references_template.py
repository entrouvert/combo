from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0008_clean'),
    ]

    operations = [
        migrations.RenameField(
            model_name='weeklyagendacell',
            old_name='agenda_reference',
            new_name='agenda_references_template',
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='user_external_template',
            field=models.CharField(
                blank=True, max_length=255, verbose_name='User external reference template'
            ),
        ),
    ]
