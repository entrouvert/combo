import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0003_familyinfoscell_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='familyinfoscell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
    ]
