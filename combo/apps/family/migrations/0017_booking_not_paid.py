from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0016_cell_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_not_paid_key',
            field=models.CharField(
                default='reglement',
                max_length=30,
                verbose_name='Booking extra_data key for the "not-paid" status',
            ),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_not_paid_value',
            field=models.CharField(
                default='Non-réglé',
                max_length=30,
                verbose_name='Booking extra_data value for the "not-paid" status',
            ),
        ),
    ]
