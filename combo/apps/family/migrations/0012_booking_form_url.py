from django.db import migrations, models

import combo.data.models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0011_agenda_subscribed_and_dates'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_form_url',
            field=models.CharField(
                blank=True,
                max_length=2000,
                validators=[combo.data.models.django_template_validator],
                verbose_name='URL to the booking Form',
            ),
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='agenda_categories',
            field=models.CharField(
                blank=True,
                max_length=2000,
                validators=[combo.data.models.django_template_validator],
                verbose_name='Agenda categories',
            ),
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='agenda_references_template',
            field=models.CharField(
                blank=True,
                max_length=2000,
                validators=[combo.data.models.django_template_validator],
                verbose_name='Agenda references template',
            ),
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='end_date_filter',
            field=models.CharField(
                blank=True,
                max_length=250,
                validators=[combo.data.models.django_template_validator],
                verbose_name='End date filter template',
            ),
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='start_date_filter',
            field=models.CharField(
                blank=True,
                max_length=250,
                validators=[combo.data.models.django_template_validator],
                verbose_name='Start date filter template',
            ),
        ),
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='user_external_template',
            field=models.CharField(
                blank=True,
                max_length=255,
                validators=[combo.data.models.django_template_validator],
                verbose_name='User external reference template',
            ),
        ),
    ]
