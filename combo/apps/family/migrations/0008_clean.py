from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0007_weekly_agenda_user_template'),
    ]

    operations = [
        migrations.DeleteModel(
            name='FamilyInfosCell',
        ),
    ]
