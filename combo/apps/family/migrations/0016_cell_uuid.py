from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0015_booking_button'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='weeklyagendacell',
            unique_together={('page', 'uuid')},
        ),
    ]
