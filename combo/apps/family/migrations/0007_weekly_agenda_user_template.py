from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0006_weekly_agenda_cell'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='weeklyagendacell',
            name='user_external_id_key',
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='user_external_template',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
