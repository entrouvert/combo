from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='familyinfoscell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
    ]
