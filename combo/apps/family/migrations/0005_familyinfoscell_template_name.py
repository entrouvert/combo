# Generated by Django 2.2.21 on 2021-07-23 11:18

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0004_familyinfoscell_last_update_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='familyinfoscell',
            name='template_name',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Cell Template'),
        ),
    ]
