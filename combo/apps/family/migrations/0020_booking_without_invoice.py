from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0019_cell_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_without_invoice_key',
            field=models.CharField(
                default='facture_guichet',
                max_length=30,
                verbose_name='Booking extra_data key for the "without-invoice" status',
            ),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_without_invoice_value',
            field=models.CharField(
                default='Sans facture',
                max_length=30,
                verbose_name='Booking extra_data value for the "without-invoice" status',
            ),
        ),
    ]
