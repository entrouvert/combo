from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0010_agenda_references_template'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='agenda_categories',
            field=models.CharField(blank=True, max_length=2000, verbose_name='Agenda categories'),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='agenda_type',
            field=models.CharField(
                choices=[
                    ('manual', 'Agenda references template'),
                    ('subscribed', 'Use agendas subscriptions'),
                ],
                default='manual',
                max_length=20,
                verbose_name='Agenda references',
            ),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='end_date_filter',
            field=models.CharField(blank=True, max_length=250, verbose_name='End date filter template'),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='start_date_filter',
            field=models.CharField(blank=True, max_length=250, verbose_name='Start date filter template'),
        ),
    ]
