from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0018_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='weeklyagendacell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
