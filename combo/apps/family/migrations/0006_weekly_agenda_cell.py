import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
        ('data', '0047_auto_20210723_1318'),
        ('family', '0005_familyinfoscell_template_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='WeeklyAgendaCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=100, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                (
                    'template_name',
                    models.CharField(blank=True, max_length=50, null=True, verbose_name='Cell Template'),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(blank=True, max_length=150, verbose_name='Title')),
                ('agenda_reference', models.CharField(max_length=128, verbose_name='Agenda')),
                ('user_external_id_key', models.CharField(max_length=50)),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Page')),
            ],
            options={
                'verbose_name': 'Weekly agenda cell',
            },
        ),
    ]
