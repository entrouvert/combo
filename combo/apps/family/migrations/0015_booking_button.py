from django.db import migrations, models

import combo.data.models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0014_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_button_position',
            field=models.CharField(
                choices=[('bottom', 'Below the weekly agenda'), ('top', 'Above the weekly agenda')],
                default='bottom',
                max_length=6,
                verbose_name='Position of the booking button',
            ),
        ),
        migrations.AddField(
            model_name='weeklyagendacell',
            name='booking_button_title',
            field=models.CharField(
                blank=True,
                help_text='Default: "Update bookings".',
                max_length=2000,
                validators=[combo.data.models.django_template_validator],
                verbose_name='Title of the booking button',
            ),
        ),
    ]
