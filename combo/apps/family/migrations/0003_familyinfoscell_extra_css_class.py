from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0002_familyinfoscell_restricted_to_unlogged'),
    ]

    operations = [
        migrations.AddField(
            model_name='familyinfoscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
