# combo - content management system
# Copyright (C) 2014-2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms

from .models import WeeklyAgendaCell


class WeeklyAgendaCellForm(forms.ModelForm):
    class Meta:
        model = WeeklyAgendaCell
        fields = [
            'agenda_type',
            'agenda_references_template',
            'agenda_categories',
            'start_date_filter',
            'end_date_filter',
            'user_external_template',
            'booking_form_url',
        ]

    def save(self, *args, **kwargs):
        commit = kwargs.pop('commit', True)
        super().save(commit=False, *args, **kwargs)
        if self.cleaned_data.get('agenda_type') == 'manual':
            self.instance.agenda_categories = ''
        else:
            self.instance.agenda_references_template = ''
        if commit:
            self.instance.save()
        return self.instance
