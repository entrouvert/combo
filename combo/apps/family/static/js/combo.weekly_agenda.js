$(function () {
  function initAgendaCell($cell, prefix) {
    const $weekList = $(prefix + '--week-list', $cell);
    const $slider = $(prefix + '--slider', $cell);
    let $currentWeek = $(prefix + '--week-title.current', $cell);
    if (!$currentWeek.length) {
      $currentWeek = $(prefix + '--week-title:first', $cell);
    }
    const $nextWeekButton = $(prefix + '--next-week', $cell);
    const $previousWeekButton = $(prefix + '--previous-week', $cell);
    const $editAgendaBtn = $(prefix + '--edit-btn', $cell);
    const nbOfWeeks = $(prefix + '--week-title', $cell).length;

    let $selectedWeek = $currentWeek;

    function getPreviousWeek() { return $selectedWeek.prevAll(prefix + '--week-title:first'); }
    function getNextWeek() { return $selectedWeek.nextAll(prefix + '--week-title:first'); }

    function setCurrentWeek($week, enableTransition) {
      if(!$week.length) {
          return;
      }
      const currentOffset = $week[0].offsetLeft;
      $slider.css({
        'transform': `translate(-${currentOffset}px)`,
        'transition': (enableTransition ? 'transform 0.5s' : '')
      });

      $selectedWeek = $week;
      $nextWeekButton.prop('disabled', getNextWeek().length == 0);
      $previousWeekButton.prop('disabled', getPreviousWeek().length == 0);

      const selectedWeekIsInPast = $selectedWeek.nextAll(prefix + '--week-title.current').length;
      $weekToEdit = selectedWeekIsInPast ? $currentWeek : $selectedWeek;
      $editAgendaBtn.prop('href', $weekToEdit.attr('data-edit-url'));
    }

    function setPreviousWeek() { setCurrentWeek(getPreviousWeek(), true); }
    function setNextWeek() { setCurrentWeek(getNextWeek(), true); }

    $previousWeekButton.on('click', setPreviousWeek);
    $nextWeekButton.on('click', setNextWeek);

    let touchStartX = 0;
    $cell.on('touchstart', (e) => {
      touchStartX = e.changedTouches[0].screenX;
    });

    $cell.on('touchend', (e) => {
      const touchEndX = e.changedTouches[0].screenX;
      if (touchEndX - touchStartX < -30) {
        setNextWeek();
      } else if (touchEndX - touchStartX > 30) {
        setPreviousWeek();
      }
    });

    function updateColumnsWidth(e, a) {
      const minWidth = $slider.css('--min-column-width');
      const availableWidth = $weekList[0].offsetWidth;
      let columnsWidth = availableWidth < minWidth ? availableWidth : availableWidth / Math.floor(availableWidth / minWidth);
      $slider.css('grid-template-columns', `repeat(${nbOfWeeks}, ${columnsWidth}px)`);

      if($selectedWeek) {
        // Update slider offset to bring it at the new position of the selectedWeek
        setCurrentWeek($selectedWeek, false);
      }
    }

    function hideEmptyDays() {
      let nbRows = 8;
      for(let i = 0; i < 7; ++i) {
        const $days = $(prefix + `--day-item[data-weekday=${i}]`, $cell);
        const $activities = $(prefix + '--activity-item', $days);
        if(!$activities.length) {
          $days.css('display', 'none');
          nbRows -= 1;
        }

        $slider.css('grid-template-rows', `repeat(${nbRows}, auto`);
      }
    }

    hideEmptyDays();
    new ResizeObserver(updateColumnsWidth).observe($weekList[0]);
  }

  $(document).on('combo:cell-loaded', function(ev, cell) {
    if ($('.weekly-agenda-cell', $(cell)).length) {
      initAgendaCell($(cell), '.weekly-agenda-cell');
    }
  });
});
