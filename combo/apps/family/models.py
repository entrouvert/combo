# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.db import models
from django.template import RequestContext, Template, TemplateSyntaxError, VariableDoesNotExist
from django.utils.translation import gettext_lazy as _

from combo.data.library import register_cell_class
from combo.data.models import JsonCellBase, django_template_validator
from combo.utils import get_templated_url


@register_cell_class
class WeeklyAgendaCell(JsonCellBase):
    title = models.CharField(_('Title'), max_length=150, blank=True)
    agenda_type = models.CharField(
        _('Agenda references'),
        max_length=20,
        default='manual',
        choices=[
            ('manual', _('Agenda references template')),
            ('subscribed', _('Use agendas subscriptions')),
        ],
    )
    agenda_references_template = models.CharField(
        _('Agenda references template'), max_length=2000, blank=True, validators=[django_template_validator]
    )
    agenda_categories = models.CharField(
        _('Agenda categories'), max_length=2000, blank=True, validators=[django_template_validator]
    )
    start_date_filter = models.CharField(
        _('Start date filter template'), max_length=250, blank=True, validators=[django_template_validator]
    )
    end_date_filter = models.CharField(
        _('End date filter template'), max_length=250, blank=True, validators=[django_template_validator]
    )
    user_external_template = models.CharField(
        _('User external reference template'),
        max_length=255,
        blank=True,
        validators=[django_template_validator],
    )
    booking_form_url = models.CharField(
        _('URL to the booking Form'), max_length=2000, blank=True, validators=[django_template_validator]
    )
    booking_button_title = models.CharField(
        _('Title of the booking button'),
        max_length=2000,
        blank=True,
        validators=[django_template_validator],
        help_text=_('Default: "Update bookings".'),
    )
    booking_button_position = models.CharField(
        _('Position of the booking button'),
        max_length=6,
        choices=[('bottom', _('Below the weekly agenda')), ('top', _('Above the weekly agenda'))],
        default='bottom',
    )
    booking_not_paid_key = models.CharField(
        _('Booking extra_data key for the "not-paid" status'),
        max_length=30,
        default='reglement',
    )
    booking_not_paid_value = models.CharField(
        _('Booking extra_data value for the "not-paid" status'),
        max_length=30,
        default='Non-réglé',
    )
    booking_without_invoice_key = models.CharField(
        _('Booking extra_data key for the "without-invoice" status'),
        max_length=30,
        default='facture_guichet',
    )
    booking_without_invoice_value = models.CharField(
        _('Booking extra_data value for the "without-invoice" status'),
        max_length=30,
        default='Sans facture',
    )

    default_template_name = 'combo/family/weekly_agenda.html'
    manager_form_template = 'combo/family/manager/weekly_agenda_cell_form.html'
    force_async = True
    cache_duration = 5

    class Meta:
        unique_together = [('page', 'uuid')]
        verbose_name = _('Weekly agenda cell')

    class Media:
        js = ('js/combo.weekly_agenda.js',)
        css = {'all': ('css/combo.weekly_agenda.css',)}

    @classmethod
    def is_enabled(cls):
        return settings.PUBLIK_FAMILY_CELL_ENABLED

    @property
    def url(self):
        chrono = list(settings.KNOWN_SERVICES.get('chrono').values())[0]
        chrono_url = chrono.get('url') or ''
        if not chrono_url.endswith('/'):
            chrono_url += '/'
        if self.agenda_type == 'manual':
            agenda_params = 'agendas=%s' % self.agenda_references_template
        else:
            agenda_params = 'subscribed=%s' % (self.agenda_categories or 'all')
        return (
            '%sapi/agendas/datetimes/?%s&date_start=%s&date_end=%s&user_external_id=%s'
            '&show_past_events=true&with_status=true&extra_data_keys=%s,%s'
            % (
                chrono_url,
                agenda_params,
                self.start_date_filter,
                self.end_date_filter,
                self.user_external_template,
                self.booking_not_paid_key,
                self.booking_without_invoice_key,
            )
        )

    def is_visible(self, request, **kwargs):
        user = getattr(request, 'user', None)
        if not user or user.is_anonymous:
            return False
        return super().is_visible(request, **kwargs)

    def get_booking_form_title(self, original_context, request):
        default = _('Update bookings')
        try:
            context = RequestContext(request)
            context.push(original_context)
            return Template(self.booking_button_title).render(context) or default
        except (VariableDoesNotExist, TemplateSyntaxError):
            return default

    @property
    def not_paid_extra_data(self):
        return f'extra_data__{self.booking_not_paid_key}'

    @property
    def without_invoice_extra_data(self):
        return f'extra_data__{self.booking_without_invoice_key}'

    def get_cell_extra_context(self, context):
        if context.get('placeholder_search_mode'):
            return {}
        if self.booking_form_url:
            context['booking_form_url'] = get_templated_url(self.booking_form_url, context=context)
        context['booking_button_title'] = self.get_booking_form_title(context, context.get('request'))

        return super().get_cell_extra_context(context)

    def get_default_form_class(self):
        from .forms import WeeklyAgendaCellForm

        return WeeklyAgendaCellForm

    def get_appearance_fields(self):
        return [
            'title',
            'custom_title',
            'booking_button_title',
            'booking_button_position',
            'booking_not_paid_key',
            'booking_not_paid_value',
            'booking_without_invoice_key',
            'booking_without_invoice_value',
        ]

    def get_computed_strings(self):
        yield from super().get_computed_strings()
        fields = [
            'agenda_references_template',
            'agenda_categories',
            'start_date_filter',
            'end_date_filter',
            'user_external_template',
            'booking_button_title',
        ]
        yield from [getattr(self, f) for f in fields]
