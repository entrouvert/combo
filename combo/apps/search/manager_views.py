# combo - content management system
# Copyright (C) 2014-2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import View

from combo.apps.search.forms import (
    CardsEngineSettingsForm,
    CardsEngineSettingsUpdateForm,
    TextEngineSettingsForm,
    TextEngineSettingsUpdateForm,
    UsersEngineSettingsForm,
    UsersEngineSettingsUpdateForm,
)
from combo.apps.search.models import SearchCell
from combo.data.models import PageSnapshot
from combo.manager.views import ManagedPageMixin
from combo.profile import default_description_template


def get_engine_options(form):
    kwargs = {
        'title': form.cleaned_data['title'],
    }
    if form.cleaned_data.get('description_template'):
        kwargs['description_template'] = form.cleaned_data['description_template']
    if form.cleaned_data.get('target_page'):
        kwargs['target_page'] = form.cleaned_data['target_page'].pk
    for key in ['without_user', 'with_description']:
        if form.cleaned_data.get(key):
            kwargs[key] = True
    return kwargs


class PageSearchCellAddEngine(ManagedPageMixin, View):
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def post(self, *args, **kwargs):
        request = self.request
        page_pk = self.kwargs['page_pk']
        cell_reference = self.kwargs['cell_reference']
        engine_slug = self.kwargs['engine_slug']
        cell = get_object_or_404(SearchCell, pk=cell_reference.split('-')[1], page=page_pk)

        def add_slug(slug, **options):
            if slug in cell.available_engines or slug.startswith('_text_page') or slug.startswith('cards:'):
                if not cell._search_services or not cell._search_services.get('data'):
                    cell._search_services = {'data': []}
                if not cell._search_services.get('options'):
                    cell._search_services['options'] = {}
                cell._search_services['data'].append(slug)
                cell._search_services['options'][slug] = options
                cell.save()
                PageSnapshot.take(cell.page, request=request, comment=_('changed cell "%s"') % cell)
            return HttpResponseRedirect(
                '%s#cell-%s' % (reverse('combo-manager-page-view', kwargs={'pk': page_pk}), cell_reference)
            )

        if engine_slug not in ['_text', 'users'] and not engine_slug.startswith('cards:'):
            # add engine without intermediary form and popup
            return add_slug(engine_slug)

        form_class = TextEngineSettingsForm
        if engine_slug == 'users':
            form_class = UsersEngineSettingsForm
        if engine_slug.startswith('cards:'):
            form_class = CardsEngineSettingsForm

        if request.method == 'POST':
            form = form_class(instance=cell, data=request.POST, engine_slug=engine_slug)
            if form.is_valid():
                kwargs = get_engine_options(form)
                return add_slug(form.get_slug(), **kwargs)
        else:
            form = form_class(instance=cell, engine_slug=engine_slug)
        context = {
            'form': form,
            'cell': cell,
        }
        return render(request, 'combo/manager/engine-form.html', context)


page_search_cell_add_engine = PageSearchCellAddEngine.as_view()


class PageSearchCellUpdateEngine(ManagedPageMixin, View):
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def post(self, *args, **kwargs):
        request = self.request
        page_pk = self.kwargs['page_pk']
        cell_reference = self.kwargs['cell_reference']
        engine_slug = self.kwargs['engine_slug']

        cell = get_object_or_404(SearchCell, pk=cell_reference.split('-')[1], page=page_pk)

        form_class = TextEngineSettingsUpdateForm
        if engine_slug == 'users':
            form_class = UsersEngineSettingsUpdateForm
        if engine_slug.startswith('cards:'):
            form_class = CardsEngineSettingsUpdateForm

        if request.method == 'POST':
            form = form_class(instance=cell, data=request.POST, engine_slug=engine_slug)
            if form.is_valid():
                kwargs = get_engine_options(form)

                if not cell._search_services.get('options'):
                    cell._search_services['options'] = {}
                cell._search_services['options'][engine_slug] = kwargs
                cell.save()
                PageSnapshot.take(cell.page, request=request, comment=_('changed cell "%s"') % cell)

                return HttpResponseRedirect(
                    '%s#cell-%s'
                    % (reverse('combo-manager-page-view', kwargs={'pk': page_pk}), cell_reference)
                )
        else:
            options = cell._search_services.get('options', {}).get(engine_slug, {})
            initial = {
                'title': options.get('title'),
                'without_user': options.get('without_user'),
                'with_description': options.get('with_description'),
                'description_template': options.get('description_template') or default_description_template,
                'target_page': options.get('target_page'),
            }
            form = form_class(instance=cell, engine_slug=engine_slug, initial=initial)
        context = {
            'form': form,
            'cell': cell,
        }
        return render(request, 'combo/manager/engine-form.html', context)


page_search_cell_update_engine = PageSearchCellUpdateEngine.as_view()


class PageSearchCellDeleteEngine(ManagedPageMixin, View):
    def get(self, *args, **kwargs):
        request = self.request
        page_pk = self.kwargs['page_pk']
        cell_reference = self.kwargs['cell_reference']
        engine_slug = self.kwargs['engine_slug']
        cell = get_object_or_404(SearchCell, pk=cell_reference.split('-')[1], page=page_pk)

        if engine_slug in cell._search_services.get('data'):
            cell._search_services['data'].remove(engine_slug)
            cell.save()
            PageSnapshot.take(cell.page, request=request, comment=_('changed cell "%s"') % cell)

        return HttpResponseRedirect(
            '%s#cell-%s' % (reverse('combo-manager-page-view', kwargs={'pk': page_pk}), cell_reference)
        )


page_search_cell_delete_engine = PageSearchCellDeleteEngine.as_view()


class PageSearchEnginesOrder(ManagedPageMixin, View):
    def get(self, *args, **kwargs):
        request = self.request
        page_pk = self.kwargs['page_pk']
        cell_reference = self.kwargs['cell_reference']
        cell = get_object_or_404(SearchCell, pk=cell_reference.split('-')[1], page=page_pk)

        if not cell._search_services.get('data'):
            return HttpResponse(status=204)

        engines = []
        for engine_slug in cell._search_services['data']:
            try:
                new_order = int(request.GET.get('pos_' + str(engine_slug)))
            except TypeError:
                new_order = 0
            engines.append((new_order, engine_slug))

        ordered_engines = [a[1] for a in sorted(engines, key=lambda a: a[0])]
        if ordered_engines != cell._search_services['data']:
            cell._search_services['data'] = ordered_engines
            cell.save()
            PageSnapshot.take(cell.page, request=request, comment=_('changed cell "%s"') % cell)

        return HttpResponse(status=204)


search_engines_order = PageSearchEnginesOrder.as_view()
