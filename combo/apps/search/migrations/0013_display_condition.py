from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0012_auto_20220105_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='searchcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
