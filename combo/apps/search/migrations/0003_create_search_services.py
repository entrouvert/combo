from django.db import migrations


def create_search_services(apps, schema_editor):
    SearchCell = apps.get_model('search', 'SearchCell')
    for searchcell in SearchCell.objects.all():
        if searchcell._search_service:
            searchcell._search_services = {'data': [searchcell._search_service]}
        else:
            searchcell._search_services = {'data': []}
        searchcell.save()


def back_to_search_service(apps, schema_editor):
    SearchCell = apps.get_model('search', 'SearchCell')
    for searchcell in SearchCell.objects.all():
        if searchcell._search_services.get('data'):
            searchcell._search_service = searchcell._search_services.get('data')[0]
        else:
            searchcell._search_service = ''
        searchcell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0002_auto_20180720_1511'),
    ]

    operations = [
        migrations.RunPython(create_search_services, back_to_search_service),
    ]
