from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0010_searchcell_template_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='searchcell',
            name='title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Title'),
        ),
    ]
