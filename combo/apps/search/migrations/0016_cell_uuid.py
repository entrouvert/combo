from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0015_indexedcell_hash_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='searchcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='searchcell',
            unique_together={('page', 'uuid')},
        ),
    ]
