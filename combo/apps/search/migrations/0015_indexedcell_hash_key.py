# Generated by Django 3.2.19 on 2024-07-09 20:05

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0014_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='indexedcell',
            name='hash_key',
            field=models.CharField(blank=True, null=True, db_index=True, max_length=64),
        ),
    ]
