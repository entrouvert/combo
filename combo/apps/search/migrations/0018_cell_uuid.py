from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0017_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='searchcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
