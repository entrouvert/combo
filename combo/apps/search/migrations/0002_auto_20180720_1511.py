from django.db import migrations
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='searchcell',
            name='_search_services',
            field=JSONField(default=dict, verbose_name='Search Services', blank=True),
        ),
    ]
