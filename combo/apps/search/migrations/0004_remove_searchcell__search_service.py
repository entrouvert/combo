from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('search', '0003_create_search_services'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='searchcell',
            name='_search_service',
        ),
    ]
