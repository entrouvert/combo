# combo - content management system
# Copyright (C) 2014-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _

from combo.apps.wcs.utils import get_matching_pages_from_card_slug
from combo.data.models import Page
from combo.profile import default_description_template

from .engines import engines
from .models import SearchCell


class SearchCellForm(forms.ModelForm):
    class Meta:
        model = SearchCell
        fields = ('autofocus', 'input_placeholder')


class SelectWithDisabled(forms.Select):
    """
    Subclass of Django's select widget that allows disabling options.
    To disable an option, pass a dict instead of a string for its label,
    of the form: {'label': 'option label', 'disabled': True}
    """

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        disabled = False
        if isinstance(label, dict):
            label, disabled = label['label'], label['disabled']
        option_dict = super().create_option(
            name, value, label, selected, index, subindex=subindex, attrs=attrs
        )
        if disabled:
            option_dict['attrs']['disabled'] = 'disabled'
        return option_dict


class TextEngineSettingsUpdateForm(forms.ModelForm):
    title = forms.CharField(label=_('Custom Title'), required=False)
    with_description = forms.BooleanField(label=_("Display page's description in results"), required=False)

    class Meta:
        model = SearchCell
        fields = []

    def __init__(self, *args, **kwargs):
        self.engine_slug = kwargs.pop('engine_slug')
        super().__init__(*args, **kwargs)

    def get_title(self):
        return _('Update "Page Contents" engine')


class TextEngineSettingsForm(TextEngineSettingsUpdateForm):
    selected_page = forms.ModelChoiceField(
        label=_('Page'),
        required=False,
        queryset=Page.objects.none(),
        help_text=_('Select a page to limit the search on this page and sub pages contents.'),
        widget=SelectWithDisabled(),
    )

    field_order = ['selected_page', 'title', 'with_description']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        used_slugs = [
            e['slug'].replace('_text_page_', '')
            for e in self.instance.search_services
            if e['slug'].startswith('_text_page_')
        ]
        pages_queryset = Page.objects.filter(snapshot__isnull=True, sub_slug='').order_by('title')
        pages = Page.get_as_reordered_flat_hierarchy(pages_queryset)
        pages_choices = [('', '---------')] + [
            (x.id, {'label': '%s %s' % ('\u00a0' * x.level * 2, x.title), 'disabled': x.slug in used_slugs})
            for x in pages
        ]
        # if '_text' without page is already selected, page is required
        if any(e['slug'] == '_text' for e in self.instance.search_services):
            pages_choices.pop(0)
            self.fields['selected_page'].required = True
        self.fields['selected_page'].queryset = pages_queryset.exclude(slug__in=used_slugs)
        self.fields['selected_page'].choices = pages_choices

    def get_title(self):
        return _('Add a "Page Contents" engine')

    def get_slug(self):
        if self.cleaned_data['selected_page'] is not None:
            return '_text_page_%s' % self.cleaned_data['selected_page'].slug
        return '_text'


class CardsEngineSettingsUpdateForm(forms.ModelForm):
    title = forms.CharField(label=_('Custom Title'), required=False)
    target_page = forms.ModelChoiceField(
        label=_('Target page'),
        queryset=Page.objects.none(),
    )

    class Meta:
        model = SearchCell
        fields = []

    def __init__(self, *args, **kwargs):
        all_engines = engines.get_engines()
        self.engine_slug = kwargs.pop('engine_slug')
        self.engine = all_engines.get(
            ':'.join(self.engine_slug.split(':')[:3]).replace('__without-user__', ''), {}
        )
        super().__init__(*args, **kwargs)

        matching_pages = get_matching_pages_from_card_slug(self.engine_slug.split(':')[2])
        if len(matching_pages) < 2:
            del self.fields['target_page']
            return

        pages_queryset = Page.objects.filter(pk__in=[p.pk for p in matching_pages])
        pages_choices = [(x.pk, x.get_full_path_titles()) for x in matching_pages]
        self.fields['target_page'].queryset = pages_queryset
        self.fields['target_page'].choices = pages_choices

    def get_title(self):
        return _('Update "%s" engine') % self.engine.get('label')


class CardsEngineSettingsForm(CardsEngineSettingsUpdateForm):
    selected_view = forms.ChoiceField(
        label=_('Custom view'),
        required=False,
    )
    without_user = forms.BooleanField(label=_('Ignore the logged-in user'), required=False)

    field_order = ['selected_view', 'without_user', 'title']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['selected_view'].choices = [(None, _('- All cards -'))] + [
            (v['id'], v['text']) for v in self.engine.get('custom_views', [])
        ]

    def get_title(self):
        return _('Add a "%s" engine') % self.engine.get('label')

    def get_slug(self):
        slug = self.engine_slug
        if self.cleaned_data['without_user']:
            slug = '%s__without-user__' % slug
        if self.cleaned_data['selected_view']:
            slug = '%s:%s' % (slug, self.cleaned_data['selected_view'])
        return slug


class UsersEngineSettingsUpdateForm(forms.ModelForm):
    title = forms.CharField(label=_('Custom Title'), required=False)
    description_template = forms.CharField(
        label=_('User search result template'),
        required=False,
        widget=forms.Textarea,
        initial=default_description_template,
    )

    class Meta:
        model = SearchCell
        fields = []

    def __init__(self, *args, **kwargs):
        self.engine_slug = kwargs.pop('engine_slug')
        super().__init__(*args, **kwargs)
        try:
            users_engine_settings = engines.get('users')
        except KeyError:
            users_engine_settings = {}
        if 'hit_description_template' in users_engine_settings:
            self.fields['description_template'].initial = users_engine_settings['hit_description_template']

    def get_title(self):
        return _('Update "Users" engine')

    def get_slug(self):
        return self.engine_slug


class UsersEngineSettingsForm(UsersEngineSettingsUpdateForm):
    def get_title(self):
        return _('Add a "Users" engine')
