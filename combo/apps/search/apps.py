# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.apps
from django.utils.translation import gettext_lazy as _

from .engines import engines


class AppConfig(django.apps.AppConfig):
    name = 'combo.apps.search'
    verbose_name = _('Search')

    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def hourly(self):
        from .utils import index_site

        index_site()

    def ready(self):
        # register built-in search engine for page contents
        engines.register(self.get_search_engines)

    def get_search_engines(self):
        from .utils import search_site

        return {
            '_text': {
                'function': search_site,
                'label': _('Page Contents'),
            }
        }
