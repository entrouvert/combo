# combo - content management system
# Copyright (C) 2014-2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.db import connection
from django.db.models import Prefetch, Q
from django.db.transaction import atomic

from combo.data.models import CellBase, Page, ValidityInfo

from .models import IndexedCell


def set_cell_groups(indexed_cell, cell):
    restricted_groups = []
    excluded_groups = []
    if not indexed_cell.public_access:
        restricted_groups = cell.prefetched_groups
        if cell.restricted_to_unlogged:
            excluded_groups = cell.page.prefetched_groups
        else:
            restricted_groups.extend(cell.page.prefetched_groups)
    if restricted_groups:
        indexed_cell.restricted_groups.set(restricted_groups)
    if excluded_groups:
        indexed_cell.excluded_groups.set(excluded_groups)


@atomic
def index_site():
    cell_classes = list(CellBase.get_cell_classes())
    # populate ContentType cache
    ContentType.objects.get_for_models(*cell_classes)

    old_cells = IndexedCell.objects.filter(hash_key=None)
    if old_cells.exists():
        # this should run only once just after the migration (0015_indexedcell_hash_key)
        # this will be the last "slow" re-indexing
        old_cells.delete()

    to_create = []
    cell_groups = []

    # load all indexed cells. limit RAM/CPU usage with .only()
    indexed_cells = {o.hash_key: o for o in IndexedCell.objects.only('id', 'hash_key', 'public_access').all()}

    validity_info_list = list(ValidityInfo.objects.select_related('content_type'))
    pages_by_pk = {
        p.pk: p for p in (Page.objects.prefetch_related(Prefetch('groups', to_attr='prefetched_groups')))
    }

    for klass in cell_classes:
        if getattr(klass, 'exclude_from_search', False) is True:
            # do not load cells marked as excluded from search (example: MenuCell, SearchCell, ...)
            continue
        queryset = (
            klass.objects.filter(page__snapshot__isnull=True, page__sub_slug='')
            .exclude(placeholder__startswith='_')
            .prefetch_related(Prefetch('groups', to_attr='prefetched_groups'))
        )

        cell_type = ContentType.objects.get_for_model(klass)

        for cell in queryset:
            cell.page = pages_by_pk.get(cell.page_id)
            # exclude cells with an inactive placeholder
            if not cell.is_placeholder_active():
                continue
            cell.prefetched_validity_info = [
                v
                for v in validity_info_list
                if v.object_id == cell.pk and v.content_type.model_class() == cell.__class__
            ]
            if cell.is_hidden_because_invalid():
                continue
            try:
                indexed_text = cell.render_for_search()
            except Exception:  # ignore rendering error
                continue

            def index_cell(cell, ctype, url, title, indexed_text):
                """get or create IndexedCells based on hash_key"""
                public_access = bool(cell.page.public and cell.public)
                hash_key = (
                    f'{ctype.id}-{cell.id}-{cell.page_id}-{public_access}-{url}-{title}-{indexed_text}'
                ).encode('utf8', 'ignore')
                hash_key = hashlib.sha256(hash_key).hexdigest()
                if hash_key in indexed_cells:
                    indexed_cell = indexed_cells.pop(hash_key)
                else:
                    indexed_cell = IndexedCell(
                        cell_type=ctype,
                        cell_pk=cell.id,
                        page_id=cell.page_id,
                        url=url,
                        title=title,
                        indexed_text=indexed_text,
                        public_access=public_access,
                        hash_key=hash_key,
                    )
                    to_create.append(indexed_cell)
                cell_groups.append((indexed_cell, cell))

            if indexed_text:
                index_cell(
                    cell,
                    cell_type,
                    url=cell.page.get_online_url(),
                    title=cell.page.title,
                    indexed_text=indexed_text,
                )

            for link_data in cell.get_external_links_data():
                # index external links
                index_cell(
                    cell,
                    cell_type,
                    url=link_data['url'],
                    title=link_data['title'],
                    indexed_text=link_data.get('text') or '',
                )

    # execute everything

    deleted = 0
    if indexed_cells:
        # delete remaining cells
        deleted = IndexedCell.objects.filter(hash_key__in=indexed_cells.keys()).delete()
        deleted = deleted[1].get('search.IndexedCell')

    if to_create:
        IndexedCell.objects.bulk_create(to_create, batch_size=100)

    # we have to recreate perms for all cells
    for cell_group in cell_groups:
        set_cell_groups(*cell_group)

    return len(to_create), deleted


def search_site(request, query, pages=None, with_description=None):
    pages = pages or []

    if connection.vendor == 'postgresql':
        config = settings.POSTGRESQL_FTS_SEARCH_CONFIG
        vector = SearchVector('title', config=config, weight='A') + SearchVector(
            'indexed_text', config=config, weight='B'
        )
        query = SearchQuery(query, config=config)
        qs = (
            IndexedCell.objects.annotate(rank=SearchRank(vector, query))
            .filter(rank__gte=0.2)
            .order_by('-rank')
        )
    else:
        qs = IndexedCell.objects.filter(Q(indexed_text__icontains=query) | Q(title__icontains=query))
    if request.user.is_anonymous:
        qs = qs.exclude(public_access=False)
    else:
        qs = qs.filter(Q(restricted_groups=None) | Q(restricted_groups__in=request.user.groups.all()))
        qs = qs.exclude(excluded_groups__in=request.user.groups.all())

    if pages:
        qs = qs.filter(page__in=pages)

    hits = []
    seen = {}
    for hit in qs:
        if hit.url in seen:
            continue
        hits.append(
            {
                'text': hit.title,
                'rank': getattr(hit, 'rank', None),
                'url': hit.url,
                'description': hit.page.description if (hit.page and with_description is True) else '',
            }
        )
        seen[hit.url] = True
        if len(hits) == 10:
            break

    return hits
