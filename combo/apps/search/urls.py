# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, re_path

from combo.urls_utils import decorated_includes, manager_required

from . import manager_views
from .models import SearchCell

search_manager_urls = [
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_]+-\d+)/engine/(?P<engine_slug>[\w_:-]+)/add/$',
        manager_views.page_search_cell_add_engine,
        name='combo-manager-page-search-cell-add-engine',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_]+-\d+)/engine/(?P<engine_slug>[\w_:-]+)/edit/$',
        manager_views.page_search_cell_update_engine,
        name='combo-manager-page-search-cell-update-engine',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_]+-\d+)/engine/(?P<engine_slug>[\w_:-]+)/delete/$',
        manager_views.page_search_cell_delete_engine,
        name='combo-manager-page-search-cell-delete-engine',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_]+-\d+)/engine/order$',
        manager_views.search_engines_order,
        name='combo-manager-search-engines-order',
    ),
]

urlpatterns = [
    re_path(r'^manage/search/', decorated_includes(manager_required, include(search_manager_urls))),
    re_path(
        r'^ajax/search/(?P<cell_pk>\d+)/(?P<service_slug>[\w_:-]+)/$',
        SearchCell.ajax_results_view,
        name='combo-search-ajax-results',
    ),
]
