# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse

from combo.data.models import Page


def menuitem_url(request, page, subpage=None):
    if subpage and subpage.redirect_url:
        return subpage.get_redirect_url()
    elif subpage:
        return request.build_absolute_uri('/'.join(['', page.slug, subpage.slug, '']))
    if page.redirect_url:
        return page.get_redirect_url()
    if page.slug == 'index' and not page.parent_id:
        return request.build_absolute_uri('/')
    return request.build_absolute_uri('/'.join(['', page.slug, '']))


@login_required
def services_js(request, *args, **kwargs):
    if not getattr(settings, 'KNOWN_SERVICES', None):
        raise Http404()
    services = []
    portal_agent_url = None
    portal_agent_title = None
    menu_items = []

    if getattr(settings, 'PUBLIK_EXPLICIT_MENU', False):
        pages = Page.objects.filter(parent_id__isnull=True, exclude_from_navigation=False)
        second_level_pages = Page.objects.filter(parent__in=pages, exclude_from_navigation=False)
        for page in pages:
            if not page.is_visible(request.user, ignore_superuser=True):
                continue
            menu_items.append(
                {
                    'label': page.title,
                    'url': menuitem_url(request, page),
                    'slug': page.slug if page.slug != 'index' else 'home',
                    'icon': request.build_absolute_uri(page.picture.url) if page.picture else None,
                }
            )
            for subpage in second_level_pages:
                if subpage.parent_id != page.id:
                    continue
                if not subpage.is_visible(request.user, ignore_superuser=True):
                    continue
                menu_items.append(
                    {
                        'label': subpage.title,
                        'sub': True,
                        'url': menuitem_url(request, page, subpage),
                        'slug': subpage.slug,
                        'icon': request.build_absolute_uri(subpage.picture.url) if subpage.picture else None,
                    }
                )
    else:
        for service_id, services_dict in settings.KNOWN_SERVICES.items():
            for service_slug, service in services_dict.items():
                if service.get('secondary') and service_id != 'authentic':
                    continue
                if service.get('is-portal-agent'):
                    portal_agent_url = service['url']
                    portal_agent_title = service['title']
                services.append(
                    {
                        'title': service['title'],
                        'slug': service_slug,
                        'service_id': service_id,
                        'uniq': bool(len([x for x in services_dict.values() if not x.get('secondary')]) == 1),
                        'url': service['url'],
                        'backoffice_menu_url': service['backoffice-menu-url'],
                    }
                )

    response_vars = {
        'PUBLIK_ENVIRONMENT_LABEL': settings.TEMPLATE_VARS.get('environment_label'),
        'PUBLIK_PORTAL_AGENT_URL': portal_agent_url,
        'PUBLIK_PORTAL_AGENT_TITLE': portal_agent_title,
        'PUBLIK_PORTAL_AGENT_EXTRA_CSS': settings.TEMPLATE_VARS.get('portal_agent_extra_css'),
        'PUBLIK_MENU_ITEMS': menu_items,
        'PUBLIK_EXPLICIT_MENU': getattr(settings, 'PUBLIK_EXPLICIT_MENU', False),
        'COMBO_KNOWN_SERVICES': services,
    }
    response_body = '\n'.join(['var %s = %s;' % (x[0], json.dumps(x[1])) for x in response_vars.items()])
    return HttpResponse(response_body, content_type='text/javascript')


services_js.mellon_no_passive = False
