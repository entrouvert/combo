# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    re_path(
        r'^api/dashboard/add/(?P<cell_reference>[\w_-]+)/$',
        views.dashboard_add_tile,
        name='combo-dashboard-add-tile',
    ),
    re_path(
        r'^api/dashboard/remove/(?P<cell_reference>[\w_-]+)/$',
        views.dashboard_remove_tile,
        name='combo-dashboard-remove-tile',
    ),
    re_path(
        r'^api/dashboard/auto-tile/(?P<key>[\w_-]+)/$',
        views.dashboard_auto_tile,
        name='combo-dashboard-auto-tile',
    ),
    re_path(
        r'^api/dashboard/reorder/(?P<dashboard_id>[\w]+)/$',
        views.dashboard_reorder_tiles,
        name='combo-dashboard-reorder-tiles',
    ),
    path(
        'api/dashboard/tile-stats/',
        views.dashboard_tile_stats,
        name='combo-dashboard-tile-stats',
    ),
]
