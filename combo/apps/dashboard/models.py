# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.conf import settings
from django.contrib.contenttypes import fields
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from combo.data.library import register_cell_class
from combo.data.models import CellBase, ValidityInfo


@register_cell_class
class DashboardCell(CellBase):
    # container for tiles
    user_dependant = True

    class Meta:
        unique_together = [('page', 'uuid')]
        verbose_name = _('Dashboard')

    class Media:
        js = ('js/dashboard.js',)

    @classmethod
    def is_enabled(cls):
        return settings.COMBO_DASHBOARD_ENABLED

    def is_relevant(self, context):
        if not (getattr(context['request'], 'user', None) and context['request'].user.is_authenticated):
            return False
        return True

    def render(self, context):
        tiles = Tile.objects.filter(dashboard=self, user=context['user'])
        validity_info_dict = {
            (x.content_type_id, x.object_id): True
            for x in ValidityInfo.objects.filter(invalid_since__lt=now() - datetime.timedelta(days=2))
        }
        context['tiles'] = [x for x in tiles if (x.cell_type_id, x.cell_pk) not in validity_info_dict]
        return super().render(context)


class Tile(models.Model):
    dashboard = models.ForeignKey(DashboardCell, on_delete=models.CASCADE)
    cell_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    cell_pk = models.PositiveIntegerField()
    cell = fields.GenericForeignKey('cell_type', 'cell_pk')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    order = models.IntegerField()

    class Meta:
        ordering = ('order',)

    @classmethod
    def get_by_cell(cls, cell):
        cell_type = ContentType.objects.get_for_model(cell)
        return cls.objects.get(cell_type__pk=cell_type.id, cell_pk=cell.id)
