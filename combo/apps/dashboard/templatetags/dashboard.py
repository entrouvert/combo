# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.core import serializers

from ..models import Tile

register = template.Library()


def get_cell_data(cell):
    # return a dictionary with cell parameters relevant for tile comparison
    if cell is None:
        return {}
    cell_data = serializers.serialize('python', [cell])[0]
    del cell_data['pk']
    for key in (
        'uuid',
        'restricted_to_unlogged',
        'groups',
        'last_update_timestamp',
        'order',
        'placeholder',
        'public',
        'page',
    ):
        del cell_data['fields'][key]
    return cell_data


@register.filter
def as_dashboard_cell(cell, user):
    cell_data = get_cell_data(cell)
    for tile in Tile.objects.filter(user=user):
        if get_cell_data(tile.cell) == cell_data:
            return tile
    return None
