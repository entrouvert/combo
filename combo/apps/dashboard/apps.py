# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

import django.apps
from django.utils.timezone import now, timedelta
from django.utils.translation import gettext_lazy as _


class AppConfig(django.apps.AppConfig):
    name = 'combo.apps.dashboard'
    verbose_name = _('Dashboard')

    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def hourly(self):
        self.clean_autotiles()

    def clean_autotiles(self):
        from combo.data.models import ConfigJsonCell

        ConfigJsonCell.objects.filter(
            placeholder='_auto_tile', last_update_timestamp__lte=now() - timedelta(days=2)
        ).delete()
