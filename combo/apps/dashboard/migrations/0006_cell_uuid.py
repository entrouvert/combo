from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dashboard', '0005_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='dashboardcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='dashboardcell',
            unique_together={('page', 'uuid')},
        ),
    ]
