from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dashboard', '0007_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dashboardcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
