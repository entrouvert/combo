from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dashboard', '0003_dashboardcell_template_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='dashboardcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
