# combo - content management system
# Copyright (C) 2017-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import PIL
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class AssetUploadForm(forms.Form):
    upload = forms.FileField(label=_('File'))
    optimize = forms.BooleanField(label=_('Optimize image'), initial=True, required=False)

    def clean(self):
        cleaned_data = super().clean()
        value = cleaned_data.get('upload', None)
        if value is None:
            return None
        try:
            PIL.Image.open(value.file)
        except PIL.UnidentifiedImageError:
            cleaned_data['is_image'] = False
        except PIL.Image.DecompressionBombError as expt:
            raise ValidationError(
                _('Uploaded image exceeds size limits: %(detail)s'), params={'detail': str(expt)}
            )
        else:
            cleaned_data['is_image'] = True
        return cleaned_data


class AssetsImportForm(forms.Form):
    assets_file = forms.FileField(
        label=_('Assets File'), help_text=_('Archive (.tar) with asset files as content.')
    )
    overwrite = forms.BooleanField(label=_('Overwrite Existing Files'), required=False)
    optimize = forms.BooleanField(label=_('Optimize image'), initial=True, required=False)
