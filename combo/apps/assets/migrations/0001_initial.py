# Generated by Django 1.11.12 on 2018-06-12 11:42

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Asset',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('key', models.CharField(max_length=128, unique=True)),
                ('asset', models.FileField(upload_to='assets')),
            ],
        ),
    ]
