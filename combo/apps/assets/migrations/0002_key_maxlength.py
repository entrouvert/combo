from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('assets', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='key',
            field=models.CharField(max_length=256, unique=True),
        ),
    ]
