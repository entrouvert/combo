# combo - content management system
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import tarfile
from io import BytesIO

import PIL
from ckeditor import image as ckeditor_image
from django.conf import settings
from django.core.exceptions import BadRequest
from django.core.files.storage import default_storage

from .models import Asset

EXIF_ORIENTATION = 0x0112


ASSET_DIRS = [
    'assets',
    'page-pictures',
    'uploads',
]


def is_asset_dir(basedir):
    # exclude dirs like cache or applications, which contain non asset files
    media_prefix = default_storage.path('')
    asset_basedirs = [os.path.join(media_prefix, ad) for ad in ASSET_DIRS]
    for adb in asset_basedirs:
        if basedir.startswith(adb):
            return True
    return False


def clean_assets_files():
    media_prefix = default_storage.path('')
    for basedir, dummy, filenames in os.walk(media_prefix):
        if not is_asset_dir(basedir):
            continue
        for filename in filenames:
            os.remove('%s/%s' % (basedir, filename))


def add_tar_content(tar, filename, content):
    file = tarfile.TarInfo(filename)
    fd = BytesIO()
    fd.write(content.encode('utf-8'))
    file.size = fd.tell()
    fd.seek(0)
    tar.addfile(file, fileobj=fd)
    fd.close()


def untar_assets_files(tar, overwrite=False):
    media_prefix = default_storage.path('')
    data = {}
    for tarinfo in tar.getmembers():
        filepath = default_storage.path(tarinfo.name)
        if not overwrite and os.path.exists(filepath):
            continue
        if tarinfo.name == '_assets.json':
            json_assets = tar.extractfile(tarinfo).read()
            data = json.loads(json_assets.decode('utf-8'))
        elif tarinfo.name != '_site.json':
            tar.extract(tarinfo, path=media_prefix)
    return data


def tar_assets_files(tar):
    media_prefix = default_storage.path('')
    for basedir, dummy, filenames in os.walk(media_prefix):
        if not is_asset_dir(basedir):
            continue
        for filename in filenames:
            tar.add(os.path.join(basedir, filename), os.path.join(basedir, filename)[len(media_prefix) :])
    export = {'assets': Asset.export_all_for_json()}
    add_tar_content(tar, '_assets.json', json.dumps(export, indent=2))


def import_assets(fd, overwrite=False):
    with tarfile.open(mode='r', fileobj=fd) as tar:
        data = untar_assets_files(tar, overwrite=overwrite)
    Asset.load_serialized_objects(data.get('assets') or [])


def export_assets(fd):
    with tarfile.open(mode='w', fileobj=fd) as tar:
        tar_assets_files(tar)


def process_image(fd, image=None, optimize=True, max_width=None, **kwargs):
    if max_width is None:
        max_width = settings.COMBO_ASSET_IMAGE_MAX_WIDTH
    if image is None:
        original_image = image = PIL.Image.open(fd)
    elif image.fp is None:
        # file was closed, re-open it
        original_image = image = PIL.Image.open(fd)
    else:
        original_image = image
    fmt = image.format

    modified = False

    try:
        exif = image._getexif()
    except Exception:
        exif = None

    if exif:
        # mark as modified to remove exif infos
        modified = True
        # orientation code from sorl.thumbnail (engines/pil_engine.py)
        orientation = exif.get(EXIF_ORIENTATION)

        if orientation == 2:
            image = image.transpose(PIL.Image.FLIP_LEFT_RIGHT)
        elif orientation == 3:
            image = image.rotate(180)
        elif orientation == 4:
            image = image.transpose(PIL.Image.FLIP_TOP_BOTTOM)
        elif orientation == 5:
            image = image.rotate(-90, expand=1).transpose(PIL.Image.FLIP_LEFT_RIGHT)
        elif orientation == 6:
            image = image.rotate(-90, expand=1)
        elif orientation == 7:
            image = image.rotate(90, expand=1).transpose(PIL.Image.FLIP_LEFT_RIGHT)
        elif orientation == 8:
            image = image.rotate(90, expand=1)

    if optimize:
        if max_width and image.width > max_width:
            height = int(image.height * (max_width / image.width))
            image = image.resize((max_width, height), resample=PIL.Image.Resampling.LANCZOS)
            modified = True

    if original_image == image:
        # we need a copy to be able to truncate the original fd
        image = image.copy()

    if modified:
        fd.truncate(0)
        fd.seek(0)
        image.save(fd, format=fmt)
        fd.seek(0)
    return (fd, image)


# monkey patch ckeditor's image_verify to handle errors / optim
def image_verify(fd):
    try:
        image = PIL.Image.open(fd)
        image.verify()
    except OSError:
        raise ckeditor_image.pillow_backend.utils.NotAnImageException
    except PIL.Image.DecompressionBombError as e:
        # should never occurs. it's an attack
        raise BadRequest(str(e))
    else:
        process_image(fd, image=image)


ckeditor_image.pillow_backend.image_verify = image_verify
