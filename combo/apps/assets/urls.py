# combo - content management system
# Copyright (C) 2017-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from combo.urls_utils import decorated_includes, staff_required

from . import api_views, views

assets_manager_urls = [
    path('', views.assets, name='combo-manager-assets'),
    re_path(r'^slots/(?P<cell_reference>[\w_-]+)/$', views.slot_assets, name='combo-manager-slot-assets'),
    path('delete', views.asset_delete, name='combo-manager-asset-delete'),
    path('overwrite/', views.asset_overwrite, name='combo-manager-asset-overwrite'),
    path('upload/', views.asset_upload, name='combo-manager-asset-upload'),
    re_path(r'^upload/(?P<key>[\w_:-]+)/$', views.slot_asset_upload, name='combo-manager-slot-asset-upload'),
    re_path(r'^delete/(?P<key>[\w_:-]+)/$', views.slot_asset_delete, name='combo-manager-slot-asset-delete'),
    path('export/', views.assets_export, name='combo-manager-assets-export'),
    path('import/', views.assets_import, name='combo-manager-assets-import'),
]

urlpatterns = [
    re_path(r'^assets/(?P<key>[\w_:-]+)$', views.serve_asset),
    re_path(r'^manage/assets/', decorated_includes(staff_required, include(assets_manager_urls))),
    re_path(r'^api/assets/set/(?P<key>[\w_:-]+)/$', api_views.view_set, name='api-assets-set'),
    path('ajax/assets-export-size/', views.assets_export_size, name='combo-manager-assets-export-size'),
]
