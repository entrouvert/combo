import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in [
        'LingoBasketCell',
        'LingoRecentTransactionsCell',
        'LingoBasketLinkCell',
        'SelfDeclaredInvoicePayment',
        'InvoicesCell',
        'PaymentsCell',
        'CreditsCell',
        'RefundsCell',
        'TipiPaymentFormCell',
    ]:
        klass = apps.get_model('lingo', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0062_invoice_cell_display_mode'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
