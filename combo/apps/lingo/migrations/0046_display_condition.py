from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0045_basketitem_remote_item_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='activeitems',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='itemshistory',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='lingobasketcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='lingobasketlinkcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='lingorecenttransactionscell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='selfdeclaredinvoicepayment',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
