from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0003_auto_20150306_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='basketitem',
            name='notification_date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
