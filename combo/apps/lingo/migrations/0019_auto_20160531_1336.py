from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0018_auto_20160527_0638'),
    ]

    operations = [
        migrations.AddField(
            model_name='basketitem',
            name='user_cancellable',
            field=models.BooleanField(default=True),
        ),
    ]
