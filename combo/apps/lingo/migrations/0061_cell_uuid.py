from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0060_refunds'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='creditscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='invoicescell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='invoicescell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='lingobasketcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='lingobasketcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='lingobasketlinkcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='lingobasketlinkcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='lingorecenttransactionscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='lingorecenttransactionscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='paymentscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='paymentscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='refundscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='refundscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='selfdeclaredinvoicepayment',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='selfdeclaredinvoicepayment',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='tipipaymentformcell',
            unique_together={('page', 'uuid')},
        ),
    ]
