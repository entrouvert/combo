from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0005_auto_20150307_1242'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='status',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
