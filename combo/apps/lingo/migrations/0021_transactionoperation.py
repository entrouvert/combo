from django.db import migrations, models
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0020_auto_20160606_1803'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionOperation',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'kind',
                    models.CharField(
                        max_length=65,
                        choices=[(b'validation', 'Validation'), (b'cancellation', 'Cancellation')],
                    ),
                ),
                ('amount', models.DecimalField(max_digits=8, decimal_places=2)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('bank_result', JSONField(default=dict, blank=True)),
                ('transaction', models.ForeignKey(to='lingo.Transaction', on_delete=models.CASCADE)),
            ],
        ),
    ]
