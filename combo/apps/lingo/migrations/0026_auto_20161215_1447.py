from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0025_auto_20161206_1713'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='regie',
            options={'ordering': ('-is_default', 'label'), 'verbose_name': 'Regie'},
        ),
        migrations.AlterField(
            model_name='basketitem',
            name='source_url',
            field=models.URLField(verbose_name='Source URL', blank=True),
        ),
    ]
