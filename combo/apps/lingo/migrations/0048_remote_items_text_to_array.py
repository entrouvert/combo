from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.indexes import GinIndex
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0047_transaction_index_startdate'),
    ]

    operations = [
        migrations.RunSQL(
            """ALTER TABLE lingo_transaction
            ALTER COLUMN remote_items TYPE character varying(512)[] USING string_to_array(remote_items, ','),
            ALTER COLUMN to_be_paid_remote_items TYPE character varying(512)[] USING string_to_array(to_be_paid_remote_items, ',');
        """
        ),
        migrations.AlterField(
            'transaction', 'remote_items', ArrayField(models.CharField(max_length=512), null=True)
        ),
        migrations.AlterField(
            'transaction', 'to_be_paid_remote_items', ArrayField(models.CharField(max_length=512), null=True)
        ),
        migrations.AddIndex('transaction', GinIndex(name='remote_items__gin_idx', fields=['remote_items'])),
    ]
