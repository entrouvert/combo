from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0063_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creditscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='invoicescell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='lingobasketcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='lingobasketlinkcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='lingorecenttransactionscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='paymentscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='refundscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='selfdeclaredinvoicepayment',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='tipipaymentformcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
