from django.db import migrations


def forwards(apps, schema_editor):
    ItemsHistory = apps.get_model('lingo', 'ItemsHistory')
    ActiveItems = apps.get_model('lingo', 'ActiveItems')
    InvoicesCell = apps.get_model('lingo', 'InvoicesCell')

    # migrate history items cell
    for cell in ItemsHistory.objects.order_by('pk'):
        new_cell = InvoicesCell(
            # base cell config
            page=cell.page,
            placeholder=cell.placeholder,
            order=cell.order,
            slug=cell.slug,
            extra_css_class=cell.extra_css_class,
            template_name=cell.template_name,
            condition=cell.condition,
            public=cell.public,
            restricted_to_unlogged=cell.restricted_to_unlogged,
            last_update_timestamp=cell.last_update_timestamp,
            # cell config
            regie=cell.regie,
            title=cell.title,
            text=cell.text,
            display_mode='historical',
            hide_if_empty=False,
        )
        new_cell.save()
        new_cell.groups.set(cell.groups.all())
        # validity infos are lost

        # update page's related_cells cache
        if 'lingo_invoicescell' not in new_cell.page.related_cells.get('cell_types', []):
            if not new_cell.page.related_cells.get('cell_types'):
                new_cell.page.related_cells['cell_types'] = []
            new_cell.page.related_cells['cell_types'].append('lingo_invoicescell')
            new_cell.page.save()

    # migrate active items cell
    for cell in ActiveItems.objects.order_by('pk'):
        new_cell = InvoicesCell(
            # base cell config
            page=cell.page,
            placeholder=cell.placeholder,
            order=cell.order,
            slug=cell.slug,
            extra_css_class=cell.extra_css_class,
            template_name=cell.template_name,
            condition=cell.condition,
            public=cell.public,
            restricted_to_unlogged=cell.restricted_to_unlogged,
            last_update_timestamp=cell.last_update_timestamp,
            # cell config
            regie=cell.regie,
            title=cell.title,
            text=cell.text,
            display_mode='active',
            hide_if_empty=cell.hide_if_empty,
        )
        new_cell.save()
        new_cell.groups.set(cell.groups.all())
        # validity infos are lost

        # update page's related_cells cache
        if 'lingo_invoicescell' not in new_cell.page.related_cells.get('cell_types', []):
            if not new_cell.page.related_cells.get('cell_types'):
                new_cell.page.related_cells['cell_types'] = []
            new_cell.page.related_cells['cell_types'].append('lingo_invoicescell')
            new_cell.page.save()


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0049_invoices_cell'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
