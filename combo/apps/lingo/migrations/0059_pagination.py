from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0058_transaction_order_id_idx'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditscell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                blank=True, null=True, verbose_name='Number of credits per page (default 10)'
            ),
        ),
        migrations.AddField(
            model_name='invoicescell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                blank=True, null=True, verbose_name='Number of invoices per page (default 10)'
            ),
        ),
        migrations.AddField(
            model_name='paymentscell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                blank=True, null=True, verbose_name='Number of payments per page (default 10)'
            ),
        ),
    ]
