import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0020_auto_20160928_1152'),
        ('lingo', '0023_auto_20160928_1152'),
    ]

    operations = [
        migrations.CreateModel(
            name='SelfDeclaredInvoicePayment',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                (
                    'extra_css_class',
                    models.CharField(
                        max_length=100, verbose_name='Extra classes for CSS styling', blank=True
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('regie', models.CharField(max_length=50, verbose_name='Regie', blank=True)),
                ('title', models.CharField(max_length=200, verbose_name='Title', blank=True)),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Self declared invoice payment',
            },
        ),
    ]
