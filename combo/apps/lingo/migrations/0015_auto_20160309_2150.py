from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0014_auto_20160218_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basketitem',
            name='subject',
            field=models.CharField(max_length=200, verbose_name='Subject'),
            preserve_default=True,
        ),
    ]
