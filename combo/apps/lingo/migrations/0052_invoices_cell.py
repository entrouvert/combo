from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0051_invoices_cell'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoicescell',
            name='include_pay_button',
            field=models.BooleanField(default=True, verbose_name='Include pay button'),
        ),
        migrations.AddField(
            model_name='invoicescell',
            name='payer_external_id_template',
            field=models.CharField(
                blank=True,
                max_length=1000,
                verbose_name='Payer external id (template)',
                help_text='The computed value will be transmitted to the billing system. It can also be left blank.',
            ),
        ),
    ]
