from django.db import migrations


def forward(apps, schema_editor):
    PageSnapshot = apps.get_model('data', 'PageSnapshot')
    for snapshot in PageSnapshot.objects.all():
        changed = False
        for cell in snapshot.serialization.get('cells') or []:
            if cell.get('model') not in ['lingo.activeitems', 'lingo.itemshistory']:
                continue
            if cell['model'] == 'lingo.activeitems':
                cell['model'] = 'lingo.invoicescell'
                cell['fields']['display_mode'] = 'active'
            elif cell['model'] == 'lingo.itemshistory':
                cell['model'] = 'lingo.invoicescell'
                cell['fields']['display_mode'] = 'historical'
                cell['fields']['hide_if_empty'] = False
            cell['fields']['include_pay_button'] = True
            cell['fields']['payer_external_id_template'] = ''
            changed = True
        if changed:
            snapshot.save()


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0052_invoices_cell'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
