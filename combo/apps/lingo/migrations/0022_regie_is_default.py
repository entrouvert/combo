from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0021_transactionoperation'),
    ]

    operations = [
        migrations.AddField(
            model_name='regie',
            name='is_default',
            field=models.BooleanField(default=False, verbose_name='Default Regie'),
        ),
    ]
