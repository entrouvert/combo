from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0022_regie_is_default'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='regie',
            options={'ordering': ('label',), 'verbose_name': 'Regie'},
        ),
        migrations.AddField(
            model_name='activeitems',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='itemshistory',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='lingobasketcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='lingobasketlinkcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='lingorecenttransactionscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
