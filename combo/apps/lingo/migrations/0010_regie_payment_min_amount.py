from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0009_auto_20150917_1456'),
    ]

    operations = [
        migrations.AddField(
            model_name='regie',
            name='payment_min_amount',
            field=models.DecimalField(
                default=0, verbose_name='Minimal payment amount', max_digits=7, decimal_places=2
            ),
            preserve_default=True,
        ),
    ]
