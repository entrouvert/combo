from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0029_auto_20170528_1334'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='to_be_paid_remote_items',
            field=models.CharField(max_length=512, null=True),
            preserve_default=True,
        ),
    ]
