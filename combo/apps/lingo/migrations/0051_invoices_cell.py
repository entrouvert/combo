from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0050_invoices_cell'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ActiveItems',
        ),
        migrations.DeleteModel(
            name='ItemsHistory',
        ),
    ]
