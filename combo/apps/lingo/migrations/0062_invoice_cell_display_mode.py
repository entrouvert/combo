from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0061_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoicescell',
            name='display_mode',
            field=models.CharField(
                choices=[
                    ('active', 'Active'),
                    ('activetopay', 'Active to pay'),
                    ('activeunpaid', 'Active unpaid'),
                    ('historical', 'Historical'),
                    ('collected', 'Collected'),
                ],
                default='active',
                max_length=15,
                verbose_name='Invoices to display',
            ),
        ),
    ]
