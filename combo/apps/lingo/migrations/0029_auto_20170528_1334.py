from django.db import migrations, models
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0028_tipipaymentformcell'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='basketitem',
            options={'ordering': ['regie', 'extra_fee', 'subject']},
        ),
        migrations.AddField(
            model_name='basketitem',
            name='extra_fee',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='basketitem',
            name='request_data',
            field=JSONField(default=dict, blank=True),
        ),
        migrations.AddField(
            model_name='regie',
            name='extra_fees_ws_url',
            field=models.URLField(verbose_name='Webservice URL to compute extra fees', blank=True),
        ),
    ]
