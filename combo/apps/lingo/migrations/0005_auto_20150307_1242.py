from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('data', '0005_auto_20150226_0903'),
        ('lingo', '0004_basketitem_notification_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='LingoRecentTransactionsCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Recent Transactions',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='transaction',
            name='user',
            field=models.ForeignKey(default=0, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=False,
        ),
    ]
