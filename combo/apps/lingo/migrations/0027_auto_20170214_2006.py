import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0026_auto_20161215_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='activeitems',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='itemshistory',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lingobasketcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lingobasketlinkcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lingorecenttransactionscell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='selfdeclaredinvoicepayment',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
    ]
