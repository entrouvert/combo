from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0012_auto_20151130_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='amount',
            field=models.DecimalField(default=0, max_digits=7, decimal_places=2),
            preserve_default=True,
        ),
    ]
