from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0017_auto_20160327_0831'),
    ]

    operations = [
        migrations.AddField(
            model_name='regie',
            name='text_on_success',
            field=models.TextField(null=True, verbose_name='Custom text displayed on success', blank=True),
        ),
    ]
