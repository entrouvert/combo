import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0016_auto_20160319_2028'),
    ]

    operations = [
        migrations.AddField(
            model_name='activeitems',
            name='text',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
        migrations.AddField(
            model_name='itemshistory',
            name='text',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
    ]
