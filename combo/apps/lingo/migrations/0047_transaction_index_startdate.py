from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0046_display_condition'),
    ]

    operations = [
        migrations.AlterField(
            model_name='Transaction',
            name='start_date',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
    ]
