from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0002_transaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regie',
            name='service',
            field=models.CharField(
                max_length=64,
                verbose_name='Payment Service',
                choices=[
                    (b'dummy', 'Dummy (for tests)'),
                    (b'systempayv2', b'systempay (Banque Populaire)'),
                    (b'sips', b'SIPS'),
                    (b'spplus', "SP+ (Caisse d'epargne)"),
                    (b'ogone', 'Ingenico (formerly Ogone)'),
                ],
            ),
            preserve_default=True,
        ),
    ]
