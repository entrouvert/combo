from django.db import migrations, models

import combo.data.fields


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0022_auto_20170214_2006'),
        ('lingo', '0027_auto_20170214_2006'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipiPaymentFormCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                (
                    'extra_css_class',
                    models.CharField(
                        max_length=100, verbose_name='Extra classes for CSS styling', blank=True
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=150, verbose_name='Title', blank=True)),
                (
                    'url',
                    models.URLField(
                        default=b'https://www.payfip.gouv.fr/tpa/paiement.web',
                        verbose_name='TIPI payment service URL',
                    ),
                ),
                (
                    'regies',
                    models.CharField(help_text='separated by commas', max_length=256, verbose_name='Regies'),
                ),
                (
                    'control_protocol',
                    models.CharField(
                        default=b'pesv2',
                        max_length=8,
                        verbose_name='Control protocol',
                        choices=[(b'pesv2', 'Indigo/PES v2'), (b'rolmre', 'ROLMRE')],
                    ),
                ),
                ('test_mode', models.BooleanField(default=False, verbose_name='Test mode')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'TIPI Payment Form',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='activeitems',
            name='text',
            field=combo.data.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
        migrations.AlterField(
            model_name='itemshistory',
            name='text',
            field=combo.data.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
        migrations.AlterField(
            model_name='selfdeclaredinvoicepayment',
            name='text',
            field=combo.data.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
    ]
