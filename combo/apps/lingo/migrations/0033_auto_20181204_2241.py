from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0032_basketitem_capture_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='exer',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=4, verbose_name='Exer', blank=True
            ),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='idligne',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=6, verbose_name='IDLIGNE', blank=True
            ),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='idpce',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=8, verbose_name='IDPCE', blank=True
            ),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='roldeb',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=2, verbose_name='ROLDEB', blank=True
            ),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='roldet',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=13, verbose_name='ROLDET', blank=True
            ),
        ),
        migrations.AddField(
            model_name='tipipaymentformcell',
            name='rolrec',
            field=models.CharField(
                help_text='Default value to be used in form', max_length=2, verbose_name='ROLREC', blank=True
            ),
        ),
    ]
