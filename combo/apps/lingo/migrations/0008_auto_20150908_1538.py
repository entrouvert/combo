import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0010_feedcell'),
        ('lingo', '0007_lingobasketlinkcell'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActiveItems',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('regie', models.CharField(max_length=50, verbose_name='Regie', blank=True)),
                ('title', ckeditor.fields.RichTextField(verbose_name='Title', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Active Items Cell',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemsHistory',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('regie', models.CharField(max_length=50, verbose_name='Regie', blank=True)),
                ('title', ckeditor.fields.RichTextField(verbose_name='Title', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Items History Cell',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='regie',
            name='webservice_url',
            field=models.URLField(verbose_name='Webservice URL to retrieve remote items', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='regie',
            name='service',
            field=models.CharField(
                max_length=64,
                verbose_name='Payment Service',
                choices=[
                    (b'dummy', 'Dummy (for tests)'),
                    (b'systempayv2', b'systempay (Banque Populaire)'),
                    (b'sips', b'SIPS'),
                    (b'spplus', "SP+ (Caisse d'epargne)"),
                    (b'ogone', 'Ingenico (formerly Ogone)'),
                    (b'paybox', 'Paybox'),
                ],
            ),
            preserve_default=True,
        ),
    ]
