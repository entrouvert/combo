from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0019_auto_20160531_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='bank_transaction_id',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
