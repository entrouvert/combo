from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0024_selfdeclaredinvoicepayment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basketitem',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
    ]
