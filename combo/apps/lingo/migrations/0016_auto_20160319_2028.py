from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0015_auto_20160309_2150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activeitems',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='itemshistory',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
