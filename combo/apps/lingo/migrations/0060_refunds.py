import django.db.models.deletion
from django.db import migrations, models

import combo.apps.lingo.models
import combo.data.fields


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('data', '0068_increase_extra_css_class9'),
        ('lingo', '0059_pagination'),
    ]

    operations = [
        migrations.CreateModel(
            name='RefundsCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=500, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                (
                    'template_name',
                    models.CharField(blank=True, max_length=50, null=True, verbose_name='Cell Template'),
                ),
                (
                    'condition',
                    models.CharField(
                        blank=True, max_length=1000, null=True, verbose_name='Display condition'
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('regie', models.CharField(blank=True, max_length=50, verbose_name='Regie')),
                ('title', models.CharField(blank=True, max_length=200, verbose_name='Title')),
                ('text', combo.data.fields.RichTextField(blank=True, null=True, verbose_name='Text')),
                ('hide_if_empty', models.BooleanField(default=False, verbose_name='Hide if no refunds')),
                (
                    'payer_external_id_template',
                    models.CharField(
                        blank=True,
                        help_text='The computed value will be transmitted to the billing system. It can also be left blank.',
                        max_length=1000,
                        verbose_name='Payer external id (template)',
                    ),
                ),
                (
                    'limit',
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name='Number of refunds per page (default 10)'
                    ),
                ),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.page')),
            ],
            options={
                'verbose_name': 'Refunds cell',
            },
            bases=(
                combo.apps.lingo.models.RegieElementsMixin,
                combo.apps.lingo.models.LingoElementsMixin,
                models.Model,
            ),
        ),
    ]
