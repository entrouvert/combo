from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lingo', '0013_transaction_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regie',
            name='service',
            field=models.CharField(
                max_length=64,
                verbose_name='Payment Service',
                choices=[
                    (b'dummy', 'Dummy (for tests)'),
                    (b'systempayv2', b'systempay (Banque Populaire)'),
                    (b'sips', 'SIPS (Atos, France)'),
                    (b'sips2', 'SIPS (Atos, other countries)'),
                    (b'spplus', "SP+ (Caisse d'epargne)"),
                    (b'ogone', 'Ingenico (formerly Ogone)'),
                    (b'paybox', 'Paybox'),
                    (b'payzen', 'PayZen'),
                    (b'tipi', 'TIPI'),
                ],
            ),
            preserve_default=True,
        ),
    ]
