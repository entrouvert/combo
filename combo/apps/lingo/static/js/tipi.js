$(function() {
    var tipi_popup;
    var tipi_timer;
    function reinit() {
        $('#refdet_error').hide();
        $('#montant_error').hide();
        $('#mel_error').hide();
    }

    function checkpopup() {
        // checks if TIPI is still open and disable form widgets
        if (tipi_popup.closed) {
            $('#tipi_form input, #tipi_form select, #tipi_form button').attr('disabled', false);
            document.forms['tipi_form'].reset();
            clearInterval(tipi_timer);
        }
    }

    function refdet_valid_value(element) {
        var value = element.val().trim();
        var re = new RegExp(element.attr('pattern'));
        if (!value || !value.match(re)) {
            $('#refdet_error').show();
            return false;
        }
        return true;
    }

    function fill_padding(element) {
        // if element is shorter than required length it should be padded with zeros
        var padding = '';
        var value = element.val().trim();
        for (var i=0; i<element.attr('maxLength')-value.length; i++) {
            padding += '0';
        }
    return padding + value;
    }

    $("form#tipi_form").on("submit", function() {
        var url = $("form#tipi_form").data("url");
        var saisie = $("form#tipi_form").data("saisie");
        var pesv2 = $("form#tipi_form").data("pesv2") == 'True';
        var params = {'refdet': function(id) {
            var exer = params.exer('exer');
            if (pesv2) {
                // if PESv2 then refdet is composed by exer + idpce + idligne
                var idpce = $('#idpce');
                var idligne = $('#idligne');
                if (refdet_valid_value(idpce) && refdet_valid_value(idligne)) {
                    refdet = fill_padding(idpce) + fill_padding(idligne);
                } else {
                    return;
                }
            } else {
                // if ROLMRE then refdet is composed by exer + rolrec + roldeb + roldet
                var rolrec = $('#rolrec');
                var roldeb = $('#roldeb');
                var roldet = $('#roldet');
                if (refdet_valid_value(rolrec) && refdet_valid_value(roldeb) && refdet_valid_value(roldet)) {
                    refdet = fill_padding(rolrec) + fill_padding(roldeb) + fill_padding(roldet);
                } else {
                    return;
                }
            }

            if (saisie == 'T') {
                // in test mode the refdet should be 999900000000999999
                if (pesv2) {
                    return "999900000000999999";
                } else { // ROLMRE
                    return "999999990000000000000";
                }
            } else {
                return exer + refdet;
            }
        }, 'montant': function(id) {
            var euros = $('#' + id + '_euros').val().trim();
            var cents = $('#' + id + '_cents').val().trim();
            if (euros && cents) {
                if (isNaN(euros) || isNaN(cents)) {
                    $('#' + id + '_error').show();
                    return false;
                }
                return parseInt(euros) * 100 + parseInt(cents);
            }
            $('#' + id + '_error').show();
            return false;
        }, 'mel': function(id) {
            var email = $('#' + id).val().trim();
            if (!email) {
                $('#' + id +'_error').show();
            }
            return email;
        }, 'exer': function(id) {
            if (saisie == 'T') {
                // in test mode exer should be 9999
                return "9999";
            }
            var exer = $('#' + id);
            if (refdet_valid_value(exer)) {
                return exer.val().trim();
            }
            return false;
        }};
        var tipi_url = url + '?saisie=' + saisie + '&numcli=' + $('#numcli').val();
        var url_params = '&';
        reinit();

        for (var field in params) {
            var valid = params[field](field);
            if (!valid) {
                return false;
            } else {
                var field_error = $('#' + field + '_error');
                if (field_error) {
                    field_error.hide();
                }
                url_params += field + '=' + valid + '&';
            }
        }
        var url = tipi_url;
        url = tipi_url + encodeURI(url_params);
        // TIPI popup requires specific params
        tipi_popup = window.open(url, 'tipi', 'height=800, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, directories=no, status=no');
        if (tipi_popup) {
            $('#tipi_form input, #tipi_form select, #tipi_form button').attr('disabled', true);
            tipi_timer = setInterval(checkpopup, 400);
            $('#popup_warning').hide();
        } else {
            $('#popup_warning').show();
        }
        return false;
    });
});
