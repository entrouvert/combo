# lingo - basket and payment system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from combo.urls_utils import decorated_includes, staff_required

from .manager_views import (
    BasketItemErrorListView,
    BasketItemMarkAsNotifiedView,
    PaymentBackendCreateView,
    PaymentBackendDeleteView,
    PaymentBackendListView,
    PaymentBackendUpdateView,
    RegieCreateView,
    RegieDeleteView,
    RegieListView,
    RegieUpdateView,
    TransactionListView,
    download_transactions_csv,
)
from .views import (
    AddBasketItemApiView,
    BasketItemPayView,
    CallbackView,
    CancelItemView,
    CancelTransactionApiView,
    CreditDownloadView,
    ItemDownloadView,
    ItemPaymentsDownloadView,
    ItemView,
    PaymentDownloadView,
    PaymentStatusView,
    PayView,
    RegiesApiView,
    RemoveBasketItemApiView,
    ReturnView,
    SelfInvoiceView,
    TransactionStatusApiView,
    ValidateTransactionApiView,
)

lingo_manager_urls = [
    path('', TransactionListView.as_view(), name='lingo-manager-homepage'),
    path('payments/error/', BasketItemErrorListView.as_view(), name='lingo-manager-payment-error-list'),
    path(
        'item/<int:item_id>/mark-as-notified/',
        BasketItemMarkAsNotifiedView.as_view(),
        name='lingo-manager-basket-item-mark-as-notified',
    ),
    path('transactions/download-csv/', download_transactions_csv, name='lingo-manager-transactions-download'),
    path('regies/', RegieListView.as_view(), name='lingo-manager-regie-list'),
    path('regies/add/', RegieCreateView.as_view(), name='lingo-manager-regie-add'),
    path('regies/<int:pk>/edit', RegieUpdateView.as_view(), name='lingo-manager-regie-edit'),
    path('regies/<int:pk>/delete', RegieDeleteView.as_view(), name='lingo-manager-regie-delete'),
    path('paymentbackends/', PaymentBackendListView.as_view(), name='lingo-manager-paymentbackend-list'),
    path('paymentbackends/add/', PaymentBackendCreateView.as_view(), name='lingo-manager-paymentbackend-add'),
    path(
        'paymentbackends/<int:pk>/edit',
        PaymentBackendUpdateView.as_view(),
        name='lingo-manager-paymentbackend-edit',
    ),
    path(
        'paymentbackends/<int:pk>/delete',
        PaymentBackendDeleteView.as_view(),
        name='lingo-manager-paymentbackend-delete',
    ),
]

urlpatterns = [
    path('api/lingo/regies', RegiesApiView.as_view(), name='api-regies'),
    path('api/lingo/add-basket-item', AddBasketItemApiView.as_view(), name='api-add-basket-item'),
    path('api/lingo/remove-basket-item', RemoveBasketItemApiView.as_view(), name='api-remove-basket-item'),
    path(
        'api/lingo/validate-transaction',
        ValidateTransactionApiView.as_view(),
        name='api-validate-transaction',
    ),
    path('api/lingo/cancel-transaction', CancelTransactionApiView.as_view(), name='api-cancel-transaction'),
    path(
        'api/lingo/transaction-status/<path:transaction_signature>/',
        TransactionStatusApiView.as_view(),
        name='api-transaction-status',
    ),
    path('lingo/pay', PayView.as_view(), name='lingo-pay'),
    path('lingo/cancel/<int:pk>/', CancelItemView.as_view(), name='lingo-cancel-item'),
    path('lingo/callback/<int:regie_pk>/', CallbackView.as_view(), name='lingo-callback'),
    re_path(
        r'^lingo/callback-payment-backend/(?P<payment_backend_pk>[A-Za-z0-9_-]+)/$',
        CallbackView.as_view(),
        name='lingo-callback-payment-backend',
    ),
    path('lingo/return/<int:regie_pk>/', ReturnView.as_view(), name='lingo-return'),
    path(
        'lingo/return-payment-backend/<int:payment_backend_pk>/<path:transaction_signature>/',
        ReturnView.as_view(),
        name='lingo-return-payment-backend',
    ),
    re_path(r'^manage/lingo/', decorated_includes(staff_required, include(lingo_manager_urls))),
    re_path(
        r'^lingo/item/(?P<regie_id>[\w,-]+)/(?P<item_crypto_id>[\w,-]+)/(?P<cell_crypto_reference>[\w,-]+)/pdf$',
        ItemDownloadView.as_view(),
        name='download-item-pdf',
    ),
    re_path(
        r'^lingo/item/(?P<regie_id>[\w,-]+)/(?P<item_crypto_id>[\w,-]+)/(?P<cell_crypto_reference>[\w,-]+)/payments/pdf$',
        ItemPaymentsDownloadView.as_view(),
        name='download-item-payments-pdf',
    ),
    re_path(
        r'^lingo/item/(?P<regie_id>[\w,-]+)/(?P<item_crypto_id>[\w,-]+)/(?P<cell_crypto_reference>[\w,-]+)/$',
        ItemView.as_view(),
        name='view-item',
    ),
    re_path(
        r'^lingo/payment/(?P<regie_id>[\w,-]+)/(?P<payment_crypto_id>[\w,-]+)/(?P<cell_crypto_reference>[\w,-]+)/pdf$',
        PaymentDownloadView.as_view(),
        name='download-payment-pdf',
    ),
    re_path(
        r'^lingo/credit/(?P<regie_id>[\w,-]+)/(?P<credit_crypto_id>[\w,-]+)/(?P<cell_crypto_reference>[\w,-]+)/pdf$',
        CreditDownloadView.as_view(),
        name='download-credit-pdf',
    ),
    path('lingo/item/<path:item_signature>/pay', BasketItemPayView.as_view(), name='basket-item-pay-view'),
    path('lingo/payment-status', PaymentStatusView.as_view(), name='payment-status'),
    path('lingo/self-invoice/<int:cell_id>/', SelfInvoiceView.as_view(), name='lingo-self-invoice'),
]
