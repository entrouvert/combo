# lingo - basket and payment system
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import logging

from django.core.management.base import BaseCommand, CommandError

from combo.apps.lingo.models import LingoException, PaymentBackend

logger = logging.getLogger('combo.apps.lingo')


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--backend', default=None, help='slug of the backend')

        parser.add_argument('--all-backends', default=False, action='store_true', help='target all backends')

        parser.add_argument(
            '--noinput',
            '--no-input',
            action='store_false',
            dest='interactive',
            help='Tells Django to NOT prompt the user for input of any kind.',
        )

        parser.add_argument(
            '--max-age-in-days', default=3, type=int, help='max age of the transaction in days'
        )

    def handle(
        self,
        *args,
        backend=None,
        all_backends=False,
        max_age_in_days=None,
        interactive=True,
        verbosity=1,
        **options,
    ):
        qs = PaymentBackend.objects.all()
        if backend and all_backends:
            raise CommandError('--backend and --all-backends cannot be used together')
        if backend:
            try:
                backend = qs.get(slug=backend)
            except PaymentBackend.DoesNotExist:
                raise CommandError('no backend with slug "%s".' % backend)
            else:
                if not backend.can_poll_backend():
                    raise CommandError('backend "%s" cannot be polled.' % backend)
                backends = [backend]
        else:
            backends = [backend for backend in qs if backend.can_poll_backend()]
            if not all_backends:
                if backends and interactive:
                    print('Choose backend by slug:')
                    while True:
                        for backend in backends:
                            print(' - %s: %s' % (backend.slug, backend))
                        print('> ', end=' ')
                        slug = input().strip()
                        if not slug:
                            continue
                        filtered_backends = qs.filter(slug__icontains=slug)
                        if filtered_backends:
                            backends = filtered_backends
                            break
                else:
                    return

        for backend in backends:
            if verbosity >= 1:
                print('Polling backend', backend, '... ', end='')
            try:
                backend.poll_backend(max_age=max_age_in_days and datetime.timedelta(days=max_age_in_days))
            except LingoException:
                logger.exception('polling failed')
                if interactive:
                    # show error
                    raise
            if verbosity >= 1:
                print('DONE')
