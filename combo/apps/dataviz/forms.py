# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dataclasses
import datetime
from collections import OrderedDict, defaultdict

from django import forms
from django.conf import settings
from django.core.cache import cache
from django.db import transaction
from django.db.models import Q
from django.db.models.fields import BLANK_CHOICE_DASH
from django.template import Context, Template, TemplateSyntaxError, VariableDoesNotExist
from django.utils.translation import gettext_lazy as _

from combo.utils import cache_during_request, requests, spooler

from .fields import StaticField
from .models import ChartCell, ChartFiltersCell, ChartNgCell
from .widgets import MultipleSelect2Widget, Select2Widget


@dataclasses.dataclass
class Choice:
    id: str
    label: str
    group: str = None

    @staticmethod
    def get_field_choices(choices):
        choices_by_group = defaultdict(list)
        for choice in choices:
            choices_by_group[choice.group].append((choice.id, choice.label))

        return list(choices_by_group.items())


class ChartForm(forms.ModelForm):
    class Meta:
        model = ChartCell
        fields = ('title', 'url')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        available_charts = []
        for site_dict in (settings.KNOWN_SERVICES.get('bijoe') or {}).values():
            result = requests.get(
                '/visualization/json/',
                remote_service=site_dict,
                without_user=True,
                headers={'accept': 'application/json'},
            ).json()
            available_charts.extend([(x['path'], x['name']) for x in result])
        available_charts.sort(key=lambda x: x[1])
        self.fields['url'].widget = forms.Select(choices=available_charts)


@cache_during_request
def trigger_statistics_list_refresh():
    transaction.on_commit(spooler.refresh_statistics_list)


class ChartFiltersMixin:
    ajax_choices = True
    time_intervals = (
        Choice('week', _('Week')),
        Choice('month', _('Month')),
        Choice('year', _('Year')),
        Choice('weekday', _('Week day')),
    )

    def get_filter_fields(self, cell):
        fields = OrderedDict()
        for filter_ in cell.available_filters:
            filter_id = filter_['id']
            if filter_.get('deprecated') and (
                filter_id not in cell.filter_params
                or cell.filter_params.get(filter_id) == filter_.get('default')
            ):
                continue

            initial = cell.filter_params.get(filter_id, filter_.get('default'))
            required = filter_.get('required', False)

            if required and {x['id'] for x in filter_['options'] if isinstance(x, dict)} == {'true', 'false'}:
                field = self.build_boolean_field(cell, filter_, initial)
            else:
                field = self.build_choice_field(cell, filter_, initial)

            fields[filter_id] = field
            if filter_.get('deprecated'):
                fields[filter_id].label += ' (%s)' % _('deprecated')
                fields[filter_id].help_text = filter_.get('deprecation_hint')
            fields[filter_id].is_filter_field = True

        return fields

    @classmethod
    def get_filter_options(cls, cell, filter_, initial):
        filter_id = filter_['id']

        filter_options = filter_['options']
        if not filter_options:
            return []

        if not isinstance(filter_options[0], list):
            # no option groups, add empty one for consistency
            filter_options = [(None, filter_options)]
        choices = [
            Choice(id=opt['id'], label=opt['label'], group=group)
            for group, options in filter_options
            for opt in options
        ]

        if filter_id == 'time_interval':
            cls.extend_time_interval_choices(choices)

        required = filter_.get('required', False)
        multiple = filter_.get('multiple')
        if not required and not multiple:
            choices.insert(0, Choice(*BLANK_CHOICE_DASH[0]))

        extra_variables = cell.page.get_extra_variables_keys()
        variable_choices = [
            Choice(id='variable:' + key, label=key, group=_('Page variables')) for key in extra_variables
        ]

        for choice in initial if isinstance(initial, list) else [initial]:
            if not choice:
                continue
            if choice.startswith('variable:'):
                variable = choice.replace('variable:', '')
                if not variable in extra_variables:
                    variable_choices.append(
                        Choice(id=choice, label=_('%s (unavailable)') % variable, group=_('Page variables'))
                    )
            elif not any(x.id == choice for x in choices):
                choices.append(Choice(id=choice, label=_('%s (unavailable)') % choice))

        if variable_choices and not multiple and filter_id != 'time_interval':
            choices.extend(variable_choices)

        return choices

    def build_choice_field(self, cell, filter_, initial):
        multiple = filter_.get('multiple')
        required = filter_.get('required', False)
        choices = self.get_filter_options(cell, filter_, initial)

        widget_class = MultipleSelect2Widget if multiple else Select2Widget
        widget = widget_class(cell, filter_['id'], choices, initial, self.ajax_choices)

        field_class = forms.MultipleChoiceField if multiple else forms.ChoiceField
        field = field_class(
            label=filter_['label'],
            choices=Choice.get_field_choices(choices),
            required=required,
            initial=initial,
        )
        field.widget = widget
        field.dataviz_choices = choices
        return field

    def build_boolean_field(self, cell, filter_, initial):
        return forms.BooleanField(
            label=filter_['label'],
            required=False,
            initial=bool(initial == 'true'),
        )

    @classmethod
    def extend_time_interval_choices(cls, choices):
        if any(choice.id == 'day' for choice in choices):
            for choice in cls.time_intervals:
                if choice not in choices:
                    choices.append(choice)

    def update_time_range_choices(self, statistic, exclude_template_choice=False):
        choices = self.fields['time_range'].choices

        if not statistic.has_future_data:
            choices = [choice for choice in choices if not choice[0].startswith('next')]

        if exclude_template_choice:
            choices = [choice for choice in choices if choice[0] != 'range-template']

        self.fields['time_range'].choices = choices

    def clean(self):
        for field, value in self.cleaned_data.items():
            if hasattr(self.fields[field], 'is_filter_field') and isinstance(value, bool):
                self.cleaned_data[field] = 'true' if value is True else 'false'


class ChartNgForm(ChartFiltersMixin, forms.ModelForm):
    class Meta:
        model = ChartNgCell
        fields = (
            'statistic',
            'time_range',
            'time_range_start',
            'time_range_end',
            'time_range_start_template',
            'time_range_end_template',
            'chart_type',
            'display_total',
            'display_line_numbers',
            'height',
            'sort_order',
            'hide_null_values',
            'print_values',
        )
        widgets = {
            'time_range_start': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'time_range_end': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
        }

    def __init__(self, *args, **kwargs):
        trigger_statistics_list_refresh()
        super().__init__(*args, **kwargs)

        stat_field = self.fields['statistic']
        if not self.instance.statistic:
            stat_field.queryset = stat_field.queryset.filter(available=True)
        else:
            # display current statistic in choices even if unavailable
            stat_field.queryset = stat_field.queryset.filter(
                Q(available=True) | Q(pk=self.instance.statistic.pk)
            )
            self.add_filter_fields()
            self.update_time_range_choices(self.instance.statistic)

        if not self.instance.statistic or self.instance.statistic.service_slug == 'bijoe':
            for field in (
                'time_range',
                'time_range_start',
                'time_range_end',
                'time_range_start_template',
                'time_range_end_template',
                'display_total',
                'display_line_numbers',
            ):
                del self.fields[field]
        else:
            if self.instance.time_range != 'range':
                del self.fields['time_range_start']
                del self.fields['time_range_end']

            if self.instance.time_range != 'range-template':
                del self.fields['time_range_start_template']
                del self.fields['time_range_end_template']

            if not self.instance.is_table_chart() or self.instance.statistic.data_type:
                del self.fields['display_total']

            if not self.instance.is_table_chart():
                del self.fields['display_line_numbers']

    def add_filter_fields(self):
        new_fields = OrderedDict()
        for field_name, field in self.fields.items():
            new_fields[field_name] = field
            if field_name == 'statistic':
                # insert filter fields after statistic field
                new_fields.update(self.get_filter_fields(self.instance))
        self.fields = new_fields

    def save(self, *args, **kwargs):
        if 'statistic' in self.changed_data:
            self.instance.filter_params.clear()
            self.instance.time_range = ''
            self.instance.subfilters.clear()
            for filter_ in self.instance.available_filters:
                if 'default' in filter_:
                    self.instance.filter_params[filter_['id']] = filter_['default']
                elif filter_.get('required'):
                    options = (
                        filter_['options'][0][1]
                        if isinstance(filter_['options'][0], list)
                        else filter_['options']
                    )
                    self.instance.filter_params[filter_['id']] = options[0]['id']
        else:
            for filter_ in self.instance.available_filters:
                if filter_['id'] in self.cleaned_data:
                    self.instance.filter_params[filter_['id']] = self.cleaned_data[filter_['id']]

        cell = super().save(*args, **kwargs)

        for filter_ in cell.available_filters:
            if filter_.get('has_subfilters') and filter_['id'] in self.changed_data:
                cell.update_subfilters()
                self.fields = OrderedDict(
                    (name, field)
                    for name, field in self.fields.items()
                    if not hasattr(field, 'is_filter_field')
                )
                self.add_filter_fields()
                break

        return cell

    def clean(self):
        super().clean()

        for template_field in ('time_range_start_template', 'time_range_end_template'):
            if not self.cleaned_data.get(template_field):
                continue
            context = {'now': datetime.datetime.now, 'today': datetime.datetime.now}
            try:
                Template('{{ %s|date:"Y-m-d" }}' % self.cleaned_data[template_field]).render(Context(context))
            except (VariableDoesNotExist, TemplateSyntaxError) as e:
                self.add_error(template_field, e)


class ChartNgPartialForm(ChartFiltersMixin, forms.ModelForm):
    overridden_filters = forms.CharField()

    prefix = 'filter'

    class Meta:
        model = ChartNgCell
        fields = (
            'time_range',
            'time_range_start',
            'time_range_end',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.update(self.get_filter_fields(self.instance))
        for field in self.fields.values():
            field.required = False

    def clean(self):
        super().clean()

        for field in self._meta.fields:
            if field not in self.cleaned_data['overridden_filters']:
                self.cleaned_data[field] = self.initial[field]

        for filter_ in self.instance.available_filters:
            if filter_['id'] in self.cleaned_data['overridden_filters']:
                self.instance.filter_params[filter_['id']] = self.cleaned_data.get(filter_['id'])

    def clean_overridden_filters(self):
        if not self.cleaned_data['overridden_filters']:
            return []

        return self.cleaned_data['overridden_filters'].split(',')


class ChartFiltersForm(ChartFiltersMixin, forms.ModelForm):
    ajax_choices = False
    overridden_filters = StaticField()

    prefix = 'filter'

    class Meta:
        model = ChartNgCell
        fields = (
            'time_range',
            'time_range_start',
            'time_range_end',
        )
        widgets = {
            'time_range_start': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'time_range_end': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
        }

    def __init__(self, *args, **kwargs):
        page = kwargs.pop('page')
        filters_cell = kwargs.pop('filters_cell')
        filters_cell_id = kwargs.pop('filters_cell_id', None)
        super().__init__(*args, **kwargs)

        chart_cells = []
        for cell in ChartNgCell.objects.filter(page=page, statistic__isnull=False).order_by('order'):
            cell.page = page  # use cached placeholders
            if cell.is_placeholder_active(traverse_cells=False):
                chart_cells.append(cell)

        if not chart_cells:
            self.fields.clear()
            return

        if filters_cell_id:
            for cell in chart_cells:
                cell.subfilters = cache.get(cell.get_cache_key(filters_cell_id), cell.subfilters)

        first_cell = chart_cells[0]
        for field in self._meta.fields:
            self.fields[field].initial = getattr(first_cell, field)
        dynamic_fields = self.get_filter_fields(first_cell)
        dynamic_fields_values = {k: v for k, v in first_cell.filter_params.items()}

        if first_cell.time_range == 'range-template':
            for field in self._meta.fields:
                self.fields.pop(field, None)
        else:
            self.update_time_range_choices(first_cell.statistic, exclude_template_choice=True)
            if not self.is_bound and first_cell.time_range != 'range':
                del self.fields['time_range_start']
                del self.fields['time_range_end']

        for cell in chart_cells[1:]:
            cell_filter_fields = self.get_filter_fields(cell)

            # keep only common fields
            dynamic_fields = {k: v for k, v in dynamic_fields.items() if k in cell_filter_fields}

            # keep only same value fields
            for field, value in cell.filter_params.items():
                if field in dynamic_fields and value != dynamic_fields_values.get(field, ''):
                    del dynamic_fields[field]

            if cell.time_range != first_cell.time_range:
                for field in self._meta.fields:
                    self.fields.pop(field, None)

            # ensure compatible choices lists
            for field_name, field in cell_filter_fields.items():
                if field_name not in dynamic_fields or isinstance(field, forms.BooleanField):
                    continue

                dynamic_fields[field_name].dataviz_choices = [
                    x for x in dynamic_fields[field_name].dataviz_choices if x in field.dataviz_choices
                ]
                dynamic_fields[field_name].choices = Choice.get_field_choices(
                    dynamic_fields[field_name].dataviz_choices
                )

                if dynamic_fields[field_name].choices == []:
                    del dynamic_fields[field_name]

            if 'time_range' in self.fields:
                self.update_time_range_choices(cell.statistic)

        self.update_backoffice_filter_choices(filters_cell, dynamic_fields)
        dynamic_fields = {
            name: field for name, field in dynamic_fields.items() if filters_cell.filters[name]['enabled']
        }
        self.fields.update(dynamic_fields)
        self.fields['overridden_filters'].initial = ','.join(self.fields)

    @staticmethod
    def update_backoffice_filter_choices(filters_cell, dynamic_fields):
        old_filters = filters_cell.filters.copy()

        # remove absent filters from cell configuration, except if it was disabled
        filters_cell.filters = {
            k: v for k, v in filters_cell.filters.items() if k in dynamic_fields or not v['enabled']
        }

        # add filters to cell configuration
        for field_name, field in dynamic_fields.items():
            if not field_name in filters_cell.filters:
                filters_cell.filters[field_name] = {'label': str(field.label), 'enabled': True}
                continue

        if filters_cell.filters != old_filters:
            filters_cell.save(force_update=True)

    def clean(self):
        super().clean()
        if self.cleaned_data.get('time_range') != 'range':
            self.fields.pop('time_range_start', None)
            self.fields.pop('time_range_end', None)


class ChartFiltersConfigForm(forms.ModelForm):
    filters = forms.MultipleChoiceField(
        label=_('Filters'), widget=forms.CheckboxSelectMultiple, required=False
    )

    class Meta:
        model = ChartFiltersCell
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.filters:
            del self.fields['filters']
            return

        self.initial['filters'] = []
        self.fields['filters'].choices = []
        for filter_id, config in self.instance.filters.items():
            self.fields['filters'].choices.append((filter_id, config['label']))

            if config['enabled']:
                self.initial['filters'].append(filter_id)

    def save(self, *args, **kwargs):
        for filter_id in self.instance.filters:
            self.instance.filters[filter_id]['enabled'] = bool(filter_id in self.cleaned_data['filters'])
        return super().save(*args, **kwargs)


class ChartNgExportForm(forms.Form):
    export_format = forms.ChoiceField(
        label=_('Format'),
        choices=(
            ('svg', _('Picture (SVG)')),
            ('ods', _('Table (ODS)')),
        ),
    )
