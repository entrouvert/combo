$(function() {
  $(document).on('combo:cell-loaded', function(ev, cell) {
    init_cube_aggregate($(cell).find('.combo-cube-aggregate'));
  });

  var Colors = {};

  Colors.spaced_hsla = function (i, n, s, l, a) {
    var h = 360 * i/n;
    return "hsla(" + h.toString() + ', ' + s.toString() + '%, ' + l.toString() + '%, ' + a.toString() + ')';
  }

  $('.combo-cube-aggregate').each(function(idx, elem) {
    init_cube_aggregate(elem);
  });

  function init_cube_aggregate(elem) {
     if ($('canvas', elem).length == 0) return;
     var aggregate_url = $(elem).data('aggregate-url');
     var model = null;
     var ctx = $('canvas', elem)[0].getContext("2d");
     var id = $(elem).data('combo-cube-aggregate-id');

     var option = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero : true,

        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines : true,

        //String - Colour of the grid lines
        scaleGridLineColor : "rgba(0,0,0,.05)",

        //Number - Width of the grid lines
        scaleGridLineWidth : 1,

        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,

        //Boolean - If there is a stroke on each bar
        barShowStroke : true,

        //Number - Pixel width of the bar stroke
        barStrokeWidth : 2,

        //Number - Spacing between each of the X value sets
        barValueSpacing : 5,

        //Number - Spacing between data sets within X values
        barDatasetSpacing : 1,

        //String - A legend template
        responsive: true,
        responsiveMinHeight: 300,
        legend: true,
        yAxisMinimumInterval: 10,
        inGraphDataShow: true,
     }
     if ($(this).data('x-label')) {
         option['xAxisLabel'] = $(this).data('x-label');
     }
     if ($(this).data('y-label')) {
         option['yAxisLabel'] = $(this).data('y-label');
     }
     var data = window['combo_cube_aggregate_' + id];
     // Set one color by dataset
     var n = data.datasets.length;
     for (var i = 0; i < n; i++) {
       var dataset = data.datasets[i];
       $.extend(dataset, {
         fillColor: Colors.spaced_hsla(i, n, 100, 30, 0.5),
         strokeColor: Colors.spaced_hsla(i, n, 100, 30, 0.75),
         highlightFill: Colors.spaced_hsla(i, n, 100, 30, 0.75),
         highlightStroke: Colors.spaced_hsla(i, n, 100, 30, 1)
       })
     }
     var clone = function(obj){
       var objClone = {};
       for (var key in obj) {
         if (obj.hasOwnProperty(key)) objClone[key] = obj[key];
       };
       return objClone;
     }
     if (data.datasets.length == 1) {
       // Set one color by bar 
       var dataset = data.datasets[0];
       var n = dataset.data.length;
       $.extend(dataset, {
         fillColor: [],
         strokeColor: [],
         highlightFill: [],
         highlightStroke: [],
       })
       for (var i = 0; i < n; i++) {
         dataset["fillColor"].push(Colors.spaced_hsla(i, n, 100, 30, 0.5));
         dataset["strokeColor"].push(Colors.spaced_hsla(i, n, 100, 30, 0.75));
         dataset["highlightFill"].push(Colors.spaced_hsla(i, n, 100, 30, 0.75));
         dataset["highlightStroke"].push(Colors.spaced_hsla(i, n, 100, 30, 1));
       }
     }
     new Chart(ctx).Bar(data, option);
  }
})
