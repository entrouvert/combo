function get_graph_querystring(extra_context, width=undefined) {
  qs = [];
  if ($('#chart-filters').length) {
    qs.push($('#chart-filters').serialize());
    qs.push('filters_cell_id=' + $('body').data('filters-cell-id'));
  }
  if (extra_context)
      qs.push('ctx=' + extra_context);
  if (window.location.search)
      qs.push(window.location.search.slice(1));
  if (width)
    qs.push('width=' + width);
  return '?' + qs.join('&');
};
