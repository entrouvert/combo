$(function() {
  var opts = {
    lines: 12, // The number of lines to draw
    angle: 0.15, // The length of each line
    lineWidth: 0.44, // The line thickness
    pointer: {
      length: 0.9, // The radius of the inner circle
      strokeWidth: 0.035, // The rotation offset
      color: '#000000' // Fill color
    },
    limitMax: 'false',   // If true, the pointer will not go past the end of the gauge
    colorStart: '#6FADCF',   // Colors
    colorStop: '#8FC0DA',    // just experiment with them
    strokeColor: '#E0E0E0',   // to see which ones work best for you
    percentColors: [[0.0, "#a9d70b" ], [0.8, "#f9c802"], [1.0, "#ff0000"]],
    generateGradient: true
  };
  $('[data-combo-gauge]').each(function(idx, elem) {
    var target = $(elem).find('canvas')[0];
    var gauge = new Gauge(target).setOptions(opts);
    gauge.maxValue = parseInt($(elem).data('gauge-max-value'))
    gauge.set(0); // set actual value
    var ajax_params = {};
    if ($(elem).data('gauge-count-url')) {
      ajax_params.dataType = 'json';
      ajax_params.url = $(elem).data('gauge-count-url');
    } else {
      ajax_params.dataType = 'jsonp';
      ajax_params.url = $(elem).data('gauge-count-jsonp-url');
      ajax_params.xhrFields = { withCredentials: true };
      ajax_params.crossDomain = true;
    }
    ajax_params.success = function(data) {
      if (data.count > gauge.maxValue) {
        gauge.animationSpeed = 16;
        gauge.set(gauge.maxValue);
      } else {
        gauge.set(data.count);
      }
      var counter_value = $('<span class="counter">' + data.count + '</span>').appendTo(elem);
    }
    $.ajax(ajax_params);
  });
});
