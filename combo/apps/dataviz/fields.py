from django import forms


class StaticField(forms.Field):
    widget = forms.HiddenInput

    def bound_data(self, data, initial):
        return initial
