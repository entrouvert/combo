from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gauge',
            name='jsonp_data_source',
            field=models.BooleanField(default=True, verbose_name='Use JSONP to get data'),
            preserve_default=True,
        ),
    ]
