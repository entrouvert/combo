import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0006_chartcell'),
    ]

    operations = [
        migrations.AddField(
            model_name='chartcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cubesbarchart',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cubestable',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gauge',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
    ]
