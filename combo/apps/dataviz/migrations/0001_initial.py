from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0012_auto_20151029_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gauge',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('title', models.CharField(max_length=150, null=True, verbose_name='Title', blank=True)),
                ('url', models.CharField(max_length=150, null=True, verbose_name='URL', blank=True)),
                (
                    'data_source',
                    models.CharField(max_length=150, null=True, verbose_name='Data Source', blank=True),
                ),
                ('max_value', models.PositiveIntegerField(null=True, verbose_name='Max Value', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Gauge',
            },
            bases=(models.Model,),
        ),
    ]
