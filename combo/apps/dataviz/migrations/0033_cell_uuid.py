from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0032_chartngcell_display_line_numbers'),
    ]

    operations = [
        migrations.AddField(
            model_name='chartcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='chartcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='chartfilterscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='chartfilterscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='chartngcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='chartngcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='gauge',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='gauge',
            unique_together={('page', 'uuid')},
        ),
    ]
