from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0012_auto_20151029_1535'),
        ('dataviz', '0002_gauge_jsonp_data_source'),
    ]

    operations = [
        migrations.CreateModel(
            name='CubesBarChart',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('title', models.CharField(max_length=150, null=True, verbose_name='Title', blank=True)),
                ('url', models.URLField(max_length=150, null=True, verbose_name='URL', blank=True)),
                ('cube', models.CharField(max_length=256, null=True, verbose_name='Cube', blank=True)),
                (
                    'aggregate1',
                    models.CharField(max_length=64, null=True, verbose_name='Aggregate', blank=True),
                ),
                (
                    'drilldown1',
                    models.CharField(max_length=64, null=True, verbose_name='Drilldown 1', blank=True),
                ),
                (
                    'drilldown2',
                    models.CharField(max_length=64, null=True, verbose_name='Drilldown 2', blank=True),
                ),
                (
                    'other_parameters',
                    models.TextField(null=True, verbose_name='Other parameters', blank=True),
                ),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Cubes Barchart',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CubesTable',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('title', models.CharField(max_length=150, null=True, verbose_name='Title', blank=True)),
                ('url', models.URLField(max_length=150, null=True, verbose_name='URL', blank=True)),
                ('cube', models.CharField(max_length=256, null=True, verbose_name='Cube', blank=True)),
                (
                    'aggregate1',
                    models.CharField(max_length=64, null=True, verbose_name='Aggregate', blank=True),
                ),
                (
                    'drilldown1',
                    models.CharField(max_length=64, null=True, verbose_name='Drilldown 1', blank=True),
                ),
                (
                    'drilldown2',
                    models.CharField(max_length=64, null=True, verbose_name='Drilldown 2', blank=True),
                ),
                (
                    'other_parameters',
                    models.TextField(null=True, verbose_name='Other parameters', blank=True),
                ),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Cubes Table',
            },
            bases=(models.Model,),
        ),
    ]
