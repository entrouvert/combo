from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0020_auto_20160928_1152'),
        ('dataviz', '0005_auto_20160928_1152'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChartCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                (
                    'extra_css_class',
                    models.CharField(
                        max_length=100, verbose_name='Extra classes for CSS styling', blank=True
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('title', models.CharField(max_length=150, null=True, verbose_name='Title', blank=True)),
                ('url', models.URLField(max_length=150, null=True, verbose_name='URL', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Chart',
            },
        ),
    ]
