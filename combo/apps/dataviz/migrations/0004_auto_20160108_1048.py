from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0003_cubesbarchart_cubestable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cubesbarchart',
            name='cube',
            field=models.CharField(max_length=256, null=True, verbose_name='Form', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cubesbarchart',
            name='drilldown1',
            field=models.CharField(max_length=64, null=True, verbose_name='Criterion 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cubesbarchart',
            name='drilldown2',
            field=models.CharField(max_length=64, null=True, verbose_name='Criterion 2', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cubestable',
            name='cube',
            field=models.CharField(max_length=256, null=True, verbose_name='Form', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cubestable',
            name='drilldown1',
            field=models.CharField(max_length=64, null=True, verbose_name='Criterion 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cubestable',
            name='drilldown2',
            field=models.CharField(max_length=64, null=True, verbose_name='Criterion 2', blank=True),
            preserve_default=True,
        ),
    ]
