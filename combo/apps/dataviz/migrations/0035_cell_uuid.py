from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0034_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chartcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='chartfilterscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='chartngcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='gauge',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
