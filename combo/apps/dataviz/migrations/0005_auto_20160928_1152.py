from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0004_auto_20160108_1048'),
    ]

    operations = [
        migrations.AddField(
            model_name='cubesbarchart',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='cubestable',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='gauge',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
