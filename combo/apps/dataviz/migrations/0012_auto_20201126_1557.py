# Generated by Django 1.11.29 on 2020-11-26 14:57

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0011_auto_20200813_1100'),
    ]

    operations = [
        migrations.CreateModel(
            name='Statistic',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('slug', models.SlugField(max_length=256, verbose_name='Slug')),
                ('label', models.CharField(max_length=256, verbose_name='Label')),
                ('site_slug', models.SlugField(max_length=256, verbose_name='Site slug')),
                ('service_slug', models.SlugField(max_length=256, verbose_name='Service slug')),
                ('site_title', models.CharField(max_length=256, verbose_name='Site title')),
                ('url', models.URLField(verbose_name='Data URL')),
                ('last_update', models.DateTimeField(auto_now=True, null=True, verbose_name='Last update')),
                ('available', models.BooleanField(default=True, verbose_name='Available data')),
            ],
            options={
                'ordering': ['-available', 'site_title', 'label'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='statistic',
            unique_together={('slug', 'site_slug', 'service_slug')},
        ),
        migrations.AddField(
            model_name='chartngcell',
            name='statistic',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name='cells',
                to='dataviz.Statistic',
                verbose_name='Data',
                help_text='This list may take a few seconds to be updated, please refresh the page if an item is missing.',
            ),
        ),
    ]
