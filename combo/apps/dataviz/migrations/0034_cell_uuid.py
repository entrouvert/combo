import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in ['Gauge', 'ChartCell', 'ChartNgCell', 'ChartFiltersCell']:
        klass = apps.get_model('dataviz', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0033_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
