from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dataviz', '0023_statistic_has_future_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='chartcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='chartfilterscell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='chartngcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='gauge',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
