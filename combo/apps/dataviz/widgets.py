# combo - content management system
# Copyright (C) 2014-2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.forms.widgets import Select, SelectMultiple
from django.urls import reverse


class Select2WidgetMixin:
    class Media:
        js = 'xstatic/select2.min.js'
        css = {'all': ('xstatic/select2.min.css',)}

    def __init__(self, cell, filter_id, choices, initial, ajax_choices):
        from .forms import Choice

        attrs = {}
        if self.enable_select2(choices):
            attrs['data-combo-autocomplete'] = 'true'
            attrs['lang'] = settings.LANGUAGE_CODE
            if ajax_choices:
                attrs['data-select2-url'] = reverse(
                    'combo-dataviz-choices', kwargs={'cell_id': cell.pk, 'filter_id': filter_id}
                )
                choices = self.filter_choices(choices, initial)

        super().__init__(choices=Choice.get_field_choices(choices), attrs=attrs)

    def enable_select2(self, choices):
        return True


class Select2Widget(Select2WidgetMixin, Select):
    min_choices = 20

    @staticmethod
    def filter_choices(choices, initial):
        return [x for x in choices if x.id == initial]

    def enable_select2(self, choices):
        return bool(len(choices) > self.min_choices)


class MultipleSelect2Widget(Select2WidgetMixin, SelectMultiple):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.attrs['multiple'] = 'multiple'

    @staticmethod
    def filter_choices(choices, initial):
        return [x for x in choices if x.id in (initial or [])]
