import pygal
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _


class VariationBadge(pygal.Bar):
    def render(self):
        self.setup()
        if len(self.x_labels) == 0:
            return '<p>%s</p>' % _('No data')
        elif len(self.x_labels) == 1:
            first_label = last_label = self.x_labels[0]
            first_value = last_value = self.all_series[0].values[0] or 0
        else:
            first_label, last_label = self.x_labels[0], self.x_labels[-1]
            first_value = self.all_series[0].values[0] or 0
            last_value = self.all_series[0].values[-1] or 0

        if first_value == 0:
            if last_value == 0:
                variation_percent = 0
            else:
                variation_percent = '+∞'
        else:
            variation_percent = round((last_value - first_value) / first_value * 100)
            if variation_percent > 0:
                variation_percent = '+%s' % variation_percent

        self.teardown()
        context = {
            'from': first_label,
            'to': last_label,
            'variation_percent': variation_percent,
        }
        return render_to_string('combo/graph/variation-badge.html', context)
