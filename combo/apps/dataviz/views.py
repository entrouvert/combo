# combo - content management system
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import io
import unicodedata

import pyexcel_ods
from django.core import signing
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http import FileResponse, Http404, HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, reverse
from django.template import TemplateSyntaxError, VariableDoesNotExist
from django.utils.translation import gettext_lazy as _
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.generic import DetailView, FormView
from django.views.generic.detail import SingleObjectMixin
from pygal import formatters
from requests.exceptions import HTTPError

from combo.utils import NothingInCacheException, get_templated_url, requests

from .forms import ChartFiltersMixin, ChartNgExportForm, ChartNgPartialForm, Choice
from .models import ChartNgCell, Gauge, MissingVariable, UnsupportedDataSet


def ajax_gauge_count(request, *args, **kwargs):
    gauge = Gauge.objects.get(id=kwargs['cell'])
    response = requests.get(get_templated_url(gauge.data_source))
    return HttpResponse(response.content, content_type='text/json')


class DatavizGraphView(DetailView):
    model = ChartNgCell
    pk_url_kwarg = 'cell'

    def dispatch(self, request, *args, **kwargs):
        self.cell = self.get_object()
        self.filters_cell_id = request.GET.get('filters_cell_id')
        self.include_subfilters = bool(request.GET.get('include-subfilters') or self.filters_cell_id)

        if not self.cell.page.is_visible(request.user):
            raise PermissionDenied()
        if not self.cell.is_visible(request):
            raise PermissionDenied()
        if not self.cell.statistic or not self.cell.statistic.url:
            raise Http404('misconfigured cell')

        if self.filters_cell_id:
            self.cell.subfilters = cache.get(
                self.cell.get_cache_key(self.filters_cell_id), self.cell.subfilters
            )
        return super().dispatch(request, *args, **kwargs)

    def get_dimension_param(self, param):
        if not self.request.GET.get(param):
            return None

        try:
            return int(self.request.GET[param])
        except ValueError:
            return None

    def get(self, request, *args, **kwargs):
        form = ChartNgPartialForm(request.GET, instance=self.cell)
        if not form.is_valid():
            return self.error(_('Wrong parameters.'))

        if request.GET.get('ctx'):
            try:
                request.extra_context = signing.loads(request.GET['ctx'])
            except signing.BadSignature:
                return HttpResponseBadRequest('bad signature', content_type='text/plain')

        form.instance._request = request
        try:
            chart = form.instance.get_chart(
                width=self.get_dimension_param('width'),
                height=self.get_dimension_param('height'),
                include_subfilters=self.include_subfilters,
            )
        except UnsupportedDataSet:
            return self.error(_('Unsupported dataset.'))
        except MissingVariable:
            return self.error(_('Page variable not found.'))
        except TemplateSyntaxError:
            return self.error(_('Syntax error in page variable.'))
        except VariableDoesNotExist:
            return self.error(_('Backoffice preview unavailable.'))
        except HTTPError as e:
            if e.response.status_code == 404:
                return self.error(_('Visualization not found.'))
            else:
                return self.error(_('Unknown HTTP error: %s' % e))

        if self.filters_cell_id and self.cell.statistic.service_slug != 'bijoe':
            self.update_subfilters_cache(form.instance)

        export_format = request.GET.get('export-format')
        if export_format == 'svg':
            return self.export_to_svg(chart)
        elif export_format == 'ods':
            return self.export_to_ods(chart)

        if self.cell.is_table_chart():
            if not chart.raw_series:
                return self.error(_('No data'))

            # force x_value_formatter on x_labels (avoid bug https://github.com/Kozea/pygal/issues/372)
            chart.x_labels = [chart.config.x_value_formatter(x) for x in chart.x_labels]

            if isinstance(chart.config.value_formatter, formatters.Default):
                chart.config.value_formatter = lambda x: str(x or 0)

            if self.cell.display_line_numbers:
                self.cell.add_line_numbers_to_table(chart)

            rendered = chart.render_table(
                transpose=bool(self.cell.chart_type == 'table-inverted'),
                total=bool(self.cell.statistic.service_slug == 'bijoe' and chart.compute_sum),
            )
            rendered = rendered.replace('<table>', '<table class="main">')
            return HttpResponse(rendered)
        elif self.cell.chart_type == 'variation-badge':
            if not chart.raw_series:
                return self.error(_('No data'))
            return HttpResponse(chart.render())

        return HttpResponse(chart.render(), content_type='image/svg+xml')

    def error(self, error_text):
        if self.cell.is_table_chart():
            return HttpResponse('<p>%s</p>' % error_text)

        context = {
            'width': self.request.GET.get('width', 200),
            'text': error_text,
        }
        return render(self.request, 'combo/dataviz-error.svg', context=context, content_type='image/svg+xml')

    def update_subfilters_cache(self, cell):
        try:
            data = cell.get_statistic_data(raise_if_not_cached=True, include_subfilters=True)
        except NothingInCacheException:
            pass  # should not happen
        else:
            cache.set(
                cell.get_cache_key(self.filters_cell_id), data.json()['data'].get('subfilters', []), 300
            )

    def export_to_svg(self, chart):
        response = HttpResponse(chart.render(), content_type='image/svg+xml')
        response['Content-Disposition'] = 'attachment; filename="%s.svg"' % self.cell.get_download_filename()
        return response

    def export_to_ods(self, chart):
        data = [[''] + chart.x_labels] if any(chart.x_labels) else []
        for serie in chart.raw_series:
            line = [serie[1]['title']] + serie[0]
            line = [x or 0 for x in line]
            data.append(line)

        if self.cell.chart_type != 'table-inverted':
            data = zip(*data)

        output = io.BytesIO()
        pyexcel_ods.save_data(output, {self.cell.title or self.cell.statistic.label: data})
        output.seek(0)
        return FileResponse(
            output,
            as_attachment=True,
            content_type='application/vnd.oasis.opendocument.spreadsheet',
            filename='%s.ods' % self.cell.get_download_filename(),
        )


dataviz_graph = xframe_options_sameorigin(DatavizGraphView.as_view())


class DatavizGraphExportView(SingleObjectMixin, FormView):
    model = ChartNgCell
    pk_url_kwarg = 'cell'
    form_class = ChartNgExportForm
    template_name = 'combo/chartngcell_export_form.html'

    def dispatch(self, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.querystring = self.request.GET.copy()
        self.querystring['export-format'] = form.cleaned_data['export_format']
        return super().form_valid(form)

    def get_success_url(self):
        return '%s?%s' % (
            reverse('combo-dataviz-graph', kwargs={'cell': self.object.pk}),
            self.querystring.urlencode(),
        )


dataviz_graph_export = DatavizGraphExportView.as_view()


class DatavizChoicesView(DetailView):
    model = ChartNgCell
    pk_url_kwarg = 'cell_id'

    def dispatch(self, *args, **kwargs):
        self.cell = self.get_object()

        filter_id = self.kwargs.get('filter_id')
        for filter_ in self.cell.available_filters:
            if filter_['id'] == filter_id:
                self.filter = filter_
                break
        else:
            raise Http404()

        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        search_term = request.GET.get('term', '')
        search_term = unicodedata.normalize('NFKC', search_term).casefold()

        try:
            page_number = int(request.GET.get('page', 1))
        except ValueError:
            page_number = 1

        initial = self.cell.filter_params.get(self.filter['id'], self.filter.get('default'))
        objects = ChartFiltersMixin.get_filter_options(self.cell, self.filter, initial)
        objects = [x for x in objects if search_term in unicodedata.normalize('NFKC', str(x)).casefold()]

        return JsonResponse(
            {
                'results': self.format_results(objects, (page_number - 1) * 10, page_number * 10),
                'pagination': {'more': bool(len(objects) >= page_number * 10)},
            }
        )

    def format_results(self, objects, start_index, end_index):
        page_objects = objects[start_index:end_index]

        if start_index > 0:
            last_displayed_group = objects[start_index - 1].group
            for option in page_objects:
                if option.group == last_displayed_group:
                    option.group = None

        return [
            {'text': group, 'children': [{'id': k, 'text': v} for k, v in choices]}
            for group, choices in Choice.get_field_choices(page_objects)
        ]


dataviz_choices = DatavizChoicesView.as_view()
