function gallery(element) {
  var element_id = '#' + $(element).attr('id');
  $(element).sortable({
    items: '> li[data-object-id]',
    containment: 'parent',
    placeholder: 'empty-image',
    update: function(event, ui) {
      var new_order = $(element).find('> li').map(function() { return $(this).data('object-id'); }).get().join();
      $.ajax({
        url: $(element).data('order-url'),
        data: {'new-order': new_order},
        success: function(data, status) {
          $(element).replaceWith($(data).find(element_id));
          gallery($(element_id));
        }
      });
    }
  });
  $('.image-delete').on('click', function() {
    $.ajax({
      url: $(this).attr('href'),
      success: function(data, status) {
        $(element).replaceWith($(data).find(element_id));
        gallery($(element_id));
      }
    });
    return false;
  });
};
