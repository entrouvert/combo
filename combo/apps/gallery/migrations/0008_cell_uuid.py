from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gallery', '0007_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallerycell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='gallerycell',
            unique_together={('page', 'uuid')},
        ),
    ]
