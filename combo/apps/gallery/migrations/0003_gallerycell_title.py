from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gallery', '0002_image_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallerycell',
            name='title',
            field=models.CharField(max_length=50, null=True, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
