from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gallery', '0004_auto_20210723_1318'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallerycell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.ImageField(upload_to='uploads/gallery/%Y/%m/', verbose_name='Image'),
        ),
    ]
