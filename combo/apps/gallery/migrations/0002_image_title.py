from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gallery', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='title',
            field=models.CharField(max_length=50, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
