from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gallery', '0009_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gallerycell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
