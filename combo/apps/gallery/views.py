# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, UpdateView

from .forms import ImageAddForm, ImageEditForm
from .models import GalleryCell, Image


class ImageAddView(CreateView):
    model = Image
    template_name = 'combo/gallery_image_form.html'
    form_class = ImageAddForm

    def dispatch(self, request, *args, **kwargs):
        self.cell = get_object_or_404(GalleryCell, pk=kwargs['gallery_pk'])
        if not self.cell.page.is_editable(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.gallery = self.cell
        other_images = form.instance.gallery.image_set.all()
        if other_images:
            form.instance.order = max(x.order for x in other_images) + 1
        else:
            form.instance.order = 0
        return super().form_valid(form)

    def get_success_url(self):
        return (
            reverse('combo-manager-page-view', kwargs={'pk': self.object.gallery.page_id})
            + '#cell-'
            + self.object.gallery.get_reference()
        )


image_add = ImageAddView.as_view()


class ImageEditView(UpdateView):
    model = Image
    template_name = 'combo/gallery_image_form.html'
    form_class = ImageEditForm

    def get_object(self, queryset=None):
        image = get_object_or_404(Image, pk=self.kwargs['pk'], gallery=self.kwargs['gallery_pk'])
        if not image.gallery.page.is_editable(self.request.user):
            raise PermissionDenied()
        return image

    def get_success_url(self):
        return (
            reverse('combo-manager-page-view', kwargs={'pk': self.object.gallery.page_id})
            + '#cell-'
            + self.object.gallery.get_reference()
        )


image_edit = ImageEditView.as_view()


def image_delete(request, gallery_pk, pk):
    image = get_object_or_404(Image, pk=pk, gallery=gallery_pk)
    if not image.gallery.page.is_editable(request.user):
        raise PermissionDenied()
    image.delete()
    return redirect(reverse('combo-manager-page-view', kwargs={'pk': image.gallery.page_id}))


def image_order(request, gallery_pk):
    gallery = get_object_or_404(GalleryCell, pk=gallery_pk)
    if not gallery.page.is_editable(request.user):
        raise PermissionDenied()
    new_order = [int(x) for x in request.GET['new-order'].split(',')]
    for image in gallery.image_set.all():
        image.order = new_order.index(image.id) + 1
        image.save()
    return redirect(reverse('combo-manager-page-view', kwargs={'pk': gallery.page_id}))
