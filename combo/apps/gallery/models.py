# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django import forms
from django.core import serializers
from django.db import models
from django.utils.translation import gettext_lazy as _

from combo.data.library import register_cell_class
from combo.data.models import CellBase


@register_cell_class
class GalleryCell(CellBase):
    title = models.CharField(_('Title'), max_length=150, blank=True, null=True)
    default_template_name = 'combo/gallerycell.html'
    manager_form_template = 'combo/gallery_manager.html'

    class Meta:
        unique_together = [('page', 'uuid')]
        verbose_name = _('Gallery')

    def get_additional_label(self):
        if self.title:
            return self.title
        return ''

    def export_subobjects(self):
        return {'images': [x.get_as_serialized_object() for x in self.image_set.all()]}

    def import_subobjects(self, cell_json):
        self.image_set.all().delete()
        images = serializers.deserialize('json', json.dumps(cell_json['images']), ignorenonexistent=True)
        for image in images:
            image.object.gallery_id = self.id
            image.save()

    def get_default_form_class(self, fields=None):
        if fields is None:
            fields = []
        return forms.models.modelform_factory(self.__class__, fields=fields)


class Image(models.Model):
    gallery = models.ForeignKey(GalleryCell, on_delete=models.CASCADE, verbose_name=_('Gallery'))
    image = models.ImageField(_('Image'), upload_to='uploads/gallery/%Y/%m/')
    order = models.PositiveIntegerField()
    title = models.CharField(_('Title'), max_length=150, blank=True)

    class Meta:
        ordering = ['order']

    def get_as_serialized_object(self):
        serialized_image = json.loads(
            serializers.serialize(
                'json', [self], use_natural_foreign_keys=True, use_natural_primary_keys=True
            )
        )[0]
        del serialized_image['fields']['gallery']
        del serialized_image['pk']
        return serialized_image
