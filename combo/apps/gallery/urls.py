# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from combo.urls_utils import decorated_includes, manager_required

from . import views

gallery_manager_urls = [
    path('<int:gallery_pk>/images/add/', views.image_add, name='combo-gallery-image-add'),
    path('<int:gallery_pk>/order', views.image_order, name='combo-gallery-image-order'),
    path(
        '<int:gallery_pk>/images/<int:pk>/edit',
        views.image_edit,
        name='combo-gallery-image-edit',
    ),
    path(
        '<int:gallery_pk>/images/<int:pk>/delete',
        views.image_delete,
        name='combo-gallery-image-delete',
    ),
]

urlpatterns = [
    re_path(r'^manage/gallery/', decorated_includes(manager_required, include(gallery_manager_urls))),
]
