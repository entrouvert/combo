from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0006_auto_20210723_1318'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationscell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='body',
            field=models.TextField(blank=True, default='', verbose_name='Body'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='url',
            field=models.URLField(blank=True, default='', verbose_name='URL'),
        ),
    ]
