import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in ['NotificationsCell']:
        klass = apps.get_model('notifications', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0009_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
