from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0010_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
