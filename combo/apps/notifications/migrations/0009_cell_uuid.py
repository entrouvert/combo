from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0008_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='notificationscell',
            unique_together={('page', 'uuid')},
        ),
    ]
