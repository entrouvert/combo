import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationscell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
    ]
