# Generated by Django 4.2.18 on 2025-02-22 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0011_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='url',
            field=models.URLField(blank=True, default='', max_length=500, verbose_name='URL'),
        ),
    ]
