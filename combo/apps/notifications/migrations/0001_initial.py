from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0020_auto_20160928_1152'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('summary', models.CharField(max_length=140, verbose_name='Label')),
                ('body', models.TextField(default=b'', verbose_name='Body', blank=True)),
                ('url', models.URLField(default=b'', verbose_name='URL', blank=True)),
                ('start_timestamp', models.DateTimeField(verbose_name='Start date and time')),
                ('end_timestamp', models.DateTimeField(verbose_name='End date and time')),
                ('acked', models.BooleanField(default=False, verbose_name='Acked')),
                ('external_id', models.SlugField(null=True, verbose_name='External identifier')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Notification',
            },
        ),
        migrations.CreateModel(
            name='NotificationsCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                (
                    'extra_css_class',
                    models.CharField(
                        max_length=100, verbose_name='Extra classes for CSS styling', blank=True
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'User Notifications',
            },
        ),
    ]
