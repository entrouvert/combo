from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notifications', '0002_notificationscell_last_update_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='origin',
            field=models.CharField(max_length=100, verbose_name='Origin', blank=True),
        ),
    ]
