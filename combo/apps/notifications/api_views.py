# combo - content management system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.db import transaction
from django.utils.encoding import force_str
from rest_framework import authentication, permissions, serializers, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from combo.profile.utils import get_user_from_name_id

from .models import Notification


class NotificationSerializer(serializers.Serializer):
    summary = serializers.CharField(required=True, allow_blank=False, max_length=140)
    id = serializers.CharField(required=False, allow_null=True)
    body = serializers.CharField(allow_blank=True, allow_null=True, default='')
    url = serializers.URLField(allow_blank=True, default='')
    origin = serializers.CharField(allow_blank=True, default='')
    start_timestamp = serializers.DateTimeField(required=False, allow_null=True)
    end_timestamp = serializers.DateTimeField(required=False, allow_null=True)
    duration = serializers.IntegerField(required=False, allow_null=True, min_value=0)
    name_ids = serializers.ListField(
        required=False, child=serializers.CharField(required=True, max_length=150)
    )

    def validate(self, attrs):
        super().validate(attrs)
        users = []
        existing = []
        missing = []
        for uuid in attrs.get('name_ids') or []:
            try:
                user = get_user_from_name_id(uuid, raise_on_missing=True)
            except User.DoesNotExist:
                missing.append(uuid)
            else:
                users.append(user)
                existing.append(uuid)
        attrs['users'] = users
        attrs['existing_uuids'] = existing
        attrs['missing_uuids'] = missing
        return attrs


class Add(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = NotificationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            response = {'err': 1, 'err_desc': serializer.errors}
            return Response(response, status.HTTP_400_BAD_REQUEST)
        payload = serializer.validated_data
        notification_ids = []
        if payload.get('name_ids'):
            # if name_ids were given, get the existing users
            users = payload.get('users')
        elif request.user and request.user.is_authenticated:
            # if not, add notification to current user (it will usually be set
            # via the custom PublikAuthentication djangorestfamework authentication
            # class).
            users = [request.user]
        else:
            response = {'err': 1, 'err_desc': 'no users given'}
            return Response(response, status.HTTP_400_BAD_REQUEST)
        try:
            with transaction.atomic():
                for user in users:
                    notification = Notification.notify(
                        user=user,
                        summary=payload['summary'],
                        id=payload.get('id'),
                        body=payload.get('body') or '',
                        url=payload.get('url'),
                        origin=payload.get('origin'),
                        start_timestamp=payload.get('start_timestamp'),
                        end_timestamp=payload.get('end_timestamp'),
                        duration=payload.get('duration'),
                    )
                    notification_ids.append(notification.public_id)
        except ValueError as e:
            response = {'err': 1, 'err_desc': {'id': [force_str(e)]}}
            return Response(response, status.HTTP_400_BAD_REQUEST)

        data = {}
        if not payload.get('name_ids'):
            data = {'id': notification_ids.pop()}
        else:
            data = {
                'nb_notify': len(notification_ids),
                'notified': payload.get('existing_uuids'),
                'unnotified': payload.get('missing_uuids'),
            }
            if payload.get('id'):
                data['id'] = payload.get('id')
            else:
                data['ids'] = notification_ids
        return Response({'err': 0, 'data': data})


add = Add.as_view()


class Ack(GenericAPIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, notification_id, *args, **kwargs):
        Notification.objects.find(request.user, notification_id).ack()
        return Response({'err': 0})


ack = Ack.as_view()


class Forget(GenericAPIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, notification_id, *args, **kwargs):
        Notification.objects.find(request.user, notification_id).forget()
        return Response({'err': 0})


forget = Forget.as_view()


class Count(GenericAPIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        total = Notification.objects.visible(request.user).count()
        new = Notification.objects.visible(request.user).new().count()
        return Response({'err': 0, 'total': total, 'new': new})


count = Count.as_view()
count.mellon_no_passive = True
