# combo - content management system
# Copyright (C) 2017-2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path

from . import api_views

urlpatterns = [
    path('api/export-import/', api_views.index, name='api-export-import'),
    path('api/export-import/bundle-check/', api_views.bundle_check),
    path('api/export-import/bundle-declare/', api_views.bundle_declare),
    path('api/export-import/bundle-import/', api_views.bundle_import),
    path('api/export-import/unlink/', api_views.bundle_unlink),
    path(
        'api/export-import/<slug:component_type>/',
        api_views.list_components,
        name='api-export-import-components-list',
    ),
    path(
        'api/export-import/<slug:component_type>/<uuid:uuid>/',
        api_views.export_component,
        name='api-export-import-component-export',
    ),
    path(
        'api/export-import/<slug:component_type>/<uuid:uuid>/dependencies/',
        api_views.component_dependencies,
        name='api-export-import-component-dependencies',
    ),
    path(
        'api/export-import/<slug:component_type>/<uuid:uuid>/redirect/',
        api_views.component_redirect,
        name='api-export-import-component-redirect',
    ),
    path(
        'api/export-import/job/<uuid:job_uuid>/status/',
        api_views.job_status,
        name='api-export-import-job-status',
    ),
]
