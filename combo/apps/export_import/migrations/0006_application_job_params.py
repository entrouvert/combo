from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('export_import', '0005_failure_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicationasyncjob',
            name='params',
            field=models.JSONField(default=dict),
        ),
    ]
