import uuid

from django.db import migrations, models

import combo.apps.export_import.models


class Migration(migrations.Migration):
    dependencies = [
        ('export_import', '0002_application'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationAsyncJob',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                (
                    'status',
                    models.CharField(
                        choices=[
                            ('registered', 'Registered'),
                            ('running', 'Running'),
                            ('failed', 'Failed'),
                            ('completed', 'Completed'),
                        ],
                        default='registered',
                        max_length=100,
                    ),
                ),
                ('exception', models.TextField()),
                ('action', models.CharField(max_length=100)),
                (
                    'bundle',
                    models.FileField(
                        blank=True, null=True, upload_to=combo.apps.export_import.models.upload_to_job_uuid
                    ),
                ),
                ('total_count', models.PositiveIntegerField(default=0)),
                ('current_count', models.PositiveIntegerField(default=0)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('completion_timestamp', models.DateTimeField(default=None, null=True)),
            ],
        ),
    ]
