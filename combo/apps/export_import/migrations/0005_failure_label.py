from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('export_import', '0004_result_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicationasyncjob',
            name='failure_label',
            field=models.TextField(blank=True),
        ),
    ]
