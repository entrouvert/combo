from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('export_import', '0003_asyncjob'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicationasyncjob',
            name='result_data',
            field=models.JSONField(default=dict),
        ),
    ]
