# combo - content management system
# Copyright (C) 2017-2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import datetime
import io
import json
import sys
import tarfile
import traceback
import uuid

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from combo.utils.misc import is_portal_agent


class BundleKeyError(Exception):
    pass


class Application(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    icon = models.FileField(
        upload_to='applications/icons/',
        blank=True,
        null=True,
    )
    description = models.TextField(blank=True)
    documentation_url = models.URLField(blank=True)
    version_number = models.CharField(max_length=100)
    version_notes = models.TextField(blank=True)
    editable = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)

    @classmethod
    def update_or_create_from_manifest(cls, manifest, tar, editable=False):
        application, dummy = cls.objects.get_or_create(
            slug=manifest.get('slug'), defaults={'editable': editable}
        )
        application.name = manifest.get('application')
        application.description = manifest.get('description') or ''
        application.documentation_url = manifest.get('documentation_url') or ''
        application.version_number = manifest.get('version_number') or 'unknown'
        application.version_notes = manifest.get('version_notes') or ''
        if not editable:
            application.editable = editable
        application.visible = manifest.get('visible', True)
        application.save()
        icon = manifest.get('icon')
        if icon:
            application.icon.save(icon, tar.extractfile(icon), save=True)
        else:
            application.icon.delete()
        return application

    @classmethod
    def select_for_object_class(cls, object_class):
        content_type = ContentType.objects.get_for_model(object_class)
        elements = ApplicationElement.objects.filter(content_type=content_type)
        return cls.objects.filter(pk__in=elements.values('application'), visible=True).order_by('name')

    @classmethod
    def populate_objects(cls, object_class, objects):
        content_type = ContentType.objects.get_for_model(object_class)
        elements = ApplicationElement.objects.filter(
            content_type=content_type, application__visible=True
        ).prefetch_related('application')
        elements_by_objects = collections.defaultdict(list)
        for element in elements:
            elements_by_objects[element.object_id].append(element)
        for obj in objects:
            applications = [element.application for element in elements_by_objects.get(obj.pk) or []]
            obj._applications = sorted(applications, key=lambda a: a.name)

    @classmethod
    def load_for_object(cls, obj):
        content_type = ContentType.objects.get_for_model(obj.__class__)
        elements = ApplicationElement.objects.filter(
            content_type=content_type, object_id=obj.pk, application__visible=True
        ).prefetch_related('application')
        applications = [element.application for element in elements]
        obj._applications = sorted(applications, key=lambda a: a.name)

    def get_objects_for_object_class(self, object_class):
        content_type = ContentType.objects.get_for_model(object_class)
        elements = ApplicationElement.objects.filter(content_type=content_type, application=self)
        return object_class.objects.filter(pk__in=elements.values('object_id'))

    @classmethod
    def get_orphan_objects_for_object_class(cls, object_class):
        content_type = ContentType.objects.get_for_model(object_class)
        elements = ApplicationElement.objects.filter(content_type=content_type, application__visible=True)
        return object_class.objects.exclude(pk__in=elements.values('object_id'))


class ApplicationElement(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ['application', 'content_type', 'object_id']

    @classmethod
    def update_or_create_for_object(cls, application, obj):
        content_type = ContentType.objects.get_for_model(obj.__class__)
        element, created = cls.objects.get_or_create(
            application=application,
            content_type=content_type,
            object_id=obj.pk,
        )
        if not created:
            element.save()
        return element


STATUS_CHOICES = [
    ('registered', _('Registered')),
    ('running', _('Running')),
    ('failed', _('Failed')),
    ('completed', _('Completed')),
]


def upload_to_job_uuid(instance, filename):
    return f'applications/bundles/{instance.uuid}/{filename}'


class ApplicationAsyncJob(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    status = models.CharField(
        max_length=100,
        default='registered',
        choices=STATUS_CHOICES,
    )
    exception = models.TextField()
    action = models.CharField(max_length=100)
    bundle = models.FileField(
        upload_to=upload_to_job_uuid,
        blank=True,
        null=True,
    )
    params = models.JSONField(default=dict)
    total_count = models.PositiveIntegerField(default=0)
    current_count = models.PositiveIntegerField(default=0)
    failure_label = models.TextField(blank=True)
    result_data = models.JSONField(default=dict)

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)
    completion_timestamp = models.DateTimeField(default=None, null=True)

    def run(self, spool=False):
        if 'uwsgi' in sys.modules and spool:
            from combo.utils.spooler import run_async_job

            run_async_job(job_id=str(self.pk))
            return
        self.status = 'running'
        self.save()
        try:
            getattr(self, self.action)()
        except BundleKeyError as e:
            self.status = 'failed'
            self.exception = str(e)
            self.failure_label = str(_('Error: %s') % str(e))
        except Exception:
            self.status = 'failed'
            self.exception = traceback.format_exc()
        finally:
            if self.status == 'running':
                self.status = 'completed'
            self.completion_timestamp = now()
            self.save()

    def check_bundle(self):
        from combo.data.models import Page, PageSnapshot

        page_type = 'portal-agent-pages' if is_portal_agent() else 'pages'
        tar_io = io.BytesIO(self.bundle.read())
        base_url = settings.SITE_BASE_URL
        differences = []
        unknown_elements = []
        no_history_elements = []
        legacy_elements = []
        uninstalled_elements = []
        with tarfile.open(fileobj=tar_io) as tar:
            try:
                manifest = json.loads(tar.extractfile('manifest.json').read().decode())
            except KeyError:
                raise BundleKeyError(_('Invalid tar file, missing manifest'))
            application_slug = manifest.get('slug')
            application_version = manifest.get('version_number')
            if not application_slug:
                raise BundleKeyError(_('Invalid tar file, missing application.'))
            if not application_version:
                raise BundleKeyError(_('Invalid tar file, missing version.'))

            elements_from_next_bundle = getattr(self, 'params', {}).get('elements_from_next_bundle')

            # count number of actions
            self.total_count = len([x for x in manifest.get('elements') if x.get('type') == page_type])

            content_type = ContentType.objects.get_for_model(Page)
            for element in manifest.get('elements'):
                if element.get('type') != page_type:
                    continue
                if f"{element['type']}/{element['slug']}" not in elements_from_next_bundle:
                    # element is not referenced in next bundle, it will be uninstalled.
                    # don't check differences
                    uninstalled_elements.append(
                        {
                            'type': element['type'],
                            'slug': element['slug'],
                        }
                    )
                    self.increment_count()
                    continue
                try:
                    page = Page.objects.get(uuid=element['slug'])
                except Page.DoesNotExist:
                    unknown_elements.append(
                        {
                            'type': element['type'],
                            'slug': element['slug'],
                        }
                    )
                    self.increment_count()
                    continue
                elements_qs = ApplicationElement.objects.filter(
                    application__slug=application_slug,
                    content_type=content_type,
                    object_id=page.pk,
                )
                if not elements_qs.exists():
                    # object exists, but not linked to the application
                    legacy_elements.append(
                        {
                            'type': element['type'],
                            'slug': element['slug'],
                            # information needed here, Relation objects may not exist yet in hobo
                            'text': page.title,
                            'url': '%s%s'
                            % (
                                base_url,
                                reverse(
                                    'api-export-import-component-redirect',
                                    kwargs={
                                        'uuid': str(page.uuid),
                                        'component_type': page.application_component_type,
                                    },
                                ),
                            ),
                        }
                    )
                    self.increment_count()
                    continue
                snapshot_for_app = (
                    PageSnapshot.objects.filter(
                        page=page,
                        application_slug=application_slug,
                        application_version=application_version,
                    )
                    .order_by('timestamp')
                    .last()
                )
                if not snapshot_for_app:
                    # no snapshot for this bundle
                    no_history_elements.append(
                        {
                            'type': element['type'],
                            'slug': element['slug'],
                        }
                    )
                    self.increment_count()
                    continue
                last_snapshot = PageSnapshot.objects.filter(page=page).latest('timestamp')
                if snapshot_for_app.pk != last_snapshot.pk:
                    differences.append(
                        {
                            'type': element['type'],
                            'slug': element['slug'],
                            'url': '%s%s?version1=%s&version2=%s'
                            % (
                                base_url,
                                reverse('combo-manager-page-history-compare', args=[page.pk]),
                                snapshot_for_app.pk,
                                last_snapshot.pk,
                            ),
                        }
                    )
                self.increment_count()

        self.result_data = {
            'differences': differences,
            'unknown_elements': unknown_elements,
            'no_history_elements': no_history_elements,
            'legacy_elements': legacy_elements,
            'uninstalled_elements': uninstalled_elements,
        }
        self.save()

    def process_bundle(self, install=True):
        page_type = 'portal-agent-pages' if is_portal_agent() else 'pages'
        pages = []
        tar_io = io.BytesIO(self.bundle.read())
        with tarfile.open(fileobj=tar_io) as tar:
            manifest = json.loads(tar.extractfile('manifest.json').read().decode())
            self.application = Application.update_or_create_from_manifest(
                manifest,
                tar,
                editable=not install,
            )

            # count number of actions
            self.total_count = len([x for x in manifest.get('elements') if x.get('type') == page_type])

            for element in manifest.get('elements'):
                if element.get('type') != page_type:
                    continue
                try:
                    pages.append(
                        json.loads(tar.extractfile(f'{page_type}/{element["slug"]}').read().decode()).get(
                            'data'
                        )
                    )
                except KeyError:
                    raise BundleKeyError(
                        'Invalid tar file, missing component %s/%s.' % (page_type, element['slug'])
                    )

        # init cache of application elements, from manifest
        self.application_elements = set()
        # install pages
        if install and pages:
            self._import_site(pages)
        # create application elements
        self.link_objects(pages, increment=not install)
        # remove obsolete application elements
        self.unlink_obsolete_objects()

    def _import_site(self, pages):
        from combo.data.library import get_cell_classes
        from combo.data.models import Page
        from combo.data.utils import import_site

        existing_pages = Page.objects.all().prefetch_related('groups', 'edit_role', 'subpages_edit_role')
        # keep pages positions (order and parent) before import
        initial_positions = {
            str(p.uuid): (str(p.parent.uuid) if p.parent else None, p.order) for p in existing_pages
        }

        # keep pages config fields before import
        page_config_attrs = ['public', 'edit_role', 'subpages_edit_role']
        initial_page_attrs = {
            str(p.uuid): {k: getattr(p, k) for k in page_config_attrs} for p in existing_pages
        }
        for page in existing_pages:
            initial_page_attrs[str(page.uuid)]['groups'] = list(page.groups.all())

        # keep cells config fields before import
        cell_config_attrs = collections.defaultdict(dict)
        initial_cell_attrs = collections.defaultdict(dict)
        for klass in get_cell_classes():
            if klass is None:
                continue
            cell_config_attrs[klass] = ['public', 'restricted_to_unlogged']
            if 'regie' in [x.name for x in klass._meta.local_concrete_fields]:
                cell_config_attrs[klass].append('regie')
            existing_cells = klass.objects.filter(uuid__isnull=False).prefetch_related('page', 'groups')
            for existing_cell in existing_cells:
                if not initial_cell_attrs[klass].get(str(existing_cell.page.uuid)):
                    initial_cell_attrs[klass][str(existing_cell.page.uuid)] = {}
                initial_cell_attrs[klass][str(existing_cell.page.uuid)][str(existing_cell.uuid)] = {
                    k: getattr(existing_cell, k) for k in cell_config_attrs[klass]
                }
                initial_cell_attrs[klass][str(existing_cell.page.uuid)][str(existing_cell.uuid)]['groups'] = (
                    list(existing_cell.groups.all())
                )

        # keep positions (order and parent) of imported pages
        imported_positions = {
            p['fields']['uuid']: (
                p['fields']['parent'][0] if p['fields']['parent'] else None,
                p['fields']['order'],
            )
            for p in pages
        }

        # import pages
        import_site({'pages': pages}, job=self)

        # get pages and cells after import
        objects_by_uuid = {str(p.uuid): p for p in Page.objects.all()}
        objects_by_uuid[None] = None
        cells_by_uuid = collections.defaultdict(dict)
        for klass in get_cell_classes():
            if klass is None:
                continue
            cells = klass.objects.filter(uuid__isnull=False).prefetch_related('page')
            for cell in cells:
                if not cells_by_uuid[klass].get(str(cell.page.uuid)):
                    cells_by_uuid[klass][str(cell.page.uuid)] = {}
                cells_by_uuid[klass][str(cell.page.uuid)][str(cell.uuid)] = cell

        # keep some settings when updating - on pages
        for page_uuid, page in objects_by_uuid.items():
            if page_uuid is None or page_uuid not in initial_page_attrs:
                continue
            changed = False
            for attr in page_config_attrs:
                if getattr(page, attr) == initial_page_attrs[page_uuid][attr]:
                    continue
                setattr(page, attr, initial_page_attrs[page_uuid][attr])
                changed = True
            if changed:
                page.save()
            page.groups.set(initial_page_attrs[page_uuid]['groups'])

        # keep some settings when updating - on cells
        for klass, pages in cells_by_uuid.items():
            for page_uuid, cells in pages.items():
                if page_uuid not in initial_cell_attrs[klass]:
                    continue
                for cell_uuid, cell in cells.items():
                    if cell_uuid not in initial_cell_attrs[klass][page_uuid]:
                        continue
                    changed = False
                    for attr in cell_config_attrs[klass]:
                        if getattr(cell, attr) == initial_cell_attrs[klass][page_uuid][cell_uuid][attr]:
                            continue
                        setattr(cell, attr, initial_cell_attrs[klass][page_uuid][cell_uuid][attr])
                        changed = True
                    if changed:
                        cell.save()
                    cell.groups.set(initial_cell_attrs[klass][page_uuid][cell_uuid]['groups'])

        # rebuild page positions: first set parents, and then set orders
        # set parents of imported pages
        for page_uuid, (parent_uuid, order) in imported_positions.items():
            if page_uuid in initial_positions:
                # page was already deployed, keep parent initially set on this instance
                objects_by_uuid[page_uuid].parent = objects_by_uuid[initial_positions[page_uuid][0]]
                objects_by_uuid[page_uuid].save()
                continue

            # page is newly deployed, set parent
            # search siblings in the application
            siblings = [k for k, v in imported_positions.items() if k != page_uuid and v[0] == parent_uuid]
            # look at siblings parents before the import, but only parents outside in the application
            parents = {
                v[0] for k, v in initial_positions.items() if k in siblings and v[0] not in imported_positions
            }
            if not parents or len(parents) > 1:
                # no parents outside the application: no change, parent is already correctly set by the import
                # more than one parent outside the application: can not decide which one to take; no change, keep parents set by the import
                continue
            # all siblings at the same place, set page under siblings parent
            parent = list(parents)[0]
            objects_by_uuid[page_uuid].parent = objects_by_uuid[parent]
            objects_by_uuid[page_uuid].save()

        # and set orders
        objects_by_uuid.pop(None)

        # find imported pages and orders from initials
        existing_positions = {k: v[1] for k, v in initial_positions.items() if k in imported_positions}
        # find not imported pages and orders from initials
        not_imported_positions = {
            k: v[1] for k, v in initial_positions.items() if k not in imported_positions
        }

        def order_children(parent):
            # find children of the parent
            children = [k for k, v in objects_by_uuid.items() if v.parent == parent]
            # find children and positions in the application
            application_children = {k: imported_positions[k][1] for k in children if k in imported_positions}
            # find imported children and initial positions
            children_existing_positions = {
                k: v for k, v in existing_positions.items() if k in application_children
            }
            # find not imported children and initial positions
            children_not_imported_positions = {
                k: v
                for k, v in not_imported_positions.items()
                if k not in application_children and k in children
            }
            # determine position of application pages
            application_position = None
            if children_existing_positions:
                application_position = min(children_existing_positions.values())
            # all children placed before application pages
            before_positions = {
                k: v
                for k, v in children_not_imported_positions.items()
                if application_position is None or v < application_position
            }
            # all children placed after application pages
            after_positions = {
                k: v
                for k, v in children_not_imported_positions.items()
                if application_position is not None and v >= application_position
            }
            # sort children
            ordered_children = [
                objects_by_uuid[u] for u in sorted(before_positions, key=lambda a: before_positions[a])
            ]
            ordered_children += [
                objects_by_uuid[u]
                for u in sorted(application_children, key=lambda a: application_children[a])
            ]
            ordered_children += [
                objects_by_uuid[u] for u in sorted(after_positions, key=lambda a: after_positions[a])
            ]
            for child in ordered_children:
                # yield child
                yield child
                # and children of this child
                yield from order_children(child)

        ordered_pages = list(order_children(None))
        order = 1
        for page in ordered_pages:
            page.order = order
            page.save()
            order += 1

    def import_bundle(self):
        self.process_bundle()

    def declare_bundle(self):
        self.process_bundle(install=False)

    def link_objects(self, pages, increment=False):
        from combo.data.models import Page, PageSnapshot

        for page in pages:
            page_uuid = page['fields']['uuid']
            try:
                existing_page = Page.objects.get(uuid=page_uuid)
            except Page.DoesNotExist:
                pass
            else:
                element = ApplicationElement.update_or_create_for_object(self.application, existing_page)
                self.application_elements.add(element.content_object)
                if self.action == 'import_bundle':
                    PageSnapshot.take(
                        existing_page,
                        comment=_('Application (%s)') % self.application,
                        application=self.application,
                    )
            if increment:
                self.increment_count()

    def unlink_obsolete_objects(self):
        known_elements = ApplicationElement.objects.filter(application=self.application)
        for element in known_elements:
            if element.content_object not in self.application_elements:
                element.delete()

    def increment_count(self, amount=1):
        self.current_count = (self.current_count or 0) + amount
        if (now() - self.last_update_timestamp).total_seconds() > 1:
            self.save()

    def get_api_status_url(self, request):
        return request.build_absolute_uri(reverse('api-export-import-job-status', args=[self.uuid]))

    def get_completion_status(self):
        current_count = self.current_count or 0

        if not current_count:
            return ''

        if not self.total_count:
            return _('%(current_count)s (unknown total)') % {'current_count': current_count}

        return _('%(current_count)s/%(total_count)s (%(percent)s%%)') % {
            'current_count': int(current_count),
            'total_count': self.total_count,
            'percent': int(current_count * 100 / self.total_count),
        }

    @classmethod
    def clean_jobs(cls):
        # remove jobs after 7 days
        for job in cls.objects.filter(last_update_timestamp__lte=now() - datetime.timedelta(days=7)):
            job.bundle.delete(save=False)
            job.delete()
