# combo - content management system
# Copyright (C) 2014-2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from django.conf import settings
from django.db import models
from django.forms import Select
from django.forms import models as model_forms
from django.utils.translation import gettext_lazy as _
from requests import HTTPError, RequestException

from combo.data.library import register_cell_class
from combo.data.models import CellBase
from combo.utils import requests

logger = logging.getLogger(__name__)


@register_cell_class
class RecentDocumentsCell(CellBase):
    default_template_name = 'combo/fargo/recent-documents-cell.html'
    fargo_site = models.CharField(_('Site'), max_length=50, blank=True)
    user_dependant = True

    class Meta:
        unique_together = [('page', 'uuid')]
        verbose_name = _('Recent Documents')

    def get_form_fields(self):
        if len(get_fargo_services()) == 1:
            return []
        return ['fargo_site']

    def get_form_widgets(self):
        if len(get_fargo_services()) == 1:
            return {}
        combo_fargo_sites = [('', _('Default'))]
        fargo_sites = [(x, y.get('title')) for x, y in get_fargo_services().items()]
        fargo_sites.sort(key=lambda x: x[1])
        combo_fargo_sites.extend(fargo_sites)
        return {'fargo_site': Select(choices=combo_fargo_sites)}

    def get_default_form_class(self):
        return model_forms.modelform_factory(
            self.__class__, fields=self.get_form_fields(), widgets=self.get_form_widgets()
        )

    def is_visible(self, request, **kwargs):
        user = getattr(request, 'user', None)
        if not user or user.is_anonymous:
            return False
        return super().is_visible(request, **kwargs)

    @classmethod
    def is_enabled(cls):
        return hasattr(settings, 'KNOWN_SERVICES') and settings.KNOWN_SERVICES.get('fargo')

    def get_json(self, path, context):
        user = self.get_concerned_user(context)
        try:
            response = requests.get(
                path,
                remote_service=get_fargo_site(self.fargo_site),
                user=user,
                raise_if_not_cached=not (context.get('synchronous')),
                headers={'accept': 'application/json'},
                log_errors=False,
            )
            response.raise_for_status()
            return response.json()
        except HTTPError as e:
            status_code = e.response.status_code
            try:
                content = e.response.json()
            except Exception:
                err = None
            else:
                err = content.get('err') if isinstance(content, dict) else None
            if status_code == 401 and err == 'user-not-found':
                log = logger.info
            else:
                log = logger.error
            log(
                'could not retrieve recent documents for user %s: status-code=%s err=%s',
                user,
                status_code,
                err,
            )
        except RequestException as e:
            logger.error('could not retrieve recent documents for user %s: %s', user, e)
        return {}

    def render(self, context):
        context.update(self.get_json('api/documents/recently-added/', context))
        return super().render(context)


def get_fargo_services():
    return settings.KNOWN_SERVICES.get('fargo') or {}


def get_fargo_site(fargo_site):
    if fargo_site:
        return settings.KNOWN_SERVICES['fargo'].get(fargo_site)
    for site in settings.KNOWN_SERVICES['fargo'].values():
        if not site.get('secondary'):
            return site
    return list(settings.KNOWN_SERVICES['fargo'].values())[0]
