from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='recentdocumentscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
