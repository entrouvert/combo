from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0005_recentdocumentscell_template_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='recentdocumentscell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
