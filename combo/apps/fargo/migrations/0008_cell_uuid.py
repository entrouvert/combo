from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0007_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='recentdocumentscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='recentdocumentscell',
            unique_together={('page', 'uuid')},
        ),
    ]
