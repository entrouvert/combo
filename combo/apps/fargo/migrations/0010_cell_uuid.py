from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0009_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recentdocumentscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
