from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0003_recentdocumentscell_last_update_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='recentdocumentscell',
            name='fargo_site',
            field=models.CharField(max_length=50, verbose_name='Site', blank=True),
        ),
    ]
