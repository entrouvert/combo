# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from combo.urls_utils import decorated_includes, staff_required

from .manager_views import (
    ManagerAddNavigationEntry,
    ManagerDeleteNavigationEntry,
    ManagerEditNavigationEntry,
    ManagerHomeView,
    manager_navigation_order,
)
from .views import (
    manifest_json,
    offline_page,
    service_worker_js,
    service_worker_registration_js,
    subscribe_push,
)

pwa_manager_urls = [
    path('', ManagerHomeView.as_view(), name='pwa-manager-homepage'),
    path('navigation/add/', ManagerAddNavigationEntry.as_view(), name='pwa-manager-navigation-add'),
    path(
        'navigation/edit/<int:pk>/',
        ManagerEditNavigationEntry.as_view(),
        name='pwa-manager-navigation-edit',
    ),
    path(
        'navigation/delete/<int:pk>/',
        ManagerDeleteNavigationEntry.as_view(),
        name='pwa-manager-navigation-delete',
    ),
    path('navigation/order/', manager_navigation_order, name='pwa-manager-navigation-order'),
]

urlpatterns = [
    re_path('^manifest.json$', manifest_json),
    re_path('^service-worker.js$', service_worker_js),
    re_path('^service-worker-registration.js$', service_worker_registration_js),
    path('api/pwa/push/subscribe', subscribe_push, name='pwa-subscribe-push'),
    path('__pwa__/offline/', offline_page),
    re_path(r'^manage/pwa/', decorated_includes(staff_required, include(pwa_manager_urls))),
]
