# combo - content management system
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.db.models import Max
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView

from combo.data.forms import get_page_choices

from .forms import PwaSettingsForm
from .models import PwaNavigationEntry, PwaSettings


class ManagerHomeView(UpdateView):
    template_name = 'combo/pwa/manager_home.html'
    model = PwaSettings
    form_class = PwaSettingsForm
    success_url = reverse_lazy('pwa-manager-homepage')

    def get_initial(self):
        initial = super().get_initial()
        initial['application_name'] = self.get_object().get_application_name()
        return initial

    def form_valid(self, form):
        if form.instance.application_name == PwaSettings.get_default_application_name():
            form.instance.application_name = ''
        return super().form_valid(form)

    def get_object(self):
        return PwaSettings.singleton()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navigation_entries'] = PwaNavigationEntry.objects.all()
        return context


class ManagerNavigationEntryMixin:
    model = PwaNavigationEntry
    fields = [
        'label',
        'url',
        'link_page',
        'icon',
        'extra_css_class',
        'notification_count',
        'use_user_name_as_label',
    ]
    template_name = 'combo/pwa/manager_form.html'
    success_url = reverse_lazy('pwa-manager-homepage')

    def get_form_class(self):
        form_class = forms.models.modelform_factory(self.model, fields=self.fields)
        form_class.base_fields['link_page'].choices = [(None, '-----')] + get_page_choices()
        return form_class

    def form_valid(self, form):
        if form.instance.order is None:
            max_order = self.model.objects.all().aggregate(Max('order'))
            form.instance.order = (max_order['order__max'] or 0) + 1
        if form.instance.link_page_id is None:
            if not form.instance.label:
                form.add_error('label', _('A label is required when no page is selected.'))
            if not form.instance.url:
                form.add_error('url', _('An URL is required when no page is selected.'))
        elif form.instance.url:
            form.add_error('url', _('An URL cannot be specified when a page is selected.'))
        if form.errors:
            return super().form_invalid(form)
        return super().form_valid(form)


class ManagerAddNavigationEntry(ManagerNavigationEntryMixin, CreateView):
    pass


class ManagerEditNavigationEntry(ManagerNavigationEntryMixin, UpdateView):
    pass


class ManagerDeleteNavigationEntry(DeleteView):
    model = PwaNavigationEntry
    success_url = reverse_lazy('pwa-manager-homepage')
    template_name = 'combo/generic_confirm_delete.html'


def manager_navigation_order(request, *args, **kwargs):
    new_order = {int(x): i for i, x in enumerate(request.GET['new-order'].split(','))}
    for entry in PwaNavigationEntry.objects.all():
        entry.order = new_order[entry.id]
        entry.save()
    return JsonResponse({'err': 0})
