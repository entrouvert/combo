# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.apps
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class AppConfig(django.apps.AppConfig):
    name = 'combo.apps.pwa'

    def ready(self):
        from . import signals  # noqa pylint: disable=unused-import

    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def get_extra_manager_actions(self):
        from django.conf import settings

        from combo.utils.misc import is_portal_agent

        if settings.TEMPLATE_VARS.get('pwa_display') == 'standalone' and not is_portal_agent():
            return [{'href': reverse('pwa-manager-homepage'), 'text': _('Mobile Application (PWA)')}]
        return []
