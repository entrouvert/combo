# combo - content management system
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import PwaSettings


class PwaSettingsForm(forms.ModelForm):
    class Meta:
        model = PwaSettings
        exclude = ('push_notifications_infos',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['application_icon'].widget.attrs = {'accept': 'image/jpeg,image/png'}

    def clean_application_icon(self):
        value = self.cleaned_data.get('application_icon')
        if hasattr(value, 'content_type') and value.content_type not in ('image/jpeg', 'image/png'):
            raise ValidationError(_('The application icon must be in JPEG or PNG format.'))
        return value
