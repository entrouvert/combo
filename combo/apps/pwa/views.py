# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import json

from cryptography.hazmat.primitives import serialization
from django.conf import settings
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.template.loader import TemplateDoesNotExist, get_template
from django.utils.encoding import force_str
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from py_vapid import Vapid

from combo import VERSION

from .models import PushSubscription, PwaSettings


def manifest_json(request, *args, **kwargs):
    try:
        template = get_template('combo/manifest.json')
    except TemplateDoesNotExist:
        raise Http404()
    context = {
        'site_base': request.build_absolute_uri('/')[:-1],
    }
    return HttpResponse(template.render(context, request), content_type='application/json')


def js_response(request, template_name, **kwargs):
    template = get_template(template_name)
    pwa_vapid_public_key = None
    pwa_settings = PwaSettings.singleton()
    if pwa_settings.push_notifications:
        if settings.PWA_VAPID_PUBLIK_KEY:  # legacy
            pwa_vapid_public_key = settings.PWA_VAPID_PUBLIK_KEY
        elif hasattr(serialization.Encoding, 'X962'):
            pwa_vapid_public_key = force_str(
                base64.urlsafe_b64encode(
                    Vapid.from_pem(pwa_settings.push_notifications_infos['private_key'].encode('ascii'))
                    .private_key.public_key()
                    .public_bytes(
                        encoding=serialization.Encoding.X962,
                        format=serialization.PublicFormat.UncompressedPoint,
                    )
                ).strip(b'=')
            )
    context = {
        'pwa_vapid_public_key': pwa_vapid_public_key,
        'pwa_notification_badge_url': settings.PWA_NOTIFICATION_BADGE_URL,
        'pwa_notification_icon_url': settings.PWA_NOTIFICATION_ICON_URL,
    }
    context.update(kwargs)
    return HttpResponse(
        template.render(context, request), content_type='application/javascript; charset=utf-8'
    )


def service_worker_js(request, *args, **kwargs):
    pwa_settings = PwaSettings.singleton()
    if pwa_settings.id:
        version = '%s-%s' % (VERSION, pwa_settings.last_update_timestamp)
    else:
        version = VERSION
    return js_response(request, 'combo/service-worker.js', version=version)


def service_worker_registration_js(request, *args, **kwargs):
    return js_response(request, 'combo/service-worker-registration.js')


@csrf_exempt
def subscribe_push(request, *args, **kwargs):
    if not (request.user and request.user.is_authenticated):
        return HttpResponseForbidden()
    if request.method != 'POST':
        return HttpResponseForbidden()

    try:
        subscription_data = json.loads(force_str(request.body))
    except json.JSONDecodeError:
        return HttpResponseBadRequest('bad json request: "%s"' % request.body, content_type='text/plain')

    if not isinstance(subscription_data, dict) or not (set(subscription_data) >= {'keys', 'endpoint'}):
        return HttpResponseBadRequest('bad json request: "%s"' % subscription_data, content_type='text/plain')
    subscription, dummy = PushSubscription.objects.get_or_create(
        user=request.user, subscription_info=subscription_data
    )
    subscription.save()
    return JsonResponse({'err': 0})


class OfflinePage(TemplateView):
    template_name = 'combo/pwa/offline.html'


offline_page = OfflinePage.as_view()
