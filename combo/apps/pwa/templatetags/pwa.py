# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from combo.apps.pwa.models import PwaNavigationEntry

register = template.Library()


@register.simple_tag(takes_context=True)
def pwa_navigation(context):
    if settings.TEMPLATE_VARS.get('pwa_display') != 'standalone':
        return ''
    pwa_navigation_template = template.loader.get_template('combo/pwa/navigation.html')
    entries = list(PwaNavigationEntry.objects.all())
    context = {
        'page': context.get('page'),
        'entries': entries,
        'include_user_name': bool([x for x in entries if x.use_user_name_as_label]),
        'user': context.get('user'),
        'render_skeleton': context.get('render_skeleton'),
        'site_base': context['request'].build_absolute_uri('/')[:-1],
    }
    return mark_safe(pwa_navigation_template.render(context).strip())
