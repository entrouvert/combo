class WcsTriggerButton extends HTMLElement {
  triggerUrl

  connectedCallback () {
    this.addEventListener('click', this.onClick.bind(this))
    const label = this.getAttribute('label')
    this.unavailable = this.getAttribute('unavailable')
    this.innerHTML = `<button>${label}<i class="wcs-trigger-button--success-icon"></i></button>`
    if (this.unavailable !== null) {
      const unavailableMode = this.getAttribute('unavailable-mode')
      if (unavailableMode === 'hide') {
        this.hidden = true
      } else {
        const button = this.querySelector('button')
        button.disabled = true
      }
    }
  }

  async onClick () {
    const confirmationMessage = this.getAttribute('confirmation-message')

    if (confirmationMessage && !confirm(confirmationMessage)) {
      return
    }

    const placeholder = this.closest('.cell')
    const extraContext = placeholder && placeholder.getAttribute('data-extra-context')
    const extraUrlParams = placeholder && placeholder.getAttribute('data-extra-url-params')
    const actionUrl = this.getAttribute('card-action-url')
    let url = actionUrl || ''
    let qs = ''

    if (window.location.search) {
      qs += window.location.search.slice(1)
    }
    if (extraContext) {
      if (qs) {
        qs += '&'
      }
      qs += 'ctx=' + extraContext
    }
    if (extraUrlParams) {
      if (qs) {
        qs += '&'
      }
      qs += extraUrlParams
    }
    if (qs) {
      if (url.includes('?')) {
        url += '&' + qs
      } else {
        url += '?' + qs
      }
    }

    const formdata = new FormData()
    formdata.append('card_id', this.getAttribute('card-id'))
    formdata.append('trigger_id', this.getAttribute('card-trigger-id'))
    const response = await fetch(
      url,
      {
        method: 'POST',
        mode: 'cors',
        body: formdata,
        credentials: 'same-origin',
        headers: {
          'X-CSRFToken': this.getAttribute('csrf-token'),
        },
      },
    )

    let error = !response.ok
    if (!error) {
      try {
        const json = await response.json()
        if (json.err) {
          error = true
        }
      } catch (e) {
        error = true
      }
    }

    const button = this.querySelector('button')
    if (error) {
      alert(this.getAttribute('error-message'))
      button.disabled = false
    } else {
      this.classList.add('success')
      button.disabled = true
    }
  }
}

window.customElements.define('wcs-trigger-button', WcsTriggerButton)

