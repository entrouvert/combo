$(function () {
    $(window).on('pageshow', update_submission_href);

    $(document).on('combo:cell-loaded combo:submission-context-update', update_submission_href);

    $(window).on('storage', function (event) {
        if (event.originalEvent != undefined && event.originalEvent.key.includes('wcs_submission_context')) {
            $('document').trigger('combo:submission-context-update');
        }
    });

    function update_submission_href() {
        const channel = window.sessionStorage.wcs_submission_context_channel;
        let url;

        $('.wcs-bo-submission-link').each(function (i, anchor) {
            if (anchor.orig_href != undefined) {
                url = anchor.orig_href;
            } else {
                url = anchor.href;
                anchor.orig_href = anchor.href;
            }
            if (channel == 'phone') {
                if (url.includes('?')) {
                    url += '&';
                } else {
                    url += '?';
                }
                url += 'channel=' + encodeURIComponent(channel);
                let caller = window.sessionStorage.wcs_submission_context_caller;
                if (caller != undefined) {
                    url += '&caller=' + encodeURIComponent(window.sessionStorage.wcs_submission_context_caller);
                }
            } else {
                /* clean the localstorage to prevent mismatches later */
                let keys_to_delete = [];
                for (let i = 0; i < window.sessionStorage.length; i++) {
                    const key = window.sessionStorage.key(i);
                    if (key.includes('wcs_submission_context_')) {
                        keys_to_delete.push(key);
                    }
                }
                for (let i = 0; i < keys_to_delete.length; i++) {
                    window.sessionStorage.removeItem(keys_to_delete[i]);
                }
            }
            anchor.href = url;
        })
    }
});
