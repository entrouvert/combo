$(function () {
  var display_function = function($element, data) {
    let cell_ref;
    $.each($element.attr("class").split(/\s+/), function(index, class_name) {
      if (class_name.startsWith('wcs_wcscardcell-')) {
        cell_ref = class_name
      }
    })
    // for card mode, remove cells, pagination and filters
    $('.cell-cards--copy.card.' + cell_ref).remove()
    $('.cell-cards--pagination[data-cell-reference=' + cell_ref + ']').remove()
    $('.cell-cards--filters[data-cell-reference=' + cell_ref + '] select').each(function () {
      if ($(this).data('select2-id')) {
        $(this).select2('destroy')
      }
    })
    $('.cell-cards--filters[data-cell-reference=' + cell_ref + ']').remove()
    var $data = $(data)
    var nav_elements_found = false
    if ($data.find('.cell-cards--filters').length) {
      $data.find('.cell-cards--filters').insertBefore($element)
        nav_elements_found = true
    }
    if ($data.find('.cell-cards--pagination').length) {
      var $pagination = $data.find('.cell-cards--pagination').insertAfter($element)
      $(document).trigger('combo:cell-loaded', $pagination)
        nav_elements_found = true
    }
    if ($data.find('.cell-cards--card').length) {
      $data.find('.cell-cards--card').each(function (index, card) {
        var $copy = $element.clone().addClass('cell-cards--copy').html(card).show()
        $copy.insertBefore($element)
        $(document).trigger('combo:cell-loaded', $copy)
      })
      $element.hide()
    } else if (!nav_elements_found) {
      $element.find('> div').html(data);
    }
  }

  $(document).on('combo:cell-loaded', function (ev, cell) {
    let $pagination = $(cell).find('.cell-cards--items-pagination')
    if ($pagination.length == 0) return
    let wrapper = $pagination.parent()
    wrapper.attr('tabindex', -1)
    let offset = parseInt($pagination.data('offset'), 10)
    let paginate_by = parseInt($pagination.data('paginate-by'), 10)
    let cell_ref = wrapper.data('cell-reference')

    function update_page (offset) {
        let items = $('.card.' + cell_ref)
        if (!items.length) {
          return
        }
        let card_cell = $(items[items.length - 1])
        let item = $(this)
        let params
        if (offset == 0) {
          params = {offset: 0}
        } else {
          params = {offset: $(item).data('offset')}
        }
        $('input,select', $('.cell-cards--filters[data-cell-reference=' + cell_ref + ']')).each(function() {
          let $field = $(this)
          let value = $field.val()
          if (value.length) {
            params[$field.prop('name')] = value
          }
        })
        card_cell.data('extra-url-params', $.param(params))

        // and load page
        combo_cell_loader({
          "element": card_cell,
          "display_function": display_function,
          "dismiss_location_search": card_cell.hasClass('ajax-loaded')
        })
    }

    $pagination.find('.cell-cards--items-pagination-item').each(function () {
      item = $(this)
      item.off('click')
      if (this.dataset.offset) {
        item.click(update_page)
      }
    })

    $('.cell-cards--filters[data-cell-reference=' + cell_ref + '] .cell-cards--filters-list select').change(function() {
      update_page(0)
    })
    $('.cell-cards--filters[data-cell-reference=' + cell_ref + ']').submit(function(e) {
      update_page(0)
      e.preventDefault()
    })
  })
  $('.cell.card [data-ajax-cell-must-load]').each(function(idx, elem) {
    combo_cell_loader({"element": $(elem).parents('div.cell')[0], "display_function": display_function});
  });
})
