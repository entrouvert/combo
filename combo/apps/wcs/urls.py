# combo - content management system
# Copyright (C) 2014-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from .views import TrackingCodeView, redirect_crypto_url, tracking_code_search

urlpatterns = [
    path('tracking-code/', TrackingCodeView.as_view(), name='wcs-tracking-code'),
    path('api/search/tracking-code/', tracking_code_search, name='wcs-tracking-code-search'),
    re_path(
        r'^api/wcs/file/(?P<session_key>\w+)/(?P<crypto_url>[\w,-]+)/$',
        redirect_crypto_url,
        name='wcs-redirect-crypto-url',
    ),
]
