# combo - content management system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib

import django.apps
from django.conf import settings
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy


class AppConfig(django.apps.AppConfig):
    name = 'combo.apps.wcs'
    verbose_name = _('Forms')

    def ready(self):
        from combo.apps.search.engines import engines

        engines.register(self.get_search_engines)

    def get_search_engines(self):
        from .utils import get_wcs_services

        wcs_services = get_wcs_services()
        if not wcs_services:
            return

        engines = self.get_portal_agent_search_engines(wcs_services)
        engines.update(self.get_card_search_engines(wcs_services))
        engines.update(self.get_formdefs_engines(wcs_services))

        return engines

    def get_formdefs_engines(self, wcs_services):
        engines = {}
        for key, service in wcs_services.items():
            if len(wcs_services.keys()) == 1:
                label = _('Available forms')
            else:
                label = _('Available forms (%s)') % service['title']
            engines['formdefs:%s' % (hashlib.md5(force_bytes(key)).hexdigest()[:8])] = {
                'url': (service['url'] + 'api/formdefs/?NameID={{ user_nameid }}&q=%(q)s'),
                'label': label,
                'signature': True,
                'hit_url_template': '{{ url }}'
                '{% if cell_context.absolute_uri %}?ReturnURL={{ cell_context.absolute_uri|iriencode }}{% endif %}',
                'hit_label_template': '{{ title }}',
            }
        return engines

    def get_backoffice_submission_engines(self, wcs_services):
        engines = {}
        for key, service in wcs_services.items():
            if len(wcs_services.keys()) == 1:
                label = _('Backoffice submission')
            else:
                label = _('Backoffice submission (%s)') % service['title']
            engines['backoffice-submission:%s' % (hashlib.md5(force_bytes(key)).hexdigest()[:8])] = {
                'url': (
                    service['url'] + 'api/formdefs/?backoffice-submission=true'
                    '&NameID={{ user_nameid }}&q=%(q)s'
                ),
                'label': label,
                'signature': True,
                'hit_url_template': '{{ backoffice_submission_url }}?'
                '{% if cell_context.name_id %}NameID={{ cell_context.name_id }}&{% endif %}'
                '{% if cell_context.absolute_uri %}ReturnURL={{ cell_context.absolute_uri|iriencode }}{% endif %}',
                'hit_label_template': '{{ title }}',
            }
        return engines

    def get_card_search_engines(self, wcs_services):
        from combo.data.models import Page

        from .utils import get_matching_pages_from_card_slug, get_wcs_json

        pages_with_sub_slug = Page.objects.exclude(sub_slug='')
        if not pages_with_sub_slug:
            return {}

        engines = {}
        for key, service in wcs_services.items():
            card_models = get_wcs_json(service, 'api/cards/@list')
            for card in card_models.get('data') or []:
                matching_pages = get_matching_pages_from_card_slug(card['id'])
                if not matching_pages:
                    continue
                card_page = matching_pages[0]
                card_page_base_url = card_page.get_online_url()
                label = card['text']
                if len(wcs_services.keys()) > 1:
                    label = '%s (%s)' % (label, service['title'])
                engines['cards:%s:%s' % (hashlib.md5(force_bytes(key)).hexdigest()[:8], card['id'])] = {
                    'url': (
                        service['url'] + 'api/cards/' + card['id'] + '/list/'
                        '{% if search_service.selected_custom_view %}{{ search_service.selected_custom_view }}{% endif %}'
                        '?{% if not search_service.without_user %}NameID={{ user_nameid }}&{% endif %}'
                        'q=%(q)s'
                    ),
                    'custom_views': card.get('custom_views') or [],
                    'label': label,
                    'signature': True,
                    'hit_url_template': '%s{{ id }}/' % card_page_base_url,
                    'hit_label_template': '{% firstof digest text %}',
                }
        return engines

    def get_portal_agent_search_engines(self, wcs_services):
        if not settings.TEMPLATE_VARS.get('is_portal_agent'):
            return {}

        engines = {
            'tracking-code': {
                'url': reverse('wcs-tracking-code-search') + '?q=%(q)s',
                'label': _('Tracking Code'),
            }
        }
        engines.update(self.get_backoffice_submission_engines(wcs_services))

        for key, service in wcs_services.items():
            label = pgettext_lazy('user-forms', 'Forms')
            if len(wcs_services.keys()) > 1:
                label = pgettext_lazy('user-forms', 'Forms (%s)') % service['title']
            engines['formdata:%s' % hashlib.md5(force_bytes(key)).hexdigest()[:8]] = {
                'url': service['url']
                + 'api/forms/?NameID={{user_nameid}}&status=all&ignore-roles=on&include-anonymised=off&q=%(q)s',
                'label': label,
                'signature': True,
                'hit_url_template': '{% if readable %}{{ form_url_backoffice }}{% endif %}',
                'hit_label_template': '{{ title }}',
                'hit_description_template': '{{ form_digest|default:"" }}',
            }
        return engines

    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def hourly(self):
        from combo.data.library import get_cell_classes
        from combo.data.models import CellBase

        cell_classes = [c for c in self.get_models() if c in get_cell_classes()]
        for cell in CellBase.get_cells(cell_filter=lambda x: x in cell_classes, page__snapshot__isnull=True):
            if hasattr(cell, 'check_validity'):
                cell.check_validity()

        self.update_db_cache()

    def update_db_cache(self):
        from combo.data.models import CellBase

        from .models import WcsCardCell, WcsCategoryCell, WcsFormCell, WcsFormsOfCategoryCell

        models_to_update = [
            WcsFormCell,
            WcsCategoryCell,
            WcsFormsOfCategoryCell,
            WcsCardCell,
        ]
        for cell in CellBase.get_cells(cell_filter=lambda x: x in models_to_update):
            cell.save()
