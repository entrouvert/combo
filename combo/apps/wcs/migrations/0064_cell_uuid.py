from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0063_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backofficesubmissioncell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='categoriescell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='trackingcodeinputcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcscardcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcscareformscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcscategorycell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcscurrentdraftscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcscurrentformscell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcsformcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
        migrations.AlterField(
            model_name='wcsformsofcategorycell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
