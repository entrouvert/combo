from django.db import migrations


def forwards(apps, schema_editor):
    WcsCardCell = apps.get_model('wcs', 'WcsCardCell')

    for cell in WcsCardCell.objects.order_by('pk'):
        if not cell.custom_schema:
            continue
        if cell.display_mode != 'table':
            continue
        if len(cell.custom_schema.get('cells')) != 1:
            continue
        cell.display_mode = 'list'
        cell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0058_care_forms_by_card'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
