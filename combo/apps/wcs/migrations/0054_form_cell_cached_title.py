from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0053_new_card_cell'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wcsformcell',
            name='cached_title',
            field=models.CharField(max_length=250, verbose_name='Title'),
        ),
    ]
