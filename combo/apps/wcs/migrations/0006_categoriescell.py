from django.db import migrations, models

import combo.apps.wcs.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0005_auto_20150226_0903'),
        ('wcs', '0005_auto_20150401_0758'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoriesCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('wcs_site', models.CharField(max_length=50, verbose_name='Site', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Form Categories',
            },
            bases=(models.Model, combo.apps.wcs.models.WcsBlurpMixin),
        ),
    ]
