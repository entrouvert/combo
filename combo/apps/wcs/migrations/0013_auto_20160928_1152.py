from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0012_wcsformsofcategorycell_manual_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoriescell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='trackingcodeinputcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='wcscategorycell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='wcscurrentdraftscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='wcsformcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='wcsformsofcategorycell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
