from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0046_display_condition'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscareformscell',
            name='custom_title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Custom Title'),
        ),
    ]
