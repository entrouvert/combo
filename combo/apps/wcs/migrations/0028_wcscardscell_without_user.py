from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0027_careforms_categories'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardscell',
            name='without_user',
            field=models.BooleanField(default=False, verbose_name='Ignore the logged-in user'),
        ),
    ]
