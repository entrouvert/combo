import django.db.models.deletion
from django.db import migrations, models

import combo.apps.wcs.models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0045_link_list_limit'),
        ('auth', '0008_alter_user_username_max_length'),
        ('wcs', '0019_wcscurrentformscell_include_drafts'),
    ]

    operations = [
        migrations.CreateModel(
            name='WcsCardsCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=100, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('carddef_reference', models.CharField(max_length=150, verbose_name='Card Model')),
                ('cached_title', models.CharField(max_length=150, verbose_name='Title')),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Page')),
            ],
            options={
                'verbose_name': 'Cards',
            },
            bases=(models.Model, combo.apps.wcs.models.WcsBlurpMixin),
        ),
    ]
