from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0061_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='backofficesubmissioncell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='backofficesubmissioncell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='categoriescell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='categoriescell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='trackingcodeinputcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='trackingcodeinputcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcscardcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcscardcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcscareformscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcscareformscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcscategorycell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcscategorycell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcscurrentdraftscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcscurrentdraftscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcscurrentformscell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcsformcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcsformcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='wcsformsofcategorycell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='wcsformsofcategorycell',
            unique_together={('page', 'uuid')},
        ),
    ]
