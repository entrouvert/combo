from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0039_card_title_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardscell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                blank=True, null=True, verbose_name='Number of cards per page (default 10)'
            ),
        ),
    ]
