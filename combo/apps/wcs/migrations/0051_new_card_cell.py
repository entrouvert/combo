import django.db.models.deletion
from django.db import migrations, models

import combo.apps.wcs.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
        ('data', '0057_pagesnapshot_label'),
        ('wcs', '0050_card_display_mode'),
    ]

    operations = [
        migrations.CreateModel(
            name='WcsCardCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=100, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                (
                    'template_name',
                    models.CharField(blank=True, max_length=50, null=True, verbose_name='Cell Template'),
                ),
                (
                    'condition',
                    models.CharField(
                        blank=True, max_length=1000, null=True, verbose_name='Display condition'
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('carddef_reference', models.CharField(max_length=150, verbose_name='Card Model')),
                (
                    'related_card_path',
                    models.CharField(
                        blank=True, max_length=1000, verbose_name='Card(s) to display', default='__all__'
                    ),
                ),
                (
                    'card_ids',
                    models.CharField(blank=True, max_length=1000, verbose_name='Other Card Identifiers'),
                ),
                (
                    'only_for_user',
                    models.BooleanField(
                        default=False, verbose_name='Limit to cards linked to the logged-in user'
                    ),
                ),
                (
                    'without_user',
                    models.BooleanField(default=False, verbose_name='Ignore the logged-in user'),
                ),
                (
                    'limit',
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name='Number of cards per page (default 10)'
                    ),
                ),
                ('custom_schema', models.JSONField(blank=True, default=dict)),
                (
                    'display_mode',
                    models.CharField(
                        choices=[('card', 'Card'), ('table', 'Table'), ('list', 'List')],
                        default='card',
                        max_length=10,
                        verbose_name='Display mode',
                    ),
                ),
                (
                    'title_type',
                    models.CharField(
                        choices=[
                            ('auto', 'Default Title (Card Label)'),
                            ('manual', 'Custom Title'),
                            ('empty', 'No Title'),
                        ],
                        default='auto',
                        max_length=20,
                        verbose_name='Title',
                    ),
                ),
                ('custom_title', models.CharField(blank=True, max_length=150, verbose_name='Custom Title')),
                ('cached_title', models.CharField(max_length=150, verbose_name='Title')),
                ('cached_json', models.JSONField(blank=True, default=dict)),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Page')),
            ],
            options={
                'verbose_name': 'Card(s)',
            },
            bases=(combo.apps.wcs.models.CardMixin, models.Model),
        ),
    ]
