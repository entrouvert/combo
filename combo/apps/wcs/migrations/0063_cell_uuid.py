import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in [
        'WcsFormCell',
        'WcsCategoryCell',
        'WcsCurrentFormsCell',
        'WcsCurrentDraftsCell',
        'WcsFormsOfCategoryCell',
        'WcsCareFormsCell',
        'CategoriesCell',
        'WcsCardCell',
        'TrackingCodeInputCell',
        'BackofficeSubmissionCell',
    ]:
        klass = apps.get_model('wcs', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0062_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
