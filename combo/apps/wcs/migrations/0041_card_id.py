from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0040_cards_limit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wcscardinfoscell',
            name='card_id',
            field=models.CharField(blank=True, max_length=1000, verbose_name='Card Identifier'),
        ),
    ]
