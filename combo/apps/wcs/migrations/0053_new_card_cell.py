from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0052_new_card_cell'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wcscardscell',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='wcscardscell',
            name='page',
        ),
        migrations.DeleteModel(
            name='WcsCardInfosCell',
        ),
        migrations.DeleteModel(
            name='WcsCardsCell',
        ),
    ]
