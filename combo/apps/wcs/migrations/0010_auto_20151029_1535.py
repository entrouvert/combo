from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0009_remove_wcscategorycell_link_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoriescell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='trackingcodeinputcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscategorycell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscurrentdraftscell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcsformcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcsformsofcategorycell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
    ]
