from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0008_wcsformcell_cached_json'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wcscategorycell',
            name='link_page',
        ),
    ]
