from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0023_card_info_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='without_user',
            field=models.BooleanField(default=False, verbose_name='Ignore the logged-in user'),
        ),
    ]
