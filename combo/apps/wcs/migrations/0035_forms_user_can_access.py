from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0034_revert_forms_user_can_access'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='include_forms_user_can_access',
            field=models.BooleanField(
                default=False, verbose_name='Include forms to which the user can access'
            ),
        ),
    ]
