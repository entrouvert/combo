from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0004_auto_20150218_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wcscurrentdraftscell',
            name='wcs_site',
            field=models.CharField(max_length=50, verbose_name='Site', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcscurrentformscell',
            name='wcs_site',
            field=models.CharField(max_length=50, verbose_name='Site', blank=True),
            preserve_default=True,
        ),
    ]
