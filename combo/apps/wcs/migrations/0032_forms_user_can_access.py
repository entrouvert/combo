from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0046_text_to_jsonb'),
        ('auth', '0008_alter_user_username_max_length'),
        ('wcs', '0031_auto_20210723_1318'),
    ]

    operations = [
        migrations.CreateModel(
            name='WcsFormsUserCanAccessCell',
            fields=[],  # this model was reverted
        ),
    ]
