from django.db import migrations
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0007_trackingcodeinputcell'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcsformcell',
            name='cached_json',
            field=JSONField(default=dict, blank=True),
            preserve_default=True,
        ),
    ]
