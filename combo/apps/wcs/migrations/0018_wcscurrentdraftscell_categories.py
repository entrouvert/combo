from django.db import migrations
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0017_wcscareformscell'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscurrentdraftscell',
            name='categories',
            field=JSONField(blank=True, default=dict, verbose_name='Categories'),
        ),
    ]
