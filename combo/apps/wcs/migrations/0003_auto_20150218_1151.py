from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0002_auto_20150217_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wcscategorycell',
            name='category_reference',
            field=models.CharField(max_length=150, verbose_name='Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcsformcell',
            name='formdef_reference',
            field=models.CharField(max_length=150, verbose_name='Form'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcsformsofcategorycell',
            name='category_reference',
            field=models.CharField(max_length=150, verbose_name='Category'),
            preserve_default=True,
        ),
    ]
