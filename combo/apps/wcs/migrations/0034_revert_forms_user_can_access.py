from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0033_card_custom_schema'),
    ]

    operations = [
        migrations.DeleteModel(
            name='WcsFormsUserCanAccessCell',
        ),
    ]
