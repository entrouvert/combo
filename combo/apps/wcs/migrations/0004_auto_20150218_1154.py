from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0003_auto_20150218_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wcscategorycell',
            name='cached_title',
            field=models.CharField(max_length=150, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcsformcell',
            name='cached_title',
            field=models.CharField(max_length=150, verbose_name='Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcsformsofcategorycell',
            name='cached_title',
            field=models.CharField(max_length=150, verbose_name='Title'),
            preserve_default=True,
        ),
    ]
