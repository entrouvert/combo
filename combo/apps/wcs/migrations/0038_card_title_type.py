from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0037_forms_user_can_access'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='title_type',
            field=models.CharField(
                choices=[
                    ('auto', 'Default Title (Card Label)'),
                    ('manual', 'Custom Title'),
                    ('empty', 'No Title'),
                ],
                default='auto',
                max_length=20,
                verbose_name='Title',
            ),
        ),
    ]
