from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscategorycell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscurrentdraftscell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcsformcell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcsformsofcategorycell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
    ]
