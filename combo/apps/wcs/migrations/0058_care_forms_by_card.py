from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0057_user_forms_card_related'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscareformscell',
            name='filter_by_card',
            field=models.BooleanField(
                default=False, verbose_name='Only display forms related to the card linked to this page'
            ),
        ),
    ]
