from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0056_wcscardcell_inline_filters'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='filter_by_card',
            field=models.BooleanField(
                default=False, verbose_name='Only display forms related to the card linked to this page'
            ),
        ),
    ]
