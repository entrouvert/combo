from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0029_cards_custom_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='custom_title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Custom Title'),
        ),
    ]
