from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0028_wcscardscell_without_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardscell',
            name='custom_title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Custom Title'),
        ),
    ]
