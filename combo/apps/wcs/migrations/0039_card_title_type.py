from django.db import migrations


def update_title_type(apps, schema_editor):
    WcsCardInfosCell = apps.get_model('wcs', 'WcsCardInfosCell')
    for cell in WcsCardInfosCell.objects.exclude(custom_title=''):
        cell.title_type = 'manual'
        cell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0038_card_title_type'),
    ]

    operations = [
        migrations.RunPython(update_title_type, migrations.RunPython.noop),
    ]
