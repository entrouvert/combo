from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0010_auto_20151029_1535'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='wcscurrentformscell',
            options={'verbose_name': 'User Forms'},
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='current_forms',
            field=models.BooleanField(default=True, verbose_name='Current Forms'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='done_forms',
            field=models.BooleanField(default=False, verbose_name='Done Forms'),
            preserve_default=True,
        ),
    ]
