from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0018_wcscurrentdraftscell_categories'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='include_drafts',
            field=models.BooleanField(default=False, verbose_name='Include drafts'),
        ),
    ]
