from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0047_careforms_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                blank=True, null=True, verbose_name='Number of cards per page (default 10)'
            ),
        ),
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='only_for_user',
            field=models.BooleanField(
                default=False, verbose_name='Limit to cards linked to the logged-in user'
            ),
        ),
    ]
