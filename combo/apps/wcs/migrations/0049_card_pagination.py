from django.db import migrations


def update_only_for_user(apps, schema_editor):
    WcsCardInfosCell = apps.get_model('wcs', 'WcsCardInfosCell')
    for cell in WcsCardInfosCell.objects.all():
        if not cell.carddef_reference:
            continue
        parts = cell.carddef_reference.split(':')
        if len(parts) <= 2:
            continue
        cell.only_for_user = True
        cell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0048_card_pagination'),
    ]

    operations = [
        migrations.RunPython(update_only_for_user, migrations.RunPython.noop),
    ]
