from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0042_current_forms_custom_title'),
    ]

    operations = [
        migrations.RenameField(
            model_name='wcscardinfoscell',
            old_name='card_id',
            new_name='card_ids',
        ),
        migrations.AlterField(
            model_name='wcscardinfoscell',
            name='card_ids',
            field=models.CharField(blank=True, max_length=1000, verbose_name='Other Card Identifiers'),
        ),
    ]
