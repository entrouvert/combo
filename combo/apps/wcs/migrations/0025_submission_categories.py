from django.db import migrations
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0024_card_info_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='backofficesubmissioncell',
            name='categories',
            field=JSONField(blank=True, default=dict, verbose_name='Categories'),
        ),
    ]
