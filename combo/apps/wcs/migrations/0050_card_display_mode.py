from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0049_card_pagination'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='display_mode',
            field=models.CharField(
                choices=[('card', 'Card'), ('table', 'Table')],
                default='card',
                max_length=10,
                verbose_name='Display mode',
            ),
        ),
    ]
