from django.db import migrations, models

import combo.apps.wcs.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WcsCategoryCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('category_reference', models.CharField(max_length=100, verbose_name='Category')),
                ('cached_title', models.CharField(max_length=50, verbose_name='Title')),
                ('cached_description', models.TextField(verbose_name='Description', blank=True)),
                ('cached_url', models.URLField(verbose_name='Cached URL')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                (
                    'link_page',
                    models.ForeignKey(
                        related_name='link', to='data.Page', null=True, on_delete=models.CASCADE
                    ),
                ),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Category Link',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WcsCurrentDraftsCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('wcs_site', models.CharField(max_length=50, verbose_name='Site')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Current Drafts',
            },
            bases=(models.Model, combo.apps.wcs.models.WcsBlurpMixin),
        ),
        migrations.CreateModel(
            name='WcsCurrentFormsCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('wcs_site', models.CharField(max_length=50, verbose_name='Site')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Current Forms',
            },
            bases=(models.Model, combo.apps.wcs.models.WcsBlurpMixin),
        ),
        migrations.CreateModel(
            name='WcsFormCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('formdef_reference', models.CharField(max_length=100, verbose_name='Form')),
                ('cached_title', models.CharField(max_length=50, verbose_name='Title')),
                ('cached_url', models.URLField(verbose_name='URL')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Form Link',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WcsFormsOfCategoryCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('category_reference', models.CharField(max_length=100, verbose_name='Category')),
                ('cached_title', models.CharField(max_length=50, verbose_name='Title')),
                ('cached_description', models.TextField(verbose_name='Description', blank=True)),
                ('cached_url', models.URLField(verbose_name='Cached URL')),
                (
                    'ordering',
                    models.CharField(
                        default=b'',
                        max_length=20,
                        verbose_name='Order',
                        blank=True,
                        choices=[(b'', 'Default'), (b'alpha', 'Alphabetical'), (b'popularity', 'Popularity')],
                    ),
                ),
                ('limit', models.PositiveSmallIntegerField(null=True, verbose_name='Limit', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Forms of Category',
            },
            bases=(models.Model, combo.apps.wcs.models.WcsBlurpMixin),
        ),
    ]
