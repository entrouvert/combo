from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0021_card'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardscell',
            name='only_for_user',
            field=models.BooleanField(
                default=False, verbose_name='Limit to cards linked to the logged-in user'
            ),
        ),
    ]
