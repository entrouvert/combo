from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0022_cards_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='card_id',
            field=models.CharField(blank=True, max_length=150, verbose_name='Card Identifier'),
        ),
    ]
