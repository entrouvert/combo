from django.db import migrations


def forwards(apps, schema_editor):
    WcsCardsCell = apps.get_model('wcs', 'WcsCardsCell')
    WcsCardInfosCell = apps.get_model('wcs', 'WcsCardInfosCell')
    WcsCardCell = apps.get_model('wcs', 'WcsCardCell')

    # migrate cards cell
    for cell in WcsCardsCell.objects.order_by('pk'):
        new_cell = WcsCardCell(
            # base cell config
            page=cell.page,
            placeholder=cell.placeholder,
            order=cell.order,
            slug=cell.slug,
            extra_css_class=cell.extra_css_class,
            template_name=cell.template_name,
            condition=cell.condition,
            public=cell.public,
            restricted_to_unlogged=cell.restricted_to_unlogged,
            last_update_timestamp=cell.last_update_timestamp,
            # cell config
            carddef_reference=cell.carddef_reference,
            related_card_path='__all__',
            card_ids='',
            only_for_user=cell.only_for_user,
            without_user=cell.without_user,
            limit=cell.limit,
            custom_schema={},
            display_mode='table',
            title_type='auto',
            custom_title='',
            cached_title=cell.cached_title,
        )
        if cell.custom_title:
            new_cell.title_type = 'manual'
            new_cell.custom_title = cell.custom_title
        new_cell.save()
        new_cell.groups.set(cell.groups.all())
        # validity infos are lost

        # update page's related_cells cache
        if 'wcs_wcscardcell' not in new_cell.page.related_cells.get('cell_types', []):
            new_cell.page.related_cells['cell_types'].append('wcs_wcscardcell')
            new_cell.page.save()

    # migrate card cell
    for cell in WcsCardInfosCell.objects.order_by('pk'):
        new_cell = WcsCardCell(
            # base cell config
            page=cell.page,
            placeholder=cell.placeholder,
            order=cell.order,
            slug=cell.slug,
            extra_css_class=cell.extra_css_class,
            template_name=cell.template_name,
            condition=cell.condition,
            public=cell.public,
            restricted_to_unlogged=cell.restricted_to_unlogged,
            last_update_timestamp=cell.last_update_timestamp,
            # cell config
            carddef_reference=cell.carddef_reference,
            related_card_path=cell.related_card_path,
            card_ids=cell.card_ids,
            only_for_user=cell.only_for_user,
            without_user=cell.without_user,
            limit=cell.limit,
            custom_schema=cell.custom_schema,
            display_mode=cell.display_mode,
            title_type=cell.title_type,
            custom_title=cell.custom_title,
            cached_title=cell.cached_title,
            cached_json=cell.cached_json,
        )

        new_cell.save()
        new_cell.groups.set(cell.groups.all())
        # validity infos are lost

        # update page's related_cells cache
        if 'wcs_wcscardcell' not in new_cell.page.related_cells.get('cell_types', []):
            new_cell.page.related_cells['cell_types'].append('wcs_wcscardcell')
            new_cell.page.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0051_new_card_cell'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
