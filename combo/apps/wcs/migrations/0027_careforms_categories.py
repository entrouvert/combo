from django.db import migrations
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0026_text_to_jsonb'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscareformscell',
            name='categories',
            field=JSONField(blank=True, default=dict, verbose_name='Categories'),
        ),
    ]
