from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0041_card_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscurrentformscell',
            name='custom_title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Custom Title'),
        ),
    ]
