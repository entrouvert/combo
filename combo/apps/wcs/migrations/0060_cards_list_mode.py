from django.db import migrations


def forwards(apps, schema_editor):
    WcsCardCell = apps.get_model('wcs', 'WcsCardCell')

    for cell in WcsCardCell.objects.order_by('pk'):
        if cell.display_mode != 'table':
            continue
        if cell.custom_schema:
            continue
        cell.display_mode = 'list'
        cell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0059_cards_list_mode'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
