from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0043_card_ids'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcscardinfoscell',
            name='related_card_path',
            field=models.CharField(blank=True, max_length=1000, verbose_name='Card Identifier'),
        ),
    ]
