import django.db.models.deletion
from django.db import migrations, models
from django.db.models import JSONField

import combo.apps.wcs.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('data', '0045_link_list_limit'),
        ('wcs', '0020_cards'),
    ]

    operations = [
        migrations.CreateModel(
            name='WcsCardInfosCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=100, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('carddef_reference', models.CharField(max_length=150, verbose_name='Card Model')),
                ('cached_title', models.CharField(max_length=150, verbose_name='Title')),
                ('cached_json', JSONField(blank=True, default=dict)),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Page')),
            ],
            options={
                'verbose_name': 'Card Information Cell',
            },
            bases=(combo.apps.wcs.models.CardMixin, models.Model),
        ),
    ]
