from django.db import migrations, models
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('wcs', '0011_auto_20151215_1121'),
    ]

    operations = [
        migrations.AddField(
            model_name='wcsformsofcategorycell',
            name='manual_order',
            field=JSONField(
                default=dict,
                help_text='Use drag and drop to reorder forms',
                verbose_name='Manual Order',
                blank=True,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wcsformsofcategorycell',
            name='ordering',
            field=models.CharField(
                default='alpha',
                max_length=20,
                verbose_name='Order',
                choices=[
                    ('alpha', 'Default (alphabetical)'),
                    ('popularity', 'Popularity'),
                    ('manual', 'Manual'),
                ],
            ),
            preserve_default=True,
        ),
    ]
