# combo - content management system
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from combo.urls_utils import decorated_includes, staff_required

from . import manager_views
from .views import GeojsonView, geocoding_view

maps_manager_urls = [
    path('', manager_views.ManagerHomeView.as_view(), name='maps-manager-homepage'),
    re_path(
        '^layers/add/(?P<kind>geojson|tiles)/$',
        manager_views.LayerAddView.as_view(),
        name='maps-manager-layer-add',
    ),
    re_path(
        r'^layers/(?P<slug>[\w-]+)/edit/$',
        manager_views.LayerEditView.as_view(),
        name='maps-manager-layer-edit',
    ),
    re_path(
        r'^layers/(?P<slug>[\w-]+)/edit/request-parameters/$',
        manager_views.LayerEditRequestParametersView.as_view(),
        name='maps-manager-layer-edit-request-parameters',
    ),
    re_path(
        r'^layers/(?P<slug>[\w-]+)/delete/$',
        manager_views.LayerDeleteView.as_view(),
        name='maps-manager-layer-delete',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/add-layer/(?P<kind>geojson|tiles)/$',
        manager_views.map_cell_add_layer,
        name='maps-manager-cell-add-layer',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/layer/(?P<layeroptions_pk>\d+)/edit/$',
        manager_views.map_cell_edit_layer,
        name='maps-manager-cell-edit-layer',
    ),
    re_path(
        r'^pages/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/layer/(?P<layeroptions_pk>\d+)/delete/$',
        manager_views.map_cell_delete_layer,
        name='maps-manager-cell-delete-layer',
    ),
]

urlpatterns = [
    re_path(r'^manage/maps/', decorated_includes(staff_required, include(maps_manager_urls))),
    re_path(
        r'^ajax/mapcell/geojson/(?P<cell_id>\d+)/(?P<layer_slug>[\w-]+)/$',
        GeojsonView.as_view(),
        name='mapcell-geojson',
    ),
    path('api/geocoding', geocoding_view, name='mapcell-geocoding'),
]
