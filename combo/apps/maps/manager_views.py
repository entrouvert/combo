# combo - content management system
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from combo.data.models import CellBase, PageSnapshot

from .forms import MapLayerForm, MapLayerOptionsForm, MapLayerRequestParametersForm
from .models import Map, MapLayer, MapLayerOptions


class MapLayerMixin:
    model = MapLayer
    success_url = reverse_lazy('maps-manager-homepage')


class ManagerHomeView(MapLayerMixin, ListView):
    template_name = 'maps/manager_home.html'
    ordering = ['kind', 'label']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['map_list'] = Map.objects.all()
        return context


class LayerAddView(MapLayerMixin, CreateView):
    form_class = MapLayerForm
    template_name = 'maps/map_layer_form.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.model(kind=self.kwargs['kind'])
        return kwargs


class LayerEditView(MapLayerMixin, UpdateView):
    form_class = MapLayerForm
    template_name = 'maps/map_layer_form.html'


class LayerEditRequestParametersView(MapLayerMixin, UpdateView):
    form_class = MapLayerRequestParametersForm
    template_name = 'maps/map_layer_request_parameters_form.html'

    def get_success_url(self):
        return reverse('maps-manager-layer-edit', kwargs={'slug': self.object.slug})


class LayerDeleteView(MapLayerMixin, DeleteView):
    template_name = 'maps/map_layer_confirm_delete.html'


class MapCellAddLayer(CreateView):
    form_class = MapLayerOptionsForm
    template_name = 'maps/layer_options_form.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            self.cell = CellBase.get_cell(kwargs['cell_reference'], page=kwargs['page_pk'])
        except Map.DoesNotExist:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = MapLayerOptions(map_cell=self.cell)
        kwargs['kind'] = self.kwargs['kind']
        return kwargs

    def form_valid(self, form):
        PageSnapshot.take(
            self.cell.page,
            request=self.request,
            comment=_('added layer "%(layer)s" to cell "%(cell)s"')
            % {'layer': form.instance.map_layer, 'cell': self.cell},
        )
        return super().form_valid(form)

    def get_success_url(self):
        return '%s#cell-%s' % (
            reverse('combo-manager-page-view', kwargs={'pk': self.kwargs.get('page_pk')}),
            self.kwargs['cell_reference'],
        )


map_cell_add_layer = MapCellAddLayer.as_view()


class MapCellEditLayer(UpdateView):
    form_class = MapLayerOptionsForm
    template_name = 'maps/layer_options_form.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            self.cell = CellBase.get_cell(kwargs['cell_reference'], page=kwargs['page_pk'])
        except Map.DoesNotExist:
            raise Http404
        self.object = get_object_or_404(MapLayerOptions, pk=kwargs['layeroptions_pk'], map_cell=self.cell)
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, *args, **kwargs):
        return self.object

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['kind'] = self.object.map_layer.kind
        return kwargs

    def form_valid(self, form):
        PageSnapshot.take(
            self.cell.page,
            request=self.request,
            comment=_('changed options of layer "%(layer)s" in cell "%(cell)s"')
            % {'layer': form.instance.map_layer, 'cell': self.cell},
        )
        return super().form_valid(form)

    def get_success_url(self):
        return '%s#cell-%s' % (
            reverse('combo-manager-page-view', kwargs={'pk': self.kwargs.get('page_pk')}),
            self.kwargs['cell_reference'],
        )


map_cell_edit_layer = MapCellEditLayer.as_view()


class MapCellDeleteLayer(DeleteView):
    template_name = 'combo/generic_confirm_delete.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            self.cell = CellBase.get_cell(kwargs['cell_reference'], page=kwargs['page_pk'])
        except Map.DoesNotExist:
            raise Http404
        self.object = get_object_or_404(MapLayerOptions, pk=kwargs['layeroptions_pk'], map_cell=self.cell)
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, *args, **kwargs):
        return self.object

    def form_valid(self, form):
        response = super().form_valid(form)
        self.post_delete_actions()
        return response

    def post_delete_actions(self):
        PageSnapshot.take(
            self.cell.page,
            request=self.request,
            comment=_('removed layer "%(layer)s" from cell "%(cell)s"')
            % {'layer': self.object.map_layer, 'cell': self.cell},
        )

    def get_success_url(self):
        return '%s#cell-%s' % (
            reverse('combo-manager-page-view', kwargs={'pk': self.kwargs.get('page_pk')}),
            self.kwargs['cell_reference'],
        )


map_cell_delete_layer = MapCellDeleteLayer.as_view()
