# combo - content management system
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import urllib.parse

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.views.generic.base import View

from combo.utils import requests
from combo.utils.api import no_redirect_on_permission_denied

from .models import Map


class GeojsonView(View):
    def get(self, request, *args, **kwargs):
        cell = get_object_or_404(Map, pk=kwargs['cell_id'])
        layer = get_object_or_404(cell.layers.all(), kind='geojson', slug=kwargs['layer_slug'])
        if not cell.page.is_visible(request.user) or not cell.is_visible(request):
            return HttpResponseForbidden()
        options = cell.maplayeroptions_set.get(map_layer=layer)
        geojson = layer.get_geojson(request, options.properties)
        content_type = 'application/json'
        return HttpResponse(json.dumps(geojson), content_type=content_type)


@no_redirect_on_permission_denied
def geocoding_view(request, *args, **kwargs):
    if 'q' not in request.GET:
        return HttpResponseBadRequest('missing q parameter', content_type='text/plain')
    if not Map.objects.filter(include_search_button=True).exists():
        raise PermissionDenied()
    q = request.GET['q']
    url = settings.COMBO_MAP_GEOCODING_URL
    if '?' in url:
        url += '&'
    else:
        url += '?'
    url += 'format=json&q=%s' % urllib.parse.quote(q)
    url += '&accept-language=%s' % settings.LANGUAGE_CODE.split('-')[0]
    return HttpResponse(
        requests.get(url, without_user=True, remote_service=False).text, content_type='application/json'
    )
