# combo - content management system
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.encoding import force_str
from django.utils.html import escape
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from combo.data.fields import TemplatableURLField

from .models import Map, MapLayer, MapLayerOptions


class IconRadioSelect(forms.RadioSelect):
    option_template_name = 'maps/icon_radio_option.html'


class ColourOrTextInput(forms.TextInput):
    template_name = 'maps/colour_or_text_input.html'


class MapLayerForm(forms.ModelForm):
    geojson_url = TemplatableURLField(label=_('Geojson URL'))

    class Meta:
        model = MapLayer
        exclude = (
            'kind',
            'include_user_identifier',
            'geojson_query_parameter',
            'geojson_accepts_circle_param',
        )
        widgets = {
            'marker_colour': ColourOrTextInput,
            'icon_colour': forms.TextInput(attrs={'type': 'color'}),
        }
        help_texts = {
            'tiles_attribution': _('For example: %s') % escape(settings.COMBO_MAP_ATTRIBUTION),
            'tiles_template_url': _('For example: %s') % settings.COMBO_MAP_TILE_URLTEMPLATE,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk is None:
            # new instance, delete some fields
            del self.fields['slug']
            del self.fields['cache_duration']
        else:
            # new widget for icon field
            self.fields['icon'].widget = IconRadioSelect()
        self.fields['icon'].choices = list(
            sorted(self.fields['icon'].choices, key=lambda x: slugify(force_str(x[1])))
        )
        if self.instance.kind == 'geojson':
            todelete_fields = ['tiles_template_url', 'tiles_attribution', 'tiles_default']
        else:
            todelete_fields = [
                'geojson_url',
                'marker_colour',
                'icon',
                'icon_colour',
                'cache_duration',
            ]
        for field in todelete_fields:
            if field in self.fields:
                del self.fields[field]

    def clean(self):
        cleaned_data = super().clean()

        if self.instance.kind == 'tiles' and cleaned_data.get('tiles_default') is True:
            if MapLayer.objects.filter(kind='tiles', tiles_default=True).exclude(pk=self.instance.pk):
                raise forms.ValidationError(_('Only one default tiles layer can be defined.'))

        return cleaned_data


class MapLayerRequestParametersForm(forms.ModelForm):
    class Meta:
        model = MapLayer
        fields = ('include_user_identifier', 'geojson_query_parameter', 'geojson_accepts_circle_param')


class MapLayerOptionsForm(forms.ModelForm):
    class Meta:
        model = MapLayerOptions
        fields = ['map_layer', 'opacity', 'properties']
        widgets = {'opacity': forms.NumberInput(attrs={'step': 0.1, 'min': 0, 'max': 1})}

    def __init__(self, *args, **kwargs):
        self.kind = kwargs.pop('kind')
        super().__init__(*args, **kwargs)
        # if edition, no possibility to change the layer
        if self.instance.pk:
            del self.fields['map_layer']
        else:
            if self.kind == 'geojson':
                self.fields['map_layer'].queryset = self.instance.map_cell.get_free_geojson_layers()
            else:
                self.fields['map_layer'].queryset = self.instance.map_cell.get_free_tiles_layers()
        # init opacity field only for tiles layers
        if self.kind == 'geojson':
            del self.fields['opacity']
            if self.instance.map_cell.marker_behaviour_onclick != 'display_data':
                help_text = self.fields['properties'].help_text
                marker_behaviour = self.instance.map_cell.get_marker_behaviour_onclick_display()
                additional_note = (
                    _('Currently this setting has no effect, since marker behaviour on click is set to "%s".')
                    % marker_behaviour
                )
                self.fields['properties'].help_text = '%s %s' % (help_text, additional_note)
        else:
            self.fields['opacity'].required = True
            self.fields['opacity'].initial = 1
            del self.fields['properties']


class MapCellEditForm(forms.ModelForm):
    class Meta:
        model = Map
        fields = ('initial_zoom', 'min_zoom', 'max_zoom')

    def clean(self):
        cleaned_data = super().clean()
        initial_zoom = int(cleaned_data['initial_zoom'])
        max_zoom = int(cleaned_data['max_zoom'])
        min_zoom = int(cleaned_data['min_zoom'])
        if min_zoom > max_zoom:
            raise ValidationError(
                _('Invalid zoom configuration: minimal zoom must be lower than maximal zoom')
            )
        if not (max_zoom >= initial_zoom >= min_zoom):
            raise ValidationError(
                _('Invalid zoom configuration: initial zoom is not between minimal & maximal zoom'),
            )
        return cleaned_data
