from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0023_include_search_button'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='map',
            unique_together={('page', 'uuid')},
        ),
    ]
