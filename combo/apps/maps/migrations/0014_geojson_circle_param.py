from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0013_geojson_query_parameter'),
    ]

    operations = [
        migrations.AddField(
            model_name='maplayer',
            name='geojson_accepts_circle_param',
            field=models.BooleanField(default=False, verbose_name='GeoJSON URL accepts a "cirle" parameter'),
        ),
    ]
