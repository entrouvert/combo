from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0011_map_layer_set_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maplayer',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Identifier'),
        ),
    ]
