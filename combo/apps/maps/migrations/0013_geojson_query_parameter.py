from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0012_map_layer_slug_unicity'),
    ]

    operations = [
        migrations.AddField(
            model_name='maplayer',
            name='geojson_query_parameter',
            field=models.CharField(
                blank=True,
                help_text='Name of the parameter to use for querying the GeoJSON layer (typically, q)',
                max_length=100,
                verbose_name='Query parameter for fulltext requests',
            ),
        ),
    ]
