import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0007_auto_20180706_1345'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            state_operations=[
                migrations.CreateModel(
                    name='MapLayerOptions',
                    fields=[
                        (
                            'id',
                            models.AutoField(
                                auto_created=True, primary_key=True, serialize=False, verbose_name='ID'
                            ),
                        ),
                    ],
                    options={
                        'db_table': 'maps_map_layers',
                    },
                ),
                migrations.AlterField(
                    model_name='map',
                    name='layers',
                    field=models.ManyToManyField(
                        blank=True, through='maps.MapLayerOptions', to='maps.MapLayer', verbose_name='Layers'
                    ),
                ),
                migrations.AddField(
                    model_name='maplayeroptions',
                    name='map_cell',
                    field=models.ForeignKey(
                        db_column='map_id', on_delete=django.db.models.deletion.CASCADE, to='maps.Map'
                    ),
                ),
                migrations.AddField(
                    model_name='maplayeroptions',
                    name='map_layer',
                    field=models.ForeignKey(
                        db_column='maplayer_id',
                        verbose_name='Layer',
                        on_delete=django.db.models.deletion.CASCADE,
                        to='maps.MapLayer',
                    ),
                ),
                migrations.AlterUniqueTogether(
                    name='maplayeroptions',
                    unique_together={('map_cell', 'map_layer')},
                ),
            ],
            database_operations=[],
        )
    ]
