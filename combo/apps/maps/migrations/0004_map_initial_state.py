from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0003_auto_20170603_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='initial_state',
            field=models.CharField(
                default=b'default-position',
                max_length=20,
                verbose_name='Initial state',
                choices=[
                    (b'default-position', 'Centered on default position'),
                    (b'device-location', 'Centered on device location'),
                    (b'fit-markers', 'Centered to fit all markers'),
                ],
            ),
        ),
    ]
