from django.db import migrations
from django.utils.text import slugify


def generate_slug(instance):
    base_slug = slugify(instance.label)[:45]
    slug = base_slug
    i = 1
    while True:
        queryset = instance._meta.model.objects.filter(slug=slug).exclude(pk=instance.pk)
        if not queryset.exists():
            break
        slug = '%s-%s' % (base_slug, i)
        i += 1
    return slug


def set_slug(apps, schema_editor):
    MapLayer = apps.get_model('maps', 'MapLayer')
    for layer in MapLayer.objects.all().order_by('-pk'):
        if not MapLayer.objects.filter(slug=layer.slug).exclude(pk=layer.pk).exists():
            continue
        layer.slug = generate_slug(layer)
        layer.save(update_fields=['slug'])


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0010_map_layer_opacity'),
    ]

    operations = [
        migrations.RunPython(set_slug, lambda x, y: None),
    ]
