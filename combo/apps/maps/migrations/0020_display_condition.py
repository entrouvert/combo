from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0019_auto_20211104_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AlterField(
            model_name='maplayer',
            name='tiles_attribution',
            field=models.CharField(
                blank=True,
                max_length=1024,
                null=True,
                verbose_name='Attribution',
            ),
        ),
        migrations.AlterField(
            model_name='maplayer',
            name='tiles_template_url',
            field=models.CharField(
                blank=True,
                max_length=1024,
                null=True,
                verbose_name='Tiles URL',
            ),
        ),
    ]
