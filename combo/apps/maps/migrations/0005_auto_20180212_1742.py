from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0004_map_initial_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='group_markers',
            field=models.BooleanField(default=False, verbose_name='Group markers in clusters'),
        ),
    ]
