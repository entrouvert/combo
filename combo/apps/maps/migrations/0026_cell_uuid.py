from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0025_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='map',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
