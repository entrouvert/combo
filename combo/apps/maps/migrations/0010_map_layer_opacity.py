import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0009_map_layer_kind'),
    ]

    operations = [
        migrations.AddField(
            model_name='maplayeroptions',
            name='opacity',
            field=models.FloatField(
                help_text='Float value between 0 (transparent) and 1 (opaque)',
                null=True,
                validators=[
                    django.core.validators.MinValueValidator(0),
                    django.core.validators.MaxValueValidator(1),
                ],
                verbose_name='Opacity',
            ),
        ),
    ]
