from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0027_page_picture'),
        ('auth', '0006_require_contenttypes_0002'),
        ('maps', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Map',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                (
                    'extra_css_class',
                    models.CharField(
                        max_length=100, verbose_name='Extra classes for CSS styling', blank=True
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=150, verbose_name='Title', blank=True)),
                (
                    'initial_zoom',
                    models.CharField(
                        default=b'13',
                        max_length=2,
                        verbose_name='Initial zoom level',
                        choices=[
                            (b'0', 'Whole world'),
                            (b'9', 'Wide area'),
                            (b'11', 'Area'),
                            (b'13', 'Town'),
                            (b'16', 'Small road'),
                            (b'18', 'Neighbourhood'),
                            (b'19', 'Ant'),
                        ],
                    ),
                ),
                (
                    'min_zoom',
                    models.CharField(
                        default=b'0',
                        max_length=2,
                        verbose_name='Minimal zoom level',
                        choices=[
                            (b'0', 'Whole world'),
                            (b'9', 'Wide area'),
                            (b'11', 'Area'),
                            (b'13', 'Town'),
                            (b'16', 'Small road'),
                            (b'18', 'Neighbourhood'),
                            (b'19', 'Ant'),
                        ],
                    ),
                ),
                (
                    'max_zoom',
                    models.CharField(
                        default=19,
                        max_length=2,
                        verbose_name='Maximal zoom level',
                        choices=[
                            (b'0', 'Whole world'),
                            (b'9', 'Wide area'),
                            (b'11', 'Area'),
                            (b'13', 'Town'),
                            (b'16', 'Small road'),
                            (b'18', 'Neighbourhood'),
                            (b'19', 'Ant'),
                        ],
                    ),
                ),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('layers', models.ManyToManyField(to='maps.MapLayer', verbose_name='Layers', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Map',
            },
        ),
    ]
