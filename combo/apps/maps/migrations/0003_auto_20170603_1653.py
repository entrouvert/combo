from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0002_map'),
    ]

    operations = [
        migrations.AddField(
            model_name='maplayer',
            name='cache_duration',
            field=models.PositiveIntegerField(
                default=60, help_text='In seconds.', verbose_name='Cache duration'
            ),
        ),
        migrations.AddField(
            model_name='maplayer',
            name='include_user_identifier',
            field=models.BooleanField(default=True, verbose_name='Include user identifier in request'),
        ),
    ]
