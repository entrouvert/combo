# Generated by Django 2.2.19 on 2021-11-04 15:03

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0018_auto_20211104_1559'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='maplayer',
            name='properties',
        ),
    ]
