from django.db import migrations, models

from combo.apps.maps.models import ICONS


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='MapLayer',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(max_length=128, verbose_name='Label')),
                ('slug', models.SlugField(verbose_name='Identifier')),
                ('geojson_url', models.CharField(max_length=1024, verbose_name='Geojson URL')),
                (
                    'marker_colour',
                    models.CharField(default=b'#0000FF', max_length=7, verbose_name='Marker colour'),
                ),
                (
                    'icon',
                    models.CharField(
                        blank=True, max_length=32, null=True, verbose_name='Marker icon', choices=ICONS
                    ),
                ),
                (
                    'icon_colour',
                    models.CharField(default=b'#000000', max_length=7, verbose_name='Icon colour'),
                ),
            ],
            options={'ordering': ('label',)},
        ),
    ]
