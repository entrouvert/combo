from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('maps', '0005_auto_20180212_1742'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='marker_behaviour_onclick',
            field=models.CharField(
                default=b'none',
                max_length=32,
                verbose_name='Marker behaviour on click',
                choices=[(b'none', 'Nothing'), (b'display_data', 'Display data in popup')],
            ),
        ),
    ]
