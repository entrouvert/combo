# combo - content management system
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import itertools

from django.db import models
from django.utils.translation import gettext_lazy as _

from combo.data.library import register_cell_class
from combo.data.models import CellBase, Page


@register_cell_class
class LatestPageUpdatesCell(CellBase):
    root_page = models.ForeignKey(
        Page,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('Root Page'),
        related_name='kb_latest_page_updates_cell_root_page',
    )
    limit = models.PositiveSmallIntegerField(_('Maximum number of entries'), default=10)

    default_template_name = 'combo/latest-page-updates-cell.html'
    exclude_from_search = True

    class Meta:
        verbose_name = _('Latest Page Updates')

    def get_cell_extra_context(self, context):
        extra_context = super().get_cell_extra_context(context)
        if self.root_page:
            pages = self.root_page.get_descendants_and_me()
        else:
            pages = Page.objects.all()
        user = self.get_concerned_user(context)
        extra_context['pages'] = itertools.islice(
            (x for x in pages.order_by('-last_update_timestamp') if x.is_visible(user=user)), self.limit
        )
        return extra_context
