from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('kb', '0007_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='latestpageupdatescell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
