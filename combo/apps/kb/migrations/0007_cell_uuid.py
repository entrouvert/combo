import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in ['LatestPageUpdatesCell']:
        klass = apps.get_model('kb', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('kb', '0006_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
