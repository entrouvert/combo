from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('kb', '0003_latestpageupdatescell_template_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='latestpageupdatescell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
