from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('profile', '0005_increase_extra_css_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilecell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
    ]
