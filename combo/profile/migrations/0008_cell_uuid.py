from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('profile', '0007_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profilecell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]
