from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('profile', '0003_profilecell_template_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilecell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
    ]
