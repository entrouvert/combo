# combo - content management system
# Copyright (C) 2014-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
from collections import OrderedDict

from django.conf import settings
from django.db import models
from django.utils.dateparse import parse_date
from django.utils.translation import gettext_lazy as _

from combo.data.library import register_cell_class
from combo.data.models import JsonCellBase
from combo.profile import utils as profile_utils
from combo.utils.misc import is_portal_agent


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    initial_login_view_timestamp = models.DateTimeField(null=True)


@register_cell_class
class ProfileCell(JsonCellBase):
    default_template_name = 'combo/profile.html'
    first_data_key = 'profile'
    cache_duration = None
    edit_label = models.CharField(
        _('Label of "modify" button'),
        max_length=100,
        blank=True,
        null=True,
        help_text=_('Leave empty to not have a "modify" button.'),
    )

    class Meta:
        verbose_name = _('Profile')

    @property
    def url(self):
        idp = list(settings.KNOWN_SERVICES.get('authentic').values())[0]
        return '%sapi/users/{{ concerned_user|name_id }}/' % idp.get('url')

    def is_visible(self, request, **kwargs):
        user = getattr(request, 'user', None)
        if not user or user.is_anonymous:
            return False
        return super().is_visible(request, **kwargs)

    def get_cell_extra_context(self, context):
        extra_context = super().get_cell_extra_context(context)
        extra_context['profile_fields'] = OrderedDict()
        if extra_context.get('profile') is not None:
            for attribute in settings.USER_PROFILE_CONFIG.get('fields'):
                extra_context['profile_fields'][attribute['name']] = copy.copy(attribute)
                value = extra_context['profile'].get(attribute['name'])
                if value:
                    if attribute['kind'] in ('birthdate', 'date'):
                        value = parse_date(value)
                    if attribute['kind'] == 'phone_number':
                        value = profile_utils.get_formatted_phone(value)
                extra_context['profile_fields'][attribute['name']]['value'] = value

            if self.edit_label:
                idp_url = list(settings.KNOWN_SERVICES.get('authentic').values())[0].get('url')
                name_id = self.get_concerned_user(context).get_name_id()
                if is_portal_agent():
                    edit_url = f'{idp_url}manage/users/uuid:{name_id}/edit/'
                else:
                    edit_url = f'{idp_url}accounts/edit/'
                extra_context['edit_url'] = edit_url
        else:
            extra_context['error'] = 'unknown user'
        return extra_context
