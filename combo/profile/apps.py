# combo - content management system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.apps
from django.utils.translation import gettext_lazy as _

from . import default_description_template


def user_get_name_id(user):
    if not hasattr(user, '_name_id'):
        user._name_id = None
        saml_identifier = user.saml_identifiers.first()
        if saml_identifier:
            user._name_id = saml_identifier.name_id

    return user._name_id


class AppConfig(django.apps.AppConfig):
    name = 'combo.profile'
    verbose_name = _('Profile')

    def ready(self):
        from combo.apps.search.engines import engines

        engines.register(self.get_search_engines)
        from django.contrib.auth import get_user_model

        get_user_model().add_to_class('get_name_id', user_get_name_id)
        get_user_model().add_to_class('nameid', property(user_get_name_id))

    def get_search_engines(self):
        from django.conf import settings

        from combo.data.models import Page

        user_page = Page.objects.filter(sub_slug='name_id').first()
        if not user_page:
            user_page = Page.objects.filter(sub_slug__contains='<name_id>').first()
        if not user_page:
            return
        user_page_base_url = user_page.get_online_url()

        # return entry using first(&only) authentic (if it exists)
        for service in settings.KNOWN_SERVICES.get('authentic', {}).values():
            return {
                'users': {
                    'url': service['url'] + 'api/users/?q=%(q)s',
                    'label': _('Users'),
                    'signature': True,
                    'data_key': 'results',
                    'hit_url_template': user_page_base_url + '{{uuid}}/',
                    'hit_label_template': '{{first_name}} {{last_name}}',
                    'hit_description_template': default_description_template,
                }
            }
