# combo - content management system
# Copyright (C) 2014-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import phonenumbers
from django.conf import settings
from django.contrib.auth.models import User
from phonenumbers.phonenumberutil import region_code_for_country_code

if 'mellon' in settings.INSTALLED_APPS:
    from mellon.models import UserSAMLIdentifier
else:
    UserSAMLIdentifier = None


class ProxiedUser:
    is_authenticated = True
    is_anonymous = False

    def __init__(self, name_id):
        self.name_id = self.nameid = name_id
        self.email = ''

    def get_name_id(self):
        return self.name_id


def get_user_from_name_id(name_id, raise_on_missing=False):
    if not UserSAMLIdentifier:
        if raise_on_missing:
            raise User.DoesNotExist()
        return None
    try:
        return UserSAMLIdentifier.objects.get(name_id=name_id).user
    except UserSAMLIdentifier.DoesNotExist:
        if raise_on_missing:
            raise User.DoesNotExist()
        return ProxiedUser(name_id=name_id)


def get_formatted_phone(value, country_code=None):
    if country_code is None:
        country_code = settings.DEFAULT_COUNTRY_CODE

    region_code = 'ZZ'  # phonenumbers' default value for unknown regions
    try:
        region_code = region_code_for_country_code(int(country_code))
    except ValueError:
        pass
    if region_code == 'ZZ':
        return value

    try:
        pn = phonenumbers.parse(value, region_code)
    except phonenumbers.NumberParseException:
        return value

    if not phonenumbers.is_valid_number(pn):
        return value

    if country_code == str(pn.country_code):
        return phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.NATIONAL)

    return phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
