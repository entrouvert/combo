#
# combo - content management system
# Copyright (C) 2014-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


default_description_template = """{% if email %}<a href="mailto:{{ email }}">{{ email }}</a>{% endif %}
{% if phone %} 📞 <a href="tel:{{ phone }}">{{ phone }}</a>{% endif %}
{% if mobile %} 📱 <a href="tel:{{ mobile }}">{{ mobile }}</a>{% endif %}
{% if address or zipcode or city %} 📨{% endif %}
{% if address %} {{ address }}{% endif %}
{% if zipcode %} {{ zipcode }}{% endif %}
{% if city %} {{ city }}{% endif %}"""
