# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.template.loader import get_template

from combo.data.models import Page
from combo.utils import NothingInCacheException


def render_menu(context, level=0, root_page=None, depth=1, ignore_visibility=False):
    context['root_page'] = root_page
    if root_page:
        level = len(root_page.get_parents_and_self())
    context = get_menu_context(
        context, level=level, current_page=None, depth=depth, ignore_visibility=ignore_visibility
    )
    template = get_template('combo/menu.html')
    return template.render(context)


def get_menu_context(context, level=0, current_page=None, depth=1, ignore_visibility=False):
    if 'hierarchical_pages_by_id' not in context:
        context['hierarchical_pages_by_id'] = Page.get_with_hierarchy_attributes()
    pages_by_id = context['hierarchical_pages_by_id']
    context['ignore_visibility'] = ignore_visibility
    context['depth'] = depth
    if current_page is None:
        current_page = pages_by_id.get(context['page'].id, context['page'])
    else:
        current_page = pages_by_id.get(current_page.id, current_page)

    if 'root_page' in context:
        root_page = pages_by_id[context['root_page'].id] if context.get('root_page') else None
        # if the menu is anchored at a specific root page, we make sure the
        # current page is in the right path, otherwise we fall back to using
        # the root page as base.
        ariane = current_page.get_parents_and_self()
        if not root_page in ariane:
            page_of_level = root_page
        else:
            page_of_level = current_page.get_page_of_level(level)
    else:
        page_of_level = current_page.get_page_of_level(level)
    if level == -1:
        # get children
        elements = current_page.get_children()
    elif page_of_level is None:
        if level > 0:
            parent_page = current_page.get_page_of_level(level - 1)
            elements = parent_page.get_children()
        else:
            context['menuitems'] = []
            return context
    else:
        if page_of_level == context.get('root_page'):
            elements = page_of_level.get_children()
        else:
            elements = page_of_level.get_siblings()
    menuitems = []
    for element in elements:
        if element.exclude_from_navigation:
            continue
        if not ignore_visibility:
            if context.get('render_skeleton'):
                if not element.public:
                    # mark the menu cell for asynchronous rendering.
                    raise NothingInCacheException()
            if not element.is_visible(context['request'].user):
                continue
        menuitem = {'page': element}
        menuitem['selected'] = bool(element == page_of_level)
        menuitems.append(menuitem)
    context['menuitems'] = menuitems
    return context
