# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from combo.middleware import get_request


# _make_key and _HashedSeq imported/adapted from functools from Python 3.2+
# pylint: disable=dangerous-default-value
def _make_key(
    args,
    kwds,
    kwd_mark=(object(),),
    fasttypes={int, str, frozenset, type(None)},
    tuple=tuple,
    type=type,
    len=len,
):
    key = args
    if kwds:
        key += kwd_mark
        for item in kwds.items():
            key += item
    if len(key) == 1 and type(key[0]) in fasttypes:
        return key[0]
    return _HashedSeq(key)


class _HashedSeq(list):
    __slots__ = ('hashvalue',)

    def __init__(self, tup, hash=hash):
        self[:] = tup
        self.hashvalue = hash(tup)

    def __hash__(self):
        return self.hashvalue


def cache_during_request(func):
    def inner(*args, **kwargs):
        request = get_request()
        if request:
            cache_key = (id(func), _make_key(args, kwargs))
            if cache_key in request.cache:
                return request.cache[cache_key]
        result = func(*args, **kwargs)
        if request:
            request.cache[cache_key] = result
        return result

    return inner
