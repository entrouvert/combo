# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import binascii

from Cryptodome import Random
from Cryptodome.Cipher import AES
from Cryptodome.Protocol.KDF import PBKDF2
from django.utils.encoding import force_str


class DecryptionError(Exception):
    pass


def aes_hex_encrypt(key, data):
    """Generate an AES key from any key material using PBKDF2, and encrypt data using CFB mode. A
    new IV is generated each time, the IV is also used as salt for PBKDF2.
    """
    iv = Random.get_random_bytes(2) * 8
    aes_key = PBKDF2(key, iv)
    aes = AES.new(aes_key, AES.MODE_CFB, iv)
    crypted = aes.encrypt(data)
    return force_str(b'%s%s' % (binascii.hexlify(iv[:2]), binascii.hexlify(crypted)))


def aes_hex_decrypt(key, payload, raise_on_error=True):
    '''Decrypt data encrypted with aes_base64_encrypt'''
    try:
        iv, crypted = payload[:4], payload[4:]
    except (ValueError, TypeError):
        if raise_on_error:
            raise DecryptionError('bad payload')
        return None
    try:
        iv = binascii.unhexlify(iv) * 8
        crypted = binascii.unhexlify(crypted)
    except (TypeError, binascii.Error):
        if raise_on_error:
            raise DecryptionError('incorrect hexadecimal encoding')
        return None
    aes_key = PBKDF2(key, iv)
    aes = AES.new(aes_key, AES.MODE_CFB, iv)
    return force_str(aes.decrypt(crypted), 'utf-8')
