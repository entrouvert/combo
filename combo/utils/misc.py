# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import html
import urllib.parse

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.template.context import BaseContext
from django.utils.html import strip_tags


def ellipsize(text, length=50):
    text = html.unescape(strip_tags(text))
    if len(text) < length:
        return text
    return text[: (length - 10)] + '...'


def flatten_context(context):
    # flatten a context to a dictionary, with full support for embedded Context
    # objects.
    flat_context = {}
    if isinstance(context, BaseContext):
        for ctx in context.dicts:
            flat_context.update(flatten_context(ctx))
    else:
        flat_context.update(context)
    return flat_context


def get_known_service_for_url(url):
    netloc = urllib.parse.urlparse(url).netloc
    for services in settings.KNOWN_SERVICES.values():
        for service in services.values():
            remote_url = service.get('url')
            if urllib.parse.urlparse(remote_url).netloc == netloc:
                return service
    return None


def is_url_from_known_service(url):
    netloc = urllib.parse.urlparse(url).netloc
    if not netloc:
        return True
    return bool(get_known_service_for_url(url))


def is_portal_agent():
    if getattr(settings, 'KNOWN_SERVICES') and 'combo' in settings.KNOWN_SERVICES:
        for service in settings.KNOWN_SERVICES['combo'].values():
            if service.get('is-portal-agent') and not service.get('secret'):
                return True
    return False


def is_ajax(request):
    return request.headers.get('x-requested-with') == 'XMLHttpRequest'


def is_bot(request):
    return request and 'bot' in (request.headers.get('User-Agent') or '').lower()


class RateLimited(PermissionDenied):
    pass
