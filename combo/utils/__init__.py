# combo - content management system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# import specific symbols for compatibility
from .cache import cache_during_request
from .crypto import DecryptionError, aes_hex_decrypt, aes_hex_encrypt
from .misc import ellipsize, flatten_context, is_ajax, is_bot
from .requests_wrapper import NothingInCacheException, requests
from .signature import check_query, check_request_signature, sign_url
from .urls import TemplateError, get_templated_url, is_templated_url
