# combo - content management system
# Copyright (C) 2014-2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.forms.widgets import Select, SelectMultiple
from django.urls import reverse


class FlexSize(Select):
    template_name = 'combo/widgets/flexsize.html'


class Select2WidgetMixin:
    select2_enabled = True

    class Media:
        js = 'xstatic/select2.min.js'
        css = {'all': ('xstatic/select2.min.css',)}

    def __init__(self, model=None, choices=()):
        super().__init__(choices=choices)

        if self.select2_enabled:
            self.attrs['data-combo-autocomplete'] = 'true'
            self.attrs['lang'] = settings.LANGUAGE_CODE
            if model:
                self.attrs['data-select2-url'] = reverse(
                    'combo-manager-select2-choices', kwargs={'model_name': model._meta.label_lower}
                )


class Select2Widget(Select2WidgetMixin, Select):
    min_choices = 20

    def __init__(self, selected_choice_id, **kwargs):
        super().__init__(**kwargs)
        if self.select2_enabled:
            self.choices = [x for x in self.choices if x[0] == selected_choice_id]
            self.attrs['data-select2-url'] += '?include_blank_choice=true'

    @property
    def select2_enabled(self):
        return bool(len(self.choices) > self.min_choices)


class MultipleSelect2Widget(Select2WidgetMixin, SelectMultiple):
    def __init__(self, selected_choices_ids=None, **kwargs):
        super().__init__(**kwargs)
        if selected_choices_ids is not None:
            self.choices = [x for x in self.choices if x[0] in selected_choices_ids]
        self.attrs['multiple'] = 'multiple'
