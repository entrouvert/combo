#
# combo - content management system
# Copyright (C) 2015-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ckeditor.fields
from django import forms
from django.conf import settings
from django.core import validators
from django.forms.widgets import TextInput
from django.utils.encoding import force_str


class RichTextField(ckeditor.fields.RichTextField):
    def formfield(self, **kwargs):
        defaults = {
            'form_class': RichTextFormField,
            'config_name': self.config_name,
            'extra_plugins': self.extra_plugins,
            'external_plugin_resources': self.external_plugin_resources,
        }
        defaults.update(kwargs)
        return super().formfield(**defaults)


class RichTextFormField(ckeditor.fields.RichTextFormField):
    def clean(self, value):
        value = super().clean(value)
        if settings.LANGUAGE_CODE.startswith('fr-'):
            # apply some typographic rules
            value = value.replace('&laquo; ', '«\u202f')
            value = value.replace('« ', '«\u202f')
            value = value.replace(' &raquo;', '\u202f»')
            value = value.replace(' »', '\u202f»')
            value = value.replace(' :', '\u00a0:')
            value = value.replace(' ;', '\u202f;')
            value = value.replace(' !', '\u202f!')
            value = value.replace(' ?', '\u202f?')
        return value


def templatable_url_validator(value):
    value = force_str(value)
    if '{{' in value or '{%' in value:
        # leave templates alone
        return
    validators.URLValidator()(value)


class TemplatableURLField(forms.URLField):
    widget = TextInput
    default_validators = [templatable_url_validator]

    def to_python(self, value):
        value = super(forms.URLField, self).to_python(value)
        if '{{' in value or '{%' in value:
            return value
        return super().to_python(value)
