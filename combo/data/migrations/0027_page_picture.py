from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0026_jsoncell_force_async'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='picture',
            field=models.ImageField(upload_to='page-pictures/', null=True, verbose_name='Picture'),
        ),
    ]
