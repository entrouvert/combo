import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('data', '0038_increase_jsoncell_url_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkListCell',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(blank=True, verbose_name='Slug')),
                (
                    'extra_css_class',
                    models.CharField(
                        blank=True, max_length=100, verbose_name='Extra classes for CSS styling'
                    ),
                ),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('groups', models.ManyToManyField(blank=True, to='auth.Group', verbose_name='Roles')),
                ('title', models.CharField(max_length=150, verbose_name='Title', blank=True)),
            ],
            options={
                'verbose_name': 'List of links',
            },
        ),
        migrations.AddField(
            model_name='linklistcell',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data.Page'),
        ),
    ]
