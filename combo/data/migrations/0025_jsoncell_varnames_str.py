from django.db import migrations, models

import combo.data.fields


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0024_configjsoncell'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsoncell',
            name='varnames_str',
            field=models.CharField(
                help_text='Comma separated list of query-string variables to be copied in template context',
                max_length=200,
                verbose_name='Variable names',
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name='textcell',
            name='text',
            field=combo.data.fields.RichTextField(null=True, verbose_name='Text', blank=True),
        ),
    ]
