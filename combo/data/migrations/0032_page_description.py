from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0031_remove_blurps'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
    ]
