from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0040_auto_20200119_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='exclude_from_navigation',
            field=models.BooleanField(default=True, verbose_name='Exclude from navigation'),
        ),
    ]
