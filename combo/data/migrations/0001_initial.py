import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlurpCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('blurp_key', models.CharField(max_length=50)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FortuneCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
            ],
            options={
                'verbose_name': 'Fortune',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(verbose_name='Slug')),
                ('template_name', models.CharField(max_length=50, verbose_name='Page Template')),
                ('order', models.PositiveIntegerField()),
                (
                    'exclude_from_navigation',
                    models.BooleanField(default=False, verbose_name='Exclude from navigation'),
                ),
                ('redirect_url', models.CharField(max_length=100, verbose_name='Redirect URL', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                (
                    'parent',
                    models.ForeignKey(blank=True, to='data.Page', null=True, on_delete=models.CASCADE),
                ),
            ],
            options={
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TextCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Text')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Text',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UnlockMarkerCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Unlock Marker',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='page',
            field=models.ForeignKey(to='data.Page', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blurpcell',
            name='page',
            field=models.ForeignKey(to='data.Page', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
