import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('data', '0043_delete_externallinksearchitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='ValidityInfo',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('object_id', models.PositiveIntegerField()),
                (
                    'invalid_reason_code',
                    models.CharField(blank=True, editable=False, max_length=100, null=True),
                ),
                ('invalid_since', models.DateTimeField(blank=True, editable=False, null=True)),
                (
                    'content_type',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'
                    ),
                ),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='validityinfo',
            unique_together={('content_type', 'object_id')},
        ),
    ]
