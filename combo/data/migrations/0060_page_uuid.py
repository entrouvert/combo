from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0059_textcell_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
    ]
