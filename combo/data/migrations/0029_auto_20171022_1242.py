from django.db import migrations, models

import combo.data.models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0028_jsoncell_timeout'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkcell',
            name='url',
            field=models.CharField(
                max_length=200,
                verbose_name='URL',
                blank=True,
                validators=[combo.data.models.django_template_validator],
            ),
        ),
    ]
