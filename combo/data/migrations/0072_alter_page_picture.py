# Generated by Django 4.2.15 on 2025-01-30 11:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0071_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='picture',
            field=models.ImageField(
                max_length=200,
                null=True,
                upload_to='page-pictures/',
                verbose_name='Picture',
            ),
        ),
    ]
