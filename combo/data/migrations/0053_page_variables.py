from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0052_auto_20211110_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='extra_variables',
            field=models.JSONField(blank=True, default=dict),
        ),
    ]
