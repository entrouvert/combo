from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0029_auto_20171022_1242'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExternalLinkSearchItem',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=150, verbose_name='Title')),
                ('text', models.TextField(blank=True)),
                ('url', models.CharField(max_length=200, verbose_name='URL', blank=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
