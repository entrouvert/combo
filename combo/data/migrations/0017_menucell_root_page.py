from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0016_feedcell_limit'),
    ]

    operations = [
        migrations.AddField(
            model_name='menucell',
            name='root_page',
            field=models.ForeignKey(
                related_name='root_page',
                verbose_name='Root Page',
                blank=True,
                to='data.Page',
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
