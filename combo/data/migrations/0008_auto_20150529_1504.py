from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0007_auto_20150529_1003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkcell',
            name='link_page',
            field=models.ForeignKey(
                related_name='link_cell', blank=True, to='data.Page', null=True, on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
    ]
