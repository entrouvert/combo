from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0010_feedcell'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='redirect_url',
            field=models.CharField(max_length=200, verbose_name='Redirect URL', blank=True),
            preserve_default=True,
        ),
    ]
