import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0004_menucell_depth'),
    ]

    operations = [
        migrations.AlterField(
            model_name='textcell',
            name='text',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True),
            preserve_default=True,
        ),
    ]
