from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0006_linkcell'),
    ]

    operations = [
        migrations.AddField(
            model_name='linkcell',
            name='anchor',
            field=models.CharField(max_length=150, verbose_name='Anchor', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='linkcell',
            name='link_page',
            field=models.ForeignKey(
                related_name='link_cell', to='data.Page', null=True, on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='linkcell',
            name='title',
            field=models.CharField(max_length=150, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='linkcell',
            name='url',
            field=models.URLField(verbose_name='URL', blank=True),
            preserve_default=True,
        ),
    ]
