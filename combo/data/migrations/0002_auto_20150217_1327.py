from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blurpcell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='textcell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='slug',
            field=models.SlugField(verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
    ]
