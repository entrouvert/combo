from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0019_create_parent_cells'),
    ]

    operations = [
        migrations.AddField(
            model_name='blurpcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='feedcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='linkcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='menucell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='parameterscell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='parentcontentcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='textcell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='extra_css_class',
            field=models.CharField(max_length=100, verbose_name='Extra classes for CSS styling', blank=True),
        ),
    ]
