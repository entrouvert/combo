from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0027_page_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsoncell',
            name='timeout',
            field=models.PositiveIntegerField(
                default=0,
                help_text='In seconds. Use 0 for default system timeout',
                verbose_name='Request timeout',
            ),
        ),
    ]
