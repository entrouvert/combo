from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0014_menucell_initial_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedcell',
            name='title',
            field=models.CharField(max_length=150, verbose_name='Title', blank=True),
            preserve_default=True,
        ),
    ]
