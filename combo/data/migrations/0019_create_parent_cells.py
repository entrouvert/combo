from django.conf import settings
from django.db import migrations


def create_parent_content_cells(apps, schema_editor):
    Page = apps.get_model('data', 'Page')
    UnlockMarkerCell = apps.get_model('data', 'UnlockMarkerCell')
    ParentContentCell = apps.get_model('data', 'ParentContentCell')
    for page in Page.objects.all():
        combo_template = settings.COMBO_PUBLIC_TEMPLATES.get(page.template_name)
        if not combo_template:
            continue
        for placeholder_key, placeholder_value in combo_template['placeholders'].items():
            if not placeholder_value.get('acquired'):
                continue
            is_unlocked = UnlockMarkerCell.objects.filter(page=page, placeholder=placeholder_key).count()
            if is_unlocked:
                continue
            # not unlocked -> add a new ParentContentCell
            cell, created = ParentContentCell.objects.get_or_create(
                page=page, placeholder=placeholder_key, order=0
            )
            if created:
                cell.save()


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0018_parentcontentcell'),
    ]

    operations = [
        migrations.RunPython(create_parent_content_cells),
    ]
