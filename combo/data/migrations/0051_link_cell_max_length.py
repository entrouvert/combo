from django.db import migrations, models

import combo.data.models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0050_populate_site_settings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkcell',
            name='url',
            field=models.CharField(
                blank=True,
                max_length=2000,
                validators=[combo.data.models.django_template_validator],
                verbose_name='URL',
            ),
        ),
    ]
