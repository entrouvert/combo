import uuid

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0061_page_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
