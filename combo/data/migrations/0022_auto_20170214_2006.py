import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0021_jsoncell'),
    ]

    operations = [
        migrations.AddField(
            model_name='blurpcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='feedcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='jsoncell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='linkcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menucell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='page',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parameterscell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parentcontentcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='textcell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='last_update_timestamp',
            field=models.DateTimeField(default=datetime.datetime.now(datetime.UTC), auto_now=True),
            preserve_default=False,
        ),
    ]
