from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0066_auto_20231003_1421'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagesnapshot',
            name='application_slug',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='pagesnapshot',
            name='application_version',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
