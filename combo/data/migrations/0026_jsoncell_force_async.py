from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0025_jsoncell_varnames_str'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsoncell',
            name='force_async',
            field=models.BooleanField(default=False, verbose_name='Force asynchronous mode'),
        ),
    ]
