from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0013_parameterscell'),
    ]

    operations = [
        migrations.AddField(
            model_name='menucell',
            name='initial_level',
            field=models.IntegerField(
                default=-1, verbose_name='Initial Level', choices=[(-1, 'Same as page'), (1, 1), (2, 2)]
            ),
            preserve_default=True,
        ),
    ]
