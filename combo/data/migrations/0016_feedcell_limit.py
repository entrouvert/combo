from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0015_feedcell_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedcell',
            name='limit',
            field=models.PositiveSmallIntegerField(
                null=True, verbose_name='Maximum number of entries', blank=True
            ),
        ),
    ]
