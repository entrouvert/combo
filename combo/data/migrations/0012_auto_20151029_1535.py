from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0011_auto_20151028_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='blurpcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feedcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='linkcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='menucell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='textcell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='restricted_to_unlogged',
            field=models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
            preserve_default=True,
        ),
    ]
