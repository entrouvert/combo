from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0030_externallinksearchitem'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blurpcell',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='blurpcell',
            name='page',
        ),
        migrations.DeleteModel(
            name='BlurpCell',
        ),
    ]
