import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in [
        'TextCell',
        'FortuneCell',
        'UnlockMarkerCell',
        'MenuCell',
        'LinkCell',
        'LinkListCell',
        'FeedCell',
        'ParentContentCell',
        'JsonCell',
        'ConfigJsonCell',
    ]:
        klass = apps.get_model('data', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0069_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
