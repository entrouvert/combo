from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0053_page_variables'),
    ]

    operations = [
        migrations.AddField(
            model_name='linkcell',
            name='bypass_url_validity_check',
            field=models.BooleanField(default=False, verbose_name='No URL validity check'),
        ),
    ]
