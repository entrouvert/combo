from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0003_menucell'),
    ]

    operations = [
        migrations.AddField(
            model_name='menucell',
            name='depth',
            field=models.PositiveIntegerField(default=1, verbose_name='Depth', choices=[(1, 1), (2, 2)]),
            preserve_default=True,
        ),
    ]
