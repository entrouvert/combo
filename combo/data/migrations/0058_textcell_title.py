from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0057_pagesnapshot_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='textcell',
            name='title',
            field=models.CharField(blank=True, max_length=150, null=True, verbose_name='Title'),
        ),
    ]
