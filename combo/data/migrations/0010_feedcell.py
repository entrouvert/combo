from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0009_auto_20150529_2247'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('url', models.URLField(verbose_name='URL', blank=True)),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'RSS/Atom Feed',
            },
            bases=(models.Model,),
        ),
    ]
