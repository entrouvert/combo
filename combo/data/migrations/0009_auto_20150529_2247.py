from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0008_auto_20150529_1504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkcell',
            name='link_page',
            field=models.ForeignKey(
                related_name='link_cell',
                verbose_name='Internal link',
                blank=True,
                to='data.Page',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
