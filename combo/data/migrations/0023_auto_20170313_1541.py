from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0022_auto_20170214_2006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='title',
            field=models.CharField(max_length=150, verbose_name='Title'),
        ),
    ]
