from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0068_increase_extra_css_class9'),
    ]

    operations = [
        migrations.AddField(
            model_name='configjsoncell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='configjsoncell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='feedcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='feedcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='fortunecell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='jsoncell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='jsoncell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='linkcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='linkcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='linklistcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='linklistcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='menucell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='menucell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='parentcontentcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='parentcontentcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='textcell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='textcell',
            unique_together={('page', 'uuid')},
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='unlockmarkercell',
            unique_together={('page', 'uuid')},
        ),
    ]
