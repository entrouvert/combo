from django.db import migrations, models
from django.db.models import JSONField


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0012_auto_20151029_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParametersCell',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                (
                    'restricted_to_unlogged',
                    models.BooleanField(default=False, verbose_name='Restrict to unlogged users'),
                ),
                ('title', models.CharField(max_length=150, verbose_name='Title', blank=True)),
                ('url', models.URLField(verbose_name='URL', blank=True)),
                ('empty_label', models.CharField(default=b'---', max_length=64, verbose_name='Empty label')),
                (
                    'parameters',
                    JSONField(
                        default=dict,
                        help_text=(
                            'Must be a JSON list, containing dictionaries with 3 keys: name, value and optionnally roles; '
                            'name must be a string, value must be a dictionary and roles must a list of role names. '
                            'Role names limit the visibility of the choice.'
                        ),
                        verbose_name='Parameters',
                        blank=True,
                    ),
                ),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Roles', blank=True)),
                ('page', models.ForeignKey(to='data.Page', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Parameters',
            },
            bases=(models.Model,),
        ),
    ]
