from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0055_page_placeholer_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='configjsoncell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='feedcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='fortunecell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='jsoncell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='linkcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='linklistcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='menucell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='parentcontentcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='textcell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AddField(
            model_name='unlockmarkercell',
            name='condition',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Display condition'),
        ),
        migrations.AlterField(
            model_name='linkcell',
            name='title',
            field=models.CharField(blank=True, max_length=150, verbose_name='Label'),
        ),
    ]
