# combo - content management system
# Copyright (C) 2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _


class MissingSubSlug(Exception):
    def __init__(self, page):
        self.page = page


class ImportSiteError(Exception):
    pass


class MissingGroups(ImportSiteError):
    def __init__(self, names):
        self.names = names

    def __str__(self):
        return _('Missing groups: %s') % ', '.join(self.names)


class PostException(Exception):
    pass
