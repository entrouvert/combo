# combo - content management system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy

from django import forms
from django.template import Template, TemplateSyntaxError
from django.utils.translation import gettext_lazy as _

from combo.utils import cache_during_request

from .models import ConfigJsonCell, LinkCell, LinkListCell, MenuCell, Page
from .widgets import Select2Widget


@cache_during_request
def get_page_choices():
    pages = Page.get_as_reordered_flat_hierarchy(Page.objects.all())
    return [(x.id, '%s%s' % ('\u00a0' * x.level * 2, x.title)) for x in pages]


class MenuCellForm(forms.ModelForm):
    class Meta:
        model = MenuCell
        fields = ('depth', 'initial_level', 'root_page')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['root_page'].widget = forms.Select(choices=get_page_choices())


class LinkCellForm(forms.ModelForm):
    class Meta:
        model = LinkCell
        fields = ('title', 'url', 'link_page', 'anchor')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        selected_choice_id = self.instance.link_page.id if self.instance.link_page else None
        self.fields['link_page'].widget = Select2Widget(
            selected_choice_id, choices=[(None, '-----')] + get_page_choices(), model=Page
        )


class LinkCellForLinkListCellForm(LinkCellForm):
    class Meta:
        model = LinkCell
        fields = (
            'title',
            'url',
            'link_page',
            'anchor',
            'bypass_url_validity_check',
            'extra_css_class',
            'condition',
        )

    def clean_condition(self):
        condition = self.cleaned_data['condition']
        try:
            Template('{%% if %s %%}OK{%% endif %%}' % condition)
        except TemplateSyntaxError:
            raise forms.ValidationError(_('Invalid syntax.'))

        return condition


class LinkListCellForm(forms.ModelForm):
    class Meta:
        model = LinkListCell
        fields = ['limit']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.get_items_with_prefetch():
            self.is_not_default = True


class ConfigJsonForm(forms.ModelForm):
    formdef = []

    class Meta:
        model = ConfigJsonCell
        fields = ('parameters',)
        widgets = {'parameters': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        parameters = copy.copy(kwargs['instance'].parameters or {})
        # reset parameters in instance as the value is actually created from
        # additional fields.
        kwargs['instance'].parameters = None
        super().__init__(*args, **kwargs)

        field_classes = {
            'string': forms.CharField,
            'bool': forms.BooleanField,
        }
        widget_classes = {
            'text': forms.widgets.Textarea,
        }
        for field in self.formdef:
            field_class = field_classes.get(field.get('type'), forms.CharField)
            self.fields['parameter_%s' % field['varname']] = field_class(
                label=field['label'],
                required=field.get('required', True),
                initial=parameters.get(field['varname']),
                widget=widget_classes.get(field.get('type')),
            )

    def clean(self):
        self.cleaned_data['parameters'] = {}
        for field in self.formdef:
            varname = 'parameter_%s' % field['varname']
            if varname not in self.cleaned_data:
                continue
            self.cleaned_data['parameters'][field['varname']] = self.cleaned_data[varname]
        return self.cleaned_data
