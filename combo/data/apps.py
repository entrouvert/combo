# combo - content management system
# Copyright (C) 2014-2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import AppConfig


class DataConfig(AppConfig):
    name = 'combo.data'
    verbose_name = 'data'

    def hourly(self):
        from combo.data.library import get_cell_classes
        from combo.data.models import CellBase

        cell_classes = [c for c in self.get_models() if c in get_cell_classes()]

        for cell in CellBase.get_cells(
            cell_filter=lambda x: x in cell_classes, page__snapshot__isnull=True, get_all_objects=True
        ):
            if hasattr(cell, 'check_validity'):
                cell.check_validity()
