# combo - content management system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys

from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext_lazy as _

from combo.data.utils import export_site, export_site_tar


class Command(BaseCommand):
    help = 'Export the site'

    def add_arguments(self, parser):
        parser.add_argument(
            '--output', metavar='FILE', default=None, help='name of a file to write output to'
        )
        parser.add_argument(
            '--format-json', action='store_true', default=False, help='use JSON format with no asset files'
        )
        parser.add_argument('--only-assets', action='store_true', default=False, help='only export assets')

    def handle(self, *args, **options):
        export_kwargs = {}
        if options.get('only_assets'):
            export_kwargs = {
                'pages': False,
                'cartography': False,
                'pwa': False,
                'assets': True,
                'payment': False,
                'site_settings': False,
            }

        if options['format_json']:
            if options['output'] and options['output'] != '-':
                with open(options['output'], 'w') as output:
                    json.dump(export_site(**export_kwargs), output, indent=2)
            else:
                json.dump(export_site(**export_kwargs), sys.stdout, indent=2)
            return

        if options['output'] and options['output'] != '-':
            try:
                with open(options['output'], 'wb') as output:
                    export_site_tar(output, export_kwargs=export_kwargs)
            except OSError as e:
                raise CommandError(e)
            return

        raise CommandError(_('TAR format require output filename parameter'))
