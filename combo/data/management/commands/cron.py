# combo - content management system
# Copyright (C) 2014-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import traceback

from django.apps import apps
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Execute scheduled commands'

    def add_arguments(self, parser):
        parser.add_argument(
            '--application',
            dest='application',
            metavar='APPLICATION',
            type=str,
            help='limit updates to given application',
        )

    def handle(self, **options):
        stdout = options.get('stdout', sys.stdout)  # for testing
        errors = []
        for appconfig in apps.get_app_configs():
            if not hasattr(appconfig, 'hourly'):
                continue
            if options.get('application') and appconfig.name.rsplit('.')[-1] != options.get('application'):
                continue
            if hasattr(appconfig, 'is_enabled') and not appconfig.is_enabled():
                continue
            try:
                appconfig.hourly()
            except Exception as e:
                errors.append(
                    {'application': appconfig.name, 'exception': e, 'traceback': traceback.format_exc()}
                )
        if errors:
            for error in errors:
                if options['verbosity'] >= 1:
                    print('%s: error: %s' % (error['application'], error['exception']), file=stdout)
                if options['verbosity'] >= 2:
                    print(error['traceback'], file=stdout)
                    print(file=stdout)
            raise CommandError('error running jobs')
