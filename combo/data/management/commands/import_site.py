# combo - content management system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import contextlib
import json
import sys
import tarfile

from django.core.management.base import BaseCommand, CommandError

from combo.data.exceptions import ImportSiteError
from combo.data.utils import import_site, import_site_tar


class Command(BaseCommand):
    help = 'Import an exported site'

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME', type=str, help='name of file to import')
        parser.add_argument('--clean', action='store_true', default=False, help='Clean site before importing')
        parser.add_argument(
            '--if-empty', action='store_true', default=False, help='Import only if site is empty'
        )
        parser.add_argument('--overwrite', action='store_true', default=False, help='Overwrite asset files')

    def handle(self, filename, *args, **options):
        def _import_site_json(fd):
            import_site(json.load(fd), if_empty=options['if_empty'], clean=options['clean'])

        def _import_site_tar(fd):
            import_site_tar(
                fd,
                if_empty=options['if_empty'],
                clean=options['clean'],
                overwrite=options['overwrite'],
            )

        with contextlib.ExitStack() as stack:
            try:
                if filename == '-':
                    _import_site_json(sys.stdin)
                    return

                try:
                    with open(filename, 'rb') as _f:
                        fd = stack.enter_context(_f)
                        try:
                            with tarfile.open(mode='r', fileobj=fd):
                                pass
                        except tarfile.TarError:
                            with open(filename) as json_fd:
                                _import_site_json(json_fd)
                        else:
                            with open(filename, 'rb') as tar_fd:
                                _import_site_tar(tar_fd)
                except OSError as e:
                    raise CommandError(e)
            except ImportSiteError as e:
                raise CommandError(e)
