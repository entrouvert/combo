# combo - content management system
# Copyright (C) 2014  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict


class Library:
    """Singleton object that serves as a registry of the classes
    providing page cells."""

    def __init__(self):
        # Preserve order of registration, to have the same order in page-view sidebar
        self.classes_by_content_str = OrderedDict()

    def get_cell_classes(self):
        return self.classes_by_content_str.values()

    def get_cell_class(self, content_type_str):
        return self.classes_by_content_str.get(content_type_str)

    def register_cell_class(self, klass):
        self.classes_by_content_str[klass.get_cell_type_str()] = klass
        return klass


library = Library()  # singleton object
register_cell_class = library.register_cell_class
get_cell_classes = library.get_cell_classes
get_cell_class = library.get_cell_class
