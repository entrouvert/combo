import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    include: 'tests/js/**/*.test.js',
    watchExclude: ['**'],
    coverage: {
      include: ['combo/**/*.js'],
      all: true,
      reporter: ['cobertura', 'html'],
    },
    environment: 'happy-dom',
  },
})

