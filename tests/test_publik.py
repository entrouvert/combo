import copy
import json
import re

import pytest
from django.conf import settings
from django.contrib.auth.models import Group

from combo.data.models import Page

from . import test_manager

pytestmark = pytest.mark.django_db


def login(app):
    test_manager.login(app, username='normal-user', password='normal-user')


class KnownServices:
    def __init__(self, known_services):
        self.known_services = known_services
        self.orig_value = settings.KNOWN_SERVICES

    def __enter__(self):
        settings.KNOWN_SERVICES = self.known_services

    def __exit__(self, *args, **kwargs):
        settings.KNOWN_SERVICES = self.orig_value


def test_services_js_no_services(app, normal_user):
    with KnownServices({}):
        app.get('/__services.js', status=302)
        login(app)
        app.get('/__services.js', status=404)


def test_services_js_multiple_wcs(settings, app, normal_user):
    KNOWN_SERVICES = copy.deepcopy(settings.KNOWN_SERVICES)
    del KNOWN_SERVICES['wcs']['default']['secondary']
    del KNOWN_SERVICES['wcs']['other']['secondary']
    settings.KNOWN_SERVICES = KNOWN_SERVICES
    login(app)
    resp = app.get('/__services.js', status=200)
    assert resp.content_type == 'text/javascript'
    assert 'var PUBLIK_ENVIRONMENT_LABEL = null' in resp.text
    assert 'var PUBLIK_PORTAL_AGENT_URL = null' in resp.text
    assert '"uniq": false' in resp.text


def test_services_js_portal_agent(app, normal_user):
    app.get('/__services.js', status=302)  # authentication required
    login(app)
    known_services = {
        'combo': {
            'portal-agent': {
                'title': 'Portal Agent',
                'url': 'http://example.net',
                'backoffice-menu-url': 'http://example.net/manage/',
                'is-portal-agent': True,
            },
            'portal-user': {
                'title': 'Portal User',
                'url': 'http://example.com',
                'backoffice-menu-url': 'http://example.com/manage/',
            },
        }
    }
    with KnownServices(known_services):
        resp = app.get('/__services.js', status=200)
        assert resp.content_type == 'text/javascript'
        assert 'var PUBLIK_PORTAL_AGENT_URL = "http://example.net";' in resp.text
        assert 'var PUBLIK_PORTAL_AGENT_TITLE = "Portal Agent"' in resp.text


def test_services_js_explicit_menu(app, normal_user, settings):
    settings.PUBLIK_EXPLICIT_MENU = True

    group1 = Group.objects.create(name='plop1')
    group2 = Group.objects.create(name='plop2')

    index_page = Page.objects.create(
        title='Home', slug='index', template_name='standard', public=False, exclude_from_navigation=False
    )
    index_page.groups.set([group1])
    other_page = Page.objects.create(
        title='Page', slug='page', template_name='standard', public=False, exclude_from_navigation=False
    )
    other_page.groups.set([group1])
    sub_page = Page.objects.create(
        title='Subpage',
        slug='subpage',
        template_name='standard',
        parent=other_page,
        public=False,
        exclude_from_navigation=False,
    )
    sub_page.groups.set([group2])
    last_page = Page.objects.create(
        title='Last page',
        slug='lastpage',
        template_name='standard',
        public=False,
        exclude_from_navigation=False,
    )
    last_page.groups.set([group1])

    def get_menu_var(resp):
        return json.loads(re.findall('var PUBLIK_MENU_ITEMS = (.*);', resp.text)[0])

    login(app)
    resp = app.get('/__services.js', status=200)
    assert [x['slug'] for x in get_menu_var(resp)] == []

    normal_user.groups.set([group1])
    resp = app.get('/__services.js', status=200)
    assert [x['slug'] for x in get_menu_var(resp)] == ['home', 'page', 'lastpage']
    assert [x['url'] for x in get_menu_var(resp)] == [
        'http://testserver/',
        'http://testserver/page/',
        'http://testserver/lastpage/',
    ]
    assert len(get_menu_var(resp)) == 3

    normal_user.groups.set([group1, group2])
    resp = app.get('/__services.js', status=200)
    assert [x['slug'] for x in get_menu_var(resp)] == ['home', 'page', 'subpage', 'lastpage']
