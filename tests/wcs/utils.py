import copy
import json
import re
import urllib.parse
from unittest import mock

from django.http import QueryDict

WCS_FORMDEFS_DATA = [
    {'slug': 'a-private-form', 'title': 'a private form', 'url': '/a-private-form/'},
    {'slug': 'a-second-form-title', 'title': 'a second form title', 'url': '/a-second-form-title/'},
    {
        'slug': 'form-title',
        'title': 'too long form title' + 'e' * 240,
        'url': '/form-title/',
        'keywords': ['foo', 'bar'],
        'description': '<p>a form description</p>',
    },
    {'slug': 'third-form-title', 'title': 'Third form title', 'url': '/third-form-title/'},
]

WCS_CATEGORIES_DATA = [
    {
        'slug': 'test-%s' % i,
        'title': 'Test %s' % i,
        'url': '/test-%s/' % i,
        'description': 'category %s description' % i,
    }
    for i in [3, 9]
]

WCS_CATEGORIES_FORMDEFS_DATA = [
    {
        'slug': 'form-title',
        'title': 'form title',
        'url': '/form-title/',
        'keywords': ['foo', 'bar'],
        'count': 42,
        'description': '<p>a form description</p>',
    },
    {
        'slug': 'a-second-form-title',
        'title': 'a second form title',
        'url': '/a-second-form-title/',
        'count': 35,
    },
]

WCS_USER_FORMS_DATA = [
    {
        'name': 'name',
        'title': 'Title',
        'url': '/form/1/',
        'form_receipt_datetime': '2015-01-01T00:00:00',
        'readable': True,
        'category_slug': 'test-9',
    },
    {'title': 'name', 'url': '/form/2/', 'form_receipt_datetime': '2015-01-01T00:00:00', 'readable': True},
    {'name': 'name', 'title': 'Title', 'url': '/form/3/', 'form_receipt_datetime': '2015-01-01T00:00:00'},
]

WCS_FORMS_DATA = [
    {
        'form_receipt_datetime': '2019-10-17T16:46:03',
        'form_url': '/foobar/1',
        'form_url_backoffice': '/backoffice/management/foobar/1/',
    },
    {
        'form_receipt_datetime': '2019-10-17T16:46:04',
        'form_url': '/foobar/2',
        'form_url_backoffice': '/backoffice/management/foobar/2/',
    },
]

WCS_USER_DRAFTS_DATA = [
    {
        'name': 'name',
        'title': 'Title',
        'url': '/form/1/',
        'form_receipt_datetime': '2015-01-01T00:00:00',
        'category_slug': 'test-9',
    },
    {'name': 'name', 'url': '/form/2/', 'form_receipt_datetime': '2015-01-01T00:00:00'},
    {'name': 'name', 'title': 'Title', 'url': '/form/3/', 'form_receipt_datetime': '2015-01-01T00:00:00'},
    {
        'name': 'name',
        'title': 'Title',
        'url': '/form/4/',
        'form_receipt_datetime': '2015-01-01T00:00:00',
        'form_status_is_endpoint': True,
        'category_slug': 'test-9',
    },
]

WCS_CARDDEFS_DATA = [
    {
        'title': 'Card Model 1',
        'text': 'Card Model 1',
        'slug': 'card_model_1',
        'id': 'card_model_1',
        'custom_views': [{'id': 'foo', 'text': 'bar'}],
    },
    {'title': 'Card Model 2', 'text': 'Card Model 2', 'slug': 'card_model_2', 'id': 'card_model_2'},
    {'title': 'Card Model 3', 'text': 'Card Model 3', 'slug': 'card_model_3', 'id': 'card_model_3'},
    {
        'title': 'Card A',
        'text': 'Card A',
        'slug': 'card_a',
        'id': 'card_a',
        'custom_views': [{'id': 'a-custom-view', 'text': 'foo bar'}],
    },
    {
        'title': 'Card B',
        'text': 'Card B',
        'slug': 'card_b',
        'id': 'card_b',
        'custom_views': [{'id': 'b-custom-view', 'text': 'foo bbr'}],
    },
    {'title': 'Card C', 'text': 'Card C', 'slug': 'card_c', 'id': 'card_c'},
    {'title': 'Card D', 'text': 'Card D', 'slug': 'card_d', 'id': 'card_d'},
    {'title': 'Card E', 'text': 'Card E', 'slug': 'card-e', 'id': 'card_e'},
]

WCS_CARDS_DATA = {
    'card_model_1': [
        {
            'id': '11',
            'display_id': '10-11',
            'display_name': 'Card Model 1 - n°10-11',
            'digest': 'a a a',
            'digests': {
                'default': 'a a a',
                'custom-view:foo': 'afoo afoo afoo',
                'custom-view:bar': 'a\na\na',
                'custom-view:utf8': 'L’€ ne fait pas le bonheur',
            },
            'text': 'aa',
            'receipt_time': '2021-11-29T14:33:37',
            'last_update_time': '2023-07-24T16:04:24',
            'url': '/backoffice/data/card_model_1/11/',
            'fields': {
                'fielda': '<i>a</i>',
                'fieldb': True,
                'fieldc': '2020-09-28',
                'fieldd': {'filename': 'file.pdf', 'url': 'http://127.0.0.1:8999/download?f=42'},
                'fieldd2': {
                    'filename': 'file.pdf',
                    'url': 'http://127.0.0.1:8999/download?f=42',
                    'thumbnail_url': 'http://127.0.0.1:8999/download?f=42&thumbnail=1',
                },
                'fielde': "lorem<strong>ipsum\n\nhello'world",
                'fieldf': 'lorem<strong>ipsum\n\nhello world',
                'fieldg': 'test@localhost',
                'fieldh': 'https://www.example.net/',
                'fieldi': "<p>lorem<strong>ipsum</p><p>hello'world</p>",
                'fieldii': "<p>lorem<strong>ipsum</p><p>hello'world</p>",
                'fieldj': 'First Value, Second Value \'',
                'fieldj_raw': ['first value', 'second value \''],
                'fieldk': 'test',
                'fieldm': 'value1, value2',
                'fieldm_raw': [{'x': 'value1'}, {'x': 'value2'}],
                'fieldm_digests': ['value1', 'value2'],
                'related': 'Foo Bar',
                'related_raw': 42,
                'related_structured': {'id': 42, 'text': 'blah'},
            },
            'user': {
                'name': 'User Foo Bar',
                'email': 'foo@bar.com',
                'first_name': 'User',
                'last_name': 'Foo Bar',
            },
            'workflow': {
                'real_status': {'id': 'recorded', 'name': 'Recorded'},
                'status': {'id': 'recorded', 'name': 'Recorded'},
                'fields': {
                    'item': 'foo',
                    'item_raw': 'foo',
                },
            },
            'actions': {'jump:trigger-1': 'https://jump.test/trigger-1'},
        },
        {
            'id': '12',
            'display_id': '10-12',
            'display_name': 'Card Model 1 - n°10-12',
            'digest': 'b b b',
            'text': 'bb',
            'url': '/backoffice/data/card_model_1/12/',
            'fields': {
                'fieldj': 'First Value, Third Value',
                'fieldj_raw': ['first value', 'third value'],
                'related': 'Abc',
                'related_raw': 13,
            },
            'workflow': {
                'real_status': {'id': 'deleted', 'name': 'Deleted'},
                'status': {'id': 'deleted', 'name': 'Deleted'},
                'fields': {
                    'item': 'bar',
                    'item_raw': 'bar',
                },
            },
        },
        {
            'id': '13',
            'display_id': '10-13',
            'display_name': 'Card Model 1 - n°10-13',
            'digest': 'c c c',
            'text': 'cc',
            'url': '/backoffice/data/card_model_1/13/',
            'fields': {
                'fieldj': None,
                'fieldj_raw': None,
                'related': None,
                'related_raw': None,
            },
            'workflow': {},
        },
    ],
    'card_a': [
        {
            'id': '1',
            'fields': {
                'cardb_raw': 1,
                'cardsb_raw': [2, 3],
                'blockb_raw': [{'cardb_raw': 4}, {'cardb_raw': 5}],
                'cardc_raw': 6,
                'cardz_raw': 42,
            },
            'workflow': {
                'fields': {
                    'bocardb_raw': 1,
                },
            },
        },
        {
            'id': '2',
            'fields': {
                'cardb_raw': 1,
                'cardsb_raw': [2, 3],
                'cardsb': ['Foo', 'Bar'],
                'blockb_raw': [{'cardb_raw': 4}, {'cardb_raw': 5}],
                'cardc_raw': 61,  # unknown card_c
            },
        },
        {
            'id': '3',
            'fields': {
                # some missing fields
                'blockb_raw': [{}],
            },
        },
        {
            'id': '4',
            'fields': {
                # some empty fields
                'cardb_raw': None,
                'cardsb_raw': None,
                'blockb_raw': [{'cardb_raw': None}],
                'cardc_raw': 7,
            },
        },
    ],
    'card_b': [
        {'id': i, 'actions': {'jump:trigger-1': 'https://jump.test/trigger-1'}, 'fields': {}}
        for i in range(1, 12)
        if i != 6
    ],
    'card_c': [
        {
            'id': '6',
            'fields': {
                'cardb_raw': 7,
                'cardsb_raw': [8, 9],
                'blockb_raw': [{'cardb_raw': 10}, {'cardb_raw': 11}],
            },
        },
        {
            'id': '7',
            'fields': {},
        },
    ],
    'card_f': [
        {'id': '41', 'fields': {'cardh': '42', 'cardh_raw': 42}},
    ],
    'card_g': [
        {'id': '43', 'fields': {'cardh_raw': 44}},
    ],
    'card_h': [
        {'id': '42', 'fields': {}},
        {'id': '44', 'fields': {}},
    ],
    'card_with_custom_id': [
        {
            'id': 'foo_-Z',
            'internal_id': '42',
            'text': 'foo',
            'url': '/backoffice/data/card_with_custom_id/foo_-Z/',
            'fields': {},
        },
        {
            'id': 'bar_-Z',
            'internal_id': '44',
            'text': 'bar',
            'url': '/backoffice/data/card_with_custom_id/bar_-Z/',
            'fields': {},
        },
    ],
}

WCS_CARDS_FILTERS_OPTIONS = {
    'card_model_1': {
        'related': [
            {'id': '13', 'text': 'Abc'},
            {'id': '42', 'text': 'Foo Bar'},
        ],
        'fieldj': [
            {'id': 'first value', 'text': 'First Value'},
            {'id': "second value '", 'text': "Second Value '"},
            {'id': 'third value', 'text': 'Third Value'},
        ],
        'item': [
            {'id': 'bar', 'text': 'bar'},
            {'id': 'foo', 'text': 'foo'},
        ],
    }
}

WCS_CARDS_CUSTOM_VIEW_DATA = [
    {
        'id': 11,
        'display_id': '10-11',
        'display_name': 'Card Model 1 - n°10-11',
        'digest': 'a a a',
        'digests': {'default': 'a a a', 'custom-view:foo': 'afoo afoo afoo'},
        'text': 'aa',
        'url': '/backoffice/data/card_model_1/11/',
    },
    {
        'id': 12,
        'display_id': '10-12',
        'display_name': 'Card Model 1 - n°10-12',
        'digest': 'b b b',
        'text': 'bb',
        'url': '/backoffice/data/card_model_1/12/',
    },
]

WCS_CARDDEF_SCHEMAS = {
    'card_model_1': {
        'name': 'Card Model 1',
        'fields': [
            {'label': 'Field A', 'varname': 'fielda', 'type': 'string'},
            {'label': 'Field B', 'varname': 'fieldb', 'type': 'bool'},
            {'label': 'Field C', 'varname': 'fieldc', 'type': 'date'},
            {'label': 'Field D', 'varname': 'fieldd', 'type': 'file'},
            {'label': 'Field D2', 'varname': 'fieldd2', 'type': 'file'},
            {'label': 'Field E', 'varname': 'fielde', 'type': 'text'},
            {'label': 'Field F', 'varname': 'fieldf', 'type': 'text', 'display_mode': 'pre'},
            {'label': 'Field G', 'varname': 'fieldg', 'type': 'email'},
            {'label': 'Field H', 'varname': 'fieldh', 'type': 'string'},
            {'label': 'Field I', 'varname': 'fieldi', 'type': 'text', 'display_mode': 'rich'},
            {'label': 'Field II', 'varname': 'fieldii', 'type': 'text', 'display_mode': 'basic-rich'},
            {'label': 'Field J', 'varname': 'fieldj', 'type': 'items'},
            {'label': 'Field K', 'type': 'item'},
            {'label': 'Field L', 'varname': 'fieldl', 'type': 'item', 'items': ['A', 'C', 'B']},
            {'label': 'Field M', 'varname': 'fieldm', 'type': 'block:xxx'},
            {'label': 'Empty', 'varname': 'empty', 'type': 'string'},
            {'label': 'Empty Email', 'varname': 'empty_email', 'type': 'email'},
            {'label': 'Related', 'varname': 'related', 'type': 'item'},
            {'label': 'Page', 'type': 'page', 'varname': 'page'},
            {'label': 'Comment', 'type': 'comment'},
            {'label': 'Title', 'type': 'title'},
            {'label': 'Subtitle', 'type': 'subtitle'},
            {'label': 'Empty', 'varname': None, 'type': 'string'},
        ],
        'user': {
            'fields': [
                {'label': 'Name', 'varname': 'name', 'type': 'string'},
                {'label': 'Email', 'varname': 'email', 'type': 'email'},
                {'label': 'First name', 'varname': 'first_name', 'type': 'string'},
                {'label': 'Last name', 'varname': 'last_name', 'type': 'string'},
            ],
        },
        'workflow': {
            'fields': [
                {'label': 'Item', 'varname': 'item', 'type': 'item'},
            ],
            'functions': {'_editor': 'Editor', '_viewer': 'Viewer'},
            'name': 'default',
            'statuses': [
                {
                    'endpoint': False,
                    'forced_endpoint': False,
                    'id': 'recorded',
                    'name': 'Recorded',
                    'waitpoint': True,
                },
                {
                    'endpoint': True,
                    'forced_endpoint': False,
                    'id': 'deleted',
                    'name': 'Deleted',
                    'waitpoint': True,
                },
            ],
        },
    },
    'card_a': {
        'name': 'Card A',
        'fields': [
            {'label': 'Card B', 'varname': 'cardb', 'type': 'item'},
            {'label': 'Cards B', 'varname': 'cardsb', 'type': 'items'},
            {'label': 'Block B', 'varname': 'blockb', 'type': 'block:b'},
            {'label': 'Card C', 'varname': 'cardc', 'type': 'item'},
        ],
        'workflow': {
            'fields': [
                {'label': 'BO Card B', 'varname': 'bocardb', 'type': 'item'},
            ],
        },
        'relations': [
            {
                'obj': 'carddef:card_b',
                'varname': 'cardb',
                'label': 'Card B',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_b',
                'varname': 'cardsb',
                'label': 'Cards B',
                'type': 'items',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_b',
                'varname': 'blockb_cardb',
                'label': 'Block B - Card B',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_c',
                'varname': 'cardc',
                'label': 'Card C',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_z',  # unknown card model
                'varname': 'cardz',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_b',
                'varname': 'bocardb',
                'label': 'BO Card B',
                'type': 'item',
                'reverse': False,
            },
        ],
    },
    'card_b': {
        'name': 'Card B',
        'fields': [],
        'relations': [
            {'obj': 'carddef:card_a', 'varname': 'cardb', 'label': 'Card B', 'type': 'item', 'reverse': True},
            {
                'obj': 'carddef:card_a',
                'varname': 'bocardb',
                'label': 'BO Card B',
                'type': 'item',
                'reverse': True,
            },
            {
                'obj': 'carddef:card_a',
                'varname': 'cardsb',
                'label': 'Cards B',
                'type': 'items',
                'reverse': True,
            },
            {
                'obj': 'carddef:card_a',
                'varname': 'blockb_cardb',
                'label': 'Block B - Card B',
                'type': 'item',
                'reverse': True,
            },
            {'obj': 'carddef:card_c', 'varname': 'cardb', 'label': 'Card B', 'type': 'item', 'reverse': True},
            {
                'obj': 'carddef:card_c',
                'varname': 'cardsb',
                'label': 'Cards B',
                'type': 'items',
                'reverse': True,
            },
            {
                'obj': 'carddef:card_c',
                'varname': 'blockb_cardb',
                'label': 'Block B - Card B',
                'type': 'item',
                'reverse': True,
            },
        ],
    },
    'card_c': {
        'name': 'Card C',
        'fields': [
            {'label': 'Card B', 'varname': 'cardb', 'type': 'item'},
            {'label': 'Cards B', 'varname': 'cardsb', 'type': 'items'},
            {'label': 'Block B', 'varname': 'blockb', 'type': 'block:b'},
        ],
        'relations': [
            {
                'obj': 'carddef:card_b',
                'varname': 'cardb',
                'label': 'Card B',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_b',
                'varname': 'cardsb',
                'label': 'Cards B',
                'type': 'items',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_b',
                'varname': 'blockb_cardb',
                'label': 'Block B - Card B',
                'type': 'item',
                'reverse': False,
            },
            {'obj': 'carddef:card_a', 'varname': 'cardc', 'label': 'Card C', 'type': 'item', 'reverse': True},
        ],
    },
    'card_d': {
        'name': 'Card D',
        'fields': [
            {'label': 'Card D', 'varname': 'cardd-foo', 'type': 'item'},
            {'label': 'Card E', 'varname': 'carde-foo', 'type': 'item'},
        ],
        'relations': [
            {
                'obj': 'carddef:card_d',
                'varname': 'cardd-foo',
                'label': 'Card D',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_d',
                'varname': 'cardd-foo',
                'label': 'Card D',
                'type': 'item',
                'reverse': True,
            },
            {
                'obj': 'carddef:card_e',
                'varname': 'carde-foo',
                'label': 'Card E',
                'type': 'item',
                'reverse': False,
            },
        ],
    },
    'card_e': {
        'name': 'Card E',
        'fields': [
            {'label': 'Card D', 'varname': 'cardd-bar', 'type': 'item'},
        ],
        'relations': [
            {
                'obj': 'carddef:card_d',
                'varname': 'cardd-bar',
                'label': 'Card D',
                'type': 'item',
                'reverse': False,
            },
            {
                'obj': 'carddef:card_d',
                'varname': 'carde-foo',
                'label': 'Card E',
                'type': 'item',
                'reverse': True,
            },
        ],
    },
    'card_f': {
        'name': 'Card F',
        'fields': [
            {'label': 'Card H', 'varname': 'cardh', 'type': 'item'},
        ],
        'relations': [
            {
                'obj': 'carddef:card_h',
                'varname': 'cardh',
                'label': 'Card H',
                'type': 'item',
                'reverse': False,
            },
        ],
    },
    'card_g': {
        'name': 'Card G',
        'fields': [
            {'label': 'Card H', 'varname': 'cardh', 'type': 'item'},
        ],
        'relations': [
            {
                'obj': 'carddef:card_h',
                'varname': 'cardh',
                'label': 'Card H',
                'type': 'item',
                'reverse': False,
            },
        ],
    },
    'card_h': {
        'name': 'Card H',
        'fields': [],
        'relations': [
            {'obj': 'carddef:card_f', 'varname': 'cardh', 'label': 'Card H', 'type': 'item', 'reverse': True},
            {'obj': 'carddef:card_g', 'varname': 'cardh', 'label': 'Card H', 'type': 'item', 'reverse': True},
        ],
    },
}


class MockUser:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False

    def get_name_id(self):
        return None


class MockUserWithNameId:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False
    is_superuser = False

    def get_name_id(self):
        return 'xyz'


class MockedRequestResponse(mock.Mock):
    status_code = 200

    def json(self):
        return json.loads(self.content)


def get_data_from_url(url, query):
    if '/api/formdefs/' in url:
        return WCS_FORMDEFS_DATA
    if '/api/categories/' in url:
        if '/formdefs/' in url:
            return WCS_CATEGORIES_FORMDEFS_DATA
        return WCS_CATEGORIES_DATA
    if '/api/user/forms' in url:
        return WCS_USER_FORMS_DATA
    if '/api/forms' in url:
        return WCS_FORMS_DATA
    if '/api/user/drafts' in url:
        return WCS_USER_DRAFTS_DATA
    if '/api/cards/@list' in url:
        return WCS_CARDDEFS_DATA
    m_schema = re.match(r'/api/cards/([a-z0-9_]+)/@schema', url)
    if m_schema:
        return WCS_CARDDEF_SCHEMAS.get(m_schema.group(1)) or {}
    m_card = re.match(r'/api/cards/([a-z0-9_]+)/(\d+)/', url)
    if m_card:
        try:
            return [d for d in WCS_CARDS_DATA[m_card.group(1)] if str(d['id']) == str(m_card.group(2))][0]
        except IndexError:
            return {}
    m_card = re.match(r'/api/cards/([a-z0-9_]+)/([a-z0-9_-]+)/(\d+)/', url)  # model/custom-view/id
    if m_card:
        try:
            return [d for d in WCS_CARDS_DATA[m_card.group(1)] if str(d['id']) == str(m_card.group(3))][0]
        except IndexError:
            return {}

    params = QueryDict(query)

    def filter_cards(values):
        if 'filter-identifier' in params:
            # filter cards on ids
            ids = params['filter-identifier'].split(',')
            if params.get('filter-identifier-operator') == 'ne':
                values = [d for d in values if str(d['id']) not in ids]
            else:
                values = [d for d in values if str(d['id']) in ids]
        limit = 100
        if 'limit' in params:
            limit = int(params['limit'])
        offset = 0
        if 'offset' in params:
            offset = int(params['offset'])
        if 'filter-user-uuid=' in query:
            # return cards with associated user if filter by user
            return [d for d in values if 'user' in d]

        return values[offset : offset + limit]

    if 'api/cards/card_model_1/list/foo' in url:
        return filter_cards(WCS_CARDS_CUSTOM_VIEW_DATA)
    m_list = re.match(r'/api/cards/([a-z0-9_]+)/list', url)
    if m_list:
        return filter_cards(WCS_CARDS_DATA.get(m_list.group(1)) or [])
    m_filter = re.match(r'/api/cards/([a-z0-9_]+)/filter-options', url)
    m_field = re.match(r'.*filter_field_id=([a-z0-9_-]+).*', query)
    if m_filter and m_field:
        return WCS_CARDS_FILTERS_OPTIONS.get(m_filter.group(1), {}).get(m_field.group(1)) or []
    return []


def mocked_requests_send(request, **kwargs):
    request_url = urllib.parse.urlparse(request.url)
    data = copy.deepcopy(get_data_from_url(request_url.path, request_url.query))

    if not isinstance(data, list):
        return MockedRequestResponse(content=json.dumps(data))

    for elem in data:
        for key in ['url', 'form_url', 'form_url_backoffice']:
            if key not in elem:
                continue
            elem_url = elem[key]
            elem[key] = '{scheme}://{netloc}{url}'.format(
                scheme=request_url.scheme, netloc=request_url.netloc, url=elem_url
            )

    json_data = {'data': data}
    if re.match(r'/api/cards/([a-z0-9_]+)/list', request_url.path):
        # update response to add fake pagination infos
        json_data.update({'count': len(data), 'previous': 'http://', 'next': 'http://'})
    return MockedRequestResponse(content=json.dumps(json_data))


def responses_callback(request):
    request_url = urllib.parse.urlparse(request.url)
    data = copy.deepcopy(get_data_from_url(request_url.path, request_url.query))

    if not isinstance(data, list):
        return (200, {'Content-Type': 'application/json'}, json.dumps(data))

    for elem in data:
        for key in ['url', 'form_url', 'form_url_backoffice']:
            if key not in elem:
                continue
            elem_url = elem[key]
            elem[key] = '{scheme}://{netloc}{url}'.format(
                scheme=request_url.scheme, netloc=request_url.netloc, url=elem_url
            )

    return (200, {'Content-Type': 'application/json'}, json.dumps({'data': data}))
