import json
import re
from unittest import mock

import pyquery
import pytest
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.core import signing
from django.core.cache import cache
from django.db import connection
from django.test.client import RequestFactory
from django.test.utils import CaptureQueriesContext
from django.urls import reverse
from pyquery import PyQuery
from requests.exceptions import ConnectionError
from requests.models import Response

from combo.apps.search.engines import engines
from combo.apps.search.models import SearchCell
from combo.apps.search.utils import index_site, search_site
from combo.apps.wcs.models import (
    BackofficeSubmissionCell,
    CategoriesCell,
    TrackingCodeInputCell,
    WcsCardCell,
    WcsCareFormsCell,
    WcsCategoryCell,
    WcsCurrentDraftsCell,
    WcsCurrentFormsCell,
    WcsFormCell,
    WcsFormsOfCategoryCell,
)
from combo.data.library import get_cell_classes
from combo.data.models import CellBase, LinkCell, LinkListCell, Page, ValidityInfo
from combo.utils import NothingInCacheException
from tests.test_manager import login
from tests.utils import manager_submit_cell

from .utils import MockedRequestResponse, MockUser, MockUserWithNameId, mocked_requests_send

pytestmark = pytest.mark.django_db


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_form_cell_setup(mock_send):
    cell = WcsFormCell()
    form_class = cell.get_default_form_class()
    form = form_class()
    assert form.fields['formdef_reference'].widget.choices == [
        ('default:a-private-form', 'test : a private form'),
        ('default:a-second-form-title', 'test : a second form title'),
        ('default:form-title', 'test : too long form title' + 'e' * 240),
        ('default:third-form-title', 'test : Third form title'),
        ('other:a-private-form', 'test2 : a private form'),
        ('other:a-second-form-title', 'test2 : a second form title'),
        ('other:form-title', 'test2 : too long form title' + 'e' * 240),
        ('other:third-form-title', 'test2 : Third form title'),
    ]
    assert 'extra_css_class' not in form.fields


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_form_cell_save_cache(mock_send):
    page = Page(title='xxx', slug='test_form_cell_save_cache', template_name='standard')
    page.save()
    cell = WcsFormCell(page=page, placeholder='content', order=0)
    assert cell.get_additional_label() is None
    cell.formdef_reference = 'default:form-title'
    cell.save()
    assert cell.cached_title == 'too long form title' + 'e' * 231
    assert cell.get_additional_label() == 'too long form title' + 'e' * 231
    # make sure cached attributes are removed from serialized pages
    assert 'cached_' not in json.dumps(page.get_serialized_page())

    # check content provided to search engine
    assert cell.render_for_search() == ''
    assert cell.get_external_links_data() == [
        {
            'title': 'too long form title' + 'e' * 231,
            'url': 'http://127.0.0.1:8999/form-title/',
            'text': '<p>a form description</p> foo bar',
        }
    ]

    # artificially change title
    WcsFormCell.objects.filter(id=cell.id).update(cached_title='XXX')
    assert WcsFormCell.objects.get(id=cell.id).cached_title == 'XXX'
    # run update db cache
    appconfig = apps.get_app_config('wcs')
    appconfig.update_db_cache()
    assert WcsFormCell.objects.get(id=cell.id).cached_title == 'too long form title' + 'e' * 231


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_form_cell_validity(mock_send):
    page = Page.objects.create(title='xxx', slug='test_form_cell_save_cache', template_name='standard')
    cell = WcsFormCell.objects.create(page=page, placeholder='content', order=0)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_form_not_defined'
    assert validity_info.invalid_since is not None

    cell.formdef_reference = 'default:form-title'
    cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve formdefs, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False

    cell.formdef_reference = 'default:foobar'
    cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_form_not_found'
    assert validity_info.invalid_since is not None
    cell.mark_as_valid()

    cell.formdef_reference = 'invalid:foobar'
    cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_form_cell_load(mock_send):
    page = Page(title='xxx', slug='test_form_cell_save_cache', template_name='standard')
    page.save()
    cell = WcsFormCell(page=page, placeholder='content', order=0)
    cell.formdef_reference = 'default:form-title'
    cell.save()
    site_export = [page.get_serialized_page()]
    cell.delete()
    assert not Page.objects.get(id=page.id).get_cells()
    Page.load_serialized_pages(site_export)
    page = Page.objects.get(slug='test_form_cell_save_cache')
    cells = page.get_cells()
    assert len(cells) == 1
    cell = cells[0]
    assert cell.cached_title == 'too long form title' + 'e' * 231


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_category_cell_save_cache(mock_send, settings):
    settings.CATEGORY_CELL_ENABLED = True
    page = Page(title='xxx', slug='test_category_cell_save_cache', template_name='standard')
    page.save()
    cell = WcsCategoryCell(page=page, placeholder='content', order=0)
    assert cell.get_additional_label() is None
    cell.category_reference = 'default:test-3'
    cell.save()
    assert cell.cached_title == 'Test 3'
    assert cell.get_additional_label() == 'Test 3'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_category_cell_render(mock_send, settings):
    settings.CATEGORY_CELL_ENABLED = True
    page = Page(title='xxx', slug='test_category_cell_save_cache', template_name='standard')
    page.save()
    cell = WcsCategoryCell(page=page, placeholder='content', order=0, category_reference='default:test-3')
    cell.save()

    context = {'synchronous': True}  # to get fresh content
    result = cell.render(context)
    assert 'Test 3' in result
    assert 'category 3 description' in result


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_category_cell_validity(mock_send, settings):
    settings.CATEGORY_CELL_ENABLED = True
    page = Page.objects.create(title='xxx', slug='test_category_cell_save_cache', template_name='standard')
    cell = WcsCategoryCell.objects.create(page=page, placeholder='content', order=0)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_defined'
    assert validity_info.invalid_since is not None

    cell.category_reference = 'default:test-3'
    cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False

    cell.category_reference = 'default:foobar'
    cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None
    cell.mark_as_valid()

    cell.category_reference = 'invalid:foobar'
    cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_categories_cell(mock_send, settings, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test', template_name='standard')
    CategoriesCell.objects.create(page=page, placeholder='content', order=0)

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.pyquery('[data-tab-slug="general"] select[name$="wcs_site"]')

    default = settings.KNOWN_SERVICES['wcs']['default']
    settings.KNOWN_SERVICES = {'wcs': {'default': default}}
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert not resp.pyquery('[data-tab-slug="general"]')


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_categories_cell_check_validity(mock_send):
    page = Page.objects.create(title='xxx', slug='test', template_name='standard')
    cell = CategoriesCell.objects.create(page=page, placeholder='content', order=0)

    # invalid wcs_site
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_form_cell_render(mock_send):
    page = Page(title='xxx', slug='test_form_cell_render', template_name='standard')
    page.save()
    cell = WcsFormCell(page=page, placeholder='content', order=0)
    cell.formdef_reference = 'default:form-title'
    cell.save()
    result = cell.render({'request': RequestFactory().get('/')})
    assert 'http://127.0.0.1:8999/form-title/tryauth' in result
    assert 'form title' in result
    assert '<p>a form description</p>' in result


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_forms_cell_setup(mock_send):
    page = Page.objects.create(title='xxx', slug='test_form_cell_render', template_name='standard')
    cell = WcsCurrentFormsCell(page=page)
    form_class = cell.get_default_form_class()
    form = form_class(instance=cell)
    assert form.fields['wcs_site'].widget.choices == [
        ('', 'All'),
        ('default', 'test'),
        ('other', 'test2'),
    ]
    assert 'current_forms' in form.fields
    assert 'filter_by_card' not in form.fields
    assert cell.get_additional_label() == 'All Sites - Current Forms'
    cell.wcs_site = 'default'
    assert cell.get_additional_label() == 'test - Current Forms'
    cell.wcs_site = None

    cell.current_forms = True
    cell.done_forms = True
    assert cell.get_additional_label() == 'All Sites - All Forms'

    cell.current_forms = False
    cell.done_forms = True
    assert cell.get_additional_label() == 'All Sites - Done Forms'

    page.sub_slug = 'foobar'
    page.save()
    form = form_class(instance=cell)
    assert 'filter_by_card' not in form.fields

    page.sub_slug = 'card_model_1_id'
    page.save()
    form = form_class(instance=cell)
    assert 'filter_by_card' in form.fields


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_forms_cell_render(mock_send, context):
    page = Page(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    page.save()
    cell = WcsCurrentFormsCell(page=page, placeholder='content', order=0)
    cell.save()

    context['request'].user = MockUser()

    # query should fail as nothing is cached
    cache.clear()
    with pytest.raises(NothingInCacheException):
        result = cell.render(context)

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert 'http://127.0.0.1:8999/form/1/' in result
    assert 'http://127.0.0.1:8999/form/2/' not in result  # no name
    assert 'http://127.0.0.1:8999/form/3/' not in result  # not readable
    assert 'http://127.0.0.2:8999/form/1/' in result
    assert 'http://127.0.0.2:8999/form/2/' not in result
    assert 'http://127.0.0.2:8999/form/3/' not in result

    # limit to categories
    cell.categories = {'data': ['default:test-3', 'other:test-6']}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert (
        requests_get.call_args_list[0][0][0]
        == '/api/user/forms/?status=open&limit=100&sort=desc&category_slugs=test-3'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert (
        requests_get.call_args_list[1][0][0]
        == '/api/user/forms/?status=open&limit=100&sort=desc&category_slugs=test-6'
    )
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    # check include drafts
    cell.categories = None
    cell.include_drafts = False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/user/forms/?status=open&limit=100&sort=desc'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/user/forms/?status=open&limit=100&sort=desc'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    cell.include_drafts = True
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert (
        requests_get.call_args_list[0][0][0]
        == '/api/user/forms/?status=open&include-drafts=on&limit=100&sort=desc'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert (
        requests_get.call_args_list[1][0][0]
        == '/api/user/forms/?status=open&include-drafts=on&limit=100&sort=desc'
    )
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    cell.categories = {'data': ['default:test-3']}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 1
    assert (
        requests_get.call_args_list[0][0][0]
        == '/api/user/forms/?status=open&include-drafts=on&limit=100&sort=desc&category_slugs=test-3'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

    cell.categories = {'data': []}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2

    # current_forms only
    cell.include_drafts = False
    cell.current_forms = True
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/user/forms/?status=open&limit=100&sort=desc'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/user/forms/?status=open&limit=100&sort=desc'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    # all forms
    cell.done_forms = True
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/user/forms/?status=all&limit=100&sort=desc'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/user/forms/?status=all&limit=100&sort=desc'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    # done forms only
    cell.current_forms = False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/user/forms/?status=done&limit=100&sort=desc'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/user/forms/?status=done&limit=100&sort=desc'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    # include forms user can see
    cell.include_forms_user_can_access = True
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert (
        requests_get.call_args_list[0][0][0]
        == '/api/user/forms/?status=done&include-accessible=on&limit=100&sort=desc'
    )

    # check empty messages and title
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.current_forms = True
        cell.done_forms = False
        cell.include_drafts = False
        result = cell.render(context)
        assert '<h2>Current Forms</h2>' in result
        assert 'There are no current forms.' in result
        cell.custom_title = 'Foo bar'
        result = cell.render(context)
        assert '<h2>Foo bar</h2>' in result
        cell.custom_title = ''
        cell.done_forms = True
        result = cell.render(context)
        assert '<h2>All Forms</h2>' in result
        assert 'There are no forms.' in result
        cell.custom_title = 'Foo bar'
        result = cell.render(context)
        assert '<h2>Foo bar</h2>' in result
        cell.custom_title = ''
        cell.current_forms = False
        result = cell.render(context)
        assert '<h2>Done Forms</h2>' in result
        assert 'There are no done forms' in result
        cell.custom_title = 'Foo bar'
        result = cell.render(context)
        assert '<h2>Foo bar</h2>' in result

    # check card filtering
    cell.include_forms_user_can_access = False
    cell.filter_by_card = True
    cell.save()
    url = cell.get_api_url(context)
    assert url == '/api/user/forms/?status=done&limit=100&sort=desc'
    page.sub_slug = 'unknown'
    page.save()
    url = cell.get_api_url(context)
    assert url == '/api/user/forms/?status=done&limit=100&sort=desc'
    page.sub_slug = 'card_model_1_id'
    page.save()
    url = cell.get_api_url(context)
    assert url == '/api/user/forms/?status=done&limit=100&sort=desc'
    context['card_model_1_id'] = '42'
    url = cell.get_api_url(context)
    assert url == '/api/user/forms/?status=done&related=carddef:card_model_1:42&limit=100&sort=desc'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_forms_cell_validity(mock_send, context):
    page = Page.objects.create(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    cell = WcsCurrentFormsCell.objects.create(page=page, placeholder='content', order=0)
    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    cell.get_data(context)
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_forms_cell_check_validity(mock_send, context):
    page = Page.objects.create(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    cell = WcsCurrentFormsCell.objects.create(page=page, placeholder='content', order=0)

    # no category
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # valid categories
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # invalid category
    cell.categories = {'data': ['default:foobar', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None

    # invalid wcs_site
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    cell.mark_as_valid()

    cell.categories = {'data': ['invalid:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_forms_cell_render_single_site(mock_send, context):
    page = Page(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    page.save()
    cell = WcsCurrentFormsCell(page=page, placeholder='content', order=0)
    cell.wcs_site = 'default'
    cell.save()

    context['request'].user = MockUser()

    # query should fail as nothing is cached
    cache.clear()
    with pytest.raises(NothingInCacheException):
        result = cell.render(context)

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert 'http://127.0.0.1:8999/form/1/' in result
    assert 'http://127.0.0.2:8999/form/1/' not in result


def test_current_forms_name_id(context):
    page = Page(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    page.save()
    cell = WcsCurrentFormsCell(page=page, placeholder='content', order=0)
    cell.wcs_site = 'default'
    cell.save()

    context['synchronous'] = True  # to get fresh content

    # anonymous user
    context['request'].user = AnonymousUser()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/user/forms/?status=open&limit=100&sort=desc'

    # user with name_id
    context['request'].user = MockUserWithNameId()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/users/xyz/forms?status=open&limit=100&sort=desc'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_care_forms_cell_setup(mock_send):
    page = Page.objects.create(title='xxx', slug='test_form_cell_render', template_name='standard')
    cell = WcsCareFormsCell(page=page)
    form_class = cell.get_default_form_class()
    form = form_class(instance=cell)
    assert form.fields['wcs_site'].widget.choices == [
        ('', 'All'),
        ('default', 'test'),
        ('other', 'test2'),
    ]
    assert 'filter_by_card' not in form.fields
    assert cell.get_additional_label() == 'All Sites'
    cell.wcs_site = 'default'
    assert cell.get_additional_label() == 'test'

    page.sub_slug = 'foobar'
    page.save()
    form = form_class(instance=cell)
    assert 'filter_by_card' not in form.fields

    page.sub_slug = 'card_model_1_id'
    page.save()
    form = form_class(instance=cell)
    assert 'filter_by_card' in form.fields


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_care_forms_cell(mock_send, app, admin_user):
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)
    resp = app.get(
        resp.html.find('option', **{'data-add-url': re.compile('wcscareformscell')})['data-add-url']
    )

    cells = Page.objects.get(id=page.id).get_cells()
    assert len(cells) == 1
    cell = cells[0]
    assert isinstance(cell, WcsCareFormsCell)

    resp = app.get('/manage/pages/%s/' % page.id)
    assert not resp.pyquery('[data-tab-slug="general"] input[name$="custom_title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="custom_title"]')
    assert (
        resp.pyquery.find('div.cell [data-tab-slug="advanced"] input[name$="cache_duration"]').val() == '120'
    )
    resp.forms[0]['c%s-cache_duration' % cell.get_reference()] = '10'
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.cache_duration == 10


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_care_forms_cell_render(mock_send, context):
    page = Page(title='xxx', slug='test_care_forms_cell_render', template_name='standard')
    page.save()
    cell = WcsCareFormsCell(page=page, placeholder='content', order=0)
    cell.save()

    context['request'].user = MockUser()

    # query should fail as nothing is cached
    cache.clear()
    with pytest.raises(NothingInCacheException):
        result = cell.render(context)

    context['synchronous'] = True  # to get fresh content

    with mock.patch('combo.apps.wcs.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.return_value = True
        result = cell.render(context)

    assert 'Forms to process - test' in result
    assert 'http://127.0.0.1:8999/backoffice/management/foobar/1' in result
    assert 'http://127.0.0.1:8999/backoffice/management/foobar/2' in result
    assert '"http://127.0.0.1:8999/backoffice/management/listing"' in result
    assert 'Forms to process - test2' in result
    assert 'http://127.0.0.2:8999/backoffice/management/foobar/1' in result
    assert 'http://127.0.0.2:8999/backoffice/management/foobar/2' in result
    assert '"http://127.0.0.2:8999/backoffice/management/listing"' in result

    cell.custom_title = 'Foo Bar'
    with mock.patch('combo.apps.wcs.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.return_value = False
        result = cell.render(context)

    assert 'Forms to process - test' not in result
    assert 'Foo Bar' in result
    assert 'http://127.0.0.1:8999/foobar/1' in result
    assert 'http://127.0.0.1:8999/foobar/2' in result
    assert 'Forms to process - test2' not in result
    assert 'Foo Bar' in result
    assert 'http://127.0.0.2:8999/foobar/1' in result
    assert 'http://127.0.0.2:8999/foobar/2' in result
    assert '/listing' not in result

    data = cell.get_data(context)
    assert 'default' in data
    assert 'other' in data

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/forms/?limit=10'
    assert requests_get.call_args_list[1][0][0] == '/api/forms/?limit=10'

    # limit to a list of categories
    cell.categories = {'data': ['default:test-3', 'other:test-4']}

    with mock.patch('combo.apps.wcs.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.return_value = True
        result = cell.render(context)
    assert '"http://127.0.0.1:8999/backoffice/management/listing?category_slugs=test-3"' in result
    assert '"http://127.0.0.2:8999/backoffice/management/listing?category_slugs=test-4"' in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/forms/?limit=10&category_slugs=test-3'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/forms/?limit=10&category_slugs=test-4'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    # limit to a single category
    cell.categories = {'data': ['default:test-3']}

    with mock.patch('combo.apps.wcs.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.return_value = True
        result = cell.render(context)
    assert '"http://127.0.0.1:8999/backoffice/management/listing?category_slugs=test-3"' in result
    assert '"http://127.0.0.2:8999/backoffice/management/listing' not in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert len(requests_get.call_args_list) == 1
    assert requests_get.call_args_list[0][0][0] == '/api/forms/?limit=10&category_slugs=test-3'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

    # no category selected: call all sites
    cell.categories = {'data': []}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert len(requests_get.call_args_list) == 2

    # check card filtering
    cell.categories = {}
    cell.filter_by_card = True
    cell.save()
    url = cell.get_api_url(context)
    assert url == '/api/forms/?limit=10'
    page.sub_slug = 'unknown'
    page.save()
    url = cell.get_api_url(context)
    assert url == '/api/forms/?limit=10'
    page.sub_slug = 'card_model_1_id'
    page.save()
    url = cell.get_api_url(context)
    assert url == '/api/forms/?limit=10'
    context['card_model_1_id'] = '42'
    url = cell.get_api_url(context)
    assert url == '/api/forms/?limit=10&related=carddef:card_model_1:42'


def test_care_forms_cell_validity(context):
    page = Page.objects.create(title='xxx', slug='test_care_forms_cell_render', template_name='standard')
    cell = WcsCareFormsCell.objects.create(page=page, placeholder='content', order=0)
    context['synchronous'] = True  # to get fresh content
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=500)
        requests_get.return_value = mock_json
        cell.get_data(context)
    assert ValidityInfo.objects.exists() is False

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=404)
        requests_get.return_value = mock_json
        cell.get_data(context)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_data_failure'
    assert validity_info.invalid_since is not None


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_care_forms_cell_check_validity(mock_send, context):
    page = Page.objects.create(title='xxx', slug='test_care_forms_cell_render', template_name='standard')
    cell = WcsCareFormsCell.objects.create(page=page, placeholder='content', order=0)

    # no category
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # valid categories
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # invalid category
    cell.categories = {'data': ['default:foobar', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None

    # invalid wcs_site
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    cell.mark_as_valid()

    cell.categories = {'data': ['invalid:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_care_forms_cell_render_single_site(mock_send, context):
    page = Page(title='xxx', slug='test_care_forms_cell_render', template_name='standard')
    page.save()
    cell = WcsCareFormsCell(page=page, placeholder='content', order=0)
    cell.wcs_site = 'default'
    cell.save()

    context['request'].user = MockUser()

    # query should fail as nothing is cached
    cache.clear()
    with pytest.raises(NothingInCacheException):
        result = cell.render(context)

    context['synchronous'] = True  # to get fresh content

    with mock.patch('combo.apps.wcs.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.return_value = True
        result = cell.render(context)
    assert '"http://127.0.0.1:8999/backoffice/management/listing"' in result
    assert '"http://127.0.0.2:8999/backoffice/management/listing"' not in result

    data = cell.get_data(context)
    assert 'default' in data
    assert 'other' not in data


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_forms_of_category_cell_setup(mock_send):
    cell = WcsFormsOfCategoryCell()
    form_class = cell.get_default_form_class()
    form = form_class()
    assert form.fields['category_reference'].widget.choices == [
        ('default:test-3', 'test : Test 3'),
        ('default:test-9', 'test : Test 9'),
        ('other:test-3', 'test2 : Test 3'),
        ('other:test-9', 'test2 : Test 9'),
    ]


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_forms_of_category_cell_render(mock_send, context):
    page = Page(title='xxx', slug='test_forms_of_category_cell_render', template_name='standard')
    page.save()
    cell = WcsFormsOfCategoryCell(page=page, placeholder='content', order=0)
    cell.category_reference = 'default:test-9'
    cell.ordering = 'alpha'
    cell.save()
    context['synchronous'] = True  # to get fresh content
    result = cell.render(context)
    assert 'form title' in result and 'a second form title' in result
    assert result.index('form title') > result.index('a second form title')
    assert 'http://127.0.0.1:8999/form-title/tryauth' in result
    assert 'http://127.0.0.1:8999/a-second-form-title/tryauth' in result
    assert 'keyword-foo' in result
    assert 'keyword-bar' in result
    assert '<p>a form description</p>' in result
    assert '<p>category 9 description</p>' not in result
    assert '?cancelurl=&' not in result

    cell.limit = 1
    cell.save()
    result = cell.render(context)
    ret = pyquery.pyquery.PyQuery(result)
    link = ret.find('.add-more-items a')
    assert link[0].attrib['aria-label'] == 'More items (%s)' % context['title']
    span = link.find('span')[0]
    assert 'aria-hidden' in span.attrib
    assert span.attrib['aria-hidden'] == 'true'
    assert span.text == '+'

    cell.limit = None
    cell.save()
    result = cell.render(context)
    ret = pyquery.pyquery.PyQuery(result)
    assert not ret.find('.add-more-items a')

    cell.ordering = 'popularity'
    cell.save()
    result = cell.render(context)
    assert 'form title' in result and 'a second form title' in result
    assert result.index('form title') < result.index('a second form title')

    cell.ordering = 'manual'
    cell.save()
    result = cell.render(context)
    # by default all forms should be present, in alphabetical order
    assert result.index('form title') > result.index('a second form title')

    # set a manual order
    cell.manual_order = {'data': ['default:test-9:a-second-form-title', 'default:test-9:form-title']}
    cell.save()
    result = cell.render(context)
    assert result.index('form title') > result.index('a second form title')

    # make sure all forms are displayed even if the manual order only specify
    # some.
    cell.manual_order = {'data': ['default:test-9:a-second-form-title']}
    cell.save()
    result = cell.render(context)
    assert result.index('form title') > result.index('a second form title')
    assert 'form title' in result and 'a second form title' in result

    # check content provided to search engine
    assert cell.render_for_search() == ''
    assert len(list(cell.get_external_links_data())) == 2

    # existing category, but empty
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        result = cell.render(context)
        assert '<h2>' not in result
        context['combo_display_even_empty_categories'] = True
        assert len(requests_get.call_args_list) == 1
        assert requests_get.call_args_list[0][0][0] == '/api/categories/test-9/formdefs/'
        assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

        result = cell.render(context)
        assert '<h2>' in result
        context.pop('combo_display_even_empty_categories')

    page.sub_slug = 'unknown'
    page.save()
    result = cell.render(context)
    assert '?cancelurl=&' not in result
    page.sub_slug = 'card_model_1_id'
    page.save()
    result = cell.render(context)
    assert '?cancelurl=&' not in result
    context['card_model_1_id'] = '42'
    result = cell.render(context)
    assert '?cancelurl=&card_model_1_id=42"' in result
    context.pop('card_id_param')
    page.sub_slug = 'card-e_id'
    page.save()
    cell.refresh_from_db()
    context['card_e_id'] = '35'
    result = cell.render(context)
    assert '?cancelurl=&card_e_id=35"' in result


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_forms_of_category_cancelurl(mock_send, app):
    page = Page(title='xxx', slug='test_forms_of_category_cell_render', template_name='standard')
    page.save()
    cell = WcsFormsOfCategoryCell(page=page, placeholder='content', order=0)
    cell.category_reference = 'default:test-9'
    cell.ordering = 'alpha'
    cell.save()
    resp = app.get(page.get_online_url())
    ajax_cell_url = PyQuery(resp.text).find('[data-ajax-cell-url]').attr('data-ajax-cell-url')
    extra_ctx = PyQuery(resp.text).find('[data-ajax-cell-url]').attr('data-extra-context')
    cell_resp = app.get(ajax_cell_url + '?ctx=' + extra_ctx)
    assert (
        PyQuery(cell_resp.text).find('a').attr('href')
        == 'http://127.0.0.1:8999/a-second-form-title/tryauth?cancelurl=http%3A//testserver/test_forms_of_category_cell_render/'
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_forms_of_category_cell_validity(mock_send, context):
    page = Page.objects.create(
        title='xxx', slug='test_forms_of_category_cell_render', template_name='standard'
    )
    cell = WcsFormsOfCategoryCell.objects.create(page=page, placeholder='content', order=0)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_defined'
    assert validity_info.invalid_since is not None
    cell.category_reference = 'default:test-9'
    cell.save()
    assert ValidityInfo.objects.exists() is False

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=404)
        requests_get.return_value = mock_json
        cell.render(context)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_data_failure'
    assert validity_info.invalid_since is not None
    cell.mark_as_valid()

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=500)
        requests_get.return_value = mock_json
        cell.render(context)
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.render(context)
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_forms_of_category_cell_check_validity(mock_send, context):
    page = Page.objects.create(
        title='xxx', slug='test_forms_of_category_cell_render', template_name='standard'
    )
    cell = WcsFormsOfCategoryCell.objects.create(page=page, placeholder='content', order=0)

    # valid category
    cell.category_reference = 'default:test-9'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # valid category but empty
    cell.category_reference = 'default:test-1'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None
    cell.mark_as_valid()

    cell.category_reference = 'invalid:foobar'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_no_tryauth_for_bot(mock_send):
    page = Page(title='xxx', slug='test_form_cell_render', template_name='standard')
    page.save()
    request = RequestFactory().get('/')
    request.META['HTTP_USER_AGENT'] = (
        'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
    )
    context = {'request': request}

    cell = WcsFormCell(page=page, placeholder='content', order=0)
    cell.formdef_reference = 'default:form-title'
    cell.save()
    result = cell.render(context)
    assert 'http://127.0.0.1:8999/form-title/' in result
    assert 'form title' in result
    # no tryout, no cancelurl
    assert 'tryauth' not in result
    assert 'cancelurl' not in result

    cell = WcsFormsOfCategoryCell(page=page, placeholder='content', order=0)
    cell.category_reference = 'default:test-9'
    cell.ordering = 'alpha'
    cell.save()
    context['synchronous'] = True  # to get fresh content
    result = cell.render(context)
    assert 'form title' in result and 'a second form title' in result
    assert result.index('form title') > result.index('a second form title')
    assert 'http://127.0.0.1:8999/form-title/' in result
    assert 'http://127.0.0.1:8999/a-second-form-title/' in result
    assert 'keyword-foo' in result
    assert 'keyword-bar' in result
    # no tryout, no cancelurl
    assert 'tryauth' not in result
    assert 'cancelurl' not in result


def test_current_drafts_cell_render_unlogged(context):
    page = Page(title='xxx', slug='test_current_drafts_cell_render', template_name='standard')
    page.save()
    cell = WcsCurrentDraftsCell(page=page, placeholder='content', order=0)
    cell.save()
    context['synchronous'] = True  # to get fresh content
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        result = cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/user/drafts'
    assert 'http://127.0.0.1:8999/third-form-title' not in result  # no form


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_drafts_cell_render_logged_in(mock_send, context):
    page = Page(title='xxx', slug='test_current_drafts_cell_render', template_name='standard')
    page.save()
    cell = WcsCurrentDraftsCell(page=page, placeholder='content', order=0)
    cell.save()
    context['synchronous'] = True  # to get fresh content

    context['request'].user = MockUser()

    # default is to get current forms from all wcs sites
    result = cell.render(context)
    assert 'http://127.0.0.1:8999/form/1/' in result
    assert 'http://127.0.0.1:8999/form/2/' not in result  # no title
    assert 'http://127.0.0.1:8999/form/3/' in result
    assert 'http://127.0.0.1:8999/form/4/' not in result  # done
    assert 'http://127.0.0.2:8999/form/1/' in result
    assert 'http://127.0.0.2:8999/form/2/' not in result
    assert 'http://127.0.0.2:8999/form/3/' in result
    assert 'http://127.0.0.2:8999/form/4/' not in result

    # check flat list
    extra_context = cell.get_cell_extra_context(context)
    assert len(extra_context['drafts']) == 8
    assert len([x for x in extra_context['drafts'] if x['site_slug'] == 'default']) == 4
    assert len([x for x in extra_context['drafts'] if x['site_slug'] == 'other']) == 4

    # limit to categories
    cell.categories = {'data': ['default:test-3', 'other:test-6']}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/user/drafts?category_slugs=test-3'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    assert requests_get.call_args_list[1][0][0] == '/api/user/drafts?category_slugs=test-6'
    assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

    cell.categories = {'data': ['default:test-3']}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 1
    assert requests_get.call_args_list[0][0][0] == '/api/user/drafts?category_slugs=test-3'
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

    cell.categories = {'data': []}
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.get_cell_extra_context(context)
    assert len(requests_get.call_args_list) == 2

    # check empty message
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        result = cell.render(context)
    assert 'There are no current drafts.' in result

    context['request'].user = MockUserWithNameId()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/users/xyz/drafts'

    # anonymous user
    context['request'].user = AnonymousUser()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/user/drafts'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_current_drafts_cell_check_validity(mock_send, context):
    page = Page.objects.create(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    cell = WcsCurrentDraftsCell.objects.create(page=page, placeholder='content', order=0)

    # no category
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # valid categories
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # invalid category
    cell.categories = {'data': ['default:foobar', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None

    # invalid wcs_site
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    cell.mark_as_valid()

    cell.categories = {'data': ['invalid:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_forms_of_category_cell(mock_send, app, admin_user):
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)
    resp = app.get(
        resp.html.find('option', **{'data-add-url': re.compile('wcsformsofcategorycell')})['data-add-url']
    )

    cells = Page.objects.get(id=page.id).get_cells()
    assert len(cells) == 1
    assert isinstance(cells[0], WcsFormsOfCategoryCell)

    resp = app.get('/manage/pages/%s/' % page.id)
    assert ('data-cell-reference="%s"' % cells[0].get_reference()) in resp.text
    resp.forms[0]['c%s-ordering' % cells[0].get_reference()].value = 'manual'
    manager_submit_cell(resp.forms[0])
    cells[0].refresh_from_db()
    resp.forms[0]['c%s-manual_order' % cells[0].get_reference()].select_multiple(
        ['default::a-second-form-title', 'default::third-form-title']
    )
    manager_submit_cell(resp.forms[0])
    cells[0].refresh_from_db()
    assert cells[0].manual_order == {'data': ['default::a-second-form-title', 'default::third-form-title']}


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_current_forms(mock_send, settings, app, admin_user):
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)
    resp = app.get(
        resp.html.find('option', **{'data-add-url': re.compile('wcscurrentformscell')})['data-add-url']
    )

    cells = Page.objects.get(id=page.id).get_cells()
    assert len(cells) == 1
    assert isinstance(cells[0], WcsCurrentFormsCell)

    resp = app.get('/manage/pages/%s/' % page.id)
    assert ('data-cell-reference="%s"' % cells[0].get_reference()) in resp.text
    assert len(resp.form['c%s-categories' % cells[0].get_reference()].options) == 4
    resp.form['c%s-categories' % cells[0].get_reference()].value = ['default:test-3', 'default:test-9']
    manager_submit_cell(resp.form)
    assert resp.form['c%s-categories' % cells[0].get_reference()].value == [
        'default:test-3',
        'default:test-9',
    ]
    resp.form['c%s-current_forms' % cells[0].get_reference()] = False
    resp.form['c%s-done_forms' % cells[0].get_reference()] = False
    resp = resp.form.submit()
    assert 'Please choose at least one option among the following: Current Forms, Done Forms' in resp
    resp = app.get('/manage/pages/%s/' % page.id)
    resp.form['c%s-done_forms' % cells[0].get_reference()] = True
    manager_submit_cell(resp.form)

    # check wcs_site field is a select box
    assert resp.form['c%s-wcs_site' % cells[0].get_reference()].tag == 'select'

    default = settings.KNOWN_SERVICES['wcs']['default']
    settings.KNOWN_SERVICES = {'wcs': {'default': default}}
    resp = app.get('/manage/pages/%s/' % page.id)
    assert resp.form['c%s-wcs_site' % cells[0].get_reference()].attrs['type'] == 'hidden'


def test_manager_current_forms_tabs(app, admin_user):
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    cell = WcsCurrentFormsCell.objects.create(page=page, placeholder='content', order=1)
    cell.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)

    assert not resp.pyquery('[data-tab-slug="general"] input[name$="custom_title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="custom_title"]')


def test_tracking_code_cell(app, nocache):
    page = Page(title='One', slug='index', template_name='standard')
    page.save()
    cell = TrackingCodeInputCell(page=page, placeholder='content', order=0)
    cell.save()

    resp = app.get('/')
    resp.form['code'] = 'FFQQBRRR'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/code/FFQQBRRR'
    assert requests_get.call_args_list[1][0][0] == '/api/code/FFQQBRRR'
    remote_service_urls = [c[1]['remote_service']['url'] for c in requests_get.call_args_list]
    assert set(remote_service_urls) == {'http://127.0.0.1:8999/', 'http://127.0.0.2:8999/'}
    assert resp.status_code == 302
    resp = resp.follow()
    assert '<li class="error">The tracking code could not been found.</li>' in resp.text

    resp = app.get('/')
    resp.form['code'] = 'FOO?BAR?bad<code>'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert requests_get.called is False
    assert resp.status_code == 302
    resp = resp.follow()
    assert '<li class="error">The tracking code could not been found.</li>' in resp.text

    resp = app.get('/')
    resp.form['code'] = 'CNPHNTFB'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(
            content=json.dumps({'err': 0, 'load_url': 'http://127.0.0.2:8999/code/CNPHNTFB/load'})
        )
        resp = resp.form.submit()
    assert resp.status_code == 302
    assert resp.location == 'http://127.0.0.2:8999/code/CNPHNTFB/load'

    # space/case
    resp = app.get('/')
    resp.form['code'] = ' cnphntfb'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            mock.Mock(status_code=200),
            MockedRequestResponse(
                content=json.dumps({'err': 0, 'load_url': 'http://127.0.0.2:8999/code/CNPHNTFB/load'})
            ),
        ]
        resp = resp.form.submit()
    assert requests_get.call_args_list[0][0][0] == '/api/code/CNPHNTFB'
    assert requests_get.call_args_list[1][0][0] == '/api/code/CNPHNTFB'
    assert resp.status_code == 302
    assert resp.location == 'http://127.0.0.2:8999/code/CNPHNTFB/load'

    # lock cell to a single site
    cell.wcs_site = 'default'
    cell.save()
    resp = app.get('/')
    resp.form['code'] = 'CNPHNTFB'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert len(requests_get.call_args_list) == 1
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
    resp = resp.follow()
    assert '<li class="error">The tracking code could not been found.</li>' in resp.text

    # unknown wcs_site
    cell.wcs_site = 'unknown'
    cell.save()
    resp = app.get('/')
    resp.form['code'] = 'CNPHNTFB'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert len(requests_get.call_args_list) == 0
    resp = resp.follow()
    assert '<li class="error">The tracking code could not been found.</li>' in resp.text

    # simulate cell being displayed on a different site
    resp = app.get('/')
    resp.form['url'] = 'http://example.org/'
    resp.form['code'] = 'CNPHNTFB'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert resp.location == 'http://example.org/?unknown-tracking-code'

    resp = app.get('/')
    resp.form['url'] = 'http://example.org/?foo=bar'
    resp.form['code'] = 'CNPHNTFB'
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        resp = resp.form.submit()
    assert resp.location == 'http://example.org/?foo=bar&unknown-tracking-code'

    # redirect to an unknown site
    resp = app.get('/')
    resp.form['url'] = 'http://example.net/'
    resp.form['code'] = 'CNPHNTFB'
    resp = resp.form.submit(status=400)

    # error handling
    resp = app.get('/')
    resp.form['cell'] = '9999'
    resp.form['code'] = 'CNPHNTFB'
    resp = resp.form.submit(status=400)

    resp = app.get('/')
    resp.form['cell'] = 'xxxx'
    resp.form['code'] = 'CNPHNTFB'
    resp = resp.form.submit(status=400)

    resp = app.post(reverse('wcs-tracking-code'), params={'cell': cell.id}, status=400)


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_tracking_code_cell_check_validity(mock_send):
    page = Page.objects.create(title='xxx', slug='test', template_name='standard')
    cell = TrackingCodeInputCell.objects.create(page=page, placeholder='content', order=0)

    # invalid wcs_site
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_cell_assets(mock_send, settings, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test_cell_assets', template_name='standard')
    cell1 = WcsFormCell.objects.create(
        page=page, placeholder='content', order=0, formdef_reference='default:form-title'
    )

    cell2 = WcsFormsOfCategoryCell.objects.create(
        page=page, placeholder='content', order=0, category_reference='default:test-9', ordering='alpha'
    )

    app = login(app)
    settings.WCS_CATEGORY_ASSET_SLOTS = {}
    settings.WCS_FORM_ASSET_SLOTS = {}
    settings.COMBO_CELL_ASSET_SLOTS = {}
    resp = app.get('/manage/assets/')
    assert 'have any asset yet.' in resp.text

    # Old settings have priority
    settings.WCS_CATEGORY_ASSET_SLOTS = {'logo': {'prefix': 'Logo'}}
    settings.WCS_FORM_ASSET_SLOTS = {'picture': {'prefix': 'Picture'}}
    settings.COMBO_CELL_ASSET_SLOTS = {
        'wcs_wcsformcell': {'picture': {'prefix': 'Picture blabla', 'suffix': 'test'}},
        'wcs_wcsformsofcategorycell': {'logo': {'prefix': 'Logo blabla', 'suffix': 'test'}},
    }
    resp = app.get('/manage/assets/')
    assert 'Logo — %s' % cell2.get_label_for_asset() in resp.text
    assert 'Logo blabla — %s' % cell2.get_label_for_asset() not in resp.text
    assert 'Picture — %s' % cell1.get_label_for_asset() in resp.text
    assert 'Picture blabla — %s' % cell1.get_label_for_asset() not in resp.text
    # New settings
    settings.WCS_CATEGORY_ASSET_SLOTS = {}
    settings.WCS_FORM_ASSET_SLOTS = {}
    settings.COMBO_CELL_ASSET_SLOTS = {
        'wcs_wcsformcell': {'picture': {'prefix': 'Picture'}},
        'wcs_wcsformsofcategorycell': {'logo': {'prefix': 'Logo'}},
    }
    resp = app.get('/manage/assets/')
    assert 'Logo — %s' % cell2.get_label_for_asset() in resp.text
    assert 'Picture — %s' % cell1.get_label_for_asset() in resp.text

    # test suffix
    settings.COMBO_CELL_ASSET_SLOTS = {
        'wcs_wcsformcell': {'picture': {'prefix': 'Picture', 'suffix': 'test'}},
        'wcs_wcsformsofcategorycell': {'logo': {'prefix': 'Logo', 'suffix': 'test'}},
    }
    resp = app.get('/manage/assets/')
    assert 'Logo — %s (test)' % cell2.get_label_for_asset() in resp.text
    assert 'Picture — %s (test)' % cell1.get_label_for_asset() in resp.text


def test_tracking_code_search(settings, app, nocache):
    settings.TEMPLATE_VARS['is_portal_agent'] = True

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        result = app.get('/api/search/tracking-code/').json
        assert len(result.get('data')) == 0
        assert result.get('err') == 0
    assert requests_get.called is False  # no code

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        assert len(app.get('/api/search/tracking-code/?q=123').json.get('data')) == 0
    assert requests_get.called is False  # no letters

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        assert len(app.get('/api/search/tracking-code/?q=BBCCDFF').json.get('data')) == 0
    assert requests_get.called is False  # too short

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        assert len(app.get('/api/search/tracking-code/?q=BBCCDDFF').json.get('data')) == 0
    assert len(requests_get.call_args_list) == 2
    assert requests_get.call_args_list[0][0][0] == '/api/code/BBCCDDFF?backoffice=true'
    assert requests_get.call_args_list[1][0][0] == '/api/code/BBCCDDFF?backoffice=true'
    remote_service_urls = [c[1]['remote_service']['url'] for c in requests_get.call_args_list]
    assert set(remote_service_urls) == {'http://127.0.0.1:8999/', 'http://127.0.0.2:8999/'}

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(
            content=json.dumps({'err': 0, 'load_url': 'http://127.0.0.2:8999/code/CNPHNTFB/load'})
        )
        assert len(app.get('/api/search/tracking-code/?q=CNPHNTFB').json.get('data')) == 1
    assert requests_get.call_args_list[0][0][0] == '/api/code/CNPHNTFB?backoffice=true'

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        assert len(app.get('/api/search/tracking-code/?q=BBCCDDFFG').json.get('data')) == 0
    assert requests_get.called is False  # too long

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            mock.Mock(status_code=200),
            MockedRequestResponse(
                content=json.dumps({'err': 0, 'load_url': 'http://127.0.0.2:8999/code/CNPHNTFB/load'})
            ),
        ]
        assert len(app.get('/api/search/tracking-code/?q= cnphntfb').json.get('data')) == 1
    assert requests_get.call_args_list[0][0][0] == '/api/code/CNPHNTFB?backoffice=true'


@pytest.mark.parametrize('invalid_code', ('../users', 'FOOBAR', 'BBCCDDF%00'))
def test_tracking_code_search_invalid(settings, app, nocache, invalid_code):
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        result = app.get(f'/api/search/tracking-code/?q={invalid_code}').json
        assert len(result.get('data')) == 0
        assert result.get('err') == 0
    assert requests_get.called is False  # no code


def test_tracking_code_search_rate_limit(settings, app):
    settings.TEMPLATE_VARS['is_portal_agent'] = True
    settings.WCS_TRACKING_CODE_RATE_LIMIT = '0/s'
    assert app.get('/api/search/tracking-code/?q=BBCCDDFF').json.get('err') == 1

    page = Page(title='One', slug='index', template_name='standard')
    page.save()
    cell = TrackingCodeInputCell(page=page, placeholder='content', order=0)
    cell.save()

    resp = app.get('/')
    resp.form['code'] = 'FFQQBRRR'
    resp = resp.form.submit()
    assert resp.status_code == 302
    resp = resp.follow()
    assert '<li class="error">Looking up tracking code is currently rate limited.</li>' in resp.text

    resp = app.get('/')
    resp.form['code'] = 'FFQQBRRR'
    resp.form['url'] = 'http://example.org/'
    resp = resp.form.submit(status=403)


def test_wcs_search_engines(app):
    settings.TEMPLATE_VARS['is_portal_agent'] = True
    search_engines = engines.get_engines()
    assert 'tracking-code' in search_engines
    assert len([x for x in search_engines if x.startswith('formdata:')]) == 2
    assert len([x for x in search_engines if x.startswith('formdefs:')]) == 2
    settings.TEMPLATE_VARS['is_portal_agent'] = False
    search_engines = engines.get_engines()
    assert 'tracking-code' not in search_engines
    assert len([x for x in search_engines if x.startswith('formdata:')]) == 0
    assert len([x for x in search_engines if x.startswith('formdefs:')]) == 2


def test_backoffice_submission_search(settings, app, nocache):
    settings.TEMPLATE_VARS['is_portal_agent'] = True

    page = Page(title='xxx', slug='xxx', template_name='standard')
    page.save()
    cell = SearchCell(page=page, _search_services={'data': ['backoffice-submission:c21f969b']}, order=0)
    cell.save()

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            MockedRequestResponse(
                content=json.dumps(
                    {'err': 0, 'data': [{'title': 'hit', 'backoffice_submission_url': 'http://example.org/'}]}
                )
            ),
        ]
        resp = app.get('/ajax/search/%s/backoffice-submission:c21f969b/?q=query' % cell.id)
        assert (
            requests_get.call_args_list[0][0][0]
            == 'http://127.0.0.1:8999/api/formdefs/?backoffice-submission=true&NameID=&q=query'
        )
        assert resp.pyquery('a').length == 1
        assert resp.pyquery('a').text() == 'hit'
        assert resp.pyquery('a').text() == 'hit'
        assert resp.pyquery('a').attr.href == 'http://example.org/?'

    # invalid signature
    app.get('/ajax/search/%s/backoffice-submission:c21f969b/?q=query&ctx=xxx' % cell.id, status=400)

    # valid user id in context
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            MockedRequestResponse(
                content=json.dumps(
                    {'err': 0, 'data': [{'title': 'hit', 'backoffice_submission_url': 'http://example.org/'}]}
                )
            ),
        ]
        signed_ctx = signing.dumps({'absolute_uri': 'http://back-url/', 'name_id': 'abcdef'})
        resp = app.get(
            '/ajax/search/%s/backoffice-submission:c21f969b/?q=query&ctx=%s' % (cell.id, signed_ctx)
        )
        assert (
            requests_get.call_args_list[0][0][0]
            == 'http://127.0.0.1:8999/api/formdefs/?backoffice-submission=true&NameID=&q=query'
        )
        assert resp.pyquery('a').length == 1
        assert resp.pyquery('a').text() == 'hit'
        assert resp.pyquery('a').text() == 'hit'
        assert resp.pyquery('a').attr.href == 'http://example.org/?NameID=abcdef&ReturnURL=http://back-url/'


def test_formdefs_search(settings, app, nocache):
    page = Page(title='xxx', slug='xxx', template_name='standard')
    page.save()
    cell = SearchCell(page=page, _search_services={'data': ['formdefs:c21f969b']}, order=0)
    cell.save()

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            MockedRequestResponse(
                content=json.dumps({'err': 0, 'data': [{'title': 'form', 'url': 'http://example.org/form/'}]})
            ),
        ]
        resp = app.get('/ajax/search/%s/formdefs:c21f969b/?q=query' % cell.id)
        assert requests_get.call_args_list[0][0][0] == 'http://127.0.0.1:8999/api/formdefs/?NameID=&q=query'
        assert resp.pyquery('a').length == 1
        assert resp.pyquery('a').text() == 'form'
        assert resp.pyquery('a').attr.href == 'http://example.org/form/'

    # invalid signature
    app.get('/ajax/search/%s/formdefs:c21f969b/?q=query&ctx=xxx' % cell.id, status=400)

    # valid user id in context
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = [
            MockedRequestResponse(
                content=json.dumps({'err': 0, 'data': [{'title': 'form', 'url': 'http://example.org/form/'}]})
            ),
        ]
        signed_ctx = signing.dumps({'absolute_uri': 'http://back-url/', 'name_id': 'abcdef'})
        resp = app.get('/ajax/search/%s/formdefs:c21f969b/?q=query&ctx=%s' % (cell.id, signed_ctx))
        assert requests_get.call_args_list[0][0][0] == 'http://127.0.0.1:8999/api/formdefs/?NameID=&q=query'
        assert resp.pyquery('a').length == 1
        assert resp.pyquery('a').text() == 'form'
        assert resp.pyquery('a').attr.href == 'http://example.org/form/?ReturnURL=http://back-url/'


def test_backoffice_submission_cell_render(context):
    page = Page(title='xxx', slug='test_backoffice_submission_cell_render', template_name='standard')
    page.save()
    cell = BackofficeSubmissionCell.objects.create(page=page, placeholder='content', order=0)

    context['synchronous'] = True  # to get fresh content

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_json = mock.Mock(status_code=200)
        requests_get.return_value = mock_json
        result = cell.render(context)
    assert requests_get.call_args_list[0][0][0] == '/api/formdefs/?backoffice-submission=on'
    assert context['all_formdefs'] == {}
    assert 'h2' not in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(
            content=json.dumps(
                {
                    'data': [
                        {
                            'backoffice_submission_url': '/backoffice/submission/a-private-form/',
                            'title': 'Foo',
                        }
                    ]
                }
            )
        )
        result = cell.render(context)
        assert '/backoffice/submission/a-private-form/' in result
        assert list(context['all_formdefs'].keys()) == ['default', 'other']
        assert 'h2' in result

        # limit to categories
        requests_get.reset_mock()
        cell.categories = {'data': ['default:test-3', 'other:test-6']}

        result = cell.render(context)
        assert len(requests_get.call_args_list) == 2
        assert (
            requests_get.call_args_list[0][0][0]
            == '/api/formdefs/?backoffice-submission=on&category_slugs=test-3'
        )
        assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'
        assert (
            requests_get.call_args_list[1][0][0]
            == '/api/formdefs/?backoffice-submission=on&category_slugs=test-6'
        )
        assert requests_get.call_args_list[1][1]['remote_service']['url'] == 'http://127.0.0.2:8999/'

        requests_get.reset_mock()
        cell.categories = {'data': ['default:test-3']}
        result = cell.render(context)
        assert len(requests_get.call_args_list) == 1
        assert (
            requests_get.call_args_list[0][0][0]
            == '/api/formdefs/?backoffice-submission=on&category_slugs=test-3'
        )
        assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

        requests_get.reset_mock()
        cell.categories = {'data': []}
        result = cell.render(context)
        assert len(requests_get.call_args_list) == 2


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_backoffice_submission_cell_check_validity(mock_send, context):
    page = Page.objects.create(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    cell = BackofficeSubmissionCell.objects.create(page=page, placeholder='content', order=0)

    # no category
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # valid categories
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve categories, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.check_validity()
    assert ValidityInfo.objects.exists() is False

    # invalid category
    cell.categories = {'data': ['default:foobar', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_category_not_found'
    assert validity_info.invalid_since is not None

    # invalid wcs_site
    cell.categories = {'data': ['default:test-3', 'default:test-9']}
    cell.wcs_site = 'invalid'
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'

    # valid wcs_site
    cell.wcs_site = 'default'
    cell.save()
    cell.check_validity()
    assert ValidityInfo.objects.exists() is False
    cell.mark_as_valid()

    cell.categories = {'data': ['invalid:test-3', 'default:test-9']}
    cell.save()
    cell.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None


def test_manager_link_list_cell_duplicate():
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = LinkListCell.objects.create(order=0, page=page)
    item = WcsFormCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        formdef_reference='default:form-title',
        cached_title='A title',
        cached_url='http://example.com',
        cached_json={'foo': 'bar'},
        order=1,
    )

    new_cell = cell.duplicate()
    assert WcsFormCell.objects.count() == 2
    assert len(new_cell.get_items()) == 1
    new_item = new_cell.get_items()[0]
    assert new_item.page == page
    assert new_item.placeholder == new_cell.link_placeholder
    assert new_item.pk != item.pk
    assert new_item.cached_title == item.cached_title
    assert new_item.cached_url == item.cached_url
    assert new_item.cached_json == item.cached_json


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_link_list_cell_condition(mock_send, nocache, app):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = LinkListCell.objects.create(order=0, placeholder='content', page=page)
    link_cell = LinkCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        title='Example Site',
        url='http://example.net/',
        order=0,
    )

    link_cell.condition = 'cards|objects:"card_model_1"|getlist:"id"|list'
    link_cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert 'Example Site' in resp

    link_cell.condition = 'cards|objects:"card_model_1"|getlist:"id"|get:42'
    link_cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert 'Example Site' not in resp

    page.extra_variables = {'var1': '{{ cards|objects:"card_model_1"|getlist:"id"|list }}'}
    page.save()
    link_cell.condition = 'var1'
    link_cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert 'Example Site' in resp

    page.extra_variables = {'var1': '{{ cards|objects:"card_model_1"|getlist:"id"|get:42|default:"" }}'}
    page.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert 'Example Site' not in resp


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_add_edit_delete_list_link_item(mock_send, app, admin_user):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    cell = LinkListCell.objects.create(order=0, placeholder='content', page=page)
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    resp = resp.click(href='.*/add-link/form-link$')
    resp.forms[0]['formdef_reference'] = 'default:form-title'
    resp.forms[0]['extra_css_class'] = 'foobar'
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert WcsFormCell.objects.count() == 1
    item = WcsFormCell.objects.get()
    assert item.formdef_reference == 'default:form-title'
    assert item.page == page
    assert item.placeholder == cell.link_placeholder
    assert item.extra_css_class == 'foobar'

    resp = resp.follow()
    resp = resp.click(href='.*/link/%s/$' % item.get_reference())
    resp.forms[0]['formdef_reference'] = 'default:a-private-form'
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert WcsFormCell.objects.count() == 1
    item.refresh_from_db()
    assert item.formdef_reference == 'default:a-private-form'

    resp = resp.follow()
    resp = resp.click(href='.*/link/%s/delete' % item.get_reference())
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert WcsFormCell.objects.count() == 0


def test_manager_link_list_tabs(app, admin_user):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    cell = LinkListCell.objects.create(order=0, placeholder='content', page=page)
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    assert not resp.pyquery('[data-tab-slug="general"] input[name$="title"]')
    assert not resp.pyquery('#tab-%s-general.pk-tabs--button-marker' % cell.get_reference())
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="title"]')

    LinkCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        title='Example Site',
        url='http://example.net/',
        link_page=page,
        order=1,
    )
    resp = app.get('/manage/pages/%s/' % page.id)
    assert resp.pyquery('#tab-%s-general.pk-tabs--button-marker' % cell.get_reference())


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_import_export_pages_with_links(mock_send):
    page = Page(title='bar', slug='bar', order=1)
    page.save()

    cell = LinkListCell.objects.create(order=0, placeholder='content', page=page)
    WcsFormCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        formdef_reference='default:form-title',
        cached_title='A title',
        cached_url='http://example.com',
        cached_json={'foo': 'bar'},
        order=0,
    )

    site_export = [x.get_serialized_page() for x in Page.objects.all()]
    assert 'cached_' not in json.dumps(site_export)

    Page.load_serialized_pages(site_export)
    new_page = Page.objects.get()
    new_cells = CellBase.get_cells(page_id=new_page.id, placeholder='content')
    assert len(new_cells[0].get_items()) == 1
    new_item = new_cells[0].get_items()[0]
    assert isinstance(new_item, WcsFormCell)
    assert new_item.cached_title == 'too long form title' + 'e' * 231
    assert new_item.cached_url == 'http://127.0.0.1:8999/form-title/'
    assert new_item.cached_json != {}

    # again
    Page.load_serialized_pages(site_export)
    new_page = Page.objects.get()
    new_cells = CellBase.get_cells(page_id=new_page.id, placeholder='content')
    assert len(new_cells[0].get_items()) == 1
    new_item = new_cells[0].get_items()[0]
    assert isinstance(new_item, WcsFormCell)
    assert new_item.cached_title == 'too long form title' + 'e' * 231
    assert new_item.cached_url == 'http://127.0.0.1:8999/form-title/'
    assert new_item.cached_json != {}


def test_list_of_links_with_form_render(app):
    page = Page(title='xxx', slug='test_list_of_links_with_form_render', template_name='standard')
    page.save()

    cell = LinkListCell.objects.create(order=0, placeholder='content', page=page)
    link = WcsFormCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        cached_title='A title',
        cached_url='http://example.com/',
        order=0,
        extra_css_class='foobar',
    )

    resp = app.get('/test_list_of_links_with_form_render/')
    assert PyQuery(resp.text).find('.links-list a').text() == 'A title'
    assert PyQuery(resp.text).find('.links-list li').attr('class') == ' foobar'
    assert (
        PyQuery(resp.text).find('.links-list a').attr('href')
        == 'http://example.com/tryauth?cancelurl=http%3A//testserver/test_list_of_links_with_form_render/'
    )

    link.cached_json = {'keywords': ['bar']}
    link.save()
    resp = app.get('/test_list_of_links_with_form_render/')
    assert PyQuery(resp.text).find('.links-list li').attr('class') == 'keyword-bar foobar'

    link.extra_css_class = ''
    link.save()
    resp = app.get('/test_list_of_links_with_form_render/')
    assert PyQuery(resp.text).find('.links-list li').attr('class') == 'keyword-bar'

    link.cached_json = {'description': 'en outre, on est des loutres'}
    link.save()
    resp = app.get('/test_list_of_links_with_form_render/')
    assert PyQuery(resp.text).find('.links-list li .description').text() == 'en outre, on est des loutres'

    WcsFormCell.objects.create(
        page=page,
        placeholder=cell.link_placeholder,
        cached_title='Another title',
        cached_url='http://example2.com/',
        order=1,
        cached_json={'description': 'ça poutre'},
    )
    cell.limit = 1
    cell.save()
    resp = app.get('/test_list_of_links_with_form_render/')
    assert PyQuery(resp.text).find('.links-list ul.more-items li .description').text() == 'ça poutre'


def test_view_page_with_wcs_cells_num_queries(app, admin_user):
    page = Page.objects.create(title='bar', slug='index', order=1)
    for i in range(0, 15):
        WcsCurrentDraftsCell.objects.create(page=page, placeholder='content', order=i)
    for i in range(15, 50):
        WcsCurrentFormsCell.objects.create(page=page, placeholder='content', order=i)
    app = login(app)
    app.get('/')  # load once to populate caches
    with CaptureQueriesContext(connection) as ctx:
        app.get('/')
        assert len(ctx.captured_queries) == 62


def test_hourly():
    appconfig = apps.get_app_config('wcs')
    page = Page.objects.create(title='xxx', slug='test_current_forms_cell_render', template_name='standard')
    cell_classes = [c for c in appconfig.get_models() if c in get_cell_classes()]
    for klass in cell_classes:
        klass.objects.create(page=page, placeholder='content', order=0)
    for klass in cell_classes:
        if klass in [
            WcsCareFormsCell,
            WcsCurrentFormsCell,
            WcsCurrentDraftsCell,
            WcsFormsOfCategoryCell,
            WcsCardCell,
            BackofficeSubmissionCell,
            CategoriesCell,
            TrackingCodeInputCell,
        ]:
            with mock.patch('combo.apps.wcs.models.%s.check_validity' % klass.__name__) as check_validity:
                appconfig.hourly()
            assert check_validity.call_args_list == [mock.call()]
        else:
            assert hasattr(klass, 'check_validity') is False


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_search_external_forms_links(mock_send, context):
    page = Page(title='xxx', slug='test_forms_of_category_cell_search', template_name='standard')
    page.save()

    cell = SearchCell(page=page, _search_services={'data': ['_text']}, order=0)
    cell.save()

    index_site()
    request = RequestFactory().get('/')
    request.user = AnonymousUser()
    hits = search_site(request, 'form')
    assert len(hits) == 0

    cell = WcsFormsOfCategoryCell(page=page, placeholder='content', order=1)
    cell.category_reference = 'default:test-9'
    cell.ordering = 'alpha'
    cell.save()
    context['synchronous'] = True  # to get fresh content
    result = cell.render(context)
    assert 'form title' in result and 'a second form title' in result
    assert 'http://127.0.0.1:8999/form-title/tryauth' in result
    assert 'http://127.0.0.1:8999/a-second-form-title/tryauth' in result

    index_site()
    request = RequestFactory().get('/')
    request.user = AnonymousUser()
    hits = search_site(request, 'form')
    assert len(hits) == 2
