import re
from importlib import import_module

import pytest
import responses
from django.conf import settings
from django.test.client import RequestFactory

from .utils import responses_callback


@pytest.fixture
def context():
    ctx = {'request': RequestFactory().get('/')}
    ctx['request'].user = None
    session_engine = import_module(settings.SESSION_ENGINE)
    ctx['request'].session = session_engine.SessionStore()
    return ctx


@pytest.fixture
def wcs_mock():
    with responses.mock as wcs_mock:
        responses.add_callback(
            responses.GET, re.compile(r'http://127.0.0.1:8999/.*'), callback=responses_callback
        )
        yield wcs_mock
