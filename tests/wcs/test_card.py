import copy
import json
import re
import sys
from io import StringIO
from unittest import mock

import pytest
import responses
from django.apps import apps
from django.contrib.auth.models import AnonymousUser, User
from django.core.cache import cache
from django.core.management import call_command
from django.urls import reverse
from pyquery import PyQuery
from requests.exceptions import ConnectionError
from requests.models import Response

from combo.apps.lingo.models import InvoicesCell
from combo.apps.wcs.forms import WcsCardCellDisplayForm
from combo.apps.wcs.models import WcsCardCell
from combo.apps.wcs.utils import get_wcs_services
from combo.data.models import Page, TextCell, ValidityInfo
from combo.data.utils import import_site
from combo.utils import requests
from tests.test_manager import login
from tests.utils import manager_submit_cell

from .utils import MockedRequestResponse, MockUser, MockUserWithNameId, mocked_requests_send

pytestmark = pytest.mark.django_db


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_setup(mock_send, app, admin_user):
    page = Page.objects.create(
        title='xxx', slug='test_card_cell_save_cache', template_name='standard', sub_slug='foobar'
    )
    cell = WcsCardCell(page=page, placeholder='content', order=0)
    form_class = cell.get_default_form_class()
    form = form_class(instance=cell)
    assert form.fields['carddef_reference'].widget.choices == [
        ('default:card_model_1', 'test : Card Model 1'),
        ('default:card_model_1:foo', 'test : Card Model 1 - bar'),
        ('default:card_model_2', 'test : Card Model 2'),
        ('default:card_model_3', 'test : Card Model 3'),
        ('default:card_a', 'test : Card A'),
        ('default:card_a:a-custom-view', 'test : Card A - foo bar'),
        ('default:card_b', 'test : Card B'),
        ('default:card_b:b-custom-view', 'test : Card B - foo bbr'),
        ('default:card_c', 'test : Card C'),
        ('default:card_d', 'test : Card D'),
        ('default:card-e', 'test : Card E'),
        ('other:card_model_1', 'test2 : Card Model 1'),
        ('other:card_model_1:foo', 'test2 : Card Model 1 - bar'),
        ('other:card_model_2', 'test2 : Card Model 2'),
        ('other:card_model_3', 'test2 : Card Model 3'),
        ('other:card_a', 'test2 : Card A'),
        ('other:card_a:a-custom-view', 'test2 : Card A - foo bar'),
        ('other:card_b', 'test2 : Card B'),
        ('other:card_b:b-custom-view', 'test2 : Card B - foo bbr'),
        ('other:card_c', 'test2 : Card C'),
        ('other:card_d', 'test2 : Card D'),
        ('other:card-e', 'test2 : Card E'),
    ]

    form_display = WcsCardCellDisplayForm(instance=cell)
    assert 'customize_display' not in form_display.fields
    assert 'custom_schema' not in form_display.fields

    cell.save()
    assert 'customize_display' not in form_display.fields
    assert 'custom_schema' not in form_display.fields

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    form_display = WcsCardCellDisplayForm(instance=cell)
    assert 'customize_display' in form_display.fields
    assert 'custom_schema' in form_display.fields
    assert 'customize_display' not in form_display.initial
    assert form_display.initial['custom_schema'] == {}

    cell.carddef_reference = 'default:card_model_1:foo'
    cell.save()
    form_display = WcsCardCellDisplayForm(instance=cell)
    assert 'customize_display' in form_display.fields
    assert 'custom_schema' in form_display.fields
    assert 'customize_display' not in form_display.initial
    assert form_display.initial['custom_schema'] == {}

    cell.carddef_reference = 'default:card_model_1'
    cell.save()

    cell.custom_schema = {'cells': [{'varname': 'foo', 'display_mode': 'value'}]}
    cell.save()
    form_display = WcsCardCellDisplayForm(instance=cell)
    assert 'customize_display' in form_display.fields
    assert 'custom_schema' in form_display.fields
    assert form_display.initial['customize_display'] is True
    assert form_display.initial['custom_schema'] == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {'varname': 'foo', 'field_content': 'value', 'display_mode': 'text', 'empty_value': '@empty@'}
        ],
    }

    WcsCardCell.objects.all().delete()

    # check adding a cell from the UI
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)
    cell_add_url = [x for x in resp.html.find_all('option') if x.text == 'Card(s)'][0].get('data-add-url')
    resp = app.get(cell_add_url).follow()
    cell = WcsCardCell.objects.all().first()
    manager_submit_cell(resp.forms[0])  # will save card model
    cell.refresh_from_db()

    # check getting back to uncustomized display reset the schema
    cell.custom_schema = {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {'varname': 'foo', 'field_content': 'value', 'display_mode': 'text', 'empty_value': '@empty@'}
        ],
    }
    cell.save()

    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-customize_display' % cell.get_reference()].value == 'on'
    resp.forms[0]['c%s-customize_display' % cell.get_reference()].value = False
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.custom_schema == {}

    cell.custom_schema = {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {'varname': 'foo', 'field_content': 'value', 'display_mode': 'text', 'empty_value': '@empty@'}
        ],
    }
    cell.save()
    mock_send.reset_mock()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert mock_send.call_args_list == []
    assert resp.forms[0]['c%s-display_mode' % cell.get_reference()].value == 'card'
    resp.forms[0]['c%s-display_mode' % cell.get_reference()].value = 'table'
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.custom_schema == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {'varname': 'foo', 'field_content': 'value', 'display_mode': 'text', 'empty_value': '@empty@'}
        ],
    }

    assert cell.related_card_path == '__all__'
    assert cell.card_ids == ''
    mock_send.reset_mock()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert mock_send.call_args_list == []
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].value == '__all__'
    resp.forms[0]['c%s-related_card_path' % cell.get_reference()].value = '--'
    resp.forms[0]['c%s-card_ids' % cell.get_reference()].value = '42'
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.related_card_path == ''
    assert cell.card_ids == ''
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].value == '--'
    resp.forms[0]['c%s-related_card_path' % cell.get_reference()].value = ''
    resp.forms[0]['c%s-card_ids' % cell.get_reference()].value = '42'
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.related_card_path == ''
    assert cell.card_ids == '42'

    # current page has a sub_slug, '--' option is present
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert '--' in [o[0] for o in resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options]

    # current_page has no sub_slug, but parent page has one
    parent_page = Page.objects.create(
        title='parent', slug='parent', template_name='standard', sub_slug='foobar'
    )
    page.parent = parent_page
    page.sub_slug = ''
    page.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert '--' in [o[0] for o in resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options]

    # no sub_slug
    parent_page.sub_slug = ''
    parent_page.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert '--' not in [o[0] for o in resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options]
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].value == ''
    resp.forms[0]['c%s-card_ids' % cell.get_reference()].value = ''
    resp = resp.forms[0].submit()
    assert resp.context['form'].errors == {'card_ids': ['This field is required.']}

    # check custom_title
    for title_type in ['auto', 'empty']:
        cell.custom_title = 'foo bar'
        cell.save()
        resp = app.get('/manage/pages/%s/' % page.pk)
        resp.forms[0]['c%s-title_type' % cell.get_reference()].value = title_type
        resp = resp.forms[0].submit()
        cell.refresh_from_db()
        assert cell.custom_title == ''


def test_card_cell_custom_schema_migration():
    cell = WcsCardCell(display_mode='table')

    cell.custom_schema = {'cells': [{'varname': 'some-field', 'empty_value': '@empty@'}]}
    assert cell.get_custom_schema() == {
        'grid_headers': False,
        'cells': [
            {
                'varname': 'some-field',
                'empty_value': '',
            }
        ],
    }

    cell.custom_schema = {
        'cells': [{'varname': 'some-field', 'display_mode': 'label', 'cell_size': 'foobar'}]
    }
    assert cell.get_custom_schema() == {
        'grid_headers': False,
        'cells': [
            {
                'varname': 'some-field',
                'display_mode': 'label',
                'cell_size': 'foobar',
            }
        ],
    }

    cell.display_mode = 'card'
    cell.custom_schema = {
        'cells': [{'varname': 'some-field', 'display_mode': 'label', 'cell_size': 'foobar'}]
    }
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {
                'varname': 'some-field',
                'field_content': 'label',
                'display_mode': 'text',
                'empty_value': '@empty@',
                'cell_size': 'foobar',
            }
        ],
    }
    cell.custom_schema = {'cells': [{'varname': 'some-field', 'display_mode': 'value'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {
                'varname': 'some-field',
                'field_content': 'value',
                'display_mode': 'text',
                'empty_value': '@empty@',
            }
        ],
    }
    cell.custom_schema = {'cells': [{'varname': 'some-field', 'display_mode': 'label-and-value'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {
                'varname': 'some-field',
                'field_content': 'label-and-value',
                'display_mode': 'text',
                'empty_value': '@empty@',
            }
        ],
    }
    cell.custom_schema = {'cells': [{'varname': 'some-field'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {
                'varname': 'some-field',
                'field_content': 'label-and-value',
                'display_mode': 'text',
                'empty_value': '@empty@',
            }
        ],
    }
    cell.custom_schema = {'cells': [{'varname': 'some-field', 'display_mode': 'title'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {
                'varname': 'some-field',
                'field_content': 'value',
                'display_mode': 'title',
                'empty_value': '@empty@',
            }
        ],
    }

    cell.custom_schema = {
        'cells': [
            {'varname': '@custom@', 'template': 'foobar', 'display_mode': 'label', 'cell_size': 'foobar'}
        ]
    }
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [
            {'varname': '@custom@', 'template': 'foobar', 'display_mode': 'label', 'cell_size': 'foobar'}
        ],
    }
    cell.custom_schema = {'cells': [{'varname': '@custom@', 'template': 'foobar', 'display_mode': 'value'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [{'varname': '@custom@', 'template': 'foobar', 'display_mode': 'text'}],
    }
    cell.custom_schema = {'cells': [{'varname': '@custom@', 'template': 'foobar', 'display_mode': 'title'}]}
    assert cell.get_custom_schema() == {
        'grid_class': 'fx-grid--auto',
        'cells': [{'varname': '@custom@', 'template': 'foobar', 'display_mode': 'title'}],
    }


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_save_cache(mock_send):
    page = Page.objects.create(title='xxx', slug='test_card_cell_save_cache', template_name='standard')
    cell = WcsCardCell(page=page, placeholder='content', order=0)
    assert cell.get_additional_label() is None
    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    assert cell.cached_title == 'Card Model 1'
    assert cell.cached_json != {}
    assert cell.get_additional_label() == 'Card Model 1'
    # make sure cached attributes are removed from serialized pages
    assert 'cached_' not in json.dumps(page.get_serialized_page())

    # artificially change title and json
    WcsCardCell.objects.filter(pk=cell.pk).update(cached_title='XXX', cached_json={})
    assert WcsCardCell.objects.get(pk=cell.pk).cached_title == 'XXX'
    assert WcsCardCell.objects.get(pk=cell.pk).cached_json == {}
    # run update db cache
    appconfig = apps.get_app_config('wcs')
    appconfig.update_db_cache()
    assert WcsCardCell.objects.get(pk=cell.pk).cached_title == 'Card Model 1'
    assert WcsCardCell.objects.get(pk=cell.pk).cached_json != {}


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_validity(mock_send):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(page=page, placeholder='content', order=0)
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_card_not_defined'
    assert validity_info.invalid_since is not None

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve data, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        cell.save()
    assert ValidityInfo.objects.exists() is False

    # can not retrieve carddefs, don't set cell as invalid
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.save()
    assert ValidityInfo.objects.exists() is False

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.json = lambda *a, **k: {'err': 1, 'err_class': 'Page not found'}
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        cell.carddef_reference = 'default:foobar'
        cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_card_not_found'
    assert validity_info.invalid_since is not None
    cell.mark_as_valid()

    cell.carddef_reference = 'invalid:card_model_1'
    cell.save()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_site_not_found'
    assert validity_info.invalid_since is not None
    assert cell.get_invalid_reason() == 'Invalid site'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_check_validity(mock_send):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_a',
        related_card_path='',
        card_ids='1',
    )
    cell2 = WcsCardCell.objects.create(
        page=page, placeholder='content', order=1, carddef_reference='default:card_b'
    )

    # no related_card_path
    cell2.check_validity()
    assert ValidityInfo.objects.exists() is False

    # correct related_card_path but sluga is not defined
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    cell2.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_card_relation_not_found'
    assert validity_info.invalid_since is not None

    # sluga is now defined
    cell.slug = 'sluga'
    cell.save()
    cell2.check_validity()
    assert ValidityInfo.objects.exists() is False

    # bad related_card_path
    cell2.related_card_path = 'sluga/foobar'
    cell2.save()
    cell2.check_validity()
    validity_info = ValidityInfo.objects.latest('pk')
    assert validity_info.invalid_reason_code == 'wcs_card_relation_not_found'
    assert validity_info.invalid_since is not None


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_card_cell(mock_send, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test_cards', template_name='standard', sub_slug='foobar')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        related_card_path='',
    )

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert 'application/json' not in resp

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert '<script id="cell-%s-card-schema-default:card_model_1" type="application/json">' % cell.pk in resp

    assert ('data-cell-reference="%s"' % cell.get_reference()) in resp.text
    assert cell.without_user is False
    assert resp.forms[0]['c%s-with_user' % cell.get_reference()].value == 'on'
    resp.forms[0]['c%s-with_user' % cell.get_reference()].value = False
    manager_submit_cell(resp.forms[0])
    cell.refresh_from_db()
    assert cell.without_user is True
    assert resp.forms[0]['c%s-with_user' % cell.get_reference()].value is None

    # card with relations
    cell.carddef_reference = 'default:card_a'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    # but only one cell on the page, no relations to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]

    # all cards
    cell.related_card_path = '__all__'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', True, 'All cards'),
        ('--', False, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]

    # add a second cell, related to the first card model
    cell.related_card_path = ''
    cell.save()
    cell2 = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=1,
        carddef_reference='default:card_b',
        related_card_path='',
    )
    resp = app.get('/manage/pages/%s/' % page.pk)
    # still no relation to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]
    # no cell with id and slug
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]

    # set a slug on first cell
    cell.slug = 'sluga'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    # still no relation to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]
    # multiple relations to follow
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluga/cardb', False, 'Linked card: "Card B"'),
        ('sluga/cardsb', False, 'Linked cards: "Cards B"'),
        ('sluga/blockb_cardb', False, 'Linked card: "Block B - Card B"'),
        ('sluga/cardc/cardb', False, 'Linked card: "Card C" -> "Card B"'),
        ('sluga/cardc/cardsb', False, 'Linked cards: "Card C" -> "Cards B"'),
        ('sluga/cardc/blockb_cardb', False, 'Linked card: "Card C" -> "Block B - Card B"'),
        ('sluga/bocardb', False, 'Linked card: "BO Card B"'),
        ('', False, 'Template'),
    ]

    # set a list of ids on first cell
    cell.card_ids = '{{ cards|objects:"card_model_1"|getlist:"id"|join:"," }}'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    # still no relation to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', False, 'Card whose identifier is in the URL'),
        ('', True, 'Template'),
    ]
    # can not user cell with multiple ids as reference
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]

    # define a slug on second cell
    cell.card_ids = ''
    cell.save()
    cell2.slug = 'slugb'
    cell2.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    # multiple relations to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('slugb/reverse:cardb', False, 'Linked cards: "Card B" (reverse relation)'),
        ('slugb/reverse:bocardb', False, 'Linked cards: "BO Card B" (reverse relation)'),
        ('slugb/reverse:cardsb', False, 'Linked cards: "Cards B" (reverse relation)'),
        ('slugb/reverse:blockb_cardb', False, 'Linked cards: "Block B - Card B" (reverse relation)'),
        ('', False, 'Template'),
    ]
    # still multiple relations to follow
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluga/cardb', False, 'Linked card: "Card B"'),
        ('sluga/cardsb', False, 'Linked cards: "Cards B"'),
        ('sluga/blockb_cardb', False, 'Linked card: "Block B - Card B"'),
        ('sluga/cardc/cardb', False, 'Linked card: "Card C" -> "Card B"'),
        ('sluga/cardc/cardsb', False, 'Linked cards: "Card C" -> "Cards B"'),
        ('sluga/cardc/blockb_cardb', False, 'Linked card: "Card C" -> "Block B - Card B"'),
        ('sluga/bocardb', False, 'Linked card: "BO Card B"'),
        ('', False, 'Template'),
    ]

    # set a related_path on cell2
    resp.forms[6]['c%s-related_card_path' % cell2.get_reference()] = 'sluga/cardb'
    resp.forms[6]['c%s-card_ids' % cell2.get_reference()] = 'foobar'
    resp = resp.forms[6].submit()
    cell2.refresh_from_db()
    assert cell2.related_card_path == 'sluga/cardb'
    assert cell2.card_ids == ''
    resp = app.get('/manage/pages/%s/' % page.pk)
    # no more relation to follow
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('', False, 'Template'),
    ]
    # still multiple relations to follow
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', False, 'Card whose identifier is in the URL'),
        ('sluga/cardb', True, 'Linked card: "Card B"'),
        ('sluga/cardsb', False, 'Linked cards: "Cards B"'),
        ('sluga/blockb_cardb', False, 'Linked card: "Block B - Card B"'),
        ('sluga/cardc/cardb', False, 'Linked card: "Card C" -> "Card B"'),
        ('sluga/cardc/cardsb', False, 'Linked cards: "Card C" -> "Cards B"'),
        ('sluga/cardc/blockb_cardb', False, 'Linked card: "Card C" -> "Block B - Card B"'),
        ('sluga/bocardb', False, 'Linked card: "BO Card B"'),
        ('', False, 'Template'),
    ]
    resp.forms[6].submit()
    cell2.refresh_from_db()
    assert cell2.related_card_path == 'sluga/cardb'
    assert cell2.card_ids == ''

    # check circular relations
    cell.slug = 'sluge'
    cell.carddef_reference = 'default:card_e'
    cell.save()
    cell2.carddef_reference = 'default:card_d'
    cell2.slug = 'slugd'
    cell2.related_card_path = ''
    cell2.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('slugd/cardd-foo/carde-foo', False, 'Linked card: "Card D" -> "Card E"'),
        ('slugd/carde-foo', False, 'Linked card: "Card E"'),
        ('', False, 'Template'),
    ]
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluge/cardd-bar', False, 'Linked card: "Card D"'),
        ('sluge/reverse:carde-foo', False, 'Linked cards: "Card E" (reverse relation)'),
        ('', False, 'Template'),
    ]

    cell.slug = 'slugd'
    cell.carddef_reference = 'default:card_d'
    cell.save()
    cell2.carddef_reference = 'default:card_d'
    cell2.slug = 'slugd-bis'
    cell2.related_card_path = ''
    cell2.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('slugd-bis/cardd-foo', False, 'Linked card: "Card D"'),
        ('slugd-bis/reverse:cardd-foo', False, 'Linked cards: "Card D" (reverse relation)'),
        ('slugd-bis/carde-foo/cardd-bar', False, 'Linked card: "Card E" -> "Card D"'),
        (
            'slugd-bis/carde-foo/reverse:carde-foo',
            False,
            'Linked cards: "Card E" -> "Card E" (reverse relation)',
        ),
        ('', False, 'Template'),
    ]
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('slugd/cardd-foo', False, 'Linked card: "Card D"'),
        ('slugd/reverse:cardd-foo', False, 'Linked cards: "Card D" (reverse relation)'),
        ('slugd/carde-foo/cardd-bar', False, 'Linked card: "Card E" -> "Card D"'),
        ('slugd/carde-foo/reverse:carde-foo', False, 'Linked cards: "Card E" -> "Card E" (reverse relation)'),
        ('', False, 'Template'),
    ]

    cell.slug = 'sluge'
    cell.carddef_reference = 'default:card_e'
    cell.save()
    cell2.carddef_reference = 'default:card_e'
    cell2.slug = 'sluge-bis'
    cell2.related_card_path = ''
    cell2.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluge-bis/cardd-bar/carde-foo', False, 'Linked card: "Card D" -> "Card E"'),
        ('', False, 'Template'),
    ]
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluge/cardd-bar/carde-foo', False, 'Linked card: "Card D" -> "Card E"'),
        ('', False, 'Template'),
    ]

    # many cells with slug
    WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=2,
        carddef_reference='default:card_e',
        slug='sluge-again',
        card_ids='42',
        related_card_path='',
    )
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0]['c%s-related_card_path' % cell.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluge-bis/cardd-bar/carde-foo', False, 'Linked card (From cell sluge-bis): "Card D" -> "Card E"'),
        (
            'sluge-again/cardd-bar/carde-foo',
            False,
            'Linked card (From cell sluge-again): "Card D" -> "Card E"',
        ),
        ('', False, 'Template'),
    ]
    assert resp.forms[6]['c%s-related_card_path' % cell2.get_reference()].options == [
        ('__all__', False, 'All cards'),
        ('--', True, 'Card whose identifier is in the URL'),
        ('sluge/cardd-bar/carde-foo', False, 'Linked card (From cell sluge): "Card D" -> "Card E"'),
        (
            'sluge-again/cardd-bar/carde-foo',
            False,
            'Linked card (From cell sluge-again): "Card D" -> "Card E"',
        ),
        ('', False, 'Template'),
    ]


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_card_cell_tabs(mock_send, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test_cards', template_name='standard', sub_slug='foobar')
    cell = WcsCardCell.objects.create(page=page, placeholder='content', order=0)

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert not resp.pyquery('[data-tab-slug="general"] select[name$="title_type"]')
    assert not resp.pyquery('[data-tab-slug="general"] input[name$="custom_title"]')
    assert not resp.pyquery('[data-tab-slug="general"] input[name$="limit"]')
    assert not resp.pyquery('#tab-%s-general.pk-tabs--button-marker' % cell.get_reference())
    assert resp.pyquery('[data-tab-slug="appearance"] select[name$="title_type"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="custom_title"]')
    assert not resp.pyquery('[data-tab-slug="appearance"] input[name$="customize_display"]')
    assert resp.pyquery('[data-tab-slug="display"] input[name$="limit"]')

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.pyquery('#tab-%s-general.pk-tabs--button-marker' % cell.get_reference())
    assert resp.pyquery('[data-tab-slug="display"] input[name$="customize_display"]')


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_invoices_cell_tabs_with_card_cell(mock_send, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test', template_name='standard')
    cell = InvoicesCell.objects.create(
        regie='remote', display_mode='active', page=page, placeholder='content', order=1
    )
    assert cell.is_enabled() is False

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id, status=200)

    # assert resp.pyquery('[data-tab-slug="general"] input')
    assert not resp.pyquery('.invoices-cell [data-tab-slug="general"] input[name$="title"]')
    assert resp.pyquery('.invoices-cell [data-tab-slug="appearance"] input[name$="title"]')

    cell2 = WcsCardCell.objects.create(page=page, placeholder='content', order=0)
    resp = app.get('/manage/pages/%s/' % page.id, status=200)
    assert resp.pyquery('.invoices-cell [data-tab-slug="general"] input')
    assert not resp.pyquery(
        '.invoices-cell [data-tab-slug="general"] input#id_cwcs_wcscardcell-%s-card_ids' % cell2.pk
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_manager_card_cell_filters(mock_send, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test_cards', template_name='standard', sub_slug='foobar')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        related_card_path='',
        carddef_reference='default:card_model_1',
    )

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    # only item and items field with varnames are allowed
    assert resp.forms[0]['c%s-filters' % cell.get_reference()].options == [
        ('', True, '---------'),
        ('status', False, 'Status'),
        ('fieldb', False, 'Field B'),
        ('fieldj', False, 'Field J'),
        ('fieldl', False, 'Field L'),
        ('related', False, 'Related'),
        ('item', False, 'Item'),
    ]

    resp.forms[0]['c%s-filters' % cell.get_reference()] = 'related'
    manager_submit_cell(resp.forms[0])

    cell.refresh_from_db()
    assert cell.filters == ['related']

    cell.filters = ['related', 'fieldj', 'status', 'item']
    cell.save()

    resp = app.get('/manage/pages/%s/' % page.pk)
    assert resp.forms[0].get('c%s-filters' % cell.get_reference(), 0).value == 'related'
    assert resp.forms[0].get('c%s-filters' % cell.get_reference(), 1).value == 'fieldj'
    assert resp.forms[0].get('c%s-filters' % cell.get_reference(), 2).value == 'status'
    assert resp.forms[0].get('c%s-filters' % cell.get_reference(), 3).value == 'item'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_load(mock_send):
    page = Page.objects.create(title='xxx', slug='test_cards', template_name='standard')
    cell = WcsCardCell(page=page, placeholder='content', order=0)
    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    site_export = [page.get_serialized_page()]
    cell.delete()
    assert not Page.objects.get(pk=page.pk).get_cells()
    Page.load_serialized_pages(site_export)
    page = Page.objects.get(slug='test_cards')
    cells = page.get_cells()
    assert len(cells) == 1
    cell = cells[0]
    assert cell.cached_title == 'Card Model 1'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='table',
        carddef_reference='default:card_model_1',
        related_card_path='',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    # no custom schema, display all card fields
    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result
    assert [PyQuery(td).text() for td in PyQuery(result).find('table th')] == [
        'Field A',
        'Field B',
        'Field C',
        'Field D',
        'Field D2',
        'Field E',
        'Field F',
        'Field G',
        'Field H',
        'Field I',
        'Field II',
        'Field J',
        'Field L',
        'Field M',
        'Empty',
        'Empty Email',
        'Related',
    ]
    assert [PyQuery(td).text() for td in PyQuery(result).find('table tr:first-child td')] == [
        '<i>a</i>',
        'yes',
        '2020-09-28',
        'file.pdf',
        'file.pdf',
        "lorem<strong>ipsum hello'world",
        'lorem<strong>ipsum hello world',
        'test@localhost',
        'https://www.example.net/',
        "loremipsum\nhello'world",
        "loremipsum\nhello'world",
        "First Value, Second Value '",
        '',
        'value1, value2',
        '',
        '',
        'Foo Bar',
    ]
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(4) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(5) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(8) a').text().strip() == 'test@localhost'
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(8) a').attr['href'] == 'mailto:test@localhost'
    )
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(9) a').text().strip()
        == 'https://www.example.net/'
    )
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(9) a').attr['href']
        == 'https://www.example.net/'
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(10) p:first-child').text() == 'loremipsum'
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(10) p:first-child strong').text() == 'ipsum'
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(10) p:last-child').text() == "hello'world"
    assert 'data-paginate-by="10"' in result

    cell.carddef_reference = 'default:card_model_1:foo'
    cell.limit = 42
    cell.save()

    result = cell.render(context)
    assert 'data-paginate-by="42"' in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps({'data': [], 'count': 0}))
        cell.render(context)
    assert len(requests_get.call_args_list) == 1
    assert (
        requests_get.call_args_list[0][0][0] == '/api/cards/card_model_1/list/foo'
        '?include-fields=on&include-submission=on&include-workflow=on&include-actions=on&filter-identifier=11&limit=42&offset=0'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps({'data': [], 'count': 0}))
        cell.render(context)
    assert len(requests_get.call_args_list) == 1
    assert (
        requests_get.call_args_list[0][0][0] == '/api/cards/card_model_1/list'
        '?include-fields=on&include-submission=on&include-workflow=on&include-actions=on&filter-identifier=11&limit=42&offset=0'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render_custom_schema_card_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {'varname': 'fielda'},
                {'varname': 'fieldb'},
                {'varname': 'fieldc'},
                {'varname': 'related'},
                {'varname': 'fieldd'},
                {'varname': 'fieldd', 'file_display_mode': 'thumbnail'},
                {'varname': 'fieldd2'},
                {'varname': 'fieldd2', 'file_display_mode': 'thumbnail'},
                {'varname': 'fielde'},
                {'varname': 'fieldf'},
                {'varname': 'fieldg'},
                {'varname': 'fieldh'},
                {'varname': 'fieldi'},
                {'varname': 'fieldii'},
                {'varname': 'fieldj'},
                {'varname': 'fieldl'},
                {'varname': 'unknown'},
                {'varname': 'user:name'},
                {'varname': 'user:email'},
                {'varname': 'user:first_name'},
                {'varname': 'user:last_name'},
                {'varname': 'user:unknown'},
                {'varname': 'info:id'},
                {'varname': 'info:receipt_time'},
                {'varname': 'info:last_update_time'},
                {'varname': 'info:status'},
                {'varname': 'info:text'},
                {},  # missing varname
            ]
        },
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert PyQuery(result).find('ul li') == []
    assert len(PyQuery(result).find('table tr td')) == 25 * 3
    assert [PyQuery(td).text() for td in PyQuery(result).find('table tr:first-child td')] == [
        '<i>a</i>',
        'yes',
        '2020-09-28',
        'Foo Bar',
        'file.pdf',
        'file.pdf',
        'file.pdf',
        '',  # it's an image !
        "lorem<strong>ipsum hello'world",  # no multiline support for now
        'lorem<strong>ipsum hello world',
        'test@localhost',
        'https://www.example.net/',
        "loremipsum\nhello'world",
        "loremipsum\nhello'world",
        "First Value, Second Value '",
        '',
        'User Foo Bar',
        'foo@bar.com',
        'User',
        'Foo Bar',
        '11',
        '2021-11-29 14:33',
        '2023-07-24 16:04',
        'Recorded',
        'aa',
    ]
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(5) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(6) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(7) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(8) a')
        .attr['href']
        .startswith('/api/wcs/file/')
    )
    assert (
        PyQuery(result)
        .find('table tr:first-child td:nth-child(8) a img')
        .attr['src']
        .startswith('/api/wcs/file/')
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(11) a').text().strip() == 'test@localhost'
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(11) a').attr['href']
        == 'mailto:test@localhost'
    )
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(12) a').text().strip()
        == 'https://www.example.net/'
    )
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(12) a').attr['href']
        == 'https://www.example.net/'
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(14) p:first-child').text() == 'loremipsum'
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(14) p:first-child strong').text() == 'ipsum'
    )
    assert PyQuery(result).find('table tr:first-child td:nth-child(14) p:last-child').text() == "hello'world"
    assert PyQuery(result).find('table tr:first-child td:nth-child(18) a').text().strip() == 'foo@bar.com'
    assert (
        PyQuery(result).find('table tr:first-child td:nth-child(18) a').attr['href'] == 'mailto:foo@bar.com'
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render_custom_schema_card_empty_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={'cells': [{'varname': 'empty', 'empty_value': ''}]},
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == ''

    cell.custom_schema['cells'][0] = {
        'varname': 'empty',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == 'Custom text'

    cell.custom_schema['cells'][0] = {
        'varname': 'empty_email',
        'empty_value': '',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == ''
    assert PyQuery(result).find('table tr:first-child td:first-child a') == []

    cell.custom_schema['cells'][0] = {
        'varname': 'empty_email',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == 'Custom text'
    assert PyQuery(result).find('table tr:first-child td:first-child a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render_custom_schema_custom_entry(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': "<b>Foo</b> bar'baz {{ card.fields.fielde }}"},
            ]
        },
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert (
        PyQuery(result).find('table tr:first-child td:first-child').text()
        == "<b>Foo</b> bar'baz lorem<strong>ipsum hello'world"
    )

    # test context
    cell.custom_schema['cells'][0][
        'template'
    ] = '{{ card.fields.fielda }} - {{ card.fields.related }} ({{ card.fields.related_structured.id }})'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == '<i>a</i> - Foo Bar (42)'

    # test filters in template
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.related|split:" "|join:"," }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == 'Foo,Bar'

    # test available context
    cell.custom_schema['cells'][0][
        'template'
    ] = 'Foo bar baz {% make_public_url url="http://127.0.0.1:8999/" %}'
    cell.save()
    result = cell.render(context)
    assert '/api/wcs/file/' in PyQuery(result).find('table tr:first-child td:first-child').text()


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render_custom_schema_link_entry(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@link@',
                    'url_template': '/foo/bar/{{ card.fields.related_structured.id }}/',
                    'template': '{{ card.fields.fielda }} - {{ card.fields.related }}',
                    'display_mode': 'link',
                },
            ]
        },
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    def test(value, href, class_name, is_file=False):
        result = cell.render(context)
        assert PyQuery(result).find('ul li') == []
        assert PyQuery(result).find('table tr:first-child td:first-child a').text() == value
        if not is_file:
            assert PyQuery(result).find('table tr:first-child td:first-child a').attr['href'] == href
        else:
            assert PyQuery(result).find('table tr:first-child td:first-child a').attr['href'].startswith(href)
        assert PyQuery(result).find('table tr:first-child td:first-child a').attr['class'] == class_name

    test('<i>a</i> - Foo Bar', '/foo/bar/42/', None)

    cell.custom_schema['cells'][0]['display_mode'] = 'button'
    cell.save()
    test('<i>a</i> - Foo Bar', '/foo/bar/42/', 'pk-button')

    # empty url: no link in output but label still shown
    cell.custom_schema['cells'][0]['url_template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []
    assert PyQuery(result).find('table tr:first-child td:first-child').text() == '<i>a</i> - Foo Bar'

    # empty label: no link in output
    cell.custom_schema['cells'][0]['url_template'] = 'foo/bar'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []

    # check with page link
    root_page = Page.objects.create(title='Root', slug='root', template_name='standard')
    page1 = Page.objects.create(
        title='Card',
        slug='card',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=root_page,
    )
    other_root_page = Page.objects.create(title='Other root', slug='other-root', template_name='standard')
    page2 = Page.objects.create(
        title='Card (bis)',
        slug='card-bis',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=other_root_page,
    )

    cell.custom_schema['cells'][0]['url_template'] = ''
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.fielda }} - {{ card.fields.related }}'
    cell.save()
    test('<i>a</i> - Foo Bar', '/root/card/11/', 'pk-button')

    cell.custom_schema['cells'][0]['page'] = page2.pk
    cell.save()
    test('<i>a</i> - Foo Bar', '/other-root/card-bis/11/', 'pk-button')

    # empty label or empty url: no link in output
    cell.custom_schema['cells'][0]['page'] = 0
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []

    # check with field
    cell.custom_schema['cells'][0]['page'] = ''
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.fielda }} - {{ card.fields.related }}'
    cell.save()
    test('<i>a</i> - Foo Bar', '/api/wcs/file/', 'pk-button', is_file=True)

    cell.custom_schema['cells'][0]['display_mode'] = 'link'
    cell.save()
    test('<i>a</i> - Foo Bar', '/api/wcs/file/', None, is_file=True)

    # empty label or no value/no file field/unknown field: no link in output
    result = cell.render(context)
    assert PyQuery(result).find('table tr:last-child td a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'fielda'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'unknown'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('table tr td a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_table_mode_render_custom_schema_action_entry(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@action@',
                    'trigger_id': 'jump:trigger-1',
                    'action_label': 'Label {{ card.fields.fielda }}',
                    'action_ask_confirmation': True,
                    'action_confirmation_template': 'Confirmation {{ card.fields.fielda }}',
                    'unavailable_action_mode': 'hide',
                },
            ]
        },
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    action_attributes = {
        (btn.attrib['card-action-url'], btn.attrib['card-id'], btn.attrib['card-trigger-id'])
        for btn in PyQuery(result).find('wcs-trigger-button')
    }
    expected_attributes = {(cell.get_ajax_url(), str(card_id), 'jump:trigger-1') for card_id in (11, 12, 13)}
    assert action_attributes == expected_attributes

    cell.custom_schema['cells'][0]['trigger_id'] = 'unavailable-trigger'
    cell.save()
    result = cell.render(context)

    action_attributes = {
        (btn.attrib['card-action-url'], btn.attrib['card-id'], btn.attrib['card-trigger-id'])
        for btn in PyQuery(result).find('wcs-trigger-button')
    }
    expected_attributes = {
        (cell.get_ajax_url(), str(card_id), 'unavailable-trigger') for card_id in (11, 12, 13)
    }
    assert action_attributes == expected_attributes

    cell.custom_schema['cells'][0].pop('trigger_id')
    cell.save()
    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    for btn in buttons:
        assert btn.attrib['unavailable'] == 'unavailable'
        assert 'card-action-url' not in btn.attrib

    cell.custom_schema['cells'][0]['action_ask_confirmation'] = False
    cell.save()
    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    for btn in buttons:
        assert 'action_confirmation_template' not in btn.attrib


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('with_headers', [True, False])
def test_card_cell_table_mode_render_with_headers(mock_send, context, with_headers):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'grid_headers': with_headers,
            'cells': [
                {'varname': '@custom@', 'template': 'foo bar'},
                {'varname': '@custom@', 'template': 'foo bar bis', 'header': 'My Custom Header'},
                {'varname': '@custom@', 'template': ''},  # not displayed
                {'varname': 'fieldb'},
                {'varname': 'user:name'},
                {'varname': 'user:email'},
                {'varname': 'user:first_name'},
                {'varname': 'user:last_name'},
                {'varname': '@link@', 'template': 'Foo', 'url_template': 'http://foo/bar', 'header': 'Link'},
                {'varname': '@link@', 'template': 'Bar', 'url_template': '{# empty #}', 'header': 'Link Bis'},
                {
                    'varname': '@link@',
                    'template': '',
                    'url_template': 'http://foo/bar',
                    'header': 'Link Not Displayed',
                },
                {
                    'varname': '@link@',
                    'template': 'Bar',
                    'url_template': '',
                    'header': 'Link Bis Not Displayed',
                },
                {'varname': 'user:unknown'},
                {'varname': 'info:id'},
                {'varname': 'info:receipt_time'},
                {'varname': 'info:last_update_time'},
                {'varname': 'info:status'},
                {'varname': 'info:text'},
                {'varname': '@action@', 'header': 'Action', 'action_label': '', 'trigger_id': ''},
            ],
        },
        display_mode='table',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    if with_headers:
        assert len(PyQuery(result).find('table thead th')) == 15
        assert PyQuery(result).find('table thead th:nth-child(1)').text() == ''
        assert PyQuery(result).find('table thead th:nth-child(2)').text() == 'My Custom Header'
        assert PyQuery(result).find('table thead th:nth-child(3)').text() == 'Field B'
        assert PyQuery(result).find('table thead th:nth-child(4)').text() == 'Name'
        assert PyQuery(result).find('table thead th:nth-child(5)').text() == 'Email'
        assert PyQuery(result).find('table thead th:nth-child(6)').text() == 'First name'
        assert PyQuery(result).find('table thead th:nth-child(7)').text() == 'Last name'
        assert PyQuery(result).find('table thead th:nth-child(8)').text() == 'Link'
        assert PyQuery(result).find('table thead th:nth-child(9)').text() == 'Link Bis'
        assert PyQuery(result).find('table thead th:nth-child(10)').text() == 'Identifier'
        assert PyQuery(result).find('table thead th:nth-child(11)').text() == 'Receipt date'
        assert PyQuery(result).find('table thead th:nth-child(12)').text() == 'Last modified'
        assert PyQuery(result).find('table thead th:nth-child(13)').text() == 'Status'
        assert PyQuery(result).find('table thead th:nth-child(14)').text() == 'Text'
        assert PyQuery(result).find('table thead th:nth-child(15)').text() == 'Action'
    else:
        assert PyQuery(result).find('table thead') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list'])
def test_card_cell_table_list_mode_render_all_cards(mock_send, nocache, app, display_mode):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
        related_card_path='__all__',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # check url called
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    # cell rendering
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render_all_cards_custom_id(mock_send, nocache, app):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='list',
        carddef_reference='default:card_with_custom_id',
        related_card_path='__all__',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # check url called
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    # cell rendering
    assert '/api/cards/card_with_custom_id/list' in mock_send.call_args_list[0][0][0].url
    assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url

    cell_div = resp.pyquery('.wcs-card-cell')[0]
    cell_url = cell_div.attrib['data-ajax-cell-url']
    extra_ctx = cell_div.attrib['data-extra-context']
    resp = app.get(cell_url + '?ctx=' + extra_ctx)
    assert [(PyQuery(x).text(), x.attrib['href']) for x in resp.pyquery('li a')] == [
        ('foo', 'http://127.0.0.1:8999/backoffice/data/card_with_custom_id/foo_-Z/'),
        ('bar', 'http://127.0.0.1:8999/backoffice/data/card_with_custom_id/bar_-Z/'),
    ]

    # add a combo page for card
    card_page = Page.objects.create(title='card', slug='card', template_name='standard')
    card_page.sub_slug = '(?P<card_with_custom_id_id>[a-z0-9]+)'
    card_page.save()

    resp = app.get(cell_url + '?ctx=' + extra_ctx)
    assert [(PyQuery(x).text(), x.attrib['href']) for x in resp.pyquery('li a')] == [
        ('foo', '/card/foo_-Z/'),
        ('bar', '/card/bar_-Z/'),
    ]


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list'])
def test_card_cell_table_list_mode_render_identifier(mock_send, nocache, app, display_mode):
    page = Page.objects.create(
        title='xxx', slug='foo', template_name='standard', sub_slug='(?P<card_model_1_id>[a-zA-Z0-9_-]+)'
    )
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
        related_card_path='',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # check url called
    mock_send.reset_mock()
    resp = app.get(page.get_online_url() + '11/')
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier=11&' in mock_send.call_args_list[0][0][0].url

    # with identifiers
    page.sub_slug = ''
    page.save()
    cell.card_ids = '42'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier=42&' in mock_send.call_args_list[0][0][0].url

    cell.card_ids = '42, , 35'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier=42%2C35&' in mock_send.call_args_list[0][0][0].url

    cell.card_ids = '{% cards|objects:"card_model_1"|last|get:"id" %}'  # syntax error
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'empty-message' in cell_resp

    cell.card_ids = '{{ cards|objects:"card_model_1"|last|get:"id" }}'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 2
    # cell rendering
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[1][0][0].url
    assert '&filter-identifier=13&' in mock_send.call_args_list[1][0][0].url

    # reset
    cell.card_ids = ''
    cell.save()
    page.sub_slug = '(?P<card_model_1_id>[a-zA-Z0-9_-]+)'
    page.save()


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list'])
def test_card_cell_table_list_mode_render_identifier_from_related(mock_send, app, jane_doe, display_mode):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell1 = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        slug='sluga',
        carddef_reference='default:card_a',
        card_ids='1',
        related_card_path='',
    )
    cell2 = WcsCardCell.objects.create(
        page=page, placeholder='content', order=1, slug='slugb', carddef_reference='default:card_b'
    )

    cell2_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell2.get_reference()},
    )

    app = login(app, username='jane.doe', password='jane.doe')

    # just do a simple test to check url calls
    def check(urls):
        resp = app.get(page.get_online_url())
        assert len(resp.context['cells']) == 2
        extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
        mock_send.reset_mock()
        app.get(cell2_url + '?ctx=' + extra_ctx[1])
        assert len(mock_send.call_args_list) == len(urls)
        for j, url_parts in enumerate(urls):
            if not isinstance(url_parts, tuple):
                url_parts = (url_parts,)
            for url_part in url_parts:
                if url_part.startswith('^'):
                    assert url_part[1:] not in mock_send.call_args_list[j][0][0].url
                else:
                    assert url_part in mock_send.call_args_list[j][0][0].url

    # direct and single relation (item)
    # clear cache to see call to /api/cards/card_a/1/
    cache.clear()
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    urls = [
        # get first cell data
        '/api/cards/card_a/1/',
        # follow cardb relation
        (
            '/api/cards/card_b/list',
            'filter-identifier=1',
            '^&limit=10&offset=0',
            '^include-fields',
        ),  # get cards ids
        ('/api/cards/card_b/list', '&filter-identifier=1&limit=10&offset=0'),  # check user access
    ]
    check(urls)

    # direct and multiple relation (items)
    # clear cache to see call to /api/cards/card_a/1/
    cache.clear()
    cell1.only_for_user = True
    cell1.carddef_reference = 'default:card_a:a-custom-view'
    cell1.save()
    cell2.carddef_reference = 'default:card_b'  # reset
    cell2.related_card_path = 'sluga/cardsb'
    cell2.save()
    urls = [
        # get first cell data
        (
            '/api/cards/card_a/a-custom-view/1/',
            '&include-files-content=off&include-evolution=off&include-roles=off'
            '&include-workflow-data=off&include-actions=off&filter-user-uuid=123456&',
        ),
        # and follow cardb relation
        (
            '/api/cards/card_b/list',
            'filter-identifier=2%2C3',
            '^&limit=10&offset=0',
            '^include-fields',
        ),  # get cards ids
        ('/api/cards/card_b/list', '&filter-identifier=2%2C3&limit=10&offset=0'),  # check user access
    ]
    with mock.patch.object(User, 'get_name_id') as mock_name_id:
        mock_name_id.return_value = '123456'
        check(urls)


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list'])
def test_card_cell_table_list_mode_lazy_card_ids(mock_send, nocache, app, display_mode):
    '''Check if templated card_ids are resolved for other non related cells on
    the same page, like a templated text cell.'''
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        slug='sluga',
        carddef_reference='default:card_a',
        card_ids='{{ cards|objects:"card_model_1"|getlist:"id"|join:"," }}',
        related_card_path='',
    )
    TextCell.objects.create(
        page=page,
        placeholder='content',
        order=1,
        slug='slugb',
        text='{{ cards|objects:"card_model_x"|getlist:"id"|join:"," }}',
    )

    resp = app.get(page.get_online_url())
    cell_divs = resp.pyquery('div.cell')
    assert len(cell_divs) == 2
    send_counts = {}
    for cell_div in cell_divs.items():
        mock_send.reset_mock()
        cell_url = cell_div.attr['data-ajax-cell-url']
        extra_context = cell_div.attr['data-extra-context']
        app.get(cell_url + '?ctx=' + extra_context)
        send_counts[cell_div.attr['id']] = len(mock_send.call_args_list)
    # rendering of "slugb" text cell should not make any request.
    assert send_counts == {'sluga': 2, 'slugb': 0}


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list'])
def test_card_cell_table_mode_render_title(mock_send, context, display_mode):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
        title_type='auto',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result

    context.pop('title')
    cell.title_type = 'empty'
    cell.save()
    result = cell.render(context)
    assert '<h2>' not in result

    cell.title_type = 'manual'
    cell.save()
    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result

    context.pop('title')
    cell.custom_title = 'Foo bar !'
    cell.save()
    result = cell.render(context)
    assert '<h2>Foo bar !</h2>' in result

    context.pop('title')
    cell.title_type = 'manual'
    cell.custom_title = '{{ foobar }}'
    cell.save()
    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result  # empty value from template, default value

    page.extra_variables = {'foobar': 'abcdef'}
    page.save()
    del page._cached_extra_variables  # clear cache
    result = cell.render(context)
    assert '<h2>abcdef</h2>' in result

    context.pop('title')
    cell.custom_title = '{% if %}'
    cell.save()
    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result  # template error, default value

    context.pop('title')
    cell.title_type = 'empty'
    cell.save()
    result = cell.render(context)
    assert '<h2>' not in result


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list', 'card'])
def test_card_cell_render_filters(mock_send, settings, nocache, context, app, display_mode):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
        related_card_path='',
        card_ids='11',
    )
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert not cell_resp.forms

    cell.filters = ['related']
    cell.save()

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(cell_resp.form.fields) == 1
    assert len(mock_send.call_args_list) == 5
    assert (
        '/api/cards/card_model_1/filter-options?NameID=&email=&orig=combo&filter_field_id=related&filter-identifier=11&'
        in mock_send.call_args_list[3][0][0].url
    )

    assert cell_resp.form['c%s-related' % cell.get_reference()].options == [
        ('13', False, 'Abc'),
        ('42', False, 'Foo Bar'),
    ]

    cell.filters = ['status']
    cell.save()

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(cell_resp.form.fields) == 1
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', False, 'Recorded'),
        ('deleted', False, 'Deleted'),
    ]

    cell.filters = ['related', 'fieldj', 'status', 'item', 'fieldl', 'fieldb']
    cell.save()

    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(cell_resp.form.fields) == 6
    assert len(mock_send.call_args_list) == 4
    assert (
        '/api/cards/card_model_1/filter-options?NameID=&email=&orig=combo&filter_field_id=related&filter-identifier=11&'
        in mock_send.call_args_list[0][0][0].url
    )
    assert (
        '/api/cards/card_model_1/filter-options?NameID=&email=&orig=combo&filter_field_id=fieldj&filter-identifier=11&'
        in mock_send.call_args_list[1][0][0].url
    )
    assert (
        '/api/cards/card_model_1/filter-options?NameID=&email=&orig=combo&filter_field_id=item&filter-identifier=11&'
        in mock_send.call_args_list[2][0][0].url
    )
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[3][0][0].url
    assert '&filter-identifier=11&limit=10&offset=0&algo' in mock_send.call_args_list[3][0][0].url

    assert cell_resp.form['c%s-fieldj' % cell.get_reference()].options == [
        ('first value', False, 'First Value'),
        ('second value \'', False, 'Second Value \''),
        ('third value', False, 'Third Value'),
    ]
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', False, 'Recorded'),
        ('deleted', False, 'Deleted'),
    ]
    assert cell_resp.form['c%s-item' % cell.get_reference()].options == [
        ('bar', False, 'bar'),
        ('foo', False, 'foo'),
    ]
    assert cell_resp.form['c%s-fieldl' % cell.get_reference()].options == [
        ('A', False, 'A'),
        ('C', False, 'C'),
        ('B', False, 'B'),
    ]
    assert cell_resp.form['c%s-fieldb' % cell.get_reference()].options == [
        ('false', False, 'no'),
        ('true', False, 'yes'),
    ]

    # check filters & pagination calls

    # one value in field filter
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(
        cell_url + '?ctx=' + extra_ctx[0] + '&c%s-fieldj[]=first value' % cell.get_reference()
    )
    assert len(mock_send.call_args_list) == 4
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[3][0][0].url
    assert (
        '&filter-identifier=11&limit=10&offset=0&filter-fieldj=first%20value&filter-fieldj-operator=in&algo'
        in mock_send.call_args_list[3][0][0].url
    )
    assert cell_resp.form['c%s-fieldj' % cell.get_reference()].options == [
        ('first value', True, 'First Value'),
        ('second value \'', False, 'Second Value \''),
        ('third value', False, 'Third Value'),
    ]
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', False, 'Recorded'),
        ('deleted', False, 'Deleted'),
    ]
    assert cell_resp.form['c%s-item' % cell.get_reference()].options == [
        ('bar', False, 'bar'),
        ('foo', False, 'foo'),
    ]
    assert cell_resp.form['c%s-fieldl' % cell.get_reference()].options == [
        ('A', False, 'A'),
        ('C', False, 'C'),
        ('B', False, 'B'),
    ]
    assert cell_resp.form['c%s-fieldb' % cell.get_reference()].options == [
        ('false', False, 'no'),
        ('true', False, 'yes'),
    ]

    # one value in status filter
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0] + '&c%s-status[]=recorded' % cell.get_reference())
    assert len(mock_send.call_args_list) == 4
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[3][0][0].url
    assert (
        '&filter-identifier=11&limit=10&offset=0&filter=recorded&filter-operator=in&algo'
        in mock_send.call_args_list[3][0][0].url
    )
    assert cell_resp.form['c%s-fieldj' % cell.get_reference()].options == [
        ('first value', False, 'First Value'),
        ('second value \'', False, 'Second Value \''),
        ('third value', False, 'Third Value'),
    ]
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', True, 'Recorded'),
        ('deleted', False, 'Deleted'),
    ]
    assert cell_resp.form['c%s-item' % cell.get_reference()].options == [
        ('bar', False, 'bar'),
        ('foo', False, 'foo'),
    ]
    assert cell_resp.form['c%s-fieldl' % cell.get_reference()].options == [
        ('A', False, 'A'),
        ('C', False, 'C'),
        ('B', False, 'B'),
    ]
    assert cell_resp.form['c%s-fieldb' % cell.get_reference()].options == [
        ('false', False, 'no'),
        ('true', False, 'yes'),
    ]

    # combine field and status filter, multi values, pagination
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(
        cell_url
        + '?ctx='
        + extra_ctx[0]
        + f'&c{cell.get_reference()}-status[]=recorded&c{cell.get_reference()}-status[]=deleted'
        + f'&c{cell.get_reference()}-item[]=bar&c{cell.get_reference()}-item[]=foo'
        + f'&c{cell.get_reference()}-fieldb[]=false'
        + '&offset=10'
    )
    assert len(mock_send.call_args_list) == 4
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[3][0][0].url
    assert (
        '&filter-identifier=11&limit=10&offset=10&filter=recorded%7Cdeleted&filter-operator=in'
        '&filter-item=bar%7Cfoo&filter-item-operator=in&filter-fieldb=false&algo'
        in mock_send.call_args_list[3][0][0].url
    )
    assert cell_resp.form['c%s-fieldj' % cell.get_reference()].options == [
        ('first value', False, 'First Value'),
        ('second value \'', False, 'Second Value \''),
        ('third value', False, 'Third Value'),
    ]
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', True, 'Recorded'),
        ('deleted', True, 'Deleted'),
    ]
    assert cell_resp.form['c%s-item' % cell.get_reference()].options == [
        ('bar', True, 'bar'),
        ('foo', True, 'foo'),
    ]
    assert cell_resp.form['c%s-fieldl' % cell.get_reference()].options == [
        ('A', False, 'A'),
        ('C', False, 'C'),
        ('B', False, 'B'),
    ]
    assert cell_resp.form['c%s-fieldb' % cell.get_reference()].options == [
        ('false', True, 'no'),
        ('true', False, 'yes'),
    ]

    # filter with True and False value
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(
        cell_url
        + '?ctx='
        + extra_ctx[0]
        + f'&c{cell.get_reference()}-status[]=recorded&c{cell.get_reference()}-status[]=deleted'
        + f'&c{cell.get_reference()}-item[]=bar&c{cell.get_reference()}-item[]=foo'
        + f'&c{cell.get_reference()}-fieldb[]=false'
        + f'&c{cell.get_reference()}-fieldb[]=true'
        + '&offset=10'
    )
    assert len(mock_send.call_args_list) == 4
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[3][0][0].url
    assert (
        '&filter-identifier=11&limit=10&offset=10&filter=recorded%7Cdeleted&filter-operator=in'
        '&filter-item=bar%7Cfoo&filter-item-operator=in&filter-fieldb=on&filter-fieldb-operator=existing&algo'
        in mock_send.call_args_list[3][0][0].url
    )
    assert cell_resp.form['c%s-fieldj' % cell.get_reference()].options == [
        ('first value', False, 'First Value'),
        ('second value \'', False, 'Second Value \''),
        ('third value', False, 'Third Value'),
    ]
    assert cell_resp.form['c%s-status' % cell.get_reference()].options == [
        ('recorded', True, 'Recorded'),
        ('deleted', True, 'Deleted'),
    ]
    assert cell_resp.form['c%s-item' % cell.get_reference()].options == [
        ('bar', True, 'bar'),
        ('foo', True, 'foo'),
    ]
    assert cell_resp.form['c%s-fieldl' % cell.get_reference()].options == [
        ('A', False, 'A'),
        ('C', False, 'C'),
        ('B', False, 'B'),
    ]
    assert cell_resp.form['c%s-fieldb' % cell.get_reference()].options == [
        ('false', True, 'no'),
        ('true', True, 'yes'),
    ]


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_filters_in_querystring(mock_send, settings, nocache, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='list',
        carddef_reference='default:card_model_1',
        related_card_path='',
        card_ids='11',
        filters=['status'],
    )
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    # check that we can pass filters through querystring with both cell reference and slug (if any)
    for slug, qs_field in (
        ('', f'c{cell.get_reference()}-status'),
        ('card_slug', f'c{cell.get_reference()}-status'),
        ('card_slug', '{cell.slug}-status'),
    ):
        cell.slug = slug
        cell.save()
        qs_field = qs_field.format(cell=cell)
        mock_send.reset_mock()
        resp = app.get(page.get_online_url())
        extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
        cell_resp = app.get(f'{cell_url}?ctx={extra_ctx[0]}&{qs_field}=recorded')
        assert len(mock_send.call_args_list) == 1
        assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
        assert (
            '&filter-identifier=11&limit=10&offset=0&filter=recorded&filter-operator=in'
            in mock_send.call_args_list[0][0][0].url
        )
        assert cell_resp.form['c%s-status' % cell.get_reference()].value == ['recorded']


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('search', [True, False])
@pytest.mark.parametrize(
    'card_ids,awaited_pages', ((['13'], ['cc\naa', 'bb']), (['13', '12'], ['cc\nbb', 'aa']))
)
def test_card_cell_pinned_cards(mock_send, settings, nocache, context, app, search, card_ids, awaited_pages):
    awaited_pages = list(awaited_pages)
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        limit=2,
        q=search,
        display_mode='list',
        carddef_reference='default:card_model_1',
        related_card_path='__all__',
        pinned_card_ids=', '.join(card_ids),
        filters=['status'],
    )
    pinned_card_ids = '%2C'.join(card_ids)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)

    mock_send.reset_mock()
    cell_resp = app.get(f'{cell_url}?ctx={extra_ctx[0]}')
    assert len(mock_send.call_args_list) == 2
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    # first we get the pinned cards
    assert f'&filter-identifier={pinned_card_ids}&' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier-operator' not in mock_send.call_args_list[0][0][0].url
    # then all cards, excluding pinned one (notice that limit is adjusted considering pinned cards)
    awaited_limit = cell.limit - len(card_ids)
    assert (
        f'&filter-identifier={pinned_card_ids}&filter-identifier-operator=ne&limit={awaited_limit}&offset=0'
        in mock_send.call_args_list[1][0][0].url
    )
    # ensure first page is ordered correctly
    assert cell_resp.pyquery('ul').text() == awaited_pages.pop(0)

    # check second page
    mock_send.reset_mock()
    cell_resp = app.get(f'{cell_url}?ctx={extra_ctx[0]}&offset=2')
    assert len(mock_send.call_args_list) == 2
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    # first we get the pinned cards
    assert f'&filter-identifier={pinned_card_ids}&' in mock_send.call_args_list[0][0][0].url
    # then all cards, excluding pinned one (limit and offset are adjusted)
    awaited_limit = (cell.limit * 2) - len(card_ids)
    awaited_offset = cell.limit - len(card_ids)
    assert (
        f'&filter-identifier={pinned_card_ids}&filter-identifier-operator=ne&'
        f'limit={awaited_limit}&offset={awaited_offset}'
    ) in mock_send.call_args_list[1][0][0].url
    # ensure second page is ordered correctly
    assert cell_resp.pyquery('ul').text() == awaited_pages.pop(0)

    # when we have a filter set then pinned cards are ignored
    mock_send.reset_mock()
    cell_resp = app.get(f'{cell_url}?ctx={extra_ctx[0]}&c{cell.get_reference()}-status=recorded')
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier' not in mock_send.call_args_list[0][0][0].url
    assert cell_resp.pyquery('ul').text() == 'aa\nbb'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list', 'card'])
def test_card_cell_render_filters_inline(mock_send, context, app, display_mode):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
        related_card_path='',
        filters=['related'],
        card_ids='11',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'cell-cards--filters-list--row' not in cell_resp.text

    cell.inline_filters = True
    cell.save()

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'cell-cards--filters-list--row' in cell_resp.text


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
@pytest.mark.parametrize('display_mode', ['table', 'list', 'card'])
def test_card_cell_render_search(mock_send, settings, nocache, context, app, display_mode):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        slug='card_slug',
        placeholder='content',
        related_card_path='__all__',
        q=True,
        order=0,
        display_mode=display_mode,
        carddef_reference='default:card_model_1',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # simple search, without filters
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'cell-cards--filters-list' not in cell_resp.text

    mock_send.reset_mock()

    form = cell_resp.forms[0]
    form['c%s-q' % cell.get_reference()] = 'foo'
    form.submit()
    assert len(mock_send.call_args_list) == 1
    assert '&q=foo' in mock_send.call_args_list[0][0][0].url

    # test with filters
    cell.filters = ['related']
    cell.save()

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'cell-cards--filters-list' in cell_resp.text

    mock_send.reset_mock()

    form = cell_resp.forms[0]
    form['c%s-q' % cell.get_reference()] = 'foo'
    form['c%s-related' % cell.get_reference()] = ['42']
    form.submit()
    assert len(mock_send.call_args_list) == 2
    assert '&q=foo&filter-related=42' in mock_send.call_args_list[1][0][0].url

    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    # ensure we can initiate the search field via querystring
    cell_resp = app.get(cell_url + '?card_slug-q=bar&ctx=' + extra_ctx[0])
    form = cell_resp.forms[0]
    assert form['c%s-q' % cell.get_reference()].value == 'bar'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='list',
        carddef_reference='default:card_model_1',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result
    assert 'cards-card_model_1' in result
    assert (
        '<a href="http://127.0.0.1:8999/backoffice/data/card_model_1/11/"><span class="card-title">aa</span></a>'
        in result
    )
    assert (
        '<a href="http://127.0.0.1:8999/backoffice/data/card_model_1/12/"><span class="card-title">bb</span></a>'
        in result
    )
    assert (
        '<a href="http://127.0.0.1:8999/backoffice/data/card_model_1/13/"><span class="card-title">cc</span></a>'
        in result
    )
    assert 'data-paginate-by="10"' in result

    # create a page with the correct subslug
    page = Page.objects.create(slug='foo', title='Foo', sub_slug='(?P<card_model_1_id>[a-zA-Z0-9_-]+)')

    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result
    assert '<a href="/foo/11/"><span class="card-title">aa</span></a>' in result
    assert '<a href="/foo/12/"><span class="card-title">bb</span></a>' in result
    assert '<a href="/foo/13/"><span class="card-title">cc</span></a>' in result

    cell.carddef_reference = 'default:card_model_1:foo'
    cell.limit = 42
    cell.save()
    page.sub_slug = 'card_model_1_id'
    page.save()

    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' in result
    assert '<a href="/foo/11/"><span class="card-title">aa</span></a>' in result
    assert '<a href="/foo/12/"><span class="card-title">bb</span></a>' in result
    assert '<a href="/foo/13/"><span class="card-title">cc</span></a>' not in result
    assert 'data-paginate-by="42"' in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps({'data': [], 'count': 0}))
        cell.render(context)
    assert len(requests_get.call_args_list) == 1
    assert (
        requests_get.call_args_list[0][0][0] == '/api/cards/card_model_1/list/foo'
        '?include-fields=on&include-submission=on&include-workflow=on&include-actions=on&limit=42&offset=0'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'

    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps({'data': [], 'count': 0}))
        cell.render(context)
    assert len(requests_get.call_args_list) == 1
    assert (
        requests_get.call_args_list[0][0][0] == '/api/cards/card_model_1/list'
        '?include-fields=on&include-submission=on&include-workflow=on&include-actions=on&limit=42&offset=0'
    )
    assert requests_get.call_args_list[0][1]['remote_service']['url'] == 'http://127.0.0.1:8999/'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render_custom_schema_card_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={'cells': [{'varname': 'fielda'}]},
        display_mode='list',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert len(PyQuery(result).find('ul li')) == 3
    assert [PyQuery(li).text() for li in PyQuery(result).find('ul li')[:1]] == ['<i>a</i>']

    # more than one cell, only first cell is displayed
    cell.custom_schema['cells'] += [
        {'varname': 'fieldb'},
    ]
    cell.save()
    result = cell.render(context)
    assert len(PyQuery(result).find('ul li')) == 3
    assert [PyQuery(li).text() for li in PyQuery(result).find('ul li')[:1]] == ['<i>a</i>']

    cells = [
        {'varname': 'fieldb'},
        {'varname': 'fieldc'},
        {'varname': 'related'},
        {'varname': 'fieldd'},
        {'varname': 'fieldd', 'file_display_mode': 'thumbnail'},
        {'varname': 'fieldd2'},
        {'varname': 'fieldd2', 'file_display_mode': 'thumbnail'},
        {'varname': 'fielde'},
        {'varname': 'fieldf'},
        {'varname': 'fieldg'},
        {'varname': 'fieldh'},
        {'varname': 'fieldi'},
        {'varname': 'fieldii'},
        {'varname': 'fieldj'},
        {'varname': 'fieldl'},
        {'varname': 'fieldm'},
        {'varname': 'unknown'},
        {'varname': 'user:name'},
        {'varname': 'user:email'},
        {'varname': 'user:first_name'},
        {'varname': 'user:last_name'},
        {'varname': 'user:unknown'},
        {'varname': 'info:id'},
        {'varname': 'info:receipt_time'},
        {'varname': 'info:last_update_time'},
        {'varname': 'info:status'},
        {'varname': 'info:text'},
        {},  # missing varname
    ]
    expecteds = [
        'yes',
        '2020-09-28',
        'Foo Bar',
        'file.pdf',
        'file.pdf',
        'file.pdf',
        '',  # it's an image !
        "lorem<strong>ipsum hello'world",  # no multiline support for now
        'lorem<strong>ipsum hello world',
        'test@localhost',
        'https://www.example.net/',
        "loremipsumhello'world",
        "loremipsumhello'world",
        "First Value, Second Value '",
        '',
        'value1, value2',
        '',
        'User Foo Bar',
        'foo@bar.com',
        'User',
        'Foo Bar',
        '',
        '11',
        '2021-11-29 14:33',
        '2023-07-24 16:04',
        'Recorded',
        'aa',
    ]
    for i, (c, expected) in enumerate(zip(cells, expecteds)):
        cell.custom_schema['cells'] = [c]
        cell.save()

        result = cell.render(context)
        assert len(PyQuery(result).find('ul li')) == 3
        assert [PyQuery(li).text() for li in PyQuery(result).find('ul li')[:1]] == [expected]

        if i == 3:
            assert PyQuery(result).find('ul li a').attr['href'].startswith('/api/wcs/file/')
        if i == 4:
            assert PyQuery(result).find('ul li a').attr['href'].startswith('/api/wcs/file/')
        if i == 5:
            assert PyQuery(result).find('ul li a').attr['href'].startswith('/api/wcs/file/')
        if i == 6:
            assert PyQuery(result).find('ul li a').attr['href'].startswith('/api/wcs/file/')
            assert PyQuery(result).find('ul li a img').attr['src'].startswith('/api/wcs/file/')
        if i == 9:
            assert PyQuery(result).find('ul li a').text().strip() == 'test@localhost'
            assert PyQuery(result).find('ul li a').attr['href'] == 'mailto:test@localhost'
        if i == 10:
            assert PyQuery(result).find('ul li a').text().strip() == 'https://www.example.net/'
            assert PyQuery(result).find('ul li a').attr['href'] == 'https://www.example.net/'
        if i == 18:
            assert PyQuery(result).find('ul li a').text().strip() == 'foo@bar.com'
            assert PyQuery(result).find('ul li a').attr['href'] == 'mailto:foo@bar.com'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render_custom_schema_card_empty_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={'cells': [{'varname': 'empty', 'empty_value': ''}]},
        display_mode='list',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == ''

    cell.custom_schema['cells'][0] = {
        'varname': 'empty',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == 'Custom text'

    cell.custom_schema['cells'][0] = {
        'varname': 'empty_email',
        'empty_value': '',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == ''
    assert PyQuery(result).find('ul li:first-child a') == []

    cell.custom_schema['cells'][0] = {
        'varname': 'empty_email',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == 'Custom text'
    assert PyQuery(result).find('ul li:first-child a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render_custom_schema_custom_entry(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': "<b>Foo</b> bar'baz {{ card.fields.fielde }}"},
            ]
        },
        display_mode='list',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert (
        PyQuery(result).find('ul li:first-child').text()
        == "<b>Foo</b> bar'baz lorem<strong>ipsum hello'world"
    )

    # test context
    cell.custom_schema['cells'][0][
        'template'
    ] = '{{ card.fields.fielda }} - {{ card.fields.related }} ({{ card.fields.related_structured.id }})'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == '<i>a</i> - Foo Bar (42)'

    # test filters in template
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.related|split:" "|join:"," }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li:first-child').text() == 'Foo,Bar'

    # test available context
    cell.custom_schema['cells'][0][
        'template'
    ] = 'Foo bar baz {% make_public_url url="http://127.0.0.1:8999/" %}'
    cell.save()
    result = cell.render(context)
    assert '/api/wcs/file/' in PyQuery(result).find('ul li:first-child').text()


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_list_mode_render_custom_schema_link_entry(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@link@',
                    'url_template': '/foo/bar/{{ card.fields.related_structured.id }}/',
                    'template': '{{ card.fields.fielda }} - {{ card.fields.related }}',
                    'display_mode': 'link',
                },
            ]
        },
        display_mode='list',
        related_card_path='__all__',
    )

    context['synchronous'] = True  # to get fresh content

    def test(value, href, class_name, is_file=False):
        result = cell.render(context)
        assert PyQuery(result).find('ul li:first-child a').text() == value
        if not is_file:
            assert PyQuery(result).find('ul li:first-child a').attr['href'] == href
        else:
            assert PyQuery(result).find('ul li:first-child a').attr['href'].startswith(href)
        assert PyQuery(result).find('ul li:first-child a').attr['class'] == class_name

    test('<i>a</i> - Foo Bar', '/foo/bar/42/', None)

    cell.custom_schema['cells'][0]['display_mode'] = 'button'
    cell.save()
    test('<i>a</i> - Foo Bar', '/foo/bar/42/', 'pk-button')

    # empty label or empty url: no link in output
    cell.custom_schema['cells'][0]['url_template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []
    cell.custom_schema['cells'][0]['url_template'] = 'foo/bar'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []

    # check with page link
    root_page = Page.objects.create(title='Root', slug='root', template_name='standard')
    page1 = Page.objects.create(
        title='Card',
        slug='card',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=root_page,
    )
    other_root_page = Page.objects.create(title='Other root', slug='other-root', template_name='standard')
    page2 = Page.objects.create(
        title='Card (bis)',
        slug='card-bis',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=other_root_page,
    )

    cell.custom_schema['cells'][0]['url_template'] = ''
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.fielda }} - {{ card.fields.related }}'
    cell.save()
    test('<i>a</i> - Foo Bar', '/root/card/11/', 'pk-button')

    cell.custom_schema['cells'][0]['page'] = page2.pk
    cell.save()
    test('<i>a</i> - Foo Bar', '/other-root/card-bis/11/', 'pk-button')

    # empty label or empty url: no link in output
    cell.custom_schema['cells'][0]['page'] = 0
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []

    # check with field
    cell.custom_schema['cells'][0]['page'] = ''
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.fielda }} - {{ card.fields.related }}'
    cell.save()
    test('<i>a</i> - Foo Bar', '/api/wcs/file/', 'pk-button', is_file=True)

    cell.custom_schema['cells'][0]['display_mode'] = 'link'
    cell.save()
    test('<i>a</i> - Foo Bar', '/api/wcs/file/', None, is_file=True)

    # empty label or no value/no file field/unknown field: no link in output
    result = cell.render(context)
    assert PyQuery(result).find('ul li:last-child a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'fielda'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'unknown'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('ul li a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='card',
        title_type='manual',
        custom_title='Foo bar {{ card.fields.title }}',
        related_card_path='',
    )

    # carddef_reference is not defined
    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert '<h2>Card Model 1</h2>' not in result
    assert '<p>Unknown Card</p>' in result

    # card id not in context
    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    del context['card_model_1_id']
    del cell._card_ids
    assert 'card_model_1_id' not in context
    result = cell.render(context)
    assert '>Card Model 1</h2>' in result  # default value
    assert '<p>Unknown Card</p>' in result

    context['card_model_1_id'] = 11
    del cell._card_ids

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 500
        requests_get.return_value = mock_resp
        result = cell.render(context)
    assert '>Card Model 1</h2>' in result  # default value
    assert '<p>Unknown Card</p>' in result
    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        requests_get.side_effect = ConnectionError()
        result = cell.render(context)
    assert '>Card Model 1</h2>' in result  # default value
    assert '<p>Unknown Card</p>' in result

    context.pop('title')
    context.pop('card_not_found_with_message')
    cell.title_type = 'auto'
    cell.save()
    mock_send.reset_mock()
    result = cell.render(context)
    assert '>Card Model 1 - aa</h2>' in result
    assert '<div class="label">Page</div>' not in result
    assert PyQuery(result).find('.label:contains("Field A") + .value').text() == '<i>a</i>'
    assert PyQuery(result).find('.label:contains("Field B") + .value').text() == 'yes'
    assert PyQuery(result).find('.label:contains("Field C") + .value').text() == '2020-09-28'
    assert PyQuery(result).find('.label:contains("Related") + .value').text() == 'Foo Bar'
    assert 'related_raw' not in result
    assert 'related_structured' not in result
    assert (
        PyQuery(result).find('.label:contains("Field D") + .value a').text() == 'file.pdf file.pdf'
    )  # Field D2 is matching ...
    assert PyQuery(result).find('.label:contains("Field D2") + .value a').text() == 'file.pdf'

    # check block digests are rendered with list items
    assert [x.text for x in PyQuery(result).find('.pk-card-field-block-items li')] == ['value1', 'value2']

    context.pop('title')
    cell.title_type = 'manual'
    cell.custom_title = '<b>Foo bar {{ card.fields.fielda }}</b>'
    cell.save()
    assert cell.get_additional_label() == '&lt;b&gt;Foo bar {{ card.fields.fielda }}&lt;/b&gt;'
    result = cell.render(context)
    assert '>&lt;b&gt;Foo bar &lt;i&gt;a&lt;/i&gt;&lt;/b&gt;</h2>' in result

    context.pop('title')
    cell.custom_title = '{{ foobar }}'
    cell.save()
    result = cell.render(context)
    assert '>Card Model 1 - aa</h2>' in result  # empty value from template, default value

    page.extra_variables = {'foobar': 'abcdef'}
    page.save()
    del page._cached_extra_variables  # clear cache
    result = cell.render(context)
    assert '>abcdef</h2>' in result

    context.pop('title')
    cell.custom_title = '{% if %}'
    cell.save()
    result = cell.render(context)
    assert '>Card Model 1 - aa</h2>' in result  # template error, default value

    context.pop('title')
    cell.title_type = 'empty'
    cell.save()
    result = cell.render(context)
    assert '<h2>' not in result

    # test available context
    cell.title_type = 'manual'
    cell.custom_title = 'X{{ site_base }}Y'
    cell.card_ids = '11'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert '>Xhttp://testserverY</h2>' in cell_resp

    cell.card_ids = '{{ cards|objects:"card_model_1"|getlist:"id"|join:"," }}'
    cell.title_type = 'manual'
    cell.custom_title = 'Foo bar X{{ repeat_index }}Y'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1

    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    for i in range(3):
        assert '>Foo bar X%sY</h2>' % i in cell_resp

    cell.limit = 42
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    for i in range(3):
        assert '>Foo bar X%sY</h2>' % i in cell_resp
    assert 'data-paginate-by="42"' in cell_resp
    cell.render(context)

    # using custom view
    cell.carddef_reference = 'default:card_model_1:foo'
    cell.save()
    result = cell.render(context)
    assert 'Foo bar X0Y' in result

    with mock.patch('combo.apps.wcs.models.requests.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 404
        requests_get.return_value = mock_resp
        result = cell.render(context)

    # nothing, hide cell
    assert not result.strip()

    # link page to model
    page.slug = 'plop'
    page.sub_slug = '(?P<card_model_1_id>[a-zA-Z0-9_-]+)'
    page.save()
    cell.related_card_path = ''
    cell.carddef_reference = 'default:card_model_1'
    cell.card_ids = ''
    cell.save()

    resp = app.get(page.get_online_url() + '11/')
    cell_resp = app.get(
        resp.pyquery('.wcs-card-cell').attr['data-ajax-cell-url']
        + '?ctx='
        + resp.pyquery('.wcs-card-cell').attr['data-extra-context']
    )
    assert cell_resp.headers['X-Page-Title'] == '"a a a"'

    cell.carddef_reference = 'default:card_model_1:foo'
    cell.save()
    resp = app.get(page.get_online_url() + '11/')
    cell_resp = app.get(
        resp.pyquery('.wcs-card-cell').attr['data-ajax-cell-url']
        + '?ctx='
        + resp.pyquery('.wcs-card-cell').attr['data-extra-context']
    )
    assert cell_resp.headers['X-Page-Title'] == '"afoo afoo afoo"'

    # sync rendering
    resp = app.get(page.get_online_url() + '11/', headers={'User-Agent': 'testbot'})
    assert resp.pyquery('title').text() == 'afoo afoo afoo | Combo - xxx'

    # custom-view:bar contains \n, header values can't contain newlines
    cell.carddef_reference = 'default:card_model_1:bar'
    cell.save()
    resp = app.get(page.get_online_url() + '11/')
    cell_resp = app.get(
        resp.pyquery('.wcs-card-cell').attr['data-ajax-cell-url']
        + '?ctx='
        + resp.pyquery('.wcs-card-cell').attr['data-extra-context']
    )
    assert cell_resp.headers['X-Page-Title'] == '"a a a"'  # \n are replaced by spaces

    # custom-view:utf8 contains utf8 chars
    cell.carddef_reference = 'default:card_model_1:utf8'
    cell.save()
    resp = app.get(page.get_online_url() + '11/')
    cell_resp = app.get(
        resp.pyquery('.wcs-card-cell').attr['data-ajax-cell-url']
        + '?ctx='
        + resp.pyquery('.wcs-card-cell').attr['data-extra-context']
    )
    # we have to test the encoded value
    title = cell_resp.headers['X-Page-Title']
    assert title.encode('ascii') == br'"L\u2019\u20ac ne fait pas le bonheur"'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_text_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='card',
        carddef_reference='default:card_model_1',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)

    # field E is split in paragraphs
    assert (
        PyQuery(result).find('.label:contains("Field E") + .value p:first-child').text().strip()
        == 'lorem<strong>ipsum'
    )
    assert (
        PyQuery(result).find('.label:contains("Field E") + .value p:last-child').text().strip()
        == "hello'world"
    )

    # field F is put in a <p class="plain-text-pre">
    assert (
        PyQuery(result).find('.label:contains("Field F") + .value p.plain-text-pre').text()
        == 'lorem<strong>ipsum hello world'
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_email_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='card',
        carddef_reference='default:card_model_1',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)

    assert PyQuery(result).find('.label:contains("Field G") + .value a').text() == 'test@localhost'

    assert (
        PyQuery(result).find('.label:contains("Field G") + .value a').attr['href'] == 'mailto:test@localhost'
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_string_with_url_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='card',
        carddef_reference='default:card_model_1',
        custom_title='Foo bar {{ card.fields.title }}',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)

    assert PyQuery(result).find('.label:contains("Field H") + .value a').text() == 'https://www.example.net/'

    assert (
        PyQuery(result).find('.label:contains("Field H") + .value a').attr['href']
        == 'https://www.example.net/'
    )


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_custom_schema_card_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='',
        custom_schema={'cells': [{'varname': 'fielda', 'field_content': 'value', 'display_mode': 'title'}]},
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert PyQuery(result).find('h3').text() == '<i>a</i>'
    assert PyQuery(result).find('h2')[0].attrib['id'] == 'wcs-card-card_model_1-11'

    cell.custom_schema['cells'][0] = {
        'varname': 'fielda',
        'field_content': 'value',
        'display_mode': 'subtitle',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h4').text() == '<i>a</i>'

    cell.custom_schema['cells'][0] = {'varname': 'fielda', 'field_content': 'label', 'display_mode': 'title'}
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h3').text() == 'Field A'

    cell.custom_schema['cells'][0] = {
        'varname': 'fielda',
        'field_content': 'label',
        'display_mode': 'subtitle',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h4').text() == 'Field A'

    cell.custom_schema['cells'][0] = {'varname': 'fielda', 'field_content': 'label', 'display_mode': 'text'}
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field A'

    cell.custom_schema['cells'][0] = {'varname': 'fielda', 'field_content': 'value', 'display_mode': 'text'}
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value').text() == '<i>a</i>'

    cell.custom_schema['cells'][0] = {
        'varname': 'fielda',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field A'
    assert PyQuery(result).find('.value').text() == '<i>a</i>'

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldb',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field B'
    assert PyQuery(result).find('.value').text() == 'yes'

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldc',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field C'
    assert PyQuery(result).find('.value').text() == '2020-09-28'

    cell.custom_schema['cells'][0] = {
        'varname': 'related',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Related'
    assert PyQuery(result).find('.value').text() == 'Foo Bar'

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldd',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D'
    assert PyQuery(result).find('.value').text() == 'file.pdf'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0]['file_display_mode'] = 'link'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D'
    assert PyQuery(result).find('.value').text() == 'file.pdf'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0]['file_display_mode'] = 'thumbnail'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D'
    assert PyQuery(result).find('.value').text() == 'file.pdf'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldd2',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D2'
    assert PyQuery(result).find('.value').text() == 'file.pdf'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0]['file_display_mode'] = 'link'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D2'
    assert PyQuery(result).find('.value').text() == 'file.pdf'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0]['file_display_mode'] = 'thumbnail'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field D2'
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')
    assert PyQuery(result).find('.value a img').attr['src'].startswith('/api/wcs/file/')

    cell.custom_schema['cells'][0] = {
        'varname': 'fielde',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    # check multiline text field is rendered with multiple paragraphs
    # (first line "lorem<strong>ipsum" and last line ("hello'world")
    # and the content is kept properly escaped.
    assert PyQuery(result).find('.label').text() == 'Field E'
    assert PyQuery(result).find('.value p:first-child').text().strip() == 'lorem<strong>ipsum'
    assert PyQuery(result).find('.value p:last-child').text().strip() == "hello'world"

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldf',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field F'
    assert PyQuery(result).find('.value p.plain-text-pre').text() == 'lorem<strong>ipsum hello world'

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldi',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field I'
    assert PyQuery(result).find('.value p:first-child').text() == 'loremipsum'
    assert PyQuery(result).find('.value p:first-child strong').text() == 'ipsum'
    assert PyQuery(result).find('.value p:last-child').text() == "hello'world"

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldi',
        'field_content': 'value',
        'display_mode': 'title',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h3').text() == "loremipsumhello'world"
    assert PyQuery(result).find('h3 p') == []  # content was stripped

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldii',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field II'
    assert PyQuery(result).find('.value p:first-child').text() == 'loremipsum'
    assert PyQuery(result).find('.value p:first-child strong').text() == 'ipsum'
    assert PyQuery(result).find('.value p:last-child').text() == "hello'world"

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldii',
        'field_content': 'value',
        'display_mode': 'title',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h3').text() == "loremipsumhello'world"
    assert PyQuery(result).find('h3 p') == []  # content was stripped

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldg',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field G'
    assert PyQuery(result).find('.value a').text() == 'test@localhost'
    assert PyQuery(result).find('.value a').attr['href'] == 'mailto:test@localhost'

    cell.custom_schema['cells'][0] = {
        'varname': 'fieldh',
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Field H'
    assert PyQuery(result).find('.value a').text() == 'https://www.example.net/'
    assert PyQuery(result).find('.value a').attr['href'] == 'https://www.example.net/'

    # wrong configuration, missing varname
    cell.custom_schema['cells'][0] = {
        'field_content': 'label-and-value',
        'display_mode': 'text',
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label') == []
    assert PyQuery(result).find('.value') == []

    # user fields
    cell.custom_schema['cells'] = [
        {
            'varname': 'user:name',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'user:email',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'user:first_name',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'user:last_name',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'user:unknown',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'info:id',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'info:receipt_time',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'info:last_update_time',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'info:status',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
        {
            'varname': 'info:text',
            'field_content': 'label-and-value',
            'display_mode': 'text',
        },
    ]
    cell.save()
    result = cell.render(context)
    assert len(PyQuery(result).find('.cell--body > div > div')) == 9
    assert PyQuery(result).find('.cell--body > div > div:nth-child(1) .label').text() == 'Name'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(1) .value').text() == 'User Foo Bar'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(2) .label').text() == 'Email'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(2) .value a').text() == 'foo@bar.com'
    assert (
        PyQuery(result).find('.cell--body > div > div:nth-child(2) .value a').attr['href']
        == 'mailto:foo@bar.com'
    )
    assert PyQuery(result).find('.cell--body > div > div:nth-child(3) .label').text() == 'First name'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(3) .value').text() == 'User'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(4) .label').text() == 'Last name'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(4) .value').text() == 'Foo Bar'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(5) .label').text() == 'Identifier'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(5) .value').text() == '11'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(6) .label').text() == 'Receipt date'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(6) .value').text() == '2021-11-29 14:33'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(7) .label').text() == 'Last modified'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(7) .value').text() == '2023-07-24 16:04'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(8) .label').text() == 'Status'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(8) .value').text() == 'Recorded'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(9) .label').text() == 'Text'
    assert PyQuery(result).find('.cell--body > div > div:nth-child(9) .value').text() == 'aa'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_custom_schema_card_empty_field(mock_send, context):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='',
        custom_schema={
            'cells': [
                {
                    'varname': 'empty',
                    'field_content': 'label-and-value',
                    'display_mode': 'text',
                    'empty_value': '@skip@',
                }
            ]
        },
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert len(PyQuery(result).find('.cell--body > div > div')) == 0
    assert PyQuery(result).find('.label') == []
    assert PyQuery(result).find('.value') == []

    cell.custom_schema['cells'][0] = {
        'varname': 'empty',
        'field_content': 'label-and-value',
        'display_mode': 'text',
        'empty_value': '@empty@',
    }
    cell.save()
    result = cell.render(context)
    assert len(PyQuery(result).find('.cell--body > div > div')) == 1
    assert PyQuery(result).find('.label').text() == 'Empty'
    assert PyQuery(result).find('.value').text() == ''

    cell.custom_schema['cells'][0] = {
        'varname': 'empty',
        'field_content': 'label-and-value',
        'display_mode': 'text',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert len(PyQuery(result).find('.cell--body > div > div')) == 1
    assert PyQuery(result).find('.label').text() == 'Empty'
    assert PyQuery(result).find('.value').text() == 'Custom text'

    for field_content in ['label', 'value']:
        for display_mode in ['text', 'title', 'subtitle']:
            if display_mode == 'title':
                html_tag = 'h3'
            elif display_mode == 'subtitle':
                html_tag = 'h4'
            elif display_mode == 'text' and field_content == 'label':
                html_tag = '.label'
            elif display_mode == 'text' and field_content == 'value':
                html_tag = '.value'
            cell.custom_schema['cells'][0] = {
                'varname': 'empty',
                'field_content': field_content,
                'display_mode': display_mode,
                'empty_value': '@skip@',
            }
            cell.save()
            result = cell.render(context)
            assert len(PyQuery(result).find('.cell--body > div > div')) == 0
            assert PyQuery(result).find(html_tag) == []

            cell.custom_schema['cells'][0] = {
                'varname': 'empty',
                'field_content': field_content,
                'display_mode': display_mode,
                'empty_value': '@empty@',
            }
            cell.save()
            result = cell.render(context)
            assert len(PyQuery(result).find('.cell--body > div > div')) == 1
            assert PyQuery(result).find(html_tag).text() == ('Empty' if field_content == 'label' else '')

            cell.custom_schema['cells'][0] = {
                'varname': 'empty',
                'field_content': field_content,
                'display_mode': display_mode,
                'empty_value': 'Custom text',
            }
            cell.save()
            result = cell.render(context)
            assert len(PyQuery(result).find('.cell--body > div > div')) == 1
            assert PyQuery(result).find(html_tag).text() == (
                'Empty' if field_content == 'label' else 'Custom text'
            )

    cell.custom_schema['cells'][0] = {
        'varname': 'empty_email',
        'field_content': 'label-and-value',
        'display_mode': 'text',
        'empty_value': 'Custom text',
    }
    cell.save()
    result = cell.render(context)
    assert len(PyQuery(result).find('.cell--body > div > div')) == 1
    assert PyQuery(result).find('.label').text() == 'Empty Email'
    assert PyQuery(result).find('.value').text() == 'Custom text'
    assert PyQuery(result).find('.value a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_custom_schema_custom_entry(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@custom@',
                    'template': "<b>Foo</b> bar'baz {{ card.fields.fielde }}",
                    'display_mode': 'title',
                },
            ]
        },
        related_card_path='',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert '&lt;b&gt;Foo&lt;/b&gt;' in result
    assert PyQuery(result).find('h3').text() == "<b>Foo</b> bar'baz lorem<strong>ipsum hello'world"

    # test context
    cell.custom_schema['cells'][0][
        'template'
    ] = '{{ card.fields.fielda }} - {{ card.fields.related }} ({{ card.fields.related_structured.id }})'
    cell.custom_schema['cells'][0]['display_mode'] = 'subtitle'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('h4').text() == '<i>a</i> - Foo Bar (42)'

    # test display_mode & filters in template
    cell.custom_schema = {
        'cells': [
            {'varname': '@custom@', 'template': 'Foo bar baz', 'display_mode': 'label'},
            {
                'varname': '@custom@',
                'template': '{{ card.fields.related|split:" "|join:"," }}',
                'display_mode': 'text',
            },
        ]
    }
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.label').text() == 'Foo bar baz'
    assert PyQuery(result).find('.value').text() == 'Foo,Bar'

    # test available context
    cell.card_ids = '11'
    cell.custom_schema = {
        'cells': [
            {
                'varname': '@custom@',
                'template': 'Foo bar baz {% make_public_url url="http://127.0.0.1:8999/" %}',
                'display_mode': 'label',
            },
        ]
    }
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert '/api/wcs/file/' in PyQuery(cell_resp.text).find('.label').text()
    assert PyQuery(cell_resp.text).find('h2')[0].attrib['id'] == 'wcs-card-card_model_1-11'

    cell.custom_schema = {
        'cells': [
            {'varname': '@custom@', 'template': 'Foo bar baz X{{ site_base }}Y', 'display_mode': 'label'},
        ]
    }
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert PyQuery(cell_resp.text).find('.label').text() == 'Foo bar baz Xhttp://testserverY'

    cell.card_ids = '{{ cards|objects:"card_model_1"|getlist:"id"|join:"," }}'
    cell.custom_schema = {
        'cells': [
            {'varname': '@custom@', 'template': 'Foo bar baz X{{ repeat_index }}Y', 'display_mode': 'label'},
        ]
    }
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert PyQuery(cell_resp.text).find('.label').text() == 'Foo bar baz X0Y Foo bar baz X1Y Foo bar baz X2Y'
    assert PyQuery(cell_resp.text).find('h2')[0].attrib['id'].startswith('wcs-card-card_model_1-')

    # custom schema but empty
    cell.custom_schema = {'cells': []}
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('div.cell--body') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_custom_schema_link_entry(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@link@',
                    'url_template': '/foo/bar/{{ card.fields.related_structured.id }}/',
                    'template': '{{ card.fields.fielda }} - {{ card.fields.related }}',
                    'display_mode': 'link',
                },
            ]
        },
        related_card_path='',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    assert PyQuery(result).find('.value a').text() == '<i>a</i> - Foo Bar'
    assert PyQuery(result).find('.value a').attr['href'] == '/foo/bar/42/'
    assert PyQuery(result).find('.value a').attr['class'] is None

    cell.custom_schema['cells'][0]['display_mode'] = 'button'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a').text() == '<i>a</i> - Foo Bar'
    assert PyQuery(result).find('.value a').attr['href'] == '/foo/bar/42/'
    assert PyQuery(result).find('.value a').attr['class'] == 'pk-button'

    cell.custom_schema['cells'][0][
        'url_template'
    ] = '{{ site_base }}/foo/bar/{{ card.fields.related_structured.id }}/'
    cell.custom_schema['cells'][0]['template'] = '<b>{{ card.fields.fielda }}</b> - {{ card.fields.related }}'
    cell.card_ids = '11'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert (
        '<div class="value"><a href="http://testserver/foo/bar/42/" class="pk-button">&lt;b&gt;&lt;i&gt;a&lt;/i&gt;&lt;/b&gt; - Foo Bar</a></div>'
        in cell_resp
    )

    # empty label or empty url: no link in output
    cell.custom_schema['cells'][0]['url_template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []
    cell.custom_schema['cells'][0]['url_template'] = 'foo/bar'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []

    # check with page link
    root_page = Page.objects.create(title='Root', slug='root', template_name='standard')
    page1 = Page.objects.create(
        title='Card',
        slug='card',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=root_page,
    )
    other_root_page = Page.objects.create(title='Other root', slug='other-root', template_name='standard')
    page2 = Page.objects.create(
        title='Card (bis)',
        slug='card-bis',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=other_root_page,
    )

    cell.custom_schema['cells'][0]['url_template'] = ''
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = 'Foo'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a').attr['href'] == '/root/card/11/'

    cell.custom_schema['cells'][0]['page'] = page2.pk
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a').attr['href'] == '/other-root/card-bis/11/'

    # empty label or empty url: no link in output
    cell.custom_schema['cells'][0]['page'] = 0
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []
    cell.custom_schema['cells'][0]['page'] = page1.pk
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []

    # check with field
    cell.custom_schema['cells'][0]['page'] = ''
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ card.fields.fielda }} - {{ card.fields.related }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')
    assert PyQuery(result).find('.value a').attr['class'] == 'pk-button'

    cell.custom_schema['cells'][0]['display_mode'] = 'link'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a').attr['href'].startswith('/api/wcs/file/')
    assert PyQuery(result).find('.value a').attr['class'] is None

    # empty label or no value/no file field/unknown field: no link in output
    cell.card_ids = ''
    cell.save()
    context['card_model_1_id'] = 12
    del cell._card_ids
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []
    context['card_model_1_id'] = 11
    del cell._card_ids
    cell.custom_schema['cells'][0]['link_field'] = 'fielda'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'unknown'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []
    cell.custom_schema['cells'][0]['link_field'] = 'fieldd'
    cell.custom_schema['cells'][0]['template'] = '{{ None|default:"" }}'
    cell.save()
    result = cell.render(context)
    assert PyQuery(result).find('.value a') == []


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_custom_schema_action_entry(mock_send, context, app):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@action@',
                    'trigger_id': 'jump:trigger-1',
                    'action_label': 'Label {{ card.fields.fielda }}',
                    'action_ask_confirmation': True,
                    'action_confirmation_template': 'Confirmation {{ card.fields.fielda }}',
                    'unavailable_action_mode': 'hide',
                },
            ]
        },
        related_card_path='',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    assert len(buttons) == 1
    assert buttons.attr['card-action-url'] == cell.get_ajax_url()
    assert buttons.attr['card-id'] == '11'
    assert buttons.attr['card-trigger-id'] == 'jump:trigger-1'

    cell.custom_schema['cells'][0]['trigger_id'] = 'unavailable-trigger'
    cell.save()
    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    assert len(buttons) == 1
    assert buttons.attr['card-action-url'] == cell.get_ajax_url()
    assert buttons.attr['card-id'] == '11'
    assert buttons.attr['card-trigger-id'] == 'unavailable-trigger'

    cell.custom_schema['cells'][0].pop('trigger_id')
    cell.save()
    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    assert len(buttons) == 1
    assert buttons.attr['unavailable'] == 'unavailable'
    assert 'card-action-url' not in buttons[0].attrib

    cell.custom_schema['cells'][0]['action_ask_confirmation'] = False
    cell.save()
    result = cell.render(context)
    buttons = PyQuery(result).find('wcs-trigger-button')
    assert len(buttons) == 1
    assert 'action_confirmation_template' not in buttons[0].attrib


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_all_cards(mock_send, nocache, app):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='__all__',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # check url called
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    # page rendering
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&limit=10&offset=0' in mock_send.call_args_list[0][0][0].url


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_identifier(mock_send, nocache, app):
    page = Page.objects.create(
        title='xxx', slug='foo', template_name='standard', sub_slug='(?P<card_model_1_id>[a-zA-Z0-9_-]+)'
    )
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='',
    )

    cell_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell.get_reference()},
    )

    # check url called
    mock_send.reset_mock()
    resp = app.get(page.get_online_url() + '11/')
    assert len(resp.context['cells']) == 1
    assert resp.context['cells'][0].pk == cell.pk
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert 'Card Model 1' in cell_resp
    assert '<p>Unknown Card</p>' not in cell_resp
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert 'filter-identifier=11&limit=10&offset=0' in mock_send.call_args_list[0][0][0].url

    # with identifiers
    page.sub_slug = ''
    page.save()
    cell.card_ids = '42'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert resp.context['cells'][0].pk == cell.pk
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert cell_resp.text.replace('\n', '').strip() == ''  # empty-cell
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert 'filter-identifier=42&limit=10&offset=0' in mock_send.call_args_list[0][0][0].url

    cell.card_ids = '42, , 35'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 1
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert '&filter-identifier=42%2C35&' in mock_send.call_args_list[0][0][0].url

    cell.card_ids = '{% cards|objects:"card_model_1"|last|get:"id" %}'  # syntax error
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert cell_resp.text.replace('\n', '').strip() == ''  # empty-cell

    cell.card_ids = '{{ cards|objects:"card_model_1"|last|get:"id" }}'
    cell.save()
    mock_send.reset_mock()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    assert resp.context['cells'][0].pk == cell.pk
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert len(mock_send.call_args_list) == 2
    # page rendering
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[0][0][0].url
    assert 'filter-identifier' not in mock_send.call_args_list[0][0][0].url
    # cell rendering
    assert '/api/cards/card_model_1/list' in mock_send.call_args_list[1][0][0].url
    assert '&filter-identifier=13&limit=10' in mock_send.call_args_list[1][0][0].url

    def check(urls):
        resp = app.get(page.get_online_url())
        assert len(resp.context['cells']) == 1
        extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
        mock_send.reset_mock()
        app.get(cell_url + '?ctx=' + extra_ctx[0])
        assert len(mock_send.call_args_list) == len(urls)
        for j, url_parts in enumerate(urls):
            if not isinstance(url_parts, tuple):
                url_parts = (url_parts,)
            for url_part in url_parts:
                if url_part.startswith('^'):
                    assert url_part[1:] not in mock_send.call_args_list[j][0][0].url
                else:
                    assert url_part in mock_send.call_args_list[j][0][0].url

    for card_ids in [
        '{% for card in cards|objects:"card_model_1" %}{{ card.id }},{% endfor %}',
        '{{ cards|objects:"card_model_1"|getlist:"id"|join:"," }}',
    ]:
        cell.card_ids = card_ids
        cell.save()
        check(
            [
                ('/api/cards/card_model_1/list', '^filter-identifier'),
                ('/api/cards/card_model_1/list', 'filter-identifier=11%2C12%2C13&limit=10&'),
            ]
        )

        cell.card_ids = '{{ var1 }}'
        cell.save()
        page.extra_variables = {'var1': card_ids}
        page.save()
        check(
            [
                ('/api/cards/card_model_1/list', '^filter-identifier'),
                ('/api/cards/card_model_1/list', 'filter-identifier=11%2C12%2C13&limit=10&'),
            ]
        )
        page.extra_variables = {}
        page.save()

    # with a card_ids template, but result is empty
    cell.card_ids = '{{ foo }}'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    cell_resp = app.get(cell_url + '?ctx=' + extra_ctx[0])
    assert cell_resp.text.replace('\n', '').strip() == ''  # empty-cell


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_card_mode_render_identifier_from_related(mock_send, nocache, app):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='card',
        slug='sluga',
        carddef_reference='default:card_a',
        related_card_path='',
        card_ids='1',
    )
    cell2 = WcsCardCell.objects.create(
        page=page, placeholder='content', order=1, slug='slugb', carddef_reference='default:card_b'
    )

    cell2_url = reverse(
        'combo-public-ajax-page-cell',
        kwargs={'page_pk': page.pk, 'cell_reference': cell2.get_reference()},
    )

    def failing(urls):
        resp = app.get(page.get_online_url())
        assert len(resp.context['cells']) >= 2
        for i in range(0, len(resp.context['cells']) - 1):
            assert resp.context['cells'][i].pk == cell.pk
        assert resp.context['cells'][-1].pk == cell2.pk
        extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
        mock_send.reset_mock()
        cell_resp = app.get(cell2_url + '?ctx=' + extra_ctx[-1])
        assert cell_resp.text.replace('\n', '').strip() == ''  # empty-cell
        assert len(mock_send.call_args_list) == len(urls)
        for j, url in enumerate(urls):
            assert url in mock_send.call_args_list[j][0][0].url

    def success(urls, reverse_cell_ordering=False):
        resp = app.get(page.get_online_url())
        assert len(resp.context['cells']) == 2
        if reverse_cell_ordering:
            assert resp.context['cells'][0].pk == cell2.pk
            assert resp.context['cells'][1].pk == cell.pk
        else:
            assert resp.context['cells'][0].pk == cell.pk
            assert resp.context['cells'][1].pk == cell2.pk
        extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
        mock_send.reset_mock()
        app.get(cell2_url + '?ctx=' + extra_ctx[1])
        assert len(mock_send.call_args_list) == len(urls)
        for j, url_parts in enumerate(urls):
            if not isinstance(url_parts, tuple):
                url_parts = (url_parts,)
            for url_part in url_parts:
                if url_part.startswith('^'):
                    assert url_part[1:] not in mock_send.call_args_list[j][0][0].url
                else:
                    assert url_part in mock_send.call_args_list[j][0][0].url

    # no cell with this slug
    cell2.related_card_path = 'slugz/cardb'
    cell2.save()
    failing(urls=[])

    # another cell with the same slug
    cell3 = WcsCardCell.objects.create(page=page, placeholder='content', order=2, slug='sluga')
    cell2.related_card_path = 'sluga/foo'
    cell2.save()
    failing(urls=[])
    cell3.delete()

    # multiple ids configured on first cell
    cell.card_ids = '{{ cards|objects:"card_a"|getlist:"id"|join:"," }}'
    cell.save()
    failing(urls=[])

    # related_card_path configured on first cell
    cell.card_ids = '1'  # reset
    cell.related_path = 'foobar'
    cell.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
        ]
    )

    # reset
    cell.related_path = ''
    cell.save()

    # another cell as the same slug, but not a WcsCardCell
    cell3 = TextCell.objects.create(page=page, placeholder='content', order=2, slug='sluga')

    # direct and single relation (item)
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # and follow cardb relation
            (
                '/api/cards/card_b/list',
                'filter-identifier=1',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_b/list', 'filter-identifier=1&limit=10&offset=0'),  # check user access
        ]
    )
    cell3.delete()  # reset

    # direct and single relation (item through a bo field)
    cell2.related_card_path = 'sluga/bocardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # and follow cardb relation
            (
                '/api/cards/card_b/list',
                'filter-identifier=1',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_b/list', 'filter-identifier=1&limit=10&offset=0'),  # check user access
        ]
    )

    cell2.related_card_path = 'sluga/cardc/cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
            # and follow cardb relation
            (
                '/api/cards/card_b/list',
                'filter-identifier=7',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_b/list', 'filter-identifier=7&limit=10&offset=0'),  # check user access
        ]
    )

    # change cell ordering - cell with slug is after cell with related
    cell.order = 42
    cell.save()
    # no error, but it does not work as expected: both cells has slug.
    app.get(page.get_online_url(), status=200)
    # remove slug of second cell
    cell2.slug = ''
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
            # and follow cardb relation
            (
                '/api/cards/card_b/list',
                'filter-identifier=7',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_b/list', 'filter-identifier=7&limit=10&offset=0'),  # check user access
        ],
        reverse_cell_ordering=True,
    )

    # reset
    cell.order = 0  # reset
    cell.save()
    cell2.slug = 'slugb'
    cell2.save()

    # test with custom_view
    cell2.carddef_reference = 'default:card_b:b-custom-view'
    cell2.save()
    success(
        urls=[
            # get first cell data
            (
                '/api/cards/card_a/1/',
                '&include-files-content=off&include-evolution=off&include-roles=off&include-workflow-data=off',
            ),
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
            # and follow cardb relation
            (
                '/api/cards/card_b/list/b-custom-view',
                'filter-identifier=7',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            (
                '/api/cards/card_b/list/b-custom-view',
                '&filter-identifier=7&limit=10&offset=0',
            ),  # check user access
        ]
    )

    # direct and multiple relation (items)
    cell2.carddef_reference = 'default:card_b'  # reset
    cell2.related_card_path = 'sluga/cardsb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # and follow cardb relation
            ('/api/cards/card_b/list', '&filter-identifier=2%2C3&'),  # get card ids
            ('/api/cards/card_b/list', '&filter-identifier=2%2C3&'),  # check user access
        ]
    )

    cell2.related_card_path = 'sluga/cardc/cardsb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
            # and follow cardb relation
            ('/api/cards/card_b/list', '&filter-identifier=8%2C9&'),  # get card ids
            ('/api/cards/card_b/list', '&filter-identifier=8%2C9&'),  # check user access
        ]
    )

    # direct and multiple relation through a block
    cell2.related_card_path = 'sluga/blockb_cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # and follow cardb relation
            ('/api/cards/card_b/list', '&filter-identifier=4%2C5&'),  # get card ids
            ('/api/cards/card_b/list', '&filter-identifier=4%2C5&'),  # check user access
        ]
    )

    cell2.related_card_path = 'sluga/cardc/blockb_cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
            # and follow cardb relation
            ('/api/cards/card_b/list', '&filter-identifier=10%2C11&'),  # get card ids
            ('/api/cards/card_b/list', '&filter-identifier=10%2C11&'),  # check user access
        ]
    )

    # unknown part in related_card_path
    cell2.related_card_path = 'sluga/foobar'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
        ]
    )
    cell2.related_card_path = 'sluga/cardc/foobar'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/6/',
        ]
    )

    # card data not found
    cell.card_ids = '42'
    cell.save()
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/42/',
        ]
    )

    cell.card_ids = '2'
    cell.save()
    cell2.related_card_path = 'sluga/cardc/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/2/',
            # get card_c schema
            '/api/cards/card_c/@schema',
            # follow cardc relation
            '/api/cards/card_c/61/',
        ]
    )
    # reset
    cell.card_ids = '1'
    cell.save()

    # last part has not the correct card slug
    cell2.related_card_path = 'sluga/cardc'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
        ]
    )

    # unknown schema
    cell2.related_card_path = 'sluga/cardz/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
            # get card_z schema
            '/api/cards/card_z/@schema',
        ]
    )

    # multiple relation of multiple relation
    cell2.related_card_path = 'sluga/cardsb/reverse:cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/1/',
        ]
    )

    # field not found
    cell.card_ids = '3'
    cell.save()
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/3/',
        ]
    )
    cell2.related_card_path = 'sluga/cardc/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/3/',
        ]
    )

    # field empty
    cell.card_ids = '4'
    cell.save()
    cell2.related_card_path = 'sluga/cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/4/',
        ]
    )

    # field not found in block
    cell.card_ids = '3'
    cell.save()
    cell2.related_card_path = 'sluga/blockb_cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/3/',
        ]
    )

    # field empty in block
    cell.card_ids = '4'
    cell.save()
    cell2.related_card_path = 'sluga/blockb_cardb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_a/4/',
        ]
    )

    # reverse relation of item
    cell.carddef_reference = 'default:card_b'
    cell.slug = 'slugb'
    cell.card_ids = '1'
    cell.related_card_path = ''
    cell.save()
    cell2.carddef_reference = 'default:card_a'
    cell2.slug = 'sluga'
    cell2.card_ids = ''
    cell2.related_card_path = 'slugb/reverse:cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_b/1/',
            # get list of card_a with cardb=1
            '/api/cards/card_a/list?orig=combo&filter-cardb=1',
            # and follow carda reverse relation
            # get card ids
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
            # check user access
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
        ]
    )

    # reverse relation of items
    cell2.related_card_path = 'slugb/reverse:cardsb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_b/1/',
            # get list of card_a with cardsb=1
            '/api/cards/card_a/list?orig=combo&filter-cardsb=1',
            # and follow carda reverse relation
            # get card ids
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
            # check user access
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
        ]
    )

    # reverse relation of item through a block
    cell2.related_card_path = 'slugb/reverse:blockb_cardb'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_b/1/',
            # get list of card_a with cardsb=1
            '/api/cards/card_a/list?orig=combo&filter-blockb_cardb=1',
            # and follow carda reverse relation
            # get card ids
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
            # check user access
            (
                '/api/cards/card_a/list',
                '&filter-identifier=1%2C2%2C3%2C4&',
            ),
        ]
    )

    # unknown part in related_card_path
    cell2.related_card_path = 'slugb/foobar'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_b/1/',
        ]
    )

    # multiple relation of multiple relation
    cell2.related_card_path = 'slugb/reverse:cardb/cardsb'
    cell2.save()
    failing(
        urls=[
            # get first cell data
            '/api/cards/card_b/1/',
        ]
    )

    # reverse relation with many models using the same varname
    cell.carddef_reference = 'default:card_h'
    cell.slug = 'slugh'
    cell.card_ids = '42'
    cell.related_card_path = ''
    cell.save()
    cell2.carddef_reference = 'default:card_f'
    cell2.slug = 'slugf'
    cell2.card_ids = ''
    cell2.related_card_path = 'slugh/reverse:cardh'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_h/42/',
            # get list of card_f with cardf=42
            '/api/cards/card_f/list?orig=combo&filter-cardh=42',
            # and follow cardf reverse relation
            (
                '/api/cards/card_f/list',
                'filter-identifier=41',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_f/list', 'filter-identifier=41&limit=10&offset=0'),  # check user access
        ]
    )

    cell.card_ids = '44'
    cell.related_card_path = ''
    cell.save()
    cell2.carddef_reference = 'default:card_g'
    cell2.slug = 'slugg'
    cell2.card_ids = ''
    cell2.related_card_path = 'slugh/reverse:cardh'
    cell2.save()
    success(
        urls=[
            # get first cell data
            '/api/cards/card_h/44/',
            # get list of card_g with cardf=44
            '/api/cards/card_g/list?orig=combo&filter-cardh=44',
            # and follow cardf reverse relation
            (
                '/api/cards/card_g/list',
                'filter-identifier=43',
                '^&limit=10&offset=0',
                '^include-fields',
            ),  # get card ids
            ('/api/cards/card_g/list', 'filter-identifier=43&limit=10&offset=0'),  # check user access
        ]
    )


@pytest.mark.parametrize('carddef_reference', ['default:card_model_1', 'default:card_model_1:foo'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_only_for_user(mock_send, context, carddef_reference):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference=carddef_reference,
        only_for_user=False,
        related_card_path='',
    )

    context['card_model_1_id'] = 11

    assert cell.is_visible(request=context['request']) is True
    context['request'].user = MockUserWithNameId()
    assert cell.is_visible(request=context['request']) is True

    cell.only_for_user = True
    cell.save()
    context['request'].user = None
    assert cell.is_visible(request=context['request']) is False
    context['request'].user = MockUserWithNameId()
    assert cell.is_visible(request=context['request']) is True

    cache.clear()
    context['synchronous'] = True  # to get fresh content
    context['request'].user = None

    mock_send.reset_mock()
    cell.render(context)
    assert 'filter-user-uuid' not in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUser()
    mock_send.reset_mock()
    cell.render(context)
    assert 'filter-user-uuid' not in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUserWithNameId()
    mock_send.reset_mock()
    cell.render(context)
    assert 'filter-user-uuid=xyz' in mock_send.call_args_list[0][0][0].url

    # reset context and change card id
    context['card_model_1_id'] = 13
    del cell._card_ids
    assert '<p>Unknown Card</p>' in cell.render(context)


@pytest.mark.parametrize('carddef_reference', ['default:card_model_1', 'default:card_model_1:foo'])
@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_render_user(mock_send, context, nocache, carddef_reference):
    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference=carddef_reference,
        related_card_path='',
    )

    context['card_model_1_id'] = 11
    context['synchronous'] = True  # to get fresh content

    assert context['request'].user is None
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID=&' in mock_send.call_args_list[0][0][0].url
    assert 'email=&' in mock_send.call_args_list[0][0][0].url

    context['request'].user = AnonymousUser()
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID=&' in mock_send.call_args_list[0][0][0].url
    assert 'email=&' in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUser()
    mock_send.reset_mock()
    cell.render(context)
    assert 'email=foo%40example.net' in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUserWithNameId()
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID=xyz' in mock_send.call_args_list[0][0][0].url

    cell.without_user = True
    cell.save()

    context['request'].user = None
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUser()
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url

    context['request'].user = MockUserWithNameId()
    mock_send.reset_mock()
    cell.render(context)
    assert 'NameID' not in mock_send.call_args_list[0][0][0].url
    assert 'email' not in mock_send.call_args_list[0][0][0].url


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_cell_condition(mock_send, nocache, app):
    page = Page.objects.create(title='xxx', slug='foo', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='',
        card_ids='{{ cards|objects:"card_model_1"|last|get:"id" }}',
    )

    cell.condition = 'cards|objects:"card_model_1"|getlist:"id"|list'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1

    cell.condition = 'cards|objects:"card_model_1"|getlist:"id"|get:42'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context.get('cells') or []) == 0

    page.extra_variables = {'var1': '{{ cards|objects:"card_model_1"|getlist:"id"|list }}'}
    page.save()
    cell.condition = 'var1'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1

    page.extra_variables = {'var1': '{{ cards|objects:"card_model_1"|getlist:"id"|get:42|default:"" }}'}
    page.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context.get('cells') or []) == 0

    page.extra_variables = {
        'var1': '{{ cards|objects:"unknown"|first|get:"id"|default:"" }}',
        'var2': '{{ cards|objects:"card_model_1"|first|get:"id"|default:"" }}',
    }
    page.save()
    cell.condition = 'not var1'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    cell.condition = 'var2'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1
    cell.condition = 'not var2'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context.get('cells') or []) == 0
    cell.condition = 'not var1 and not var2'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context.get('cells') or []) == 0
    cell.condition = 'not var1 and var2'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context['cells']) == 1

    # wrong condition
    cell.condition = 'cards|objects:"card_model_1"|first|filter_by:"fielda"|filter_value:"foo"'
    cell.save()
    resp = app.get(page.get_online_url())
    assert len(resp.context.get('cells') or []) == 0


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_file_redirection(mock_send, app):
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    cell = WcsCardCell(page=page, placeholder='content', order=0)
    cell.carddef_reference = 'default:card_model_1'
    cell.card_ids = '11'
    cell.save()
    resp = app.get('/one/')
    ajax_cell_url = PyQuery(resp.text).find('[data-ajax-cell-url]').attr['data-ajax-cell-url']
    extra_ctx = re.findall(r'data-extra-context="(.*)"', resp.text)
    resp = app.get(ajax_cell_url + '?ctx=' + extra_ctx[0])
    file_url = PyQuery(resp.text).find('[download]').attr['href']
    resp = app.get(file_url)
    assert 'download?f=42' in resp.location
    assert '&signature=' in resp.location

    # invalid crypto
    resp = app.get(file_url[:-2] + 'X/', status=403)

    # invalid session key
    resp = app.get(file_url.replace('file/', 'file/X'), status=403)


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_with_request_already_in_cache(mock_send, app):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        related_card_path='__all__',
        display_mode='card',
    )

    wcs_site = get_wcs_services().get(cell.wcs_site)
    cache_key = requests.get_cache_key(
        url=requests._build_url(
            '/api/cards/card_model_1/list?include-fields=on&include-submission=on&include-workflow=on',
            remote_service=wcs_site,
            user=None,
            without_user=False,
        ),
        params={},
    )
    cache.set('%s_called' % cache_key, True, 2)
    app.get(page.get_online_url())  # no WaitForCacheException


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_card_cell_assets(mock_send, settings, app, admin_user):
    page = Page.objects.create(title='xxx', slug='test_cell_assets', template_name='standard')
    cell1 = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        display_mode='card',
        slug='slug1',
    )
    cell2 = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        display_mode='table',
        slug='slug2',
    )

    app = login(app)
    settings.COMBO_CELL_ASSET_SLOTS = {}
    resp = app.get('/manage/assets/')
    assert 'have any asset yet.' in resp.text

    settings.COMBO_CELL_ASSET_SLOTS = {
        'wcs_wcscardscell': {
            'logo': {
                'prefix': 'Logo',
            },
        },
        'wcs_wcscardinfoscell': {
            'picture': {
                'prefix': 'Picture',
            },
        },
    }
    resp = app.get('/manage/assets/')
    assert 'Picture — %s' % cell1.get_label_for_asset() in resp.text
    assert 'Logo — %s' % cell2.get_label_for_asset() in resp.text


@responses.activate
def test_card_cell_nocache(app, freezer):
    from . import utils

    cache.clear()

    User.objects.create(username='foo')

    responses.get(
        'http://127.0.0.1:8999/api/cards/card_model_1/@schema', json=utils.WCS_CARDDEF_SCHEMAS['card_model_1']
    )

    data = copy.deepcopy(utils.WCS_CARDS_DATA['card_model_1'])

    responses.get('http://127.0.0.1:8999/api/cards/card_model_1/list', json={'data': data})

    page = Page(id=1, title='example page', slug='index')
    page.save()

    cell = WcsCardCell(id=1, page=page, placeholder='content', order=0)
    cell.carddef_reference = 'default:card_model_1'
    cell.save()
    assert len(responses.calls) == 1

    resp = app.get('/')
    cell_pyquery = list(resp.pyquery('.cell').items())[0]
    cell_url = cell_pyquery[0].attrib['data-ajax-cell-url']
    assert cell_url == '/ajax/cell/1/wcs_wcscardcell-1/'
    assert cell_pyquery.text() == 'Loading...'
    assert len(responses.calls) == 1

    # value of FieldB
    resp = app.get(cell_url)
    assert list(resp.pyquery('div.value').items())[1].text() == 'yes'
    assert len(responses.calls) == 2

    # change value of FieldB for first card
    assert data[0]['fields']['fieldb'] is True
    data[0]['fields']['fieldb'] = False
    responses.replace(responses.GET, 'http://127.0.0.1:8999/api/cards/card_model_1/list', json={'data': data})

    # nocache, no user
    resp = app.get('/?nocache')
    cell_pyquery = list(resp.pyquery('.cell').items())[0]
    cell_url = cell_pyquery[0].attrib['data-ajax-cell-url']
    assert cell_url == '/ajax/cell/1/wcs_wcscardcell-1/'
    assert cell_pyquery.text() == 'Loading...'
    assert len(responses.calls) == 2

    # value of FieldB
    resp = app.get(cell_url)
    assert list(resp.pyquery('div.value').items())[1].text() == 'yes'
    assert len(responses.calls) == 2

    # nocache, user
    app.set_user('foo')
    resp = app.get('/?nocache')
    cell_pyquery = list(resp.pyquery('.cell').items())[0]
    cell_url = cell_pyquery[0].attrib['data-ajax-cell-url']
    assert cell_url == '/ajax/cell/1/wcs_wcscardcell-1/?nocache'
    assert cell_pyquery.text() == 'Loading...'
    assert len(responses.calls) == 2

    # value of FieldB
    resp = app.get(cell_url)
    assert list(resp.pyquery('div.value').items())[1].text() == 'no'
    assert len(responses.calls) == 3


def get_output_of_command(command, *args, **kwargs):
    old_stdout = sys.stdout
    output = sys.stdout = StringIO()
    call_command(command, format_json=True, *args, **kwargs)
    sys.stdout = old_stdout
    return output.getvalue()


def test_export_import_card_cell_with_page_link():
    root_page = Page.objects.create(title='Root', slug='root', template_name='standard')
    card_page = Page.objects.create(
        title='Card',
        slug='card',
        template_name='standard',
        sub_slug='card_model_1_id',
        parent=root_page,
    )

    page = Page.objects.create(title='xxx', template_name='standard')
    WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@link@',
                    'page': card_page.pk,
                    'template': 'Foo',
                    'display_mode': 'link',
                },
            ]
        },
    )

    site_export = get_output_of_command('export_site')
    site_data = json.loads(site_export)
    assert len(site_data['pages']) == 3
    assert site_data['pages'][-1]['cells'][0]['fields']['custom_schema']['cells'][0]['page'] == str(
        card_page.uuid
    )
    import_site(data={}, clean=True)
    assert Page.objects.all().count() == 0

    import_site(data=site_data, clean=True)
    new_card_page = Page.objects.get(slug='card')
    new_cell = WcsCardCell.objects.get()
    assert new_cell.custom_schema['cells'][0]['page'] == new_card_page.pk

    # unknown target page
    new_cell.custom_schema['cells'][0]['page'] = 0
    new_cell.save()
    site_export = get_output_of_command('export_site')
    site_data = json.loads(site_export)
    assert site_data['pages'][-1]['cells'][0]['fields']['custom_schema']['cells'][0]['page'] == ''

    site_data['pages'][-1]['cells'][0]['fields']['custom_schema']['cells'][0]['page'] = 'unknown'
    import_site(data=site_data, clean=True)
    new_card_page = Page.objects.get(slug='card')
    new_cell = WcsCardCell.objects.get()
    assert new_cell.custom_schema['cells'][0]['page'] == ''


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_page_edit_linked_card(mock_send, app, admin_user):
    page = Page.objects.create(title='One', slug='one', template_name='two')

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    resp = resp.click(href='.*/linked-card')
    assert [o[0] for o in resp.form['carddef_reference'].options] == [
        '',
        'default:card_model_1',
        'default:card_model_2',
        'default:card_model_3',
        'default:card_a',
        'default:card_b',
        'default:card_c',
        'default:card_d',
        'default:card-e',
        'other:card_model_1',
        'other:card_model_2',
        'other:card_model_3',
        'other:card_a',
        'other:card_b',
        'other:card_c',
        'other:card_d',
        'other:card-e',
    ]
    resp.form['carddef_reference'] = 'default:card_model_3'
    resp = resp.form.submit().follow()
    page.refresh_from_db()
    assert page.sub_slug == 'card_model_3_id'
    resp = app.get('/manage/pages/%s/slug' % page.pk)
    assert 'sub_slug' not in resp.context['form'].fields

    resp = app.get('/manage/pages/%s/linked-card' % page.pk)
    assert resp.form['carddef_reference'].value == 'default:card_model_3'
    resp.form['carddef_reference'] = ''
    resp = resp.form.submit().follow()
    page.refresh_from_db()
    assert page.sub_slug == ''

    resp = app.get('/manage/pages/%s/slug' % page.pk)
    assert 'sub_slug' in resp.context['form'].fields
    resp.form['sub_slug'] = 'foobar'
    resp = resp.form.submit().follow()
    page.refresh_from_db()
    assert page.sub_slug == 'foobar'


def test_card_action_view(wcs_mock, app, context):
    wcs_mock.post('https://jump.test/trigger-1', json={'err': 0, 'data': 'ok'})

    page = Page.objects.create(title='xxx', template_name='standard')
    cell = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        carddef_reference='default:card_model_1',
        custom_schema={
            'cells': [
                {
                    'varname': '@action@',
                    'trigger_id': 'jump:trigger-1',
                    'action_label': 'Label {{ card.fields.fielda }}',
                    'action_ask_confirmation': True,
                    'action_confirmation_template': 'Confirmation {{ card.fields.fielda }}',
                    'unavailable_action_mode': 'hide',
                },
            ]
        },
        display_mode='table',
        related_card_path='__all__',
    )

    url = cell.get_ajax_url()

    resp = app.post(url, {'card_id': '11', 'trigger_id': 'jump:trigger-1'}, status=200)
    assert resp.json == {'err': 0, 'data': 'ok'}

    app.post(url, status=400)

    app.post(url, {'card_id': '51', 'trigger_id': 'jump:trigger-1'}, status=404)

    app.post(url, {'card_id': '11', 'trigger_id': 'i_dont_exist'}, status=404)

    # Check cell and page level permissions
    cell.only_for_user = True
    cell.save()
    app.post(url, {'card_id': '11', 'trigger_id': 'jump:trigger-1'}, status=403)

    cell.only_for_user = False
    cell.save()
    page.public = False
    page.save()
    app.post(url, {'card_id': '11', 'trigger_id': 'jump:trigger-1'}, status=403)


def test_card_action_related(wcs_mock, app, context, synchronous_cells):
    page = Page.objects.create(
        title='xxx', slug='foo', template_name='standard', sub_slug='(?P<card_a_id>[a-zA-Z0-9_-]+)'
    )
    WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='table',
        carddef_reference='default:card_a',
        slug='sluga',
        related_card_path='',
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': 'CardA {{ card.id }}'},
            ],
        },
    )
    cell_b = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=1,
        display_mode='table',
        carddef_reference='default:card_b',
        related_card_path='sluga/cardsb',
        slug='slugb',
        limit=1,
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': 'CardB {{ card.id }}'},
                {
                    'varname': '@action@',
                    'trigger_id': 'jump:trigger-1',
                    'action_label': 'CardB {{ card.id }}',
                    'action_ask_confirmation': False,
                    'action_confirmation_template': '',
                    'unavailable_action_mode': 'hide',
                },
            ]
        },
    )

    # Check rendering of cells
    resp = app.get(page.get_online_url() + '2/')
    assert resp.pyquery('.sluga tbody tr').text() == 'CardA 2'
    assert resp.pyquery('.slugb tbody tr').text() == 'CardB 2'
    assert resp.pyquery('.slugb tbody wcs-trigger-button')
    trigger_button = resp.pyquery('.slugb tbody wcs-trigger-button')
    assert len(trigger_button) == 1
    extra_context = resp.pyquery('.slugb').attr('data-extra-context')
    action_url = trigger_button.attr('card-action-url')
    assert action_url == cell_b.get_ajax_url()
    card_id = trigger_button.attr('card-id')
    assert card_id == '2'
    trigger_id = trigger_button.attr('card-trigger-id')
    assert trigger_id == 'jump:trigger-1'

    # Check that without the ?ctx= parameter no card is found
    post_resp = app.post(action_url, {'card_id': card_id, 'trigger_id': trigger_id}, status=404)
    assert post_resp.json['err'] == 1
    assert post_resp.json['err_desc'] == 'card_id not found'

    wcs_mock.post('https://jump.test/trigger-1', json={'err': 0, 'data': 'ok'})

    # With ?ctx= everything is ok
    post_resp = app.post(
        action_url + '?ctx=' + extra_context, {'card_id': card_id, 'trigger_id': trigger_id}, status=200
    )
    assert post_resp.json == {'err': 0, 'data': 'ok'}

    # Try on card_id 3 which is not visible because of pagination
    post_resp = app.post(
        action_url + '?ctx=' + extra_context, {'card_id': '3', 'trigger_id': trigger_id}, status=200
    )
    assert post_resp.json == {'err': 0, 'data': 'ok'}


def test_card_action_related_extra_variables(wcs_mock, app, context, synchronous_cells):
    page = Page.objects.create(
        title='xxx',
        slug='foo',
        template_name='standard',
        extra_variables={'card_a_id': '{{ request.GET.card_a_id }}'},
    )
    WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        display_mode='table',
        carddef_reference='default:card_a',
        slug='sluga',
        related_card_path='',
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': 'CardA {{ card.id }}'},
            ],
        },
    )
    cell_b = WcsCardCell.objects.create(
        page=page,
        placeholder='content',
        order=1,
        display_mode='table',
        carddef_reference='default:card_b',
        related_card_path='sluga/cardsb',
        slug='slugb',
        limit=1,
        custom_schema={
            'cells': [
                {'varname': '@custom@', 'template': 'CardB {{ card.id }}'},
                {
                    'varname': '@action@',
                    'trigger_id': 'jump:trigger-1',
                    'action_label': 'CardB {{ card.id }}',
                    'action_ask_confirmation': False,
                    'action_confirmation_template': '',
                    'unavailable_action_mode': 'hide',
                },
            ]
        },
    )

    # Check rendering of cells
    resp = app.get(page.get_online_url() + '?card_a_id=2')
    assert resp.pyquery('.sluga tbody tr').text() == 'CardA 2'
    assert resp.pyquery('.slugb tbody tr').text() == 'CardB 2'
    assert resp.pyquery('.slugb tbody wcs-trigger-button')
    trigger_button = resp.pyquery('.slugb tbody wcs-trigger-button')
    assert len(trigger_button) == 1
    extra_context = resp.pyquery('.slugb').attr('data-extra-context')
    action_url = trigger_button.attr('card-action-url')
    assert action_url == cell_b.get_ajax_url()
    card_id = trigger_button.attr('card-id')
    assert card_id == '2'
    trigger_id = trigger_button.attr('card-trigger-id')
    assert trigger_id == 'jump:trigger-1'

    # Check that without the ?ctx= parameter no card is found
    post_resp = app.post(action_url, {'card_id': card_id, 'trigger_id': trigger_id}, status=404)
    assert post_resp.json['err'] == 1
    assert post_resp.json['err_desc'] == 'card_id not found'

    wcs_mock.post('https://jump.test/trigger-1', json={'err': 0, 'data': 'ok'})

    # With ?ctx= everything is ok
    post_resp = app.post(
        action_url + '?card_a_id=2&ctx=' + extra_context,
        {'card_id': card_id, 'trigger_id': trigger_id},
        status=200,
    )
    assert post_resp.json == {'err': 0, 'data': 'ok'}

    # Try on card_id 3 which is not visible because of pagination
    post_resp = app.post(
        action_url + '?card_a_id=2&ctx=' + extra_context,
        {'card_id': '3', 'trigger_id': trigger_id},
        status=200,
    )
    assert post_resp.json == {'err': 0, 'data': 'ok'}
