import json
from unittest import mock

import pytest
from django.template import Context, Template
from django.test.client import RequestFactory
from publik_django_templatetags.wcs.context_processors import Cards


@pytest.fixture
def context():
    ctx = Context(
        {
            'cards': Cards(),
            'request': RequestFactory().get('/'),
        }
    )
    ctx['request'].user = None
    return ctx


class MockedRequestResponse(mock.Mock):
    status_code = 200

    def json(self):
        return json.loads(self.content)


def mocked_requests_send(request, **kwargs):
    data = [{'id': 1, 'fields': {'foo': 'bar'}}, {'id': 2, 'fields': {'foo': 'baz'}}]  # fake result
    return MockedRequestResponse(content=json.dumps({'data': data}))


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_publik_django_templatetags_integration(mock_send, context, nocache):
    t = Template('{{ cards|objects:"foo"|count }}')
    assert t.render(context) == '2'


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_with_explicit_load_wcs(mock_send, context, nocache):
    t = Template('{% load wcs %}{{ cards|objects:"foo"|count }}')
    assert t.render(context) == '2'
