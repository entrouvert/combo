import datetime
import os
import sys
from io import StringIO

import pytest
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.core.management import call_command
from django.test import override_settings
from django.test.client import RequestFactory
from django.utils.timezone import now

from combo.data.management.commands.export_site import Command as ExportSiteCommand
from combo.data.management.commands.import_site import Command as ImportSiteCommand
from combo.data.models import CellBase, LinkCell, LinkListCell, Page, PageSnapshot, TextCell
from combo.manager.forms import PageVisibilityForm

pytestmark = pytest.mark.django_db


def test_page_order():
    page = Page()
    page.title = 'foo'
    page.save()
    page2 = Page()
    page2.title = 'bar'
    page2.save()

    assert Page.objects.get(id=page.id).order < Page.objects.get(id=page2.id).order


def test_page_url():
    page = Page()
    page.slug = 'foo'
    page.save()
    assert page.get_online_url() == '/foo/'
    page2 = Page()
    page2.slug = 'bar'
    page2.parent = page
    assert page2.get_online_url() == '/foo/bar/'

    # directly give redirect url of linked page
    page2.redirect_url = 'https://www.example.org/test'
    page2.save()
    assert page2.get_online_url() == 'https://www.example.org/test'

    # unless it is a relative path
    page2.redirect_url = '../test'
    page2.save()
    assert page2.get_online_url() == '/foo/bar/'

    # or if's a template
    page2.redirect_url = '{{test_url}}plop'
    page2.save()
    assert page2.get_online_url() == '/foo/bar/'


def test_page_slug():
    Page.objects.all().delete()
    page = Page(title='first page')
    page.save()
    assert page.slug == 'index'  # first page always get 'index'
    page = Page(title='my title')
    page.save()
    assert page.slug == 'my-title'
    page = Page(title='my title')
    page.save()
    assert page.slug == 'my-title-2'
    page = Page(title='a' * 149)
    page.save()
    assert page.slug == 'a' * 140
    page = Page(title='a' * 149)
    page.save()
    assert page.slug == f"{'a' * 140}-2"


def test_page_of_level():
    parent_page = None
    for i in range(10):
        page = Page()
        page.slug = 'level%d' % i
        if parent_page:
            page.parent = parent_page
        page.save()
        parent_page = page

    assert page.get_page_of_level(2).slug == 'level2'
    assert page.get_page_of_level(4).slug == 'level4'
    assert page.get_page_of_level(14) is None


def test_page_siblings():
    Page.objects.all().delete()
    page = Page()
    page.slug = 'foo'
    page.save()
    page2 = Page()
    page2.slug = 'bar'
    page2.parent = page
    page2.save()
    page3 = Page()
    page3.slug = 'baz'
    page3.parent = page
    page3.save()
    assert [x.slug for x in page3.get_siblings()] == ['bar', 'baz']

    assert page.has_children()
    assert not page2.has_children()


def test_flat_hierarchy():
    Page.objects.all().delete()
    page = Page()
    page.slug = 'foo'
    page.save()
    page2 = Page()
    page2.slug = 'bar'
    page2.parent = page
    page2.save()
    page3 = Page()
    page3.slug = 'baz'
    page3.parent = page
    page3.save()

    pages = Page.get_as_reordered_flat_hierarchy(Page.objects.all())
    assert [x.slug for x in pages] == ['foo', 'bar', 'baz']

    page.order = 17
    page.save()
    pages = Page.get_as_reordered_flat_hierarchy(Page.objects.all())
    assert [x.slug for x in pages] == ['foo', 'bar', 'baz']

    page2.parent = None
    page2.save()
    pages = Page.get_as_reordered_flat_hierarchy(Page.objects.all())
    assert [x.slug for x in pages] == ['bar', 'foo', 'baz']

    page.parent = page2
    page.save()
    pages = Page.get_as_reordered_flat_hierarchy(Page.objects.all())
    assert [x.slug for x in pages] == ['bar', 'foo', 'baz']


def test_page_visibility():
    page = Page()
    assert page.is_visible()

    page.public = False
    page.save()
    assert not page.is_visible()

    group = Group(name='foobar')
    group.save()
    user1 = User(username='foo')
    user1.save()
    user1.groups.set([group])
    user2 = User(username='bar')
    user2.save()

    assert page.is_visible(user1)
    assert page.is_visible(user2)

    page.groups.set([group])
    assert not page.is_visible()
    assert page.is_visible(user1)
    assert not page.is_visible(user2)


def test_page_visibility_groups_init():
    page = Page.objects.create()
    group1 = Group.objects.create(name='foobar')
    group2 = Group.objects.create(name='another group')
    Group.objects.create(name='_technical-group')

    form = PageVisibilityForm(instance=page)
    assert form['groups'].initial == []

    page.groups.set([group1])
    form = PageVisibilityForm(instance=page)
    assert form['groups'].initial == [group1]

    # test sorting after excluding underscore-prefixed groups
    assert list(form.fields['groups'].queryset) == [group2, group1]


def test_import_export_pages():
    page = Page(title='foo', slug='foo', order=0, description="Foo's page")
    page.save()

    cell = TextCell(page=page, text='foo', order=0, placeholder='content')
    cell.save()

    page2 = Page(title='bar', slug='bar', order=1, parent=page, description="Bar's page")
    page2.save()

    cell = TextCell(page=page2, text='bar', order=0, placeholder='content')
    cell.save()

    old_page = Page(title='baz', slug='baz', order=2, description="baz's page")
    old_page.save()

    linklist = LinkListCell(order=1, page=page, placeholder='content')
    linklist.save()

    link = LinkCell(
        page=page,
        placeholder=linklist.link_placeholder,
        title='Example Site',
        link_page=old_page,
        order=0,
    )
    link.save()

    site_export = [x.get_serialized_page() for x in Page.objects.exclude(slug='baz')]
    Page.objects.all().delete()

    Page.load_serialized_pages(site_export)
    assert Page.objects.count() == 2
    assert list(Page.objects.all().order_by('slug').values_list('slug', flat=True)) == ['bar', 'foo']

    new_page_1 = Page.objects.all().order_by('order')[0]
    new_page_2 = Page.objects.all().order_by('order')[1]
    assert new_page_1.order < new_page_2.order
    assert new_page_1.title == 'foo'
    assert new_page_2.title == 'bar'
    assert new_page_1.description == "Foo's page"
    assert new_page_2.description == "Bar's page"
    assert len(CellBase.get_cells(page_id=new_page_1.id)) == 3
    assert isinstance(CellBase.get_cells(page_id=new_page_1.id)[0], TextCell)
    assert CellBase.get_cells(page_id=new_page_1.id)[0].text == 'foo'

    assert isinstance(CellBase.get_cells(page_id=new_page_1.id)[1], LinkCell)
    link = CellBase.get_cells(page_id=new_page_1.id)[1]
    assert link.title == 'Example Site'
    assert not link.link_page


def test_import_export_pages_with_links():
    page = Page(title='foo', slug='foo', order=0)
    page.save()

    page2 = Page(title='bar', slug='bar', order=1)
    page2.save()

    cell = LinkCell(page=page, title='bar', placeholder='content', link_page=page2, order=1)
    cell.save()

    cell2 = LinkCell(page=page2, title='foo', placeholder='content', link_page=page, order=1)
    cell2.save()

    cell3 = LinkListCell.objects.create(page=page, placeholder='content', order=2)
    item1 = LinkCell.objects.create(
        page=page,
        placeholder=cell3.link_placeholder,
        title='Example Site',
        url='http://example.net/',
        order=0,
    )
    item2 = LinkCell.objects.create(
        page=page,
        placeholder=cell3.link_placeholder,
        title='blah',
        link_page=page2,
        order=1,
    )

    site_export = [x.get_serialized_page() for x in Page.objects.all()]
    Page.objects.all().delete()

    for page in site_export:
        for cell in page['cells']:
            if 'links' not in cell:
                continue
            for link in cell['links']:
                assert 'pk' not in link
                assert 'placeholder' not in link['fields']
                assert 'page' not in link['fields']
    Page.load_serialized_pages(site_export)

    new_page_1 = Page.objects.all().order_by('order')[0]
    new_page_2 = Page.objects.all().order_by('order')[1]
    new_cells_1 = CellBase.get_cells(page_id=new_page_1.id, placeholder='content')
    new_cells_2 = CellBase.get_cells(page_id=new_page_2.id, placeholder='content')
    assert new_cells_1[0].link_page_id == new_page_2.id
    assert new_cells_2[0].link_page_id == new_page_1.id
    assert len(new_cells_1[1].get_items()) == 2
    new_item_1 = new_cells_1[1].get_items()[0]
    assert isinstance(new_item_1, LinkCell)
    assert new_item_1.title == item1.title
    assert new_item_1.url == item1.url
    assert new_item_1.link_page is None
    assert new_item_1.pk != item1.pk
    new_item_2 = new_cells_1[1].get_items()[1]
    assert isinstance(new_item_2, LinkCell)
    assert new_item_2.title == item2.title
    assert new_item_2.url == ''
    assert new_item_2.link_page == new_page_2
    assert new_item_2.pk != item2.pk


def test_duplicate_page():
    group1 = Group.objects.create(name='foobar')
    group2 = Group.objects.create(name='fooblah')

    Page.objects.create()  # page without snapshot, with slug 'index'
    page = Page.objects.create(title='foo', slug='foo', description="Foo's page")
    page.groups.set([group1, group2])
    snapshot = PageSnapshot.objects.create(page=page)
    page.snapshot = snapshot
    page.save()

    cell1 = TextCell.objects.create(page=page, text='foo1', order=0, placeholder='content')
    cell1.groups.set([group1, group2])
    cell2 = TextCell.objects.create(page=page, text='foo2', order=1, placeholder='content')

    new_page = page.duplicate()
    assert new_page.pk != page.pk
    assert new_page.title == 'Copy of foo'
    assert new_page.slug == 'copy-of-foo'
    assert new_page.description == page.description
    assert new_page.parent is None
    assert new_page.snapshot is None
    assert list(new_page.groups.all()) == [group1, group2]
    assert len(new_page.get_cells()) == 2

    new_cell1 = TextCell.objects.get(page=new_page, text='foo1')
    new_cell2 = TextCell.objects.get(page=new_page, text='foo2')
    assert new_cell1.pk != cell1.pk
    assert new_cell1.text == cell1.text
    assert new_cell1.placeholder == cell1.placeholder
    assert list(new_cell1.groups.all()) == [group1, group2]
    assert new_cell2.pk != cell2.pk
    assert new_cell2.text == cell2.text
    assert new_cell2.placeholder == cell2.placeholder
    assert list(new_cell2.groups.all()) == []

    # duplicate again !
    new_page = page.duplicate()
    assert new_page.slug == 'copy-of-foo-2'

    parent = Page.objects.create()
    page.parent = parent
    page.save()
    new_page = page.duplicate()
    assert new_page.parent == parent


def test_next_previous():
    Page.objects.all().delete()
    page = Page()
    page.slug = 'foo'
    page.save()
    page2 = Page()
    page2.slug = 'bar'
    page2.parent = page
    page2.order = 1
    page2.save()
    page3 = Page()
    page3.slug = 'baz'
    page3.parent = page
    page3.order = 2
    page3.save()

    assert page.get_next_page(None).id == page2.id
    assert page.get_previous_page(None) is None

    assert page2.get_next_page(None).id == page3.id
    assert page2.get_previous_page(None).id == page.id

    page2.public = False
    page2.save()
    assert page.get_next_page(None).id == page3.id
    assert page3.get_previous_page(None).id == page.id

    user1 = User(username='foo')
    user1.save()
    assert page.get_next_page(user1).id == page2.id
    assert page3.get_previous_page(user1).id == page2.id

    page3.public = False
    page3.save()
    assert page.get_next_page(None) is None

    assert page.get_next_page(check_visibility=False).id == page2.pk
    assert page2.get_next_page(check_visibility=False).id == page3.pk
    assert page3.get_previous_page(check_visibility=False).id == page2.pk


def test_import_export_management_commands():
    page = Page(title='foo', slug='foo', order=0)
    page.save()

    cell = TextCell(page=page, text='foo', order=0, placeholder='content')
    cell.save()

    page2 = Page(title='bar', slug='bar', order=1, parent=page)
    page2.save()

    cell = TextCell(page=page2, text='bar', order=0, placeholder='content')
    cell.save()

    export_filename = os.path.join(settings.MEDIA_ROOT, 'site-export.json')
    if os.path.exists(export_filename):
        os.unlink(export_filename)

    cmd = ExportSiteCommand()
    cmd.handle(output=export_filename, format_json=True)
    assert os.path.exists(export_filename)

    stdout = sys.stdout
    try:
        sys.stdout = StringIO()
        cmd.handle(output='-', format_json=True)
        with open(export_filename) as fd:
            assert sys.stdout.getvalue() == fd.read()
    finally:
        sys.stdout = stdout

    Page.objects.all().delete()

    cmd = ImportSiteCommand()
    cmd.handle(filename=export_filename, if_empty=False, clean=False)

    new_page_1 = Page.objects.all().order_by('order')[0]
    new_page_2 = Page.objects.all().order_by('order')[1]
    assert new_page_1.order < new_page_2.order
    assert new_page_1.title == 'foo'
    assert new_page_2.title == 'bar'
    assert len(CellBase.get_cells(page_id=new_page_1.id)) == 1
    assert isinstance(CellBase.get_cells(page_id=new_page_1.id)[0], TextCell)
    assert CellBase.get_cells(page_id=new_page_1.id)[0].text == 'foo'


def test_get_placeholders():
    page = Page(title='foo', slug='foo', template_name='standard-sidebar', order=0)
    request = RequestFactory().get('/')
    placeholders = page.get_placeholders(request=request)
    assert [x.key for x in placeholders] == ['content', 'sidebar', 'footer']
    assert placeholders[0].acquired is False
    assert placeholders[-1].acquired is True

    with override_settings(
        COMBO_PUBLIC_TEMPLATES={
            'standard-sidebar': {
                'name': 'Test',
                'template': 'combo/page_template_sidebar.html',
                'placeholders': {
                    'content': {'name': 'Content'},
                    'content2': {'name': 'Second Content'},
                    'sidebar': {'name': 'Sidebar'},
                    'footer': {'name': 'Footer', 'acquired': False},
                },
            }
        }
    ):
        placeholders = page.get_placeholders(request=request)
        assert {x.key for x in placeholders} == {'content', 'content2', 'sidebar', 'footer'}
        footer_placeholder = [x for x in placeholders if x.key == 'footer'][0]
        assert footer_placeholder.acquired is False


def test_render(app):
    page = Page(
        title='foo', slug='foo', template_name='standard-sidebar', order=0, description='page description'
    )
    page.save()
    response = app.get(page.get_online_url())
    assert '<meta name="description" content="page description" />' in response.text
    assert '<title>Combo - foo</title>' in response.text


def test_render_cell_having_href_template_error(app):
    page = Page(
        title='foo', slug='foo', template_name='standard-sidebar', order=0, description='page description'
    )
    page.save()
    cell = TextCell(
        page=page, text='<a href="{{e-service_url}}backoffice/...">link</a>', order=0, placeholder='content'
    )
    cell.save()
    response = app.get(page.get_online_url())
    assert '{{e-service_url}}backoffice/...' in response.text  # href not rendered


def test_cell_maintain_page_cell_cache(freezer):
    freezer.move_to('2020-01-01')
    page = Page(title='page-1', slug='page-1')
    page.save()
    cell = TextCell(page=page, order=0, slug='cell-1', text='foo')
    cell.save()
    assert page.last_update_timestamp.isoformat().startswith('2020-01-01')

    freezer.move_to('2020-02-02')
    cell.text = 'bar'
    cell.save()
    assert page.last_update_timestamp.isoformat().startswith('2020-02-02')


def test_page_is_new(freezer):
    freezer.move_to('2020-01-01')
    page = Page(title='page-1', slug='page-1')
    page.save()
    assert page.is_new()

    freezer.move_to('2020-01-08')
    page = Page.objects.get(slug='page-1')
    assert not page.is_new()


def test_get_descendants_and_me():
    '''    1
           |
          2 6
        /     \
     3 4 5   7 8 9
    '''

    def add_pages(depth=1, parent_id=None):
        if depth > 3:
            return
        page = Page(title='%s' % add_pages.num, slug='page-%s' % add_pages.num)
        page.parent_id = parent_id
        page.save()
        add_pages.num += 1
        for dummy in range(0, depth + 1):
            add_pages(depth + 1, page.id)

    add_pages.num = 1
    add_pages()

    page = Page.objects.get(slug='page-1')
    assert len(page.get_descendants()) == 8
    assert len(page.get_descendants_and_me()) == 9
    page = Page.objects.get(slug='page-2')
    assert [int(x.title) for x in page.get_descendants()] == [3, 4, 5]
    assert [int(x.title) for x in page.get_descendants_and_me()] == [2, 3, 4, 5]
    page = Page.objects.get(slug='page-3')
    assert [int(x.title) for x in page.get_descendants()] == []
    assert [int(x.title) for x in page.get_descendants_and_me()] == [3]


def test_snapshot_page():
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    PageSnapshot.take(page)
    snapshot = PageSnapshot.objects.get()
    assert 'order' not in snapshot.serialization['fields']
    assert 'parent' not in snapshot.serialization['fields']


def test_clear_snapshot_pages():
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    TextCell.objects.create(page=page, text='foo', order=0, placeholder='content')
    PageSnapshot.take(page)
    snapshot = PageSnapshot.objects.get()
    snapshot_page = snapshot.get_page()
    assert snapshot_page.snapshot == snapshot
    assert snapshot_page.pk != page.pk
    assert TextCell.objects.filter(page=page).count() == 1
    assert TextCell.objects.filter(page=snapshot_page).count() == 1

    # too soon
    call_command('clear_snapshot_pages')
    assert Page.objects.count() == 1
    assert Page.snapshots.count() == 1
    assert PageSnapshot.objects.count() == 1
    assert TextCell.objects.filter(page=page).count() == 1
    assert TextCell.objects.filter(page=snapshot_page).count() == 1

    # still too soon
    Page.snapshots.update(last_update_timestamp=now() - datetime.timedelta(days=1, minutes=-1))
    call_command('clear_snapshot_pages')
    assert Page.objects.count() == 1
    assert Page.snapshots.count() == 1
    assert PageSnapshot.objects.count() == 1
    assert TextCell.objects.filter(page=page).count() == 1
    assert TextCell.objects.filter(page=snapshot_page).count() == 1

    # ok, 24H after page snapshot creation
    Page.snapshots.update(last_update_timestamp=now() - datetime.timedelta(days=1))
    call_command('clear_snapshot_pages')
    assert Page.objects.count() == 1
    assert Page.snapshots.count() == 0
    assert PageSnapshot.objects.count() == 1
    assert TextCell.objects.count() == 1
    assert TextCell.objects.filter(page=page).count() == 1
