import os

import pytest
from django.core.files import File

from combo.apps.export_import.models import Application, ApplicationElement
from combo.data.models import Page

pytestmark = pytest.mark.django_db

TESTS_DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')


def login(app, username='admin', password='admin'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


@pytest.fixture
def application_with_icon():
    application = Application.objects.create(
        name='App 1',
        slug='app-1',
        version_number='1',
    )
    with open(os.path.join(TESTS_DATA_DIR, 'black.jpeg'), mode='rb') as fd:
        application.icon.save('black.jpeg', File(fd), save=True)
    return application


@pytest.fixture
def application_without_icon():
    application = Application.objects.create(
        name='App 2',
        slug='app-2',
        version_number='1',
    )
    return application


@pytest.mark.parametrize('icon', [True, False])
def test_pages(app, admin_user, application_with_icon, application_without_icon, icon):
    if icon:
        application = application_with_icon
    else:
        application = application_without_icon

    page1 = Page.objects.create(title='Page 1', slug='page-1')
    page2 = Page.objects.create(title='Page 2', slug='page-2', parent=page1)
    ApplicationElement.objects.create(content_object=page2, application=application)
    page3 = Page.objects.create(title='Page 3', slug='page-3', parent=page2)
    ApplicationElement.objects.create(content_object=page3, application=application)

    app = login(app)

    resp = app.get('/manage/')
    assert len(resp.pyquery('#pages-list div.page')) == 3
    assert resp.pyquery('#pages-list div.page:nth-child(1)').attr['data-level'] == '0'
    assert resp.pyquery('#pages-list div.page:nth-child(2)').attr['data-level'] == '1'
    assert resp.pyquery('#pages-list div.page:nth-child(3)').attr['data-level'] == '2'
    assert resp.pyquery('#pages-list div.page:nth-child(1)').text() == '⣿ Page 1'
    assert resp.pyquery('#pages-list div.page:nth-child(2)').text() == '⣿ Page 2'
    assert resp.pyquery('#pages-list div.page:nth-child(3)').text() == '⣿ Page 3'
    if icon:
        assert len(resp.pyquery('#pages-list div.page img')) == 2
        assert len(resp.pyquery('#pages-list div.page:nth-child(1) img')) == 0
        assert len(resp.pyquery('#pages-list div.page:nth-child(2) img.application-icon')) == 1
        assert len(resp.pyquery('#pages-list div.page:nth-child(3) img.application-icon')) == 1
    else:
        assert len(resp.pyquery('#pages-list div.page img')) == 0
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0
    assert 'Pages outside applications' in resp

    # check application view
    resp = resp.click(application.name)
    assert resp.pyquery('h2').text() == application.name
    if icon:
        assert len(resp.pyquery('h2 img.application-logo')) == 1
    else:
        assert len(resp.pyquery('h2 img')) == 0
    assert len(resp.pyquery('#pages-list div.page')) == 2
    assert resp.pyquery('#pages-list div.page:nth-child(1)').attr['data-level'] == '0'
    assert resp.pyquery('#pages-list div.page:nth-child(2)').attr['data-level'] == '1'
    assert resp.pyquery('#pages-list div.page:nth-child(1)').text() == 'Page 2'
    assert resp.pyquery('#pages-list div.page:nth-child(2)').text() == 'Page 3'
    assert len(resp.pyquery('#pages-list div.page img')) == 0

    # check pages outside applications
    resp = app.get('/manage/')
    resp = resp.click('Pages outside applications')
    assert resp.pyquery('h2').text() == 'Pages outside applications'
    assert len(resp.pyquery('#pages-list div.page')) == 1
    assert resp.pyquery('#pages-list div.page:nth-child(1)').attr['data-level'] == '0'
    assert resp.pyquery('#pages-list div.page:nth-child(1)').text() == 'Page 1'

    # check detail page
    resp = app.get('/manage/pages/%s/' % page1.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
    resp = app.get('/manage/pages/%s/' % page2.pk)
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0

    # check visible flag
    application.visible = False
    application.save()
    resp = app.get('/manage/')
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('#pages-list div.page img')) == 0
    app.get('/manage/?application=%s' % application.slug, status=404)
    resp = app.get('/manage/pages/%s/' % page2.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
