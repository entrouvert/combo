import os

import pytest
from django.contrib.auth.models import Group
from django.urls import reverse

from combo.apps.gallery.models import GalleryCell
from combo.data.models import Page

from .test_manager import login

TESTS_DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')

pytestmark = pytest.mark.django_db


@pytest.fixture
def group(john_doe):
    group = Group.objects.create(name='foobar')
    john_doe.groups.set([group])
    return group


@pytest.mark.parametrize('is_admin', [True, False])
def test_adding_gallery_images(app, is_admin, john_doe, group):
    john_doe.is_superuser = is_admin
    john_doe.save()

    page = Page(title='Pictures', slug='test_gallery_cell', template_name='standard')
    page.edit_role = group
    page.save()
    cell = GalleryCell(page=page, placeholder='content', order=0)
    cell.save()

    app.post(
        reverse('combo-gallery-image-add', kwargs={'gallery_pk': cell.id}),
        params={'image': ['foo'], 'title': 'white'},
        status=403,
    )

    app = login(app, username='john.doe', password='john.doe')

    resp = app.get('/manage/pages/%s/' % page.id)
    assert '<ul class="gallery"' in resp.text
    assert 'js/combo.gallery.js' in resp.text

    assert len(cell.image_set.all()) == 0
    form = app.get(reverse('combo-gallery-image-add', kwargs={'gallery_pk': cell.id})).form
    form['image'] = [os.path.join(TESTS_DATA_DIR, 'black.jpeg')]
    form['title'] = 'black'
    form.submit()
    form = app.get(reverse('combo-gallery-image-add', kwargs={'gallery_pk': cell.id})).form
    form['image'] = [os.path.join(TESTS_DATA_DIR, 'black.jpeg')]
    form['title'] = 'white'
    form.submit()
    assert len(cell.image_set.all()) == 2

    image_1, image_2 = cell.image_set.all().order_by('id')
    form = app.get(reverse('combo-gallery-image-edit', kwargs={'gallery_pk': cell.id, 'pk': image_2.id})).form
    form['title'] = 'pink'
    form.submit()
    assert len(cell.image_set.filter(title='pink')) == 1

    resp = app.get('/%s/' % page.slug, status=200)
    assert 'pink' in resp.text

    resp = app.get(
        reverse('combo-gallery-image-delete', kwargs={'gallery_pk': cell.id, 'pk': image_2.id}), status=302
    )
    assert len(cell.image_set.all()) == 1

    resp = app.get('/%s/' % page.slug, status=200)
    assert 'pink' not in resp.text

    # image does not exist
    app.get(reverse('combo-gallery-image-edit', kwargs={'gallery_pk': cell.id, 'pk': 0}), status=404)
    app.get(reverse('combo-gallery-image-delete', kwargs={'gallery_pk': cell.id, 'pk': 0}), status=404)

    # cell does not exist
    app.get(reverse('combo-gallery-image-add', kwargs={'gallery_pk': 0}), status=404)
    app.get(
        reverse('combo-gallery-image-edit', kwargs={'gallery_pk': 0, 'pk': cell.image_set.first().pk}),
        status=404,
    )
    app.get(
        reverse('combo-gallery-image-delete', kwargs={'gallery_pk': 0, 'pk': cell.image_set.first().pk}),
        status=404,
    )
    app.get(reverse('combo-gallery-image-order', kwargs={'gallery_pk': 0}), status=404)

    if not is_admin:
        page.edit_role = None
        page.save()

        other_page = Page(title='other', slug='other', template_name='standard')
        other_page.edit_role = group
        other_page.save()

        app.get(reverse('combo-gallery-image-add', kwargs={'gallery_pk': cell.id}), status=403)
        app.get(
            reverse('combo-gallery-image-edit', kwargs={'gallery_pk': cell.id, 'pk': image_1.id}), status=403
        )
        app.get(
            reverse('combo-gallery-image-delete', kwargs={'gallery_pk': cell.id, 'pk': image_1.id}),
            status=403,
        )
        app.get(reverse('combo-gallery-image-order', kwargs={'gallery_pk': cell.id}), status=403)


def test_manager_gallery_cell_tabs(app, admin_user):
    page = Page(title='Pictures', slug='test_gallery_cell', template_name='standard')
    page.save()
    cell = GalleryCell(page=page, placeholder='content', order=0)
    cell.save()

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)

    assert not resp.pyquery('[data-tab-slug="general"] input[name$="title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="title"]')
