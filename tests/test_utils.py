import pytest
from django.conf import settings
from django.template import Context, RequestContext
from django.test import override_settings
from django.test.client import RequestFactory
from django.utils.encoding import force_bytes

from combo.utils import TemplateError, aes_hex_decrypt, aes_hex_encrypt, get_templated_url


class MockUser:
    email = 'foo=3@example.net'
    is_authenticated = True

    def __init__(self, samlized=True):
        self.samlized = samlized

    def get_name_id(self):
        if self.samlized:
            return 'r2&d2'
        return None


def test_crypto_url():
    invoice_id = '12-1234'
    key = settings.SECRET_KEY
    assert aes_hex_decrypt(key, aes_hex_encrypt(key, force_bytes(invoice_id))) == invoice_id


def test_templated_url():
    assert get_templated_url('foobar') == 'foobar'
    assert get_templated_url('foo[]bar') == 'foo[]bar'
    assert get_templated_url('foo[bar') == 'foo[bar'
    assert get_templated_url('foo]bar') == 'foo]bar'
    assert get_templated_url('foo]bar[') == 'foo]bar['
    assert get_templated_url('foo]bar]') == 'foo]bar]'
    assert get_templated_url('foobar[[]') == 'foobar['
    assert get_templated_url('foobar[[]]') == 'foobar[]'
    assert get_templated_url('foobar[[]test]') == 'foobar[test]'

    assert get_templated_url('{{ test_url }}') == ''
    with pytest.raises(TemplateError, match='unknown variable test_url'):
        get_templated_url('[test_url]')

    with override_settings(TEMPLATE_VARS={'test_url': 'http://www.example.net'}):
        assert get_templated_url('{{ test_url }}') == 'http://www.example.net'
        assert get_templated_url('[test_url]') == 'http://www.example.net'
        assert get_templated_url('{{ test_url }}/hello') == 'http://www.example.net/hello'
        assert get_templated_url('[test_url]/hello') == 'http://www.example.net/hello'

    # contexts without users
    request = RequestFactory().get('/')
    request.user = None
    for context in (None, Context({}), Context({'request': None}), Context({'request': request})):
        assert get_templated_url('NameID={{ user_nameid }}', context=context) == 'NameID='
        assert get_templated_url('email={{ user_email }}', context=context) == 'email='
        if context is None:
            with pytest.raises(TemplateError, match='unknown variable user_nameid'):
                get_templated_url('NameID=[user_nameid]', context=context)
            with pytest.raises(TemplateError, match='unknown variable user_email'):
                get_templated_url('email=[user_email]', context=context)
        else:
            assert get_templated_url('NameID=[user_nameid]', context=context) == 'NameID='
            assert get_templated_url('email=[user_email]', context=context) == 'email='
        with pytest.raises(TemplateError, match='unknown variable bar'):
            get_templated_url('foo=[bar]', context=context)
        if context:
            context['foobar'] = 'barfoo'
            assert get_templated_url('{{foobar}}', context=context) == 'barfoo'
            assert get_templated_url('[foobar]', context=context) == 'barfoo'

    # contexts with users
    request = RequestFactory().get('/')
    request.user = MockUser(samlized=False)
    context = Context({'request': request})
    assert get_templated_url('email={{ user_email }}', context=context) == 'email=foo%3D3%40example.net'
    assert get_templated_url('email=[user_email]', context=context) == 'email=foo%3D3%40example.net'
    request.user = MockUser(samlized=True)
    assert (
        get_templated_url('email={{user_email}}&NameID={{user_nameid}}', context=context)
        == 'email=foo%3D3%40example.net&NameID=r2%26d2'
    )
    assert (
        get_templated_url('email=[user_email]&NameID=[user_nameid]', context=context)
        == 'email=foo%3D3%40example.net&NameID=r2%26d2'
    )

    # mixed sources
    request = RequestFactory().get('/')
    request.user = MockUser(samlized=True)
    context = Context({'request': request})
    context['foobar'] = 'barfoo'
    assert (
        get_templated_url('{{ foobar }}/email={{ user_email }}&NameID={{ user_nameid }}', context=context)
        == 'barfoo/email=foo%3D3%40example.net&NameID=r2%26d2'
    )
    assert (
        get_templated_url('[foobar]/email=[user_email]&NameID=[user_nameid]', context=context)
        == 'barfoo/email=foo%3D3%40example.net&NameID=r2%26d2'
    )
    with override_settings(TEMPLATE_VARS={'test_url': 'http://www.example.net'}):
        request = RequestFactory().get('/')
        request.user = MockUser(samlized=True)
        context = Context({'foobar': 'barfoo', 'request': request})
        assert (
            get_templated_url(
                '{{test_url}}/{{foobar}}/?NameID={{user_nameid}}&email={{user_email}}', context=context
            )
            == 'http://www.example.net/barfoo/?NameID=r2%26d2&email=foo%3D3%40example.net'
        )
        assert (
            get_templated_url('[test_url]/[foobar]/?NameID=[user_nameid]&email=[user_email]', context=context)
            == 'http://www.example.net/barfoo/?NameID=r2%26d2&email=foo%3D3%40example.net'
        )

    # packed contexts
    request = RequestFactory().get('/')
    request.user = None
    context = Context({'request': request})
    context.update({'foo': 'bar'})
    ctx = Context()
    ctx.update(context)
    assert get_templated_url('{{ foo }}', context=ctx) == 'bar'
    assert get_templated_url('[foo]', context=ctx) == 'bar'

    # accept django syntax
    assert get_templated_url('{% if "foo" %}ok{% endif %}') == 'ok'
    assert get_templated_url('{% if foo %}{{ foo }}{% endif %}') == ''
    assert get_templated_url('{% if foo %}{{ foo }}{% endif %}', context={'foo': 'ok'}) == 'ok'
    assert get_templated_url('{{ bar|default:"ok" }}') == 'ok'
    assert get_templated_url('{{ foo|default:"ko" }}', context={'foo': 'ok'}) == 'ok'
    assert get_templated_url('{nodjango}') == '{nodjango}'
    assert get_templated_url('{{') == '{{'
    assert get_templated_url('{%{{ ok }}{%') == '{%{%'
    assert get_templated_url('{{ foo.bar }}', context={'foo': {'bar': 'ok'}}) == 'ok'
    assert get_templated_url('{{ foo.bar }}') == ''
    assert get_templated_url('{{ foo.0 }}{{ foo.1 }}{{ foo.2 }}', context={'foo': ['ok', 'doc']}) == 'okdoc'
    assert get_templated_url('{{ foo.0.bar }}{{ foo.0.zoo }}', context={'foo': [{'bar': 'ok'}, 'ko']}) == 'ok'
    # catch django syntax errors in TemplateError
    for template in ('{% foobar %}', '{% if "coucou" %}', '{{}}', '{{ if "x" }}', '{{ _private }}'):
        with pytest.raises(TemplateError, match='syntax error'):
            assert get_templated_url(template, context=ctx) == 'bar'

    # requestcontext
    with override_settings(TEMPLATE_VARS={'test_url': 'http://www.example.net'}):
        request = RequestFactory().get('/')
        ctx = RequestContext(request, {'foo': 'bar'})
        assert get_templated_url('{{ test_url }}/{{ foo }}', context=ctx) == 'http://www.example.net/bar'
        assert get_templated_url('[test_url]/[foo]', context=ctx) == 'http://www.example.net/bar'
