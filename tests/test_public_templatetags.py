import datetime
import json
import os
import shutil
import time
from io import StringIO
from unittest import mock

import pytest
from django.contrib.auth.models import AnonymousUser, Group, User
from django.core.files import File
from django.core.files.storage import default_storage
from django.template import Context, RequestContext, Template
from django.test import override_settings
from django.test.client import RequestFactory
from django.utils.timezone import now
from requests.models import Response

from combo.apps.assets.models import Asset
from combo.data.models import Page, TextCell
from combo.profile.utils import get_user_from_name_id

pytestmark = pytest.mark.django_db


def test_templatevars_bool_and_none_aliases():
    ctx = RequestContext(RequestFactory().get('/'))

    t_true = Template('{% if var == true %}OK{% endif %}')
    t_false = Template('{% if var == false %}OK{% endif %}')
    t_null = Template('{% if var == null %}OK{% endif %}')
    t_is_true = Template('{% if var is true %}OK{% endif %}')
    t_is_false = Template('{% if var is false %}OK{% endif %}')
    t_is_null = Template('{% if var is null %}OK{% endif %}')

    ctx.update({'var': True})
    assert t_true.render(ctx) == 'OK'
    assert t_false.render(ctx) == ''
    assert t_null.render(ctx) == ''
    assert t_is_true.render(ctx) == 'OK'
    assert t_is_false.render(ctx) == ''
    assert t_is_null.render(ctx) == ''

    ctx.update({'var': False})
    assert t_true.render(ctx) == ''
    assert t_false.render(ctx) == 'OK'
    assert t_null.render(ctx) == ''
    assert t_is_true.render(ctx) == ''
    assert t_is_false.render(ctx) == 'OK'
    assert t_is_null.render(ctx) == ''

    ctx.update({'var': None})
    assert t_true.render(ctx) == ''
    assert t_false.render(ctx) == ''
    assert t_null.render(ctx) == 'OK'
    assert t_is_true.render(ctx) == ''
    assert t_is_false.render(ctx) == ''
    assert t_is_null.render(ctx) == 'OK'


def test_templatevars_now_and_today():
    now = datetime.datetime.now()
    ctx = RequestContext(RequestFactory().get('/'))

    assert Template('{{now|date:"Y"}}').render(ctx) == str(now.year)
    assert Template('{{today|date:"Y"}}').render(ctx) == str(now.year)

    assert Template('{{ today }}').render(ctx) == now.strftime('%Y-%m-%d')
    assert Template('{{ now }}').render(ctx) == now.strftime('%Y-%m-%d %H:%M')

    assert Template('{{ today|add_days:1 }}').render(ctx) == (now + datetime.timedelta(days=1)).strftime(
        '%Y-%m-%d'
    )
    assert Template('{{ now|add_days:1 }}').render(ctx) == (now + datetime.timedelta(days=1)).strftime(
        '%Y-%m-%d'
    )

    ctx.update({'old_date': datetime.datetime(2000, 1, 1)})
    assert Template('{% if now > old_date %}OK{% endif %}').render(ctx) == 'OK'
    assert Template('{% if now < old_date %}KO{% endif %}').render(ctx) == ''


def test_strptime():
    t = Template('{{ someday|strptime:"%Y-%m-%d"|date:"Y" }}')
    assert t.render(Context({'someday': '2015-04-15'})) == '2015'
    assert t.render(Context({'someday': 'foobar'})) == ''
    assert t.render(Context({'someday': None})) == ''
    assert t.render(Context({'someday': {'foo': 'bar'}})) == ''
    assert t.render(Context({'someday': ['foo', 'bar']})) == ''


def test_parse_datetime():
    t = Template('{{ someday|parse_datetime|date:"Y m d H i s T" }}')
    expected = '2015 04 15 13 11 12 UTC'
    assert t.render(Context({'someday': '2015-04-15T13:11:12'})) == expected
    assert t.render(Context({'someday': '2015-04-15 13:11:12'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12Z'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12+00:00'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12.12345'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12.12345Z'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12.12345+00:00'})) == expected
    assert (
        t.render(Context({'someday': time.localtime(time.mktime((2015, 4, 15, 13, 11, 12, 0, 0, 0)))}))
        == expected
    )
    assert t.render(Context({'someday': '2015-04-15T13:11'})) == '2015 04 15 13 11 00 UTC'
    assert t.render(Context({'someday': '2015-04-15T13'})) == '2015 04 15 13 00 00 UTC'
    assert t.render(Context({'someday': 'foobar'})) == ''
    assert t.render(Context({'someday': ''})) == ''
    assert t.render(Context({'someday': None})) == ''
    assert t.render(Context({'someday': {'foo': 'bar'}})) == ''
    assert t.render(Context({'someday': ['foo', 'bar']})) == ''

    t = Template('{{ someday|parse_date|date:"Y m d" }}')
    expected = '2015 04 15'
    assert t.render(Context({'someday': '2015-04-15'})) == expected
    assert t.render(Context({'someday': '2015-04-15T13:11:12Z'})) == expected
    assert t.render(Context({'someday': 'foobar'})) == ''
    assert t.render(Context({'someday': ''})) == ''
    assert t.render(Context({'someday': None})) == ''
    assert t.render(Context({'someday': {'foo': 'bar'}})) == ''
    assert t.render(Context({'someday': ['foo', 'bar']})) == ''

    t = Template('{{ someday|parse_time|date:"H i s" }}')
    expected = '13 11 12'
    assert t.render(Context({'someday': '13:11:12'})) == expected
    assert t.render(Context({'someday': '13:11:12Z'})) == expected
    assert t.render(Context({'someday': '13:11:12+00:00'})) == expected
    assert t.render(Context({'someday': '13:11:12.12345'})) == expected
    assert t.render(Context({'someday': '13:11:12.12345Z'})) == expected
    assert t.render(Context({'someday': '13:11:12.12345+00:00'})) == expected
    assert t.render(Context({'someday': '13:11'})) == '13 11 00'
    assert t.render(Context({'someday': '13:99'})) == ''
    assert t.render(Context({'someday': '13'})) == '13 00 00'
    assert t.render(Context({'someday': 'foobar'})) == ''
    assert t.render(Context({'someday': ''})) == ''
    assert t.render(Context({'someday': None})) == ''
    assert t.render(Context({'someday': {'foo': 'bar'}})) == ''
    assert t.render(Context({'someday': ['foo', 'bar']})) == ''


def test_has_role():
    t = Template('{{ request.user|has_role:"Role1" }}')

    request = RequestFactory().get('/')
    user = User(username='foo', email='foo@example.net')
    user.save()
    request.user = user
    context = Context({'request': request})
    assert t.render(context) == 'False'

    group = Group(name='Role1')
    group.save()
    user.groups.add(group)
    user.save()
    assert t.render(context) == 'True'

    t = Template('{{ request.user|has_role:"Role2" }}')
    assert t.render(context) == 'False'
    group = Group(name='Role2')
    group.save()
    user.groups.add(group)
    user.save()
    assert t.render(context) == 'True'

    # no request, no user, anonymous user
    context = Context()
    assert t.render(context) == 'False'
    request = RequestFactory().get('/')
    context = Context({'request': request})
    assert t.render(context) == 'False'
    request.user = AnonymousUser()
    assert t.render(context) == 'False'
    context['selected_user'] = get_user_from_name_id('unknown')
    t = Template('{{ selected_user|has_role:"Role2" }}')
    assert t.render(context) == 'False'

    # filter not on user object
    t = Template('{{ request|has_role:"Role1" }}')
    assert t.render(context) == 'False'


def test_strip_templatetag():
    tmpl = Template('{{ foo|strip }}')
    assert tmpl.render(Context()) == ''
    assert tmpl.render(Context({'foo': None})) == ''
    assert tmpl.render(Context({'foo': ' foo bar '})) == 'foo bar'
    assert tmpl.render(Context({'foo': ' foo bar\t'})) == 'foo bar'
    assert tmpl.render(Context({'foo': ' félé  '})) == 'félé'
    tmpl = Template('{{ foo|strip:"XY" }}')
    assert tmpl.render(Context({'foo': 'XXfoo barXYX'})) == 'foo bar'
    assert tmpl.render(Context({'foo': ' foo barXX'})) == ' foo bar'


def test_get_group():
    context = Context(
        {
            'cities': [
                {'name': 'Mumbai', 'population': '19,000,000', 'country': 'India'},
                {'name': 'New York', 'population': '20,000,000', 'country': 'USA'},
                {'name': 'Calcutta', 'population': '15,000,000', 'country': 'India'},
                {'name': 'Chicago', 'population': '7,000,000', 'country': 'USA'},
                {'name': 'Tokyo', 'population': '33,000,000', 'country': 'Japan'},
            ]
        }
    )
    t = Template(
        '{% regroup cities by country as country_list %}'
        '{% for c in country_list|get_group:"USA" %}{{c.name}},{% endfor %}'
    )
    assert t.render(context) == 'New York,Chicago,'


def test_asset_template_tags():
    for path in ('uploads', 'assets'):
        if os.path.exists(default_storage.path(path)):
            shutil.rmtree(default_storage.path(path))
    assert Asset.objects.count() == 0

    with override_settings(COMBO_ASSET_SLOTS={'collectivity:banner': {'label': 'Banner'}}):
        t = Template(
            '''{% load assets %}{% get_asset "collectivity:banner" as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context()) == ''

        t = Template('''{% load assets %}{% asset_url "collectivity:banner" %}''')
        assert t.render(Context()) == ''
        t = Template('''{% load assets %}{% asset_css_url "collectivity:banner" %}''')
        assert t.render(Context()) == 'none'

        t = Template(
            '''{% load assets %}{% get_asset "collectivity:banner" as banner %}{% if banner %}BANNER{% endif %}'''
        )
        Asset(key='collectivity:banner', asset=File(StringIO('test'), 'test.png')).save()
        assert t.render(Context()) == 'BANNER'

        t = Template('''{% load assets %}{% asset_url "collectivity:banner" %}''')
        assert t.render(Context()) == '/media/assets/test.png'
        t = Template('''{% load assets %}{% asset_css_url "collectivity:banner" %}''')
        assert t.render(Context()) == 'url(/media/assets/test.png)'

        page = Page(title='Home', slug='index', template_name='standard')
        page.save()

        t = Template('''{% load assets %}{% asset_url page.picture "collectivity:banner" %}''')
        assert t.render(Context()) == '/media/assets/test.png'
        t = Template('''{% load assets %}{% asset_css_url page.picture "collectivity:banner" %}''')
        assert t.render(Context()) == 'url(/media/assets/test.png)'

        page.picture = File(StringIO('test'), 'test2.png')
        page.save()
        t = Template('''{% load assets %}{% asset_url page.picture "collectivity:banner" %}''')
        assert t.render(Context({'page': page})) == '/media/page-pictures/test2.png'

        # pass image to sorl.thumbnail
        t = Template('''{% load assets %}{% asset_url page.picture "collectivity:banner" size="200x200" %}''')
        assert t.render(Context()).startswith('/media/cache/')

        # unless file is missing
        os.remove(page.picture.path)
        del page.picture.file
        t = Template('''{% load assets %}{% asset_url page.picture "collectivity:banner" size="200x200" %}''')
        assert t.render(Context({'page': page})) == '/media/page-pictures/test2.png'

        # unless it's in SVG
        page.picture = File(StringIO('test'), 'test2.svg')
        page.save()
        t = Template('''{% load assets %}{% asset_url page.picture "collectivity:banner" size="200x200" %}''')
        assert t.render(Context({'page': page})) == '/media/page-pictures/test2.svg'

    cell = TextCell()
    with override_settings(COMBO_CELL_ASSET_SLOTS={'data_textcell': {'picture': {'prefix': 'Picture'}}}):
        # no slug
        t = Template(
            '''{% load assets %}{% get_asset cell=cell type='picture' as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context({'cell': cell})) == ''

        # no asset
        cell.slug = 'foobar'
        t = Template(
            '''{% load assets %}{% get_asset cell=cell type='picture' as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context({'cell': cell})) == ''

        # ok
        Asset.objects.create(key=cell.get_asset_slot_key('picture'), asset=File(StringIO('test'), 'test.png'))
        t = Template(
            '''{% load assets %}{% get_asset cell=cell type='picture' as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context({'cell': cell})) == 'BANNER'

        # no context: AttributeError
        t = Template(
            '''{% load assets %}{% get_asset cell=cell type='picture' as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context()) == ''

    with override_settings(COMBO_CELL_ASSET_SLOTS={}):
        # cell type not defined in COMBO_CELL_ASSET_SLOTS
        t = Template(
            '''{% load assets %}{% get_asset cell=cell type='picture' as banner %}{% if banner %}BANNER{% endif %}'''
        )
        assert t.render(Context({'cell': cell})) == ''


def test_startswith():
    t = Template('{% if foo|startswith:"bar" %}ok{% endif %}')
    context = Context({'foo': None})
    assert t.render(context) == ''
    context = Context({'foo': 'xx'})
    assert t.render(context) == ''
    context = Context({'foo': 'bar'})
    assert t.render(context) == 'ok'


def test_endswith_templatetag():
    tmpl = Template('{% if foo|endswith:"bar" %}ok{% endif %}')
    assert tmpl.render(Context({'foo': None})) == ''
    assert tmpl.render(Context({'foo': 'bar-baz'})) == ''
    assert tmpl.render(Context({'foo': 'baz-bar'})) == 'ok'


def test_datetime_templatetags():
    tmpl = Template('{{ plop|datetime }}')
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '2017-12-21 10:32'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '2017-12-21 10:32'
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-21 00:00'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '2017-12-21 00:00'
    assert tmpl.render(Context({'plop': '10h32'})) == ''
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{{ plop|datetime:"d i" }}')
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '21 32'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32:42'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32:42'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '21 00'
    assert tmpl.render(Context({'plop': '10h32'})) == ''
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{% if d1|datetime > d2|datetime %}d1>d2{% else %}d1<=d2{% endif %}')
    assert tmpl.render(Context({'d1': '2017-12-22', 'd2': '2017-12-21'})) == 'd1>d2'
    assert tmpl.render(Context({'d1': '2017-12-21', 'd2': '2017-12-21'})) == 'd1<=d2'
    assert tmpl.render(Context({'d1': '2017-12-21 10:30', 'd2': '2017-12-21 09:00'})) == 'd1>d2'
    assert tmpl.render(Context({'d1': '2017-12-21 10:30', 'd2': '2017-12-21'})) == 'd1>d2'
    assert tmpl.render(Context({'d1': '2017-12-22'})) == 'd1<=d2'
    assert tmpl.render(Context({'d2': '2017-12-22'})) == 'd1<=d2'

    tmpl = Template('{{ plop|date }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32:42'})) == '2017-12-21'
    assert tmpl.render(Context({'plop': '10:32'})) == ''
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{{ plop|date:"d" }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '21'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '21'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '21'
    assert tmpl.render(Context({'plop': '10:32'})) == ''
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{% if d1|date > d2|date %}d1>d2{% else %}d1<=d2{% endif %}')
    assert tmpl.render(Context({'d1': '2017-12-22', 'd2': '2017-12-21'})) == 'd1>d2'
    assert tmpl.render(Context({'d1': '2017-12-21', 'd2': '2017-12-21'})) == 'd1<=d2'
    assert tmpl.render(Context({'d1': '2017-12-22'})) == 'd1<=d2'
    assert tmpl.render(Context({'d2': '2017-12-22'})) == 'd1<=d2'

    tmpl = Template('{{ plop|time }}')
    assert tmpl.render(Context({'plop': '10:32'})) == '10:32 a.m.'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '10:32 a.m.'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '10:32 a.m.'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == 'midnight'
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{{ plop|time:"H i" }}')
    assert tmpl.render(Context({'plop': '10:32'})) == '10 32'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '10 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '10 32'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '00 00'
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    # old fashion, with parse_*
    tmpl = Template('{{ plop|parse_datetime|date:"d i" }}')
    assert tmpl.render(Context({'plop': '2017-12-21 10:32'})) == '21 32'
    assert tmpl.render(Context({'plop': '2017-12-21 10:32:42'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10:32:42'})) == '21 32'
    assert tmpl.render(Context({'plop': '21/12/2017 10h32'})) == '21 32'
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{{ plop|parse_date|date:"d" }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '21'
    assert tmpl.render(Context({'plop': '21/12/2017'})) == '21'
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''

    tmpl = Template('{{ plop|parse_time|date:"H i" }}')
    assert tmpl.render(Context({'plop': '10:32'})) == '10 32'
    assert tmpl.render(Context({'plop': 'x'})) == ''
    assert tmpl.render(Context({'plop': None})) == ''
    assert tmpl.render(Context({'plop': 3})) == ''
    assert tmpl.render(Context({'plop': {'foo': 'bar'}})) == ''
    assert tmpl.render(Context()) == ''


def test_date_maths():
    tmpl = Template('{{ plop|add_days:4 }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-25'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-25'
    tmpl = Template('{{ plop|add_days:"-1" }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-20'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-20'
    tmpl = Template('{{ plop|add_days:1.5 }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-22'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-22'
    tmpl = Template('{{ plop|add_days:"1.5" }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-22'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-22'

    tmpl = Template('{{ plop|add_hours:24 }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-22 00:00'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-22 18:00'
    tmpl = Template('{{ plop|add_hours:"12.5" }}')
    assert tmpl.render(Context({'plop': '2017-12-21'})) == '2017-12-21 12:30'
    assert tmpl.render(Context({'plop': '2017-12-21 18:00'})) == '2017-12-22 06:30'


def test_age_in():
    context = {
        'form_var_datefield': time.strptime('2018-07-31', '%Y-%m-%d'),
        'form_var_datefield2': time.strptime('2018-08-31', '%Y-%m-%d'),
        'form_var_datestring': '2018-07-31',
        'today': datetime.date.today(),
        'now': datetime.datetime.now(),
    }
    for condition_value in (  # hope date is > 2018
        # age_in_days
        '"1970-01-01"|age_in_days > 0',
        '"01/01/1970"|age_in_days > 0',
        '"2500-01-01"|age_in_days < 0',
        '"01/01/2500"|age_in_days < 0',
        'form_var_datefield|age_in_days > 50',
        'form_var_datefield|age_in_days:form_var_datestring == 0',
        'form_var_datefield|age_in_days:form_var_datefield2 == 31',
        'form_var_datefield2|age_in_days:form_var_datefield == -31',
        'form_var_datefield|age_in_days:form_var_datefield == 0',
        'form_var_datestring|age_in_days:form_var_datefield == 0',
        'form_var_datestring|age_in_days:form_var_datestring == 0',
        'today|add_days:-5|age_in_days == 5',
        'today|add_days:5|age_in_days == -5',
        'today|age_in_days == 0',
        # with datetimes
        '"1970-01-01 02:03"|age_in_days > 0',
        '"01/01/1970 02h03"|age_in_days > 0',
        '"2500-01-01 02:03"|age_in_days < 0',
        '"01/01/2500 02h03"|age_in_days < 0',
        'now|age_in_days == 0',
        'now|add_hours:-24|age_in_days == 1',
        'now|add_hours:24|age_in_days == -1',
        '"2010-11-12 13:14"|age_in_days:"2010-11-12 13:14" == 0',
        '"2010-11-12 13:14"|age_in_days:"2010-11-12 12:14" == 0',
        '"2010-11-12 13:14"|age_in_days:"2010-11-12 14:14" == 0',
        '"2010-11-12 13:14"|age_in_days:"2010-11-13 13:13" == 1',
        '"2010-11-12 13:14"|age_in_days:"2010-11-13 13:15" == 1',
        # age_in_hours
        'now|add_hours:-5|age_in_hours == 5',
        'now|add_hours:25|age_in_hours == -24',
        'now|age_in_hours == 0',
        '"2010-11-12 13:14"|age_in_hours:"2010-11-12 13:14" == 0',
        '"2010-11-12 13:14"|age_in_hours:"2010-11-12 12:14" == -1',
        '"2010-11-12 13:14"|age_in_hours:"2010-11-12 14:14" == 1',
        '"2010-11-12 13:14"|age_in_hours:"2010-11-13 13:13" == 23',
        '"2010-11-12 13:14"|age_in_hours:"2010-11-13 13:15" == 24',
        '"1970-01-01 02:03"|age_in_hours > 0',
        '"01/01/1970 02h03"|age_in_hours > 0',
        '"2500-01-01 02:03"|age_in_hours < 0',
        '"01/01/2500 02h03"|age_in_hours < 0',
        # with dates
        '"1970-01-01"|age_in_hours > 0',
        '"01/01/1970"|age_in_hours > 0',
        '"2500-01-01"|age_in_hours < 0',
        '"01/01/2500"|age_in_hours < 0',
        'form_var_datefield|age_in_hours > 1200',
        'form_var_datefield|age_in_hours:form_var_datestring == 0',
        'form_var_datefield|age_in_hours:form_var_datefield2 == 744',  # 31*24
        'form_var_datefield2|age_in_hours:form_var_datefield == -744',
        'form_var_datefield|age_in_hours:form_var_datefield == 0',
        'form_var_datestring|age_in_hours:form_var_datefield == 0',
        'form_var_datestring|age_in_hours:form_var_datestring == 0',
        'today|add_days:-1|age_in_hours >= 24',
        'today|add_days:1|age_in_hours <= -0',
        'today|add_days:1|age_in_hours >= -24',
        'today|age_in_hours >= 0',
        # age_in_years
        '"1970-01-01"|age_in_years > 0',
        '"01/01/1970"|age_in_years > 0',
        '"2500-01-01"|age_in_years < 0',
        '"01/01/2500"|age_in_years < 0',
        'form_var_datefield|age_in_years:"2019-07-31" == 1',
        'form_var_datefield|age_in_years:"2019-09-20" == 1',
        'form_var_datefield|age_in_years:"2020-07-30" == 1',
        'form_var_datefield|age_in_years:"2020-07-31" == 2',
        'form_var_datestring|age_in_years:"2019-07-31" == 1',
        'today|age_in_years == 0',
        'today|add_days:-500|age_in_years == 1',
        'today|add_days:-300|age_in_years == 0',
        'today|add_days:300|age_in_years == -1',
        'now|age_in_years == 0',
        'now|add_days:-500|age_in_years == 1',
        'now|add_days:-300|age_in_years == 0',
        'now|add_days:300|age_in_years == -1',
        '"1970-01-01 02:03"|age_in_years > 0',
        '"2500-01-01 02:03"|age_in_years < 0',
        # age_in_months
        'form_var_datefield|age_in_months:form_var_datefield2 == 1',
        'form_var_datefield2|age_in_months:form_var_datefield == -1',
        'form_var_datefield|age_in_months:"2019-07-31" == 12',
        'form_var_datefield|age_in_months:"2019-08-20" == 12',
        'form_var_datefield|age_in_months:"2019-09-20" == 13',
        'form_var_datestring|age_in_months:"2019-09-20" == 13',
        '"1970-01-01"|age_in_months > 0',
        '"01/01/1970"|age_in_months > 0',
        '"2500-01-01"|age_in_months < 0',
        '"01/01/2500"|age_in_months < 0',
        '"1970-01-01 02:03"|age_in_months > 0',
        '"2500-01-01 02:03"|age_in_months < 0',
        # fail produce empty string
        'foobar|age_in_days == ""',
        '"foobar"|age_in_days == ""',
        '"1970-01-01"|age_in_days:"foobar" == ""',
        'foobar|age_in_hours == ""',
        '"foobar"|age_in_hours == ""',
        '"1970-01-01"|age_in_hours:"foobar" == ""',
        'foobar|age_in_years == ""',
        '"foobar"|age_in_years == ""',
        '"1970-01-01"|age_in_years:"foobar" == ""',
        'foobar|age_in_months == ""',
        '"foobar"|age_in_months == ""',
        '"1970-01-01"|age_in_months:"foobar" == ""',
    ):
        tmpl = Template('{%% if %s %%}Good{%% endif %%}' % condition_value)
        assert tmpl.render(Context(context)) == 'Good'


@pytest.mark.parametrize(
    'value, expected',
    [
        (None, False),
        ('', False),
        ('foobar', False),
        (42, False),
        ('1970-06-15T12:01:03', True),
        ('2500-06-15T12:01:02', False),
        ('1970-01-01 02:03', True),
        ('2500-01-01 02:03', False),
        ('01/01/1970 02h03', True),
        ('01/01/2500 02h03', False),
        ('1970-01-01', True),
        ('2500-01-01', False),
        ('01/01/1970', True),
        ('01/01/2500', False),
        (datetime.datetime(1970, 6, 15, 12, 1, 3), True),
        (datetime.datetime(2500, 6, 15, 12, 1, 2), False),
        (datetime.date(1970, 6, 15), True),
        (datetime.date(2500, 6, 15), False),
        (datetime.datetime.now(), True),
        (datetime.datetime.now() + datetime.timedelta(hours=1), False),
        (now(), True),
        (now() + datetime.timedelta(hours=1), False),
        (datetime.date.today(), True),
        (datetime.date.today() + datetime.timedelta(days=1), False),
    ],
)
def test_datetime_in_past(value, expected):
    t = Template('{{ value|datetime_in_past }}')
    assert t.render(Context({'value': value})) == str(expected)


def test_adjust_to_week_monday():
    t = Template('{{ value|adjust_to_week_monday|date:"Y-m-d" }}')
    assert t.render(Context({'value': '2021-06-13'})) == '2021-06-07'
    t = Template('{{ value|adjust_to_week_monday|date:"Y-m-d" }}')
    assert t.render(Context({'value': '2021-06-14'})) == '2021-06-14'
    t = Template('{{ value|adjust_to_week_monday|date:"Y-m-d" }}')
    assert t.render(Context({'value': datetime.datetime(2021, 6, 14, 0, 0)})) == '2021-06-14'


def test_iterate_days_until():
    t = Template(
        '{% for day in value|iterate_days_until:value2 %}{{ day|date:"Y-m-d" }}{% if not forloop.last %}, {% endif %}{% endfor %}'
    )
    assert (
        t.render(Context({'value': '2021-06-13', 'value2': '2021-06-16'}))
        == '2021-06-13, 2021-06-14, 2021-06-15, 2021-06-16'
    )

    assert t.render(Context({'value': 'error1', 'value2': 'error2'})) == ''


def test_phonenumber_fr():
    t = Template('{{ number|phonenumber_fr }}')
    assert t.render(Context({'number': '01 23 45 67 89'})) == '01 23 45 67 89'
    assert t.render(Context({'number': '0 1 23 45 67 89'})) == '01 23 45 67 89'
    assert t.render(Context({'number': '0123456789'})) == '01 23 45 67 89'
    assert t.render(Context({'number': '01.23.45.67.89'})) == '01 23 45 67 89'
    assert t.render(Context({'number': '01 23 45 67 89'})) == '01 23 45 67 89'

    assert t.render(Context({'number': '00 33 1 23 45 67 89'})) == '00 33 1 23 45 67 89'
    assert t.render(Context({'number': '00 33 1 23 45 67 89'})) == '00 33 1 23 45 67 89'
    assert t.render(Context({'number': '+33 1 23 45 67 89'})) == '+33 1 23 45 67 89'
    assert t.render(Context({'number': '+33 (0)1 23 45 67 89'})) == '+33 1 23 45 67 89'

    # drom
    assert t.render(Context({'number': '02 62 11 22 33'})) == '02 62 11 22 33'
    assert t.render(Context({'number': '00 262 11 22 33'})) == '00 262 11 22 33'
    assert t.render(Context({'number': '+262 112233'})) == '+262 11 22 33'

    t = Template('{{ number|phonenumber_fr:"." }}')
    assert t.render(Context({'number': '01 23 45 67 89'})) == '01.23.45.67.89'

    # unknown
    assert t.render(Context({'number': '12 3'})) == '12 3'
    assert t.render(Context({'number': 'bla bla'})) == 'bla bla'
    assert t.render(Context({'number': None})) == 'None'
    t = Template('{{ number|decimal|phonenumber_fr }}')
    assert t.render(Context({'number': '1,33'})) == '1.33'


def test_django_contrib_humanize_filters():
    tmpl = Template('{{ foo|intcomma }}')
    assert tmpl.render(Context({'foo': 10000})) == '10,000'
    assert tmpl.render(Context({'foo': '10000'})) == '10,000'
    with override_settings(LANGUAGE_CODE='fr-fr'):
        assert tmpl.render(Context({'foo': 10000})) == '10 000'
        assert tmpl.render(Context({'foo': '10000'})) == '10 000'


@mock.patch('combo.utils.requests_wrapper.RequestsSession.request')
def test_user_id_for_service_filter(mock_request):
    response = {'data': {'user': {'id': '424242'}}}
    mock_response = Response()
    mock_response.status_code = 200
    mock_response.json = lambda: response
    mock_request.return_value = mock_response

    tmpl = Template('{{ request.user|user_id_for_service:"fake-service_slug" }}')
    request = RequestFactory().get('/')
    request.user = get_user_from_name_id('fake-user_uuid')
    context = Context({'request': request})

    # OK tests
    assert tmpl.render(context) == '424242'
    mock_request.assert_called_once()
    assert mock_request.call_args[0] == (
        'GET',
        'https://authentic.example.org/api/users/fake-user_uuid/service/fake-service_slug/',
    )
    assert mock_request.call_args[1]['timeout'] == 5

    tmpl = Template('{{ "str"|user_id_for_service:"slug" }}')
    assert tmpl.render(context) == '424242'
    assert mock_request.call_count == 2
    assert mock_request.call_args[0] == ('GET', 'https://authentic.example.org/api/users/str/service/slug/')

    # KO tests
    mock_response.status_code = 404
    assert tmpl.render(context) == ''
    assert mock_request.call_count == 3

    mock_response.status_code = 200
    for bad_json in (
        None,
        'blah',
        ['1', '2'],
        {},
        {'data': None},
        {'data': {'user': None}},
        {'data': {'user': {}}},
        {'data': {'user': {'id': None}}},
    ):
        mock_response.json = lambda: bad_json  # noqa pylint: disable=cell-var-from-loop
        assert tmpl.render(context) == ''

    def crash_json():
        raise json.JSONDecodeError(msg='bad json', doc='plop', pos=0)

    mock_response.json = crash_json
    assert tmpl.render(context) == ''

    assert mock_request.call_count == 12

    tmpl = Template('{{ ""|user_id_for_service:"fake-service_slug" }}')  # empty uuid
    assert tmpl.render(context) == ''
    tmpl = Template('{{ request|user_id_for_service:"fake-service_slug" }}')  # not a user object
    assert tmpl.render(context) == ''
    tmpl = Template('{{ request.user|user_id_for_service:"fake-service_slug" }}')
    request.user = None  # no user in request
    context = Context({'request': request})
    assert tmpl.render(context) == ''

    assert mock_request.call_count == 12
