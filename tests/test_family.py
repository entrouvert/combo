import json
from unittest import mock

import pytest
from django.test.client import RequestFactory
from publik_django_templatetags.wcs.context_processors import Cards
from pyquery import PyQuery

from combo.apps.family.models import WeeklyAgendaCell
from combo.data.models import Page

from .test_manager import login
from .utils import manager_submit_cell

pytestmark = pytest.mark.django_db


@pytest.fixture
def context():
    ctx = {'cards': Cards(), 'request': RequestFactory().get('/')}
    ctx['request'].user = None
    ctx['request'].session = {}
    return ctx


class MockUser:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False

    def get_name_id(self):
        return None


class MockUserWithNameId:
    email = 'foo@example.net'
    is_authenticated = True
    is_anonymous = False
    is_superuser = False

    def get_name_id(self):
        return 'xyz'


class MockedRequestResponse(mock.Mock):
    status_code = 200

    def json(self):
        return json.loads(self.content)


def test_manager_weeklyagenda_cell(app, admin_user):
    page = Page.objects.create(title='Family', slug='family', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)

    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)
    assert ('data-cell-reference="%s"' % cell.get_reference()) in resp
    assert cell.agenda_type == 'manual'
    assert cell.agenda_references_template == ''
    assert cell.agenda_categories == ''
    assert cell.start_date_filter == ''
    assert cell.end_date_filter == ''
    resp.forms[0]['c%s-agenda_type' % cell.get_reference()].value = 'subscribed'
    resp.forms[0]['c%s-agenda_references_template' % cell.get_reference()].value = 'foo,bar'
    resp.forms[0]['c%s-agenda_categories' % cell.get_reference()].value = 'bar,foo'
    resp.forms[0]['c%s-start_date_filter' % cell.get_reference()].value = '{{ start_date }}'
    resp.forms[0]['c%s-end_date_filter' % cell.get_reference()].value = '{{ end_date }}'
    manager_submit_cell(resp.form)
    cell.refresh_from_db()
    assert cell.agenda_type == 'subscribed'
    assert cell.agenda_references_template == ''
    assert cell.agenda_categories == 'bar,foo'
    assert cell.start_date_filter == '{{ start_date }}'
    assert cell.end_date_filter == '{{ end_date }}'
    assert cell.booking_form_url == ''
    assert cell.booking_button_title == ''
    assert cell.booking_button_position == 'bottom'
    assert cell.booking_not_paid_key == 'reglement'
    assert cell.booking_not_paid_value == 'Non-réglé'
    assert cell.booking_without_invoice_key == 'facture_guichet'
    assert cell.booking_without_invoice_value == 'Sans facture'

    resp.forms[0]['c%s-agenda_type' % cell.get_reference()].value = 'manual'
    resp.forms[0]['c%s-agenda_references_template' % cell.get_reference()].value = 'foo,bar'
    resp.forms[0]['c%s-agenda_categories' % cell.get_reference()].value = 'bar,foo'
    resp.forms[0]['c%s-booking_form_url' % cell.get_reference()].value = 'http://example.com/foobar/'
    resp.forms[0]['c%s-booking_button_title' % cell.get_reference()].value = 'Foo Bar'
    resp.forms[0]['c%s-booking_button_position' % cell.get_reference()].value = 'top'
    resp.forms[0]['c%s-booking_not_paid_key' % cell.get_reference()].value = 'not-paid-key'
    resp.forms[0]['c%s-booking_not_paid_value' % cell.get_reference()].value = 'not-paid-value'
    resp.forms[0]['c%s-booking_without_invoice_key' % cell.get_reference()].value = 'without-invoice-key'
    resp.forms[0]['c%s-booking_without_invoice_value' % cell.get_reference()].value = 'without-invoice-value'
    manager_submit_cell(resp.form)
    cell.refresh_from_db()
    assert cell.agenda_type == 'manual'
    assert cell.agenda_references_template == 'foo,bar'
    assert cell.agenda_categories == ''
    assert cell.start_date_filter == '{{ start_date }}'
    assert cell.end_date_filter == '{{ end_date }}'
    assert cell.booking_form_url == 'http://example.com/foobar/'
    assert cell.booking_button_title == 'Foo Bar'
    assert cell.booking_button_position == 'top'
    assert cell.booking_not_paid_key == 'not-paid-key'
    assert cell.booking_not_paid_value == 'not-paid-value'
    assert cell.booking_without_invoice_key == 'without-invoice-key'
    assert cell.booking_without_invoice_value == 'without-invoice-value'


def test_manager_weeklyagenda_cell_tabs(app, admin_user):
    page = Page.objects.create(title='Family', slug='family', template_name='standard')
    WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)
    login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    assert not resp.pyquery('[data-tab-slug="general"] input[name$="title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="booking_button_title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] select[name$="booking_button_position"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="booking_not_paid_key"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="booking_not_paid_value"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="booking_without_invoice_key"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="booking_without_invoice_value"]')


def test_weeklyagenda_cell_user_external_id(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0, agenda_type='manual')

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )

    context['request'].user = MockUserWithNameId()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )

    cell.user_external_template = 'some-key:{{ user_nameid }}'  # check that templating is ok
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=some-key:xyz&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )


def test_weeklyagenda_cell_agenda_references_template(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0, agenda_type='manual')

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )

    cell.agenda_references_template = 'some-agenda,other-agenda'
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=some-agenda,other-agenda&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )

    context['request'].user = MockUserWithNameId()
    cell.agenda_references_template = (
        '{{ cards|objects:"foo"|get_full|first|get:"fields"|get:"bar"|default:"" }}'
        ',some-agenda,other-agenda,{{ user_nameid }}'  # check that templating is ok
    )
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[1][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=,some-agenda,other-agenda,xyz&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )


def test_weeklyagenda_cell_agenda_subscribed(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(
        page=page, placeholder='content', order=0, agenda_type='subscribed'
    )

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?subscribed=all&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )


def test_weeklyagenda_cell_agenda_agenda_categories(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(
        page=page,
        placeholder='content',
        order=0,
        agenda_type='subscribed',
        agenda_categories='foo,bar,foobar',
    )

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?subscribed=foo,bar,foobar&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )

    context['request'].user = MockUserWithNameId()
    cell.agenda_categories = 'foo,bar,foobar,{{ user_nameid }}'  # check that templating is ok
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?subscribed=foo,bar,foobar,xyz&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet'
        )


@pytest.mark.parametrize(
    'agenda_type,agenda_param', [('manual', 'agendas='), ('subscribed', 'subscribed=all')]
)
def test_weeklyagenda_cell_filter_dates(context, agenda_type, agenda_param):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0, agenda_type=agenda_type)

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?%s&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet' % agenda_param
        )

    cell.start_date_filter = '2021-09-01'
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?%s&date_start=2021-09-01&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet' % agenda_param
        )

    cell.end_date_filter = '2022-08-31'
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?%s&date_start=2021-09-01&date_end=2022-08-31&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet' % agenda_param
        )

    context['request'].user = MockUserWithNameId()
    cell.start_date_filter = ''
    cell.end_date_filter = '{{ user_nameid }}'  # check that templating is ok
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?%s&date_start=&date_end=xyz&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,facture_guichet' % agenda_param
        )


def test_weeklyagenda_cell_agenda_not_paid_key(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(
        page=page, placeholder='content', order=0, booking_not_paid_key='foobar'
    )

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=foobar,facture_guichet'
        )


def test_weeklyagenda_cell_agenda_without_invoice_key(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(
        page=page, placeholder='content', order=0, booking_without_invoice_key='foobar'
    )

    context['request'].user = MockUser()
    context['synchronous'] = True  # to get fresh content

    data = {'data': []}
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        cell.render(context)
        assert requests_get.call_args_list[0][0][0] == (
            'http://chrono.example.org/api/agendas/datetimes/'
            '?agendas=&date_start=&date_end=&user_external_id=&show_past_events=true&with_status=true'
            '&extra_data_keys=reglement,foobar'
        )


def test_weeklyagenda_cell_booking_button(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)
    data = {
        'data': [
            {'text': 'Foo', 'date': '2022-02-28'},
        ]
    }

    context['synchronous'] = True  # to get fresh content
    context['request'].user = MockUserWithNameId()

    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert 'pk-button weekly-agenda-cell--edit-btn' not in result
        assert 'data-edit-url' not in result

    cell.booking_form_url = 'http://example.com/foobar/'
    cell.booking_button_title = 'Foo Bar'
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert 'pk-button weekly-agenda-cell--edit-btn' in result
        assert 'data-edit-url="http://example.com/foobar/?current=2022-02-28"' in result
        assert PyQuery(result)('.weekly-agenda-cell--edit-btn.bottom').text() == 'Foo Bar'

    cell.booking_form_url = 'http://example.com/foobar/?user={{ user_nameid }}'
    cell.booking_button_title = 'Foo {{ 39|add:3 }} Bar'
    cell.booking_button_position = 'top'
    cell.save()
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert 'pk-button weekly-agenda-cell--edit-btn' in result
        assert 'data-edit-url="http://example.com/foobar/?user=xyz&current=2022-02-28"' in result
        assert PyQuery(result)('.weekly-agenda-cell--edit-btn.top').text() == 'Foo 42 Bar'


def test_weeklyagenda_cell_event_full(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)
    data = {
        'data': [
            {'label': 'Foo', 'date': '2022-02-28', 'places': {'full': True}, 'status': 'booked'},
            {'label': 'Bar', 'date': '2022-03-01', 'places': {'full': True}, 'status': 'free'},
        ]
    }

    context['synchronous'] = True  # to get fresh content
    context['request'].user = MockUserWithNameId()

    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert 'Foo</span>' in result
        assert 'Bar (full)</span>' in result


def test_weeklyagenda_cell_agenda_not_paid(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)

    context['synchronous'] = True  # to get fresh content
    context['request'].user = MockUserWithNameId()

    data = {
        'data': [
            {'label': 'Foo 1', 'date': '2022-02-26'},
            {'label': 'Foo 2', 'date': '2022-02-27', 'extra_data__reglement': 'Non-réglé'},
            {'label': 'Foo 3', 'date': '2022-02-28', 'extra_data__reglement': 'FooBar'},
        ]
    }
    span_pending = (
        '<span title="pending payment" class="weekly-agenda-cell--pending-payment">'
        '<span class="weekly-agenda-cell--pending-payment-icon" aria-hidden="true"></span>'
        '<span class="sr-only">(pending payment) </span>'
        '</span>'
    )
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 1</span></div>' in result
        assert (
            f'<div class="weekly-agenda-cell--activity-label">{span_pending}<span>Foo 2</span></div>'
            in result
        )
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 3</span></div>' in result

    cell.booking_not_paid_key = 'foobar'
    cell.booking_not_paid_value = 'bazbaz'
    cell.save()
    data = {
        'data': [
            {'label': 'Foo 1', 'date': '2022-02-26'},
            {'label': 'Foo 2', 'date': '2022-02-27', 'extra_data__foobar': 'Non-réglé'},
            {'label': 'Foo 3', 'date': '2022-02-28', 'extra_data__foobar': 'bazbaz'},
        ]
    }
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 1</span></div>' in result
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 2</span></div>' in result
        assert (
            f'<div class="weekly-agenda-cell--activity-label">{span_pending}<span>Foo 3</span></div>'
            in result
        )


def test_weeklyagenda_cell_agenda_without_invoice(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)

    context['synchronous'] = True  # to get fresh content
    context['request'].user = MockUserWithNameId()

    data = {
        'data': [
            {'label': 'Foo 1', 'date': '2022-02-26'},
            {'label': 'Foo 2', 'date': '2022-02-27', 'extra_data__facture_guichet': 'Sans facture'},
            {'label': 'Foo 3', 'date': '2022-02-28', 'extra_data__facture_guichet': 'FooBar'},
        ]
    }
    span_without_invoice = (
        '<span title="without invoice" class="weekly-agenda-cell--without-invoice">'
        '<span class="weekly-agenda-cell--without-invoice-icon" aria-hidden="true"></span>'
        '<span class="sr-only">(without invoice) </span>'
        '</span>'
    )
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 1</span></div>' in result
        assert (
            f'<div class="weekly-agenda-cell--activity-label">{span_without_invoice}<span>Foo 2</span></div>'
            in result
        )
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 3</span></div>' in result

    cell.booking_without_invoice_key = 'foobar'
    cell.booking_without_invoice_value = 'bazbaz'
    cell.save()
    data = {
        'data': [
            {'label': 'Foo 1', 'date': '2022-02-26'},
            {'label': 'Foo 2', 'date': '2022-02-27', 'extra_data__foobar': 'Sans facture'},
            {'label': 'Foo 3', 'date': '2022-02-28', 'extra_data__foobar': 'bazbaz'},
        ]
    }
    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 1</span></div>' in result
        assert '<div class="weekly-agenda-cell--activity-label"><span>Foo 2</span></div>' in result
        assert (
            f'<div class="weekly-agenda-cell--activity-label">{span_without_invoice}<span>Foo 3</span></div>'
            in result
        )


def test_weeklyagenda_cell_event_display_template(context):
    page = Page.objects.create(title='Family', slug='index', template_name='standard')
    cell = WeeklyAgendaCell.objects.create(page=page, placeholder='content', order=0)
    data = {
        'data': [
            {'label': 'Foo', 'date': '2022-02-28', 'text': 'xxx'},
            {'label': 'Bar', 'date': '2022-03-01', 'text': 'Other', 'has_display_template': True},
        ]
    }

    context['synchronous'] = True  # to get fresh content
    context['request'].user = MockUserWithNameId()

    with mock.patch('requests.Session.get') as requests_get:
        requests_get.return_value = MockedRequestResponse(content=json.dumps(data))
        result = cell.render(context)
        assert 'Foo</span>' in result
        assert 'Other</span>' in result
