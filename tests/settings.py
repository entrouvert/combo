import os
import tempfile

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.postgresql_psycopg2'),
        'NAME': 'combo-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:35],
    }
}

LANGUAGE_CODE = 'en-us'

KNOWN_SERVICES = {
    'wcs': {
        'default': {
            'title': 'test',
            'url': 'http://127.0.0.1:8999/',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://127.0.0.1:8999/backoffice/',
            'secondary': False,
        },
        'other': {
            'title': 'test2',
            'url': 'http://127.0.0.2:8999/',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://127.0.0.2:8999/backoffice/',
            'secondary': True,
        },
    },
    'authentic': {
        'idp': {
            'url': 'https://authentic.example.org',
            'title': 'a2',
            'backoffice-menu-url': 'https://authentic.example.org/manage/',
        },
    },
    'passerelle': {
        'default': {
            'title': 'test',
            'url': 'http://example.org',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://example.org/manage/',
        }
    },
    'chrono': {
        'default': {
            'title': 'test',
            'url': 'http://chrono.example.org',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://chrono.example.org/manage/',
            'secondary': False,
        },
        'other': {
            'title': 'other',
            'url': 'http://other.chrono.example.org',
            'secret': 'combo',
            'orig': 'combo',
            'backoffice-menu-url': 'http://other.chrono.example.org/manage/',
            'secondary': True,
        },
    },
}

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

LINGO_API_SIGN_KEY = '12345'
LINGO_SIGNATURE_KEY = '54321'

COMBO_DASHBOARD_ENABLED = True

MEDIA_ROOT = tempfile.mkdtemp('combo-test')

if 'DISABLE_MIGRATIONS' in os.environ:

    class DisableMigrations:
        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return 'notmigrations'

    MIGRATION_MODULES = DisableMigrations()


BOOKING_CALENDAR_CELL_ENABLED = True
LEGACY_CHART_CELL_ENABLED = True
PUBLIK_FAMILY_CELL_ENABLED = True
CHART_FILTERS_CELL_ENABLED = True

USER_PROFILE_CONFIG = {
    'fields': [
        {
            'name': 'first_name',
            'kind': 'string',
            'label': 'First Name',
            'user_visible': True,
        },
        {
            'name': 'birthdate',
            'kind': 'birthdate',
            'label': 'Birth Date',
            'user_visible': True,
        },
        {
            'name': 'phone',
            'kind': 'phone_number',
            'label': 'Phone',
            'user_visible': True,
        },
    ]
}

LEGACY_URLS_MAPPING = {'old.org': 'new.org'}

PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher']

REST_FRAMEWORK = {
    # this is the value set by hobo but by explicitely setting it
    # we avoid a collision with django-webtest erasing the setting
    # while patching it
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
    ],
    'EXCEPTION_HANDLER': 'combo.utils.api.exception_handler',
}

DEFAULT_COUNTRY_CODE = '33'
