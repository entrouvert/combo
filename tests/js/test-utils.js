import { test } from 'vitest'

export const domTest = test.extend({
  // Empty {} is required by vitest
  // eslint-disable-next-line no-empty-pattern
  appendToDom: async ({}, use) => {
    const wrappers = []
    const appendToDom = (htmlContent) => {
      const wrapper = document.createElement('div')
      wrapper.innerHTML = htmlContent
      document.appendChild(wrapper)
      wrappers.push(wrapper)
      return wrapper
    }
    await use(appendToDom)
    for (const wrapper of wrappers) {
      wrapper.remove()
    }
  },
})

export async function flushPromises () {
  await new Promise((resolve) => setTimeout(resolve))
}
