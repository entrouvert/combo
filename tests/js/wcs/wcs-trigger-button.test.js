import { expect, vi} from 'vitest'
import { domTest, flushPromises } from '../test-utils.js'
import '../../../combo/apps/wcs/static/js/combo.wcs-trigger-button.js'

domTest('render button label', async ({appendToDom}) => {
  let dom = appendToDom(`
    <wcs-trigger-button label="Test label"></wcs-trigger-button>
  `)
  const innerButton = dom.querySelector('button')
  expect(innerButton.innerText).toBe('Test label')
})

domTest('show button if trigger-url attribute is set', async ({appendToDom}) => {
  const dom = appendToDom(`
    <wcs-trigger-button label="Test label" trigger-url="https://dummy.org"></wcs-trigger-button>
  `)
  const triggerButton = dom.querySelector('wcs-trigger-button')
  const innerButton = dom.querySelector('button')
  expect(triggerButton.hidden).toBe(false)
  expect(innerButton.disabled).toBe(false)
})

domTest('hide button if unavailable and unavailable-mode is hide', async ({appendToDom}) => {
  const dom = appendToDom(`
    <wcs-trigger-button unavailable-mode="hide" unavailable="unavailable" ></wcs-trigger-button>
  `)
  const triggerButton = dom.querySelector('wcs-trigger-button')
  const innerButton = dom.querySelector('button')
  expect(triggerButton.hidden).toBe(true)
  expect(innerButton.disabled).toBe(false)
})

domTest('disable button if unavailable is not set and unavailable-mode is disable', async ({appendToDom}) => {
  const dom = appendToDom(`
    <wcs-trigger-button unavailable="unavailable" unavailable-mode="disable"></wcs-trigger-button>
  `)
  const triggerButton = dom.querySelector('wcs-trigger-button')
  const innerButton = dom.querySelector('button')
  expect(triggerButton.hidden).toBe(false)
  expect(innerButton.disabled).toBe(true)
})

export const triggerTest = domTest.extend({
  clickTrigger: async ({appendToDom}, use) => {
    const load = async (domContent, mockFetch) => {
      const dom = appendToDom(domContent)

      fetch.mockImplementationOnce(mockFetch)

      const innerButton = dom.querySelector('button')
      innerButton.dispatchEvent(new Event('click', {bubbles: true}))
      await flushPromises()

      return dom
    }

    const alertBackup = global.alert
    const confirmBackup = global.confirm
    const fetchBackup = global.fetch

    global.fetch = vi.fn()
    global.alert = vi.fn()
    global.confirm = vi.fn()

    await use(load)

    global.fetch = fetchBackup
    global.confirm = confirmBackup
    global.alert = alertBackup
  },
})

triggerTest('card-action is called on button click', async ({clickTrigger}) => {
  const dom = await clickTrigger(
    `<wcs-trigger-button
      card-action-url="https://card-action.test"
      card-id="1"
      card-trigger-id="action"
      csrf-token="test-csrf-token">
    </wcs-trigger-button>`,
    async (url, options) => {
      expect(url).toBe('https://card-action.test')
      expect(options.headers['X-CSRFToken']).toBe('test-csrf-token')
      expect(options.method).toBe('POST')
      expect(new URLSearchParams(options.body).toString()).toBe('card_id=1&trigger_id=action')
      return {ok: true, json: async () => ({err: 0}) }
    },
  )

  const button = dom.querySelector('wcs-trigger-button')
  const innerButton = dom.querySelector('wcs-trigger-button button')

  expect(fetch).toHaveBeenCalledOnce()
  expect(button.classList.contains('success')).toBe(true)
  // Button should be disabled after successfull call
  expect(innerButton.disabled).toBe(true)
})

triggerTest('error message is shown on unsuccessfull trigger call', async ({clickTrigger}) => {
  const dom = await clickTrigger(
    '<wcs-trigger-button error-message="Error message"></wcs-trigger-button>',
    async () => ({ok: false, json: async () => ({err: 1}) }),
  )

  expect(fetch).toHaveBeenCalledOnce()
  expect(alert).toHaveBeenCalledWith('Error message')

  const button = dom.querySelector('wcs-trigger-button')
  const innerButton = dom.querySelector('wcs-trigger-button button')

  expect(innerButton.disabled).toBe(false)
  expect(button.classList.contains('success')).toBe(false)
})

triggerTest('confirmation message is shown if set', async ({clickTrigger}) => {
  confirm.mockImplementationOnce(() => true)
  await clickTrigger(
    `<wcs-trigger-button confirmation-message="Confirmation">
    </wcs-trigger-button>`,
    async () => ({ok: true, json: async () => ({}) }),
  )

  expect(confirm).toHaveBeenCalledWith('Confirmation')
  expect(fetch).toHaveBeenCalledOnce()
})

triggerTest('trigger is not called if confirmation is dismissed', async ({clickTrigger}) => {
  confirm.mockImplementationOnce(() => false)
  await clickTrigger(
    `<wcs-trigger-button confirmation-message="Confirmation">
    </wcs-trigger-button>`,
  )

  expect(confirm).toHaveBeenCalledWith('Confirmation')
  expect(fetch).not.toHaveBeenCalledOnce()
})

triggerTest('card-action is called with page qs and ?ctx= on button click', async ({clickTrigger}) => {
  document.location.search = '?offre=foobar'
  await clickTrigger(
    `<div class="cell" data-extra-context="1234"><wcs-trigger-button
      card-action-url="https://card-action.test"
      card-id="1"
      card-trigger-id="action"
      csrf-token="test-csrf-token">
    </wcs-trigger-button></div>`,
    async (url) => {
      expect(url).toBe('https://card-action.test?offre=foobar&ctx=1234')
      return {ok: true, json: async () => ({}) }
    },
  )

  expect(fetch).toHaveBeenCalledOnce()
})
