# combo - content management system
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.import pytest

import re

import pytest
from django.test.client import RequestFactory

from combo.apps.kb.models import LatestPageUpdatesCell
from combo.data.models import CellBase, Page, TextCell

from .utils import manager_submit_cell

pytestmark = pytest.mark.django_db


def login(app, username='admin', password='admin'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


@pytest.mark.freeze_time('2020-01-01')
def test_manage_updated_pages_cell(app, admin_user):
    page = Page(title='example page', slug='example-page')
    page.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id, status=200)

    add_cell_url = resp.pyquery('optgroup[label="Knowledge Base"] option').attr['data-add-url']
    assert 'kb_latestpageupdatescell' in add_cell_url
    resp = app.get(add_cell_url, status=302)
    resp = resp.follow()
    cells = CellBase.get_cells(page_id=page.id)
    assert ('data-cell-reference="%s"' % cells[0].get_reference()) in resp.text
    manager_submit_cell(resp.forms[0])

    resp = app.get('/example-page/', status=200)
    assert 'on 2020-01-01' in resp.pyquery('div.latest-page-updates-cell').text()


def test_updated_pages_cell_new_page(freezer):
    freezer.move_to('2020-01-01')
    page = Page(title='example page', slug='example-page')
    page.save()
    cell = LatestPageUpdatesCell(page=page, order=0)
    cell.save()
    ctx = {}
    assert 'on 2020-01-01' in cell.render(ctx)
    assert page.snapshot is None
    assert '(new page)' in cell.render(ctx)

    freezer.move_to('2020-01-08')
    assert '(new page)' not in cell.render(ctx)


def test_updated_pages_cell_limit(freezer):
    for number in range(1, 4):
        page = Page(title='page %s' % number, slug='page-%i' % number)
        page.save()
    cell = LatestPageUpdatesCell(page=page, order=0)
    cell.save()
    ctx = {}
    assert cell.render(ctx).count('<li') == 3

    cell.limit = 2
    cell.save()
    assert cell.render(ctx).count('<li') == 2


def test_updated_pages_cell_sort(freezer):
    for day in [30, 11, 2, 22]:
        freezer.move_to('2020-01-%02i' % day)
        page = Page(title='page %s' % day, slug='page-%i' % day)
        page.save()
        cell = TextCell(page=page, order=0, slug='cell-%i' % day, text='foo')
        cell.save()
    cell = LatestPageUpdatesCell(page=page, order=0, limit=3)
    cell.save()
    ctx = {}
    assert len(re.findall('<time', cell.render(ctx))) == 3
    times = re.finditer('<time[^>]*>([^<]*)</time>', cell.render(ctx))
    assert 'on 2020-01-30' in next(times).group(0)
    assert 'on 2020-01-22' in next(times).group(0)
    assert 'on 2020-01-11' in next(times).group(0)

    # update contained cell
    freezer.move_to('2020-01-31')
    text_cell = TextCell.objects.get(slug='cell-2')
    text_cell.text = 'bar'
    text_cell.save()
    assert len(re.findall('<time', cell.render(ctx))) == 3
    times = re.finditer('<time[^>]*>([^<]*)</time>', cell.render(ctx))
    assert 'on 2020-01-31' in next(times).group(0)
    assert 'on 2020-01-30' in next(times).group(0)
    assert 'on 2020-01-22' in next(times).group(0)


def test_updated_pages_cell_root_page():
    """1  3
    |
    2
    """
    page1 = Page(title='page-1', slug='page-1')
    page1.save()
    page2 = Page(title='page-2', slug='page-2')
    page2.parent_id = page1.id
    page2.save()
    page3 = Page(title='page-3', slug='page-3')
    page3.save()
    cell = LatestPageUpdatesCell(slug='me')
    cell.page = page3
    cell.order = 0
    cell.save()
    ctx = {}
    assert cell.render(ctx).count('<li') == 3

    cell.root_page = Page.objects.get(slug='page-1')
    assert cell.render(ctx).count('<li') == 2
    cell.root_page = Page.objects.get(slug='page-2')
    assert cell.render(ctx).count('<li') == 1

    Page.objects.get(slug='page-2').delete()
    cell = CellBase.get_cells(slug='me')[0]  # reload cell
    assert cell.render(ctx).count('<li') == 2


def test_updated_pages_cell_visibility(freezer, admin_user):
    Page.objects.all().delete()
    page = Page(title='example page', slug='example-page')
    page.save()
    Page(title='second example page', slug='second-example-page').save()
    Page(title='third example page', slug='third-example-page', public=False).save()
    cell = LatestPageUpdatesCell(page=page, order=0)
    cell.save()

    request = RequestFactory().get('/')
    request.user = None
    ctx = {'request': request}
    rendered = cell.render(ctx)
    assert rendered.count('<a') == 2
    assert '/example-page' in rendered
    assert '/second-example-page' in rendered
    assert '/third-example-page' not in rendered

    request.user = admin_user
    rendered = cell.render(ctx)
    assert rendered.count('<a') == 3
    assert '/example-page' in rendered
    assert '/second-example-page' in rendered
    assert '/third-example-page' in rendered
