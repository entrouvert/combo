from django.db import connection
from django.db.migrations.executor import MigrationExecutor


def test_page_snapshot_with_old_lingo_invoices_cells_migration(transactional_db):
    migrate_from = [('lingo', '0052_invoices_cell')]
    migrate_to = [('lingo', '0053_invoices_cell')]

    executor = MigrationExecutor(connection)
    old_apps = executor.loader.project_state(migrate_from).apps
    executor.migrate(migrate_from)

    pagesnapshot_class = old_apps.get_model('data', 'PageSnapshot')

    snapshot = pagesnapshot_class.objects.create(
        serialization={
            'cells': [
                {
                    'model': 'lingo.activeitems',
                    'fields': {
                        'slug': 'foo',
                        'text': 'Foo',
                        'order': 1,
                        'regie': 'blah',
                        'title': 'Fooo',
                        'groups': [],
                        'public': True,
                        'condition': None,
                        'placeholder': 'content',
                        'hide_if_empty': True,
                        'template_name': None,
                        'extra_css_class': '',
                        'last_update_timestamp': '2023-04-11T15:28:14.052Z',
                        'restricted_to_unlogged': False,
                    },
                },
                {
                    'model': 'lingo.itemshistory',
                    'fields': {
                        'slug': 'bar',
                        'text': 'Bar',
                        'order': 2,
                        'regie': 'blah',
                        'title': 'Baar',
                        'groups': [],
                        'public': True,
                        'condition': None,
                        'placeholder': 'content',
                        'template_name': None,
                        'extra_css_class': '',
                        'last_update_timestamp': '2023-04-11T15:28:27.174Z',
                        'restricted_to_unlogged': False,
                    },
                },
            ]
        }
    )

    executor = MigrationExecutor(connection)
    executor.migrate(migrate_to)
    executor.loader.build_graph()

    apps = executor.loader.project_state(migrate_to).apps
    pagesnapshot_class = apps.get_model('data', 'PageSnapshot')

    snapshot = pagesnapshot_class.objects.get()
    assert snapshot.serialization['cells'][0] == {
        'model': 'lingo.invoicescell',
        'fields': {
            'slug': 'foo',
            'text': 'Foo',
            'order': 1,
            'regie': 'blah',
            'title': 'Fooo',
            'groups': [],
            'public': True,
            'condition': None,
            'placeholder': 'content',
            'hide_if_empty': True,
            'template_name': None,
            'extra_css_class': '',
            'last_update_timestamp': '2023-04-11T15:28:14.052Z',
            'restricted_to_unlogged': False,
            'display_mode': 'active',
            'payer_external_id_template': '',
            'include_pay_button': True,
        },
    }
    assert snapshot.serialization['cells'][1] == {
        'model': 'lingo.invoicescell',
        'fields': {
            'slug': 'bar',
            'text': 'Bar',
            'order': 2,
            'regie': 'blah',
            'title': 'Baar',
            'groups': [],
            'public': True,
            'condition': None,
            'placeholder': 'content',
            'hide_if_empty': False,
            'template_name': None,
            'extra_css_class': '',
            'last_update_timestamp': '2023-04-11T15:28:27.174Z',
            'restricted_to_unlogged': False,
            'display_mode': 'historical',
            'payer_external_id_template': '',
            'include_pay_button': True,
        },
    }
