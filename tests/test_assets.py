import base64
import io
import os
import tarfile
from io import BytesIO

import PIL
import pytest
from django.core.files import File
from django.core.files.storage import default_storage
from django.test import override_settings
from django.urls import reverse

from combo.apps.assets.models import Asset
from combo.apps.assets.utils import (
    add_tar_content,
    clean_assets_files,
    export_assets,
    import_assets,
    process_image,
    tar_assets_files,
    untar_assets_files,
)

pytestmark = pytest.mark.django_db


def _clean_media():
    media_prefix = default_storage.path('')
    for basedir, dummy, filenames in os.walk(media_prefix):
        for filename in filenames:
            os.remove('%s/%s' % (basedir, filename))


@pytest.fixture(autouse=True)
def clean_media():
    yield
    _clean_media()


@pytest.fixture
def some_assets():
    Asset(key='banner', asset=File(BytesIO(b'test'), 'test.png')).save()
    Asset(key='favicon', asset=File(BytesIO(b'test2'), 'test2.png')).save()


def count_asset_files():
    nb_assets = 0
    media_prefix = default_storage.path('')
    for dummy, dummy, filenames in os.walk(media_prefix):
        nb_assets += len(filenames)
    return nb_assets


def test_asset_set_api(app, john_doe):
    app.authorization = ('Basic', (john_doe.username, john_doe.username))
    resp = app.post_json(
        reverse('api-assets-set', kwargs={'key': 'plop'}),
        params={
            'asset': {
                'content': base64.encodebytes(b'plop').decode('ascii'),
                'content_type': 'text/plain',
                'filename': 'plop.txt',
            }
        },
    )
    assert Asset.objects.get(key='plop').asset.read() == b'plop'

    resp = app.post_json(
        reverse('api-assets-set', kwargs={'key': 'plop'}),
        params={
            'asset': {
                'content': base64.encodebytes(b'plop2').decode('ascii'),
                'content_type': 'text/plain',
                'filename': 'plop.txt',
            }
        },
    )
    assert resp.json.get('err') == 0
    assert resp.json.get('url') == 'http://testserver/assets/plop'
    assert Asset.objects.get(key='plop').asset.read() == b'plop2'

    resp = app.post_json(reverse('api-assets-set', kwargs={'key': 'plop'}), params={}, status=400)
    assert resp.json.get('err') == 1

    for invalid_value in (None, 'not base 64', 'éléphant'):
        resp = app.post_json(
            reverse('api-assets-set', kwargs={'key': 'plop'}),
            params={
                'asset': {
                    'content': invalid_value,
                    'content_type': 'text/plain',
                    'filename': 'plop.txt',
                }
            },
            status=400,
        )
        assert resp.json.get('err') == 1


def test_clean_assets_files(some_assets):
    path = default_storage.path('')
    os.makedirs(os.path.join(path, 'page-pictures'), exist_ok=True)
    with open('%s/page-pictures/pictures.png' % path, 'w') as fd:
        fd.write('foo')
    os.makedirs(os.path.join(path, 'applications'), exist_ok=True)  # application icons
    with open('%s/applications/appli.png' % path, 'w') as fd:
        fd.write('foo')
    os.makedirs(os.path.join(path, 'cache', '07', 'db'), exist_ok=True)  # cache for thumbnails
    with open('%s/cache/07/db/hash.png' % path, 'w') as fd:
        fd.write('foo')
    assert count_asset_files() == 5
    clean_assets_files()
    assert count_asset_files() == 2  # application icons and thumbnail cache not removed


def test_add_tar_content(tmpdir):
    filename = os.path.join(str(tmpdir), 'file.tar')
    with tarfile.open(filename, 'w') as tar:
        add_tar_content(tar, 'foo.txt', 'bar')

    with tarfile.open(filename, 'r') as tar:
        tarinfo = tar.getmember('foo.txt')
        assert tar.extractfile(tarinfo).read().decode('utf-8') == 'bar'


def test_tar_untar_assets(some_assets):
    assert Asset.objects.count() == 2
    assert count_asset_files() == 2
    fd = BytesIO()

    with tarfile.open(mode='w', fileobj=fd) as tar:
        tar_assets_files(tar)
        tar_bytes = fd.getvalue()

    path = default_storage.path('')
    os.remove('%s/assets/test.png' % path)
    with open('%s/assets/test2.png' % path, 'w') as fd:
        fd.write('foo')
    assert count_asset_files() == 1
    Asset.objects.all().delete()
    assert Asset.objects.count() == 0
    fd = BytesIO(tar_bytes)

    with tarfile.open(mode='r', fileobj=fd) as tar:
        data = untar_assets_files(tar)
    assert [x['fields']['key'] for x in data['assets']] == ['banner', 'favicon']
    assert count_asset_files() == 2
    with open('%s/assets/test.png' % path) as fd:
        assert fd.read() == 'test'
    with open('%s/assets/test2.png' % path) as fd:
        assert fd.read() == 'foo'


def test_import_export_assets(some_assets, tmpdir):
    path = default_storage.path('')
    os.makedirs(os.path.join(path, 'page-pictures'), exist_ok=True)
    with open('%s/page-pictures/pictures.png' % path, 'w') as fd:
        fd.write('foo')
    os.makedirs(os.path.join(path, 'applications'), exist_ok=True)  # application icons
    with open('%s/applications/appli.png' % path, 'w') as fd:
        fd.write('foo')
    os.makedirs(os.path.join(path, 'cache', '07', 'db'), exist_ok=True)  # cache for thumbnails
    with open('%s/cache/07/db/hash.png' % path, 'w') as fd:
        fd.write('foo')
    filename = os.path.join(str(tmpdir), 'file.tar')
    assert Asset.objects.count() == 2
    assert count_asset_files() == 5
    with open(filename, 'wb') as fd:
        export_assets(fd)

    os.remove('%s/assets/test.png' % path)
    with open('%s/assets/test2.png' % path, 'w') as fd:
        fd.write('foo')
    assert count_asset_files() == 4
    Asset.objects.all().delete()
    assert Asset.objects.count() == 0

    with open(filename, 'rb') as fd:
        import_assets(fd, overwrite=True)
    assert count_asset_files() == 5
    with open('%s/assets/test.png' % path) as fd:
        assert fd.read() == 'test'
    with open('%s/assets/test2.png' % path) as fd:
        assert fd.read() == 'test2'
    clean_assets_files()
    assert count_asset_files() == 2

    _clean_media()
    assert count_asset_files() == 0
    with open(filename, 'rb') as fd:
        import_assets(fd, overwrite=True)
    assert count_asset_files() == 3  # only assets and page-picture in tar file and imported


def test_assets_export_size_view(app, some_assets):
    resp = app.get(reverse('combo-manager-assets-export-size'))
    assert resp.text.split() == ['(9', 'bytes)']


def test_assets_nginx_x_accel(app, some_assets):
    resp = app.get('/assets/banner')
    assert resp.location == '/media/assets/test.png'
    with override_settings(COMBO_X_ACCEL_ASSETS=True):
        resp = app.get('/assets/banner')
        assert resp.headers['Content-Type'] == ''
        assert resp.headers['X-Accel-Redirect'] == '/media/assets/test.png'


def test_process_image_with_exif():
    exif_with_date = (
        b'Exif\x00\x00MM\x00*\x00\x00\x00\x08\x00\x01\x87i\x00\x04\x00\x00\x00\x01\x00\x00\x00\x1a\x00\x00'
        b'\x00\x00\x00\x01\x90\x03\x00\x02\x00\x00\x00\x14\x00\x00\x00(2000:00:00 10:10:10\x00'
    )
    image = PIL.Image.new('RGB', (100, 300), 'black')

    fd = io.BytesIO()
    image.save(fd, format='jpeg', exif=exif_with_date)
    process_image(fd)

    image = PIL.Image.open(fd)
    assert image._getexif() is None

    # orientation = 8
    exif_with_orientation = (
        b'Exif\x00\x00MM\x00*\x00\x00\x00\x08\x00\x01\x01\x12\x00\x03\x00\x00\x00\x01\x00\x08\x00\x00\x00'
        b'\x00\x00\x00'
    )

    fd = io.BytesIO()
    image.save(fd, format='jpeg', exif=exif_with_orientation)
    assert image.width == 100

    process_image(fd)

    image = PIL.Image.open(fd)
    assert image._getexif() is None
    assert image.width == 300
