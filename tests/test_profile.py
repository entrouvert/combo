import datetime
import json
from unittest import mock

import pytest
from django.contrib.auth import get_user_model
from django.test import override_settings

from combo.data.models import Page
from combo.profile.models import ProfileCell

pytestmark = pytest.mark.django_db


@override_settings(KNOWN_SERVICES={'authentic': {'idp': {'title': 'IdP', 'url': 'http://example.org/'}}})
@mock.patch('combo.utils.requests.get')
def test_profile_cell(requests_get, app, admin_user):
    page = Page()
    page.save()

    cell = ProfileCell(page=page, order=0)
    cell.save()

    data = {
        'first_name': 'Foo',
        'birthdate': '2018-08-10',
        'phone': '+33612345678',
    }
    requests_get.return_value = mock.Mock(content=json.dumps(data), json=lambda: data, status_code=200)

    admin_user.get_name_id = lambda: '123456'

    context = cell.get_cell_extra_context({'synchronous': True, 'selected_user': admin_user})
    assert context['profile_fields']['first_name']['value'] == 'Foo'
    assert context['profile_fields']['birthdate']['value'] == datetime.date(2018, 8, 10)
    assert context['profile_fields']['phone']['value'] == '06 12 34 56 78'
    assert requests_get.call_args[0][0] == 'http://example.org/api/users/123456/'

    # modify button
    cell.edit_label = 'edit'
    cell.save()
    context = cell.get_cell_extra_context({'synchronous': True, 'selected_user': admin_user})
    assert context['edit_url'] == 'http://example.org/accounts/edit/'

    # in portal agent
    with mock.patch('combo.profile.models.is_portal_agent') as is_portal_agent:
        is_portal_agent.side_effect = lambda: True
        context = cell.get_cell_extra_context({'synchronous': True, 'selected_user': admin_user})
        assert context['edit_url'] == 'http://example.org/manage/users/uuid:123456/edit/'

    # foreign number remains in its international representation
    data['phone'] = '+221 33 889 00 00'  # Dakar landline number
    requests_get.return_value = mock.Mock(content=json.dumps(data), json=lambda: data, status_code=200)
    context = cell.get_cell_extra_context({'synchronous': True, 'selected_user': admin_user})
    assert context['profile_fields']['phone']['value'] == '+221 33 889 00 00'  # international representation

    # erroneous number is not parsed at all
    data['phone'] = '+336a23c5678'
    requests_get.return_value = mock.Mock(content=json.dumps(data), json=lambda: data, status_code=200)
    context = cell.get_cell_extra_context({'synchronous': True, 'selected_user': admin_user})
    assert context['profile_fields']['phone']['value'] == '+336a23c5678'


def test_user_nameid_property():
    user = get_user_model().objects.create(
        username='john.doe', first_name='John', last_name='Doe', email='john.doe@example.net'
    )
    user._name_id = 'abcd'  # fake cached nameid
    assert user.nameid == 'abcd'
