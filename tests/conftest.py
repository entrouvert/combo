import os
import shutil

import django_webtest
import pytest
from django.contrib.auth.models import User
from django.core.files.storage import default_storage


@pytest.fixture
def clean_media():
    for path in ('uploads', 'assets', 'page-pictures', 'pwa'):
        if os.path.exists(default_storage.path(path)):
            shutil.rmtree(default_storage.path(path))


@pytest.fixture
def app(request, clean_media):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def john_doe():
    try:
        user = User.objects.get(username='john.doe')
    except User.DoesNotExist:
        user = User.objects.create_user('john.doe', email='john.doe@example.com', password='john.doe')
    return user


@pytest.fixture
def jane_doe():
    try:
        admin2 = User.objects.get(username='jane.doe')
    except User.DoesNotExist:
        admin2 = User.objects.create_user('jane.doe', email='jane.doe@example.com', password='jane.doe')
    return admin2


@pytest.fixture
def admin_user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_superuser('admin', email=None, password='admin')
    return user


@pytest.fixture
def normal_user():
    try:
        user = User.objects.get(username='normal-user')
        user.groups = []
    except User.DoesNotExist:
        user = User.objects.create_user(username='normal-user', email='', password='normal-user')
    return user


@pytest.fixture
def nocache(settings):
    settings.CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
    }


@pytest.fixture
def synchronous_cells(settings):
    class M:
        @staticmethod
        def on():
            settings.COMBO_TEST_ALWAYS_RENDER_CELLS_SYNCHRONOUSLY = True

        @staticmethod
        def off():
            settings.COMBO_TEST_ALWAYS_RENDER_CELLS_SYNCHRONOUSLY = False

    M.on()
    return M
