from unittest import mock

import pytest

from combo.apps.maps.models import Map, MapLayer, MapLayerOptions
from combo.data.models import Page

from .utils import manager_submit_cell

pytestmark = pytest.mark.django_db


@pytest.fixture
def layer():
    return MapLayer.objects.create(
        label='Test',
        kind='geojson',
        geojson_url='http://example.org/geojson',
        icon='bicycle',
        marker_colour='#FFFFFF',
        icon_colour='#FFFFFF',
    )


@pytest.fixture
def tiles_layer():
    return MapLayer.objects.create(
        label='Test2',
        kind='tiles',
        tiles_template_url='http://somedomain.com/blabla/{z}/{x}/{y}{r}.png',
        tiles_attribution='Foo bar',
        tiles_default=True,
    )


def login(app, username='admin', password='admin'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


def test_access(app, admin_user):
    app = login(app)
    resp = app.get('/manage/', status=200)
    assert '/manage/maps/' in resp.text


def test_add_geojson_layer(app, admin_user):
    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('New GeoJSON layer')
    assert 'slug' not in resp.context['form'].fields
    assert 'cache_duration' not in resp.context['form'].fields
    assert 'tiles_template_url' not in resp.context['form'].fields
    assert 'tiles_attribution' not in resp.context['form'].fields
    assert 'tiles_default' not in resp.context['form'].fields
    resp.form['label'] = 'Test'
    resp.form['geojson_url'] = 'http://example.net/geojson'
    assert resp.form['marker_colour'].value == '#0000FF'
    resp.form['marker_colour'] = '#FFFFFF'
    resp.form['icon'] = 'bicycle'
    assert resp.form['icon_colour'].value == '#000000'
    resp.form['icon_colour'] = '#FFFFFF'
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    assert MapLayer.objects.count() == 1
    layer = MapLayer.objects.get()
    assert layer.label == 'Test'
    assert layer.slug == 'test'
    assert layer.kind == 'geojson'


def test_add_tiles_layer(app, admin_user):
    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('New tiles layer')
    assert 'slug' not in resp.context['form'].fields
    assert 'cache_duration' not in resp.context['form'].fields
    assert 'geojson_url' not in resp.context['form'].fields
    assert 'marker_colour' not in resp.context['form'].fields
    assert 'icon' not in resp.context['form'].fields
    assert 'icon_colour' not in resp.context['form'].fields
    resp.forms[0]['label'] = 'Test'
    resp.forms[0]['tiles_template_url'] = 'http://somedomain.com/blabla/{z}/{x}/{y}{r}.png'
    resp.forms[0]['tiles_attribution'] = 'Foo bar'
    resp.forms[0]['tiles_default'] = True
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    assert MapLayer.objects.count() == 1
    layer = MapLayer.objects.get()
    assert layer.label == 'Test'
    assert layer.slug == 'test'
    assert layer.kind == 'tiles'
    assert layer.tiles_default is True

    # only one default layer
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('New tiles layer')
    resp.forms[0]['label'] = 'Test2'
    resp.forms[0]['tiles_template_url'] = 'http://somedomain.com/blabla/{z}/{x}/{y}{r}.png'
    resp.forms[0]['tiles_attribution'] = 'Foo bar'
    resp.forms[0]['tiles_default'] = True
    resp = resp.forms[0].submit()
    assert '<li>Only one default tiles layer can be defined.</li>' in resp.text

    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('New tiles layer')
    resp.forms[0]['label'] = 'Test2'
    resp.forms[0]['tiles_template_url'] = 'http://somedomain.com/blabla/{z}/{x}/{y}{r}.png'
    resp.forms[0]['tiles_attribution'] = 'Foo bar'
    resp.forms[0]['tiles_default'] = False
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    assert MapLayer.objects.count() == 2


def test_edit_geojson_layer(app, admin_user, layer):
    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('Test')
    assert 'tiles_template_url' not in resp.context['form']
    assert 'tiles_attribution' not in resp.context['form']
    assert 'tiles_default' not in resp.context['form']
    assert '<span class="icon-ambulance"><label' in resp.text
    resp.forms[0]['geojson_url'] = 'http://example.net/new_geojson'
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    layer.refresh_from_db()
    assert layer.geojson_url == 'http://example.net/new_geojson'

    resp = resp.follow().click('Test')
    resp = resp.click('Request parameters')
    resp.form['geojson_query_parameter'] = 'foobar'
    resp.form['geojson_accepts_circle_param'] = True
    resp.form['include_user_identifier'] = True
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/maps/layers/%s/edit/' % layer.slug)
    layer.refresh_from_db()
    assert layer.geojson_query_parameter == 'foobar'
    assert layer.geojson_accepts_circle_param is True
    assert layer.include_user_identifier is True


def test_edit_tiles_layer(app, admin_user, tiles_layer):
    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('Test')
    assert 'cache_duration' not in resp.context['form']
    assert 'geojson_url' not in resp.context['form']
    assert 'marker_colour' not in resp.context['form']
    assert 'icon' not in resp.context['form']
    assert 'icon_colour' not in resp.context['form']
    assert 'Request parameters' not in resp.text
    resp.forms[0]['tiles_default'] = False
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    tiles_layer.refresh_from_db()
    assert tiles_layer.tiles_default is False


def test_delete_layer(app, admin_user, layer):
    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    resp = resp.click('remove')
    assert 'Are you sure you want to delete this?' in resp.text
    resp = resp.forms[0].submit()
    assert resp.location.endswith('/manage/maps/')
    assert MapLayer.objects.count() == 0


def test_list_layers(app, admin_user, layer):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    map1 = Map.objects.create(page=page, placeholder='map 1', order=0)
    map2 = Map.objects.create(page=page, placeholder='map 2', order=1)

    app = login(app)
    resp = app.get('/manage/maps/', status=200)
    assert f'/manage/pages/{page.pk}/#cell-{map1.get_reference()}' in resp.text
    assert f'/manage/pages/{page.pk}/#cell-{map2.get_reference()}' in resp.text


@mock.patch('combo.apps.maps.models.requests.get')
def test_download_geojson(mock_request, app, admin_user, layer):
    mocked_response = mock.Mock()
    mock_request.GET = {}
    mocked_response.json.return_value = [
        {
            'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [2.3233688436448574, 48.83369263315934]},
            'properties': {'property': 'property value'},
        }
    ]
    mocked_response.ok.return_value = True
    mock_request.return_value = mocked_response
    geojson = layer.get_geojson(mock_request)['features']
    assert len(geojson) > 0
    for item in geojson:
        assert item['type'] == 'Feature'
        assert item['geometry']['type'] == 'Point'
        assert item['geometry']['coordinates'] == [2.3233688436448574, 48.83369263315934]

    mocked_response.json.return_value = {
        'type': 'FeatureCollection',
        'features': [
            {
                'geometry': {'type': 'Point', 'coordinates': [2.3233688436448574, 48.83369263315934]},
                'properties': {'property': 'a random value', 'display_fields': [('foo', 'bar')]},
            }
        ],
    }
    mocked_response.ok.return_value = True
    mock_request.return_value = mocked_response
    geojson = layer.get_geojson(mock_request)['features']
    assert len(geojson) > 0
    for item in geojson:
        assert item['geometry']['type'] == 'Point'
        assert item['geometry']['coordinates'] == [2.3233688436448574, 48.83369263315934]


def test_add_delete_layer(app, admin_user, layer, tiles_layer):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, public=True, title='Map')
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    assert list(cell.get_free_geojson_layers()) == [layer]
    assert list(cell.get_free_tiles_layers()) == [tiles_layer]
    resp = resp.click(href='.*/add-layer/geojson/$')
    assert list(resp.context['form'].fields['map_layer'].queryset) == [layer]
    assert 'opacity' not in resp.context['form'].fields
    resp.forms[0]['map_layer'] = layer.pk
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert MapLayerOptions.objects.count() == 1
    options = MapLayerOptions.objects.get()
    assert options.map_cell == cell
    assert options.map_layer == layer

    resp = resp.follow()
    assert list(cell.get_free_geojson_layers()) == []
    assert list(cell.get_free_tiles_layers()) == [tiles_layer]
    assert '/add-layer/geojson/' not in resp.text

    resp = resp.click(href='.*/layer/%s/edit/$' % options.pk)
    assert 'map_layer' not in resp.context['form'].fields
    assert 'opacity' not in resp.context['form'].fields
    assert (
        'this setting has no effect, since marker behaviour on click is set to "Nothing".'
        in resp.context['form'].fields['properties'].help_text
    )
    resp = resp.click('Cancel')

    resp.form['cmaps_map-%s-marker_behaviour_onclick' % cell.pk] = 'display_data'
    manager_submit_cell(resp.form)
    resp = resp.click(href='.*/layer/%s/edit/$' % options.pk)
    assert 'this setting has no effect' not in resp.context['form'].fields['properties'].help_text
    resp.form['properties'] = 'a, b'
    resp = resp.form.submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    options = MapLayerOptions.objects.get()
    options.refresh_from_db()
    assert options.properties == 'a, b'

    resp = resp.follow()
    resp = resp.click(href='.*/layer/%s/delete/$' % options.pk)
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert MapLayerOptions.objects.count() == 0

    resp = resp.follow()
    assert list(cell.get_free_geojson_layers()) == [layer]
    assert list(cell.get_free_tiles_layers()) == [tiles_layer]
    resp = resp.click(href='.*/add-layer/tiles/$')
    assert list(resp.context['form'].fields['map_layer'].queryset) == [tiles_layer]
    assert 'properties' not in resp.context['form'].fields
    resp.forms[0]['map_layer'] = tiles_layer.pk
    resp.forms[0]['opacity'] = 1
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert MapLayerOptions.objects.count() == 1
    options = MapLayerOptions.objects.get()
    assert options.map_cell == cell
    assert options.map_layer == tiles_layer
    assert options.opacity == 1

    resp = resp.follow()
    assert len(resp.pyquery('ul.list-of-layers li a.edit')) == 1
    assert resp.pyquery('ul.list-of-layers li a.edit').attr('title') == 'Edit'
    assert resp.pyquery('ul.list-of-layers li a.edit').text() == 'Test2 (Tiles)'
    resp = resp.click(href='.*/layer/%s/edit/$' % options.pk)
    assert 'map_layer' not in resp.context['form'].fields
    assert 'properties' not in resp.context['form'].fields
    resp.forms[0]['opacity'] = 0.5
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    options.refresh_from_db()
    assert options.opacity == 0.5

    resp = resp.follow()
    assert list(cell.get_free_geojson_layers()) == [layer]
    assert list(cell.get_free_tiles_layers()) == []
    assert '/add-layer/tiles/' not in resp.text
    resp = resp.click(href='.*/layer/%s/delete/$' % options.pk)
    resp = resp.forms[0].submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/manage/pages/%s/#cell-%s' % (page.pk, cell.get_reference()))
    assert MapLayerOptions.objects.count() == 0


def test_manager_maps_cell_tabs(app, admin_user):
    page = Page.objects.create(title='One', slug='one', template_name='standard')
    Map.objects.create(page=page, placeholder='content', order=0, public=True, title='Map')
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.pk)

    assert not resp.pyquery('[data-tab-slug="general"] input[name$="title"]')
    assert resp.pyquery('[data-tab-slug="appearance"] input[name$="title"]')


def test_manager_map_edit_zoom_check(app, layer, admin_user):
    app = login(app)
    page = Page(title='xxx', slug='test', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, public=True)
    cell.title = 'TestMap'
    cell.save()

    resp = app.get(f'/manage/pages/{page.pk}/')

    # Valid zooms
    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 0
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 0
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 0
    manager_submit_cell(resp.form)
    assert resp.status_int == 200

    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 0
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 6
    manager_submit_cell(resp.form)
    assert resp.status_int == 200

    # Invalid zooms
    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 0
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 6
    manager_submit_cell(resp.form, expect_errors=True)
    assert resp.status_int == 200
    assert 'Invalid zoom configuration: initial zoom is not between minimal &amp; maximal zoom' in resp.text

    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 0
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 0
    manager_submit_cell(resp.form, expect_errors=True)
    assert resp.status_int == 200
    assert 'Invalid zoom configuration: initial zoom is not between minimal &amp; maximal zoom' in resp.text

    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 0
    manager_submit_cell(resp.form, expect_errors=True)
    assert resp.status_int == 200
    assert 'Invalid zoom configuration: minimal zoom must be lower than maximal zoom' in resp.text

    # String comparison problem #86631
    resp.form[f'cmaps_map-{cell.pk}-initial_zoom'] = 16
    resp.form[f'cmaps_map-{cell.pk}-min_zoom'] = 6
    resp.form[f'cmaps_map-{cell.pk}-max_zoom'] = 16
    manager_submit_cell(resp.form)
    assert resp.status_int == 200
