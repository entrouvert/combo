import json
import re
from unittest import mock

import pytest
import responses
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.test.client import RequestFactory
from django.urls import reverse
from requests.models import Response

from combo.apps.maps.models import Map, MapLayer, MapLayerOptions
from combo.data.models import Page

from .test_manager import login

pytestmark = pytest.mark.django_db

SAMPLE_GEOJSON_CONTENT = '''{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
         "name": "Foo",
         "extra": "Baz",
         "color": "#0a0a0a",
         "subcolor": {
           "color": "#0a0a0a"
         }
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          2.548828125,
          48.83579746243093
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
         "name": "Bar",
         "extra": "Baz",
         "subdict": {
           "plop": "Whatever"
         }
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          2.558828125,
          48.84579746243093
        ]
      }
    }
  ]
}'''

SAMPLE_WCS_GEOJSON_CONTENT = '''{
  "type": "FeatureCollection",
  "features": [
     {
       "type" : "Feature",
       "geometry" : {
          "type" : "Point",
          "coordinates" : [
             6.175303,
             48.684512
          ]
       },
       "properties" : {
          "name" : "Test - n°144-4",
          "view_label" : "Voir",
          "status_name" : "Foo",
          "display_fields" : [
             {
                "varname" : "id",
                "html_value" : "144-4",
                "value" : "144-4",
                "label" : "Numéro"
             },
             {
                "varname" : null,
                "html_value" : "toto",
                "value" : "toto",
                "label" : "toto"
             }
          ]
        }
     }
  ]
}'''


@pytest.fixture
def user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_user('admin', email='admin@localhost', password='admin')
    return user


@pytest.fixture
def layer():
    try:
        layer = MapLayer.objects.get()
    except MapLayer.DoesNotExist:
        layer = MapLayer()
        layer.label = 'bicycles'
        layer.geojson_url = 'http://example.org/geojson'
        layer.marker_colour = '#FF0000'
        layer.icon = 'fa-bicycle'
        layer.icon_colour = '#0000FF'
        layer.save()
    return layer


@pytest.fixture
def tiles_layer():
    return MapLayer.objects.create(
        label='Test2',
        kind='tiles',
        tiles_template_url='http://somedomain.com/blabla/{z}/{x}/{y}{r}.png',
        tiles_attribution='Foo bar',
        tiles_default=True,
    )


def test_cell_disabled():
    MapLayer.objects.all().delete()
    assert Map.is_enabled() is False


def test_cell_enabled(layer):
    assert Map.is_enabled() is True


def test_cell_rendering(app, layer, tiles_layer):
    page = Page(title='xxx', slug='test_map_cell', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, title='Map with points')
    cell.save()
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    context = {'request': RequestFactory().get('/')}
    rendered = cell.render(context)
    assert 'data-init-zoom="13"' in rendered
    assert 'data-min-zoom="0"' in rendered
    assert 'data-max-zoom="19"' in rendered
    assert 'data-init-lat="48.83369263315934"' in rendered
    assert 'data-init-lng="2.3233688436448574"' in rendered
    assert '/ajax/mapcell/geojson/%s/%s/' % (cell.id, layer.slug) in rendered
    assert 'data-group-markers="1"' not in rendered
    resp = app.get('/test_map_cell/')
    assert 'xstatic/leaflet.js' in resp.text
    assert 'js/combo.map.js' in resp.text
    assert 'xstatic/leaflet.css' in resp.text
    assert 'css/combo.map.css' in resp.text
    assert re.findall(r'geojson_\d+\["bicycles"\].*marker_size": "_large"', resp.text)

    cell.group_markers = True
    cell.save()
    rendered = cell.render(context)
    assert 'data-group-markers="1"' in rendered

    layer.marker_size = '_small'
    layer.save()
    rendered = cell.render(context)
    assert re.findall(r'geojson_\d+\["bicycles"\].*marker_size": "_small"', rendered)


def test_cell_tiles_layers(tiles_layer):
    page = Page.objects.create(title='xxx', slug='test_map_cell', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, title='Map with points')

    # no tiles layer for this map, take default tiles layers, tiles_layer
    assert cell.get_tiles_layers() == [
        {
            'tile_urltemplate': tiles_layer.tiles_template_url,
            'map_attribution': tiles_layer.tiles_attribution,
            'opacity': 1,
        }
    ]

    # tiles_layer is not set as default, fallback on settings
    tiles_layer.tiles_default = False
    tiles_layer.save()
    assert cell.get_tiles_layers() == [
        {
            'tile_urltemplate': settings.COMBO_MAP_TILE_URLTEMPLATE,
            'map_attribution': settings.COMBO_MAP_ATTRIBUTION,
            'opacity': 1,
        }
    ]

    # add a tile layer to the map, with opacity 1
    options = MapLayerOptions.objects.create(map_cell=cell, map_layer=tiles_layer, opacity=1)
    assert cell.get_tiles_layers() == [
        {
            'tile_urltemplate': tiles_layer.tiles_template_url,
            'map_attribution': tiles_layer.tiles_attribution,
            'opacity': 1,
        }
    ]

    # opacity is less than 1 => add default tiles layer, defined in settings
    options.opacity = 0.5
    options.save()
    assert cell.get_tiles_layers() == [
        {
            'tile_urltemplate': settings.COMBO_MAP_TILE_URLTEMPLATE,
            'map_attribution': settings.COMBO_MAP_ATTRIBUTION,
            'opacity': 1,
        },
        {
            'tile_urltemplate': tiles_layer.tiles_template_url,
            'map_attribution': tiles_layer.tiles_attribution,
            'opacity': 0.5,
        },
    ]

    # set tiles_layer as default => add tiles_layer
    tiles_layer.tiles_default = True
    tiles_layer.save()
    assert cell.get_tiles_layers() == [
        {
            'tile_urltemplate': tiles_layer.tiles_template_url,
            'map_attribution': tiles_layer.tiles_attribution,
            'opacity': 1,
        },
        {
            'tile_urltemplate': tiles_layer.tiles_template_url,
            'map_attribution': tiles_layer.tiles_attribution,
            'opacity': 0.5,
        },
    ]


def test_get_geojson_not_found(app, layer):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, title='Map with points')
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)

    app.get(reverse('mapcell-geojson', kwargs={'cell_id': 0, 'layer_slug': layer.slug}), status=404)
    app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.pk, 'layer_slug': 'foo'}), status=404)


def test_get_geojson_on_non_public_page(app, layer):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard', public=False)
    cell = Map.objects.create(page=page, placeholder='content', order=0, title='Map with points')
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}), status=403)


def test_get_geojson_on_non_publik_cell(app, layer):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = Map.objects.create(
        page=page, placeholder='content', order=0, title='Map with points', public=False
    )
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}), status=403)


def test_geojson_on_restricted_cell(app, layer, user):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    group = Group.objects.create(name='map tester')
    cell = Map(page=page, placeholder='content', order=0, public=False)
    cell = Map.objects.create(
        page=page, placeholder='content', order=0, title='Map with points', public=False
    )
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    cell.groups.add(group)
    login(app)
    app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}), status=403)
    user.groups.add(group)
    user.save()

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))


def test_get_geojson(app, layer, user):
    page = Page(title='xxx', slug='new', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, public=True)
    cell.title = 'Map'
    cell.save()
    layer.geojson_url = 'geojson?t1'
    layer.save()
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)

    geojson_url = reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug})

    # invalid url - missing scheme
    resp = app.get(geojson_url)
    assert len(resp.json['features']) == 0
    assert resp.json['_combo_err_desc'].startswith('Bad response from requested URL (Invalid URL')

    layer.geojson_url = 'http://example.org/geojson?t1'
    layer.save()

    # invalid content
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.get') as requests_get:
        mock_resp = Response()
        mock_resp.status_code = 200
        requests_get.return_value = mock_resp
        resp = app.get(geojson_url)
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Non JSON response from requested URL'

        mock_resp.json = lambda: None
        resp = app.get(geojson_url)
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Empty JSON response'

        mock_resp.status_code = 500
        resp = app.get(geojson_url)
        assert len(resp.json['features']) == 0
        assert (
            resp.json['_combo_err_desc']
            == 'Bad response from requested URL (500 Server Error: None for url: None)'
        )

        wrong_data = {
            'data': [
                {'id': 1, 'text': 'foo 1'},
                {'id': 2, 'text': 'foo 2'},
            ]
        }
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Wrong GeoJSON response'

        wrong_data = ['foo', 'bar']
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Wrong GeoJSON response'

        wrong_data = [
            {'id': 1, 'text': 'foo 1'},
        ]
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Wrong GeoJSON response'

        wrong_data = [
            {'id': 1, 'text': 'foo 1', 'geometry': None},
        ]
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Wrong GeoJSON response'

        wrong_data = [
            {'id': 1, 'text': 'foo 1', 'properties': None},
        ]
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0
        assert resp.json['_combo_err_desc'] == 'Wrong GeoJSON response'

    # check cache duration
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url)
        assert len(json.loads(resp.text)['features']) == 2
        assert requests_get.call_count == 1
        resp = app.get(geojson_url)
        assert requests_get.call_count == 1  # cache was used
        layer.cache_duration = 0
        layer.save()
        resp = app.get(geojson_url)
        assert requests_get.call_count == 2  # cache was not used

    # check user params
    layer.geojson_url = 'http://example.org/geojson?t2'
    layer.save()
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url)
        assert 'orig=combo' in requests_get.call_args[0][1]
        assert 'email=&' in requests_get.call_args[0][1]

    login(app)
    layer.geojson_url = 'http://example.org/geojson?t3'
    layer.save()
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url)
        assert 'orig=combo' in requests_get.call_args[0][1]
        assert 'email=admin%40localhost&' in requests_get.call_args[0][1]

    layer.geojson_url = 'http://example.org/geojson?t4'
    layer.include_user_identifier = False
    layer.save()
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url)
        assert 'orig=combo' in requests_get.call_args[0][1]
        assert 'email=admin%40localhost&' not in requests_get.call_args[0][1]

    # check query on geojson
    layer.geojson_url = 'http://example.org/geojson?t5'
    layer.include_user_identifier = False
    layer.save()
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url + '?q=bar')
        assert len(json.loads(resp.text)['features']) == 1
        assert 'orig=combo' in requests_get.call_args[0][1]
        assert 'email=admin%40localhost&' not in requests_get.call_args[0][1]

        # query against layer name
        resp = app.get(geojson_url + '?q=bicycle')
        assert len(json.loads(resp.text)['features']) == 2

        # query against subproperty
        resp = app.get(geojson_url + '?q=whatever')
        assert len(json.loads(resp.text)['features']) == 1

    # check distance query on geojson
    layer.geojson_url = 'http://example.org/geojson?t6'
    layer.include_user_identifier = False
    layer.save()
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=2000')
        assert len(json.loads(resp.text)['features']) == 2

        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=1000')
        assert len(json.loads(resp.text)['features']) == 1

        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=100')
        assert len(json.loads(resp.text)['features']) == 0

        # missing params
        resp = app.get(geojson_url + '?lat=48.84&distance=10')
        assert len(json.loads(resp.text)['features']) == 2
        resp = app.get(geojson_url + '?lng=2.54&distance=10')
        assert len(json.loads(resp.text)['features']) == 2

        # bad params
        resp = app.get(geojson_url + '?lng=foo&lat=48.84&distance=10')
        assert len(json.loads(resp.text)['features']) == 2
        resp = app.get(geojson_url + '?lng=2.54&lat=foo&distance=10')
        assert len(json.loads(resp.text)['features']) == 2
        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=foo')
        assert len(json.loads(resp.text)['features']) == 2

    # check on multiple words
    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url + '?q=bar baz')
        assert len(json.loads(resp.text)['features']) == 1

        resp = app.get(geojson_url + '?q=quux baz')
        assert len(json.loads(resp.text)['features']) == 0

    # add a second layer
    layer2 = MapLayer()
    layer2.label = 'xxx'
    layer2.geojson_url = 'http://example.org/geojson'
    layer2.marker_colour = 'FF0000'
    layer2.icon = 'fa-bicycle'
    layer2.icon_colour = '0000FF'
    layer2.save()
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer2)
    geojson_url2 = reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer2.slug})

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(geojson_url + '?q=bar')
        assert len(json.loads(resp.text)['features']) == 1
        resp = app.get(geojson_url2 + '?q=bar')
        assert len(json.loads(resp.text)['features']) == 1
        resp = app.get(geojson_url + '?q=xyz')
        assert len(json.loads(resp.text)['features']) == 0
        resp = app.get(geojson_url2 + '?q=xyz')
        assert len(json.loads(resp.text)['features']) == 0
        # query against layer name, it should get results
        resp = app.get(geojson_url + '?q=bicycle')
        assert len(json.loads(resp.text)['features']) == 2
        resp = app.get(geojson_url2 + '?q=bicycle')
        assert len(json.loads(resp.text)['features']) == 0
        resp = app.get(geojson_url + '?q=bar bicycle')
        assert len(json.loads(resp.text)['features']) == 1
        resp = app.get(geojson_url2 + '?q=bar bicycle')
        assert len(json.loads(resp.text)['features']) == 0

        resp = app.get(geojson_url + '?q=quux bicycle')
        assert len(json.loads(resp.text)['features']) == 0
        resp = app.get(geojson_url2 + '?q=quux bicycle')
        assert len(json.loads(resp.text)['features']) == 0


def test_get_geojson_query_parameter(app, layer, user):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, public=True)
    layer.geojson_url = 'http://example.org/geojson'
    layer.save()
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    geojson_url = reverse('mapcell-geojson', kwargs={'cell_id': cell.pk, 'layer_slug': layer.slug})
    mock_get = mock.Mock(
        content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
    )

    # no query param defined
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url)
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?q=bar')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 1

    # with query param defined as 'q'
    layer.geojson_query_parameter = 'q'
    layer.save()
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url)
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?q=bar')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson?q=bar'
    assert len(json.loads(resp.text)['features']) == 2

    # with query param defined (but not 'q')
    layer.geojson_query_parameter = 'foo'
    layer.save()
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url)
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?q=bar')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?foo=bar')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson?q=bar'
    assert len(json.loads(resp.text)['features']) == 2


def test_get_geojson_accepts_circle_param(app, layer, user):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, public=True)
    layer.geojson_url = 'http://example.org/geojson'
    layer.save()
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)
    geojson_url = reverse('mapcell-geojson', kwargs={'cell_id': cell.pk, 'layer_slug': layer.slug})
    mock_get = mock.Mock(
        content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
    )

    # circle param not accepted
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url)
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=1000')
        assert len(json.loads(resp.text)['features']) == 1
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 1

    # circle param accepted
    layer.geojson_accepts_circle_param = True
    layer.save()
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url)
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=1000')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson?circle=2.54%2C48.84%2C1000.0'
    assert len(json.loads(resp.text)['features']) == 2
    # missing params
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lat=48.84&distance=1000')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=2.54&distance=1000')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    # bad params
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=foo&lat=48.84&distance=1000')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=2.54&lat=foo&distance=1000')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2
    with mock.patch('combo.utils.requests.get', return_value=mock_get) as requests_get:
        resp = app.get(geojson_url + '?lng=2.54&lat=48.84&distance=foo')
    assert requests_get.call_args_list[0][0][0] == 'http://example.org/geojson'
    assert len(json.loads(resp.text)['features']) == 2


def test_get_geojson_properties(app, layer, user):
    page = Page(title='xxx', slug='new', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, public=True)
    cell.title = 'Map'
    cell.save()
    layer.cache_duration = 0
    layer.save()
    options = MapLayerOptions.objects.create(map_cell=cell, map_layer=layer)

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t1'
        layer.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert 'name' in features[0]['properties']
        assert 'extra' in features[0]['properties']

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t2'
        layer.save()
        options.properties = 'name, hop'
        options.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert 'name' in features[0]['properties']
        assert 'extra' not in features[0]['properties']

        wrong_data = {
            'data': [
                {'id': 1, 'text': 'foo 1'},
                {'id': 2, 'text': 'foo 2'},
            ]
        }
        requests_get.return_value = mock.Mock(
            content=json.dumps(wrong_data), json=lambda: wrong_data, status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        assert len(resp.json['features']) == 0

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t2'
        layer.properties = 'name, hop'
        layer.marker_colour = 'color'
        layer.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert 'name' in features[0]['properties']
        assert 'extra' not in features[0]['properties']
        assert 'color' in features[0]['properties']

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t2'
        layer.properties = 'name, hop'
        layer.marker_colour = 'subcolor.color'
        layer.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_GEOJSON_CONTENT, json=lambda: json.loads(SAMPLE_GEOJSON_CONTENT), status_code=200
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert 'name' in features[0]['properties']
        assert 'extra' not in features[0]['properties']
        assert 'subcolor' in features[0]['properties']
        assert 'color' in features[0]['properties']['subcolor']

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t3'
        layer.save()
        options.properties = ''
        options.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_WCS_GEOJSON_CONTENT,
            json=lambda: json.loads(SAMPLE_WCS_GEOJSON_CONTENT),
            status_code=200,
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert len(features[0]['properties']['display_fields']) == 2

    with mock.patch('combo.utils.requests_wrapper.RequestsSession.request') as requests_get:
        layer.geojson_url = 'http://example.org/geojson?t4'
        layer.save()
        options.properties = 'id'
        options.save()
        requests_get.return_value = mock.Mock(
            content=SAMPLE_WCS_GEOJSON_CONTENT,
            json=lambda: json.loads(SAMPLE_WCS_GEOJSON_CONTENT),
            status_code=200,
        )
        resp = app.get(reverse('mapcell-geojson', kwargs={'cell_id': cell.id, 'layer_slug': layer.slug}))
        features = json.loads(resp.text)['features']
        assert len(features[0]['properties']['display_fields']) == 1


def test_duplicate(layer):
    page = Page.objects.create(title='xxx', slug='new', template_name='standard')
    cell = Map.objects.create(page=page, placeholder='content', order=0, public=True, title='Map')
    MapLayerOptions.objects.create(map_cell=cell, map_layer=layer, opacity=0.5, properties='a, b')

    new_cell = cell.duplicate()
    assert list(new_cell.layers.all()) == [layer]
    options = new_cell.maplayeroptions_set.get()
    assert options.map_layer == layer
    assert options.opacity == 0.5
    assert options.properties == 'a, b'


@responses.activate
def test_geocoding(app):
    page = Page(title='xxx', slug='test_map_cell', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, title='Map')
    cell.save()

    resp = app.get('/test_map_cell/')
    assert not resp.pyquery('[data-search-url]')
    cell.include_search_button = True
    cell.save()
    resp = app.get('/test_map_cell/')
    search_url = resp.pyquery('[data-search-url]').attr('data-search-url')
    app.get(search_url, status=400)
    responses.get(
        settings.COMBO_MAP_GEOCODING_URL, json=[{'display_name': 'address', 'lat': '1', 'lon': '2'}]
    )
    resp = app.get(search_url + '?q=test')
    assert resp.json == [{'display_name': 'address', 'lat': '1', 'lon': '2'}]
    assert responses.calls[0].request.path_url == '/search?format=json&q=test&accept-language=en'

    cell.include_search_button = False
    cell.save()
    app.get(search_url + '?q=test', status=403)


def test_unsaved_map_cell_get_context(app):
    page = Page(title='xxx', slug='test_map_cell', template_name='standard')
    page.save()
    cell = Map(page=page, placeholder='content', order=0, title='Map')
    # check it has the default tile layer
    assert cell.get_cell_extra_context({}).get('tiles_layers')
