import pytest
from django.contrib.auth.models import Group, User

from combo.data.models import Page, TextCell


@pytest.fixture
def group(db):
    return Group.objects.create(name='Group')


@pytest.fixture(autouse=True)
def setup(db, group):
    pg = Page.objects.create(title='Test', slug='test', template_name='standard')

    order = 0

    def make_cell(**kwargs):
        nonlocal order
        try:
            return TextCell.objects.create(page=pg, placeholder='content', order=order, **kwargs)
        finally:
            order += 1

    make_cell(text='<p>Always visible</p>')
    make_cell(text='<p>Visible to unlogged only</p>', restricted_to_unlogged=True)
    make_cell(text='<p>Visible to logged only</p>', public=False)
    make_cell(text='<p>Visible only to members of group</p>', public=False).groups.add(group)
    make_cell(
        text='<p id="visible-to-non-members-of-group">Visible only to non-members of group</p>',
        public=False,
        restricted_to_unlogged=True,
    ).groups.add(group)


def test_anonymous(app):
    response = app.get('/test/')

    assert 'Always visible' in response
    assert 'Visible to unlogged only' in response
    assert 'Visible to logged only' not in response
    assert 'Visible only to members of group' not in response
    assert 'Visible only to non-members of group' not in response
    assert len(response.pyquery('.shown-because-admin')) == 0


def test_user(app):
    User.objects.create(username='user')
    response = app.get('/test/', user='user')

    assert 'Always visible' in response
    assert 'Visible to unlogged only' not in response
    assert 'Visible to logged only' in response
    assert 'Visible only to members of group' not in response
    assert 'Visible only to non-members of group' in response
    assert len(response.pyquery('.shown-because-admin')) == 0


def test_user_with_role(app, group):
    User.objects.create(username='user').groups.add(group)
    response = app.get('/test/', user='user')

    assert 'Always visible' in response
    assert 'Visible to unlogged only' not in response
    assert 'Visible to logged only' in response
    assert 'Visible only to members of group' in response
    assert 'Visible only to non-members of group' not in response
    assert len(response.pyquery('.shown-because-admin')) == 0


def test_superuser(app):
    User.objects.create(username='superuser', is_superuser=True)
    response = app.get('/test/', user='superuser')

    assert 'Always visible' in response
    assert 'Visible to unlogged only' in response
    assert 'Visible to logged only' in response
    assert 'Visible only to members of group' in response
    assert 'Visible only to non-members of group' in response
    assert {elt.text() for elt in response.pyquery.items('.shown-because-admin')} == {
        'Visible to unlogged only',
        'Visible only to members of group',
    }


def test_superuser_with_role(app, group):
    User.objects.create(username='superuser', is_superuser=True).groups.add(group)
    response = app.get('/test/', user='superuser')

    assert 'Always visible' in response
    assert 'Visible to unlogged only' in response
    assert 'Visible to logged only' in response
    assert 'Visible only to members of group' in response
    assert 'Visible only to non-members of group' in response
    assert {elt.text() for elt in response.pyquery.items('.shown-because-admin')} == {
        'Visible to unlogged only',
        'Visible only to non-members of group',
    }
