import eopayment
import pytest
from django.contrib.auth.models import User
from django.test.client import RequestFactory
from django.utils import timezone
from django.utils.http import quote

from combo.apps.lingo.models import (
    BasketItem,
    LingoBasketCell,
    LingoBasketLinkCell,
    LingoRecentTransactionsCell,
    PaymentBackend,
    Regie,
    TipiPaymentFormCell,
    Transaction,
)
from combo.data.models import Page

pytestmark = pytest.mark.django_db


@pytest.fixture
def user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_user('admin', email=None, password='admin')
    return user


@pytest.fixture
def regie():
    try:
        payment_backend = PaymentBackend.objects.get(slug='test1')
    except PaymentBackend.DoesNotExist:
        payment_backend = PaymentBackend.objects.create(label='test1', slug='test1')
    try:
        regie = Regie.objects.get(slug='test')
    except Regie.DoesNotExist:
        regie = Regie()
        regie.label = 'Test'
        regie.slug = 'test'
        regie.description = 'test'
        regie.payment_backend = payment_backend
        regie.save()
    return regie


def test_cell_disabled():
    Regie.objects.all().delete()
    assert LingoBasketCell.is_enabled() is False
    assert LingoRecentTransactionsCell.is_enabled() is False


def test_cell_enabled(regie):
    assert LingoBasketCell.is_enabled() is True
    assert LingoRecentTransactionsCell.is_enabled() is True


def test_basket_cell(regie, user):
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.save()
    cell = LingoBasketCell(page=page, placeholder='content', order=0)

    context = {'request': RequestFactory().get('/')}
    context['request'].user = None
    assert cell.is_relevant(context) is False
    assert cell.get_badge(context) is None
    context['request'].user = user
    assert cell.is_relevant(context) is False
    assert cell.get_badge(context) is None
    item = BasketItem()
    item.user = user
    item.regie = regie
    item.subject = 'foo'
    item.source_url = 'http://example.net'
    item.amount = 12345
    item.save()
    item_anonymous = BasketItem()
    item_anonymous.user = None
    item_anonymous.regie = regie
    item_anonymous.subject = 'randall'
    item_anonymous.source_url = 'http://example.net'
    item_anonymous.amount = 667
    item_anonymous.save()

    assert cell.is_relevant(context) is True
    assert cell.get_badge(context) == {'badge': '12345€'}

    item.cancellation_date = timezone.now()
    item.save()
    assert cell.is_relevant(context) is False

    item.cancellation_date = None
    item.save()

    content = cell.render(context)
    assert '12345' in content
    assert '667' not in content

    item.amount = 123.45
    item.save()
    assert cell.get_badge(context) == {'badge': '123.45€'}


def test_basket_cell_can_pay_only_one_basket_item(regie, user):
    regie.can_pay_only_one_basket_item = True
    regie.save()
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.save()
    cell = LingoBasketCell(page=page, placeholder='content', order=0)

    item = BasketItem.objects.create(user=user, regie=regie, subject='foo', amount=123)
    BasketItem.objects.create(user=user, regie=regie, subject='bar', amount=123)

    context = {'request': RequestFactory(user=user).get('/')}
    context['request'].user = user
    assert cell.is_relevant(context)

    content = cell.render(context)
    assert content.count('Pay') == 2
    assert item.payment_url in content
    assert 'Total' not in content


def test_recent_transaction_cell(regie, user):
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.save()
    cell = LingoRecentTransactionsCell(page=page, placeholder='content', order=0)

    context = {'request': RequestFactory().get('/')}
    context['request'].user = None
    assert cell.is_relevant(context) is False

    context['request'].user = user
    assert cell.is_relevant(context) is False

    transaction = Transaction(user=user, status=eopayment.PAID)
    transaction.save()

    assert cell.is_relevant(context) is True

    item = BasketItem()
    item.user = user
    item.regie = regie
    item.subject = 'foo'
    item.source_url = 'http://example.net'
    item.amount = 12345
    item.save()

    transaction.items.add(item)

    content = cell.render(context)
    assert '12345' in content
    assert item.source_url in content
    assert 'open' in content

    item.source_url = ''
    item.save()
    content = cell.render(context)
    assert not 'open' in content


def test_basket_link_cell(regie, user):
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.save()
    cell = LingoBasketLinkCell(page=page, placeholder='content', order=0)

    context = {'request': RequestFactory().get('/')}
    context['request'].user = None
    assert cell.is_relevant(context) is False
    context['request'].user = user
    assert cell.is_relevant(context) is False
    item = BasketItem()
    item.user = user
    item.regie = regie
    item.subject = 'foo'
    item.source_url = 'http://example.net'
    item.amount = 12345
    item.save()

    assert cell.is_relevant(context) is True

    # no basket cell to link to
    content = cell.render(context)
    assert content == ''

    cell2 = LingoBasketCell(page=page, placeholder='content', order=0)
    cell2.save()

    content = cell.render(context)
    assert '12345' in content
    assert page.get_online_url() in content


def test_basket_link_cell_in_skeleton(app, regie, user):
    Page.objects.all().delete()
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.redirect_url = 'http://example.net/foo/'
    page.save()
    cell = LingoBasketLinkCell(page=page, placeholder='content', order=0)
    cell.save()
    app.get('/__skeleton__/?source=%s' % quote('http://example.net/foo/bar'))


def test_tipi_cell():
    page = Page(title='example page', slug='example-page')
    page.save()
    cell = TipiPaymentFormCell()
    cell.page = page
    cell.title = 'TIPI Payment'
    cell.regies = '1234'
    cell.order = 0
    cell.save()
    assert cell.control_protocol == 'pesv2'
    assert cell.url == 'https://www.payfip.gouv.fr/tpa/paiement.web'
    assert cell.default_template_name == 'lingo/tipi_form.html'
    html = cell.render({})
    assert '<h2>TIPI Payment</h2>' in html
    assert 'Community identifier' not in html
    assert '<input type="hidden" id="numcli" value="1234" />' in html
    assert 'id="exer"' in html
    assert 'id="idpce"' in html
    assert 'id="idligne"' in html
    assert 'id="rolrec"' not in html
    assert 'id="roldeb"' not in html
    assert 'id="roldet"' not in html
    assert 'data-saisie="M"' in html
    assert 'data-pesv2="True"' in html

    cell.regies = '1234 - test regie'
    cell.save()
    html = cell.render({})
    assert '<input type="hidden" id="numcli" value="1234" />' in html

    cell.regies = 'test regie'
    cell.save()
    html = cell.render({})
    assert '<input type="hidden" id="numcli" value="" />' in html

    cell.control_protocol = 'rolmre'
    cell.test_mode = True
    cell.save()
    html = cell.render({})
    assert 'id="rolrec"' in html
    assert 'id="roldeb"' in html
    assert 'id="roldet"' in html
    assert 'id="idpce"' not in html
    assert 'id="idligne"' not in html
    assert 'data-saisie="T"' in html
    assert 'data-pesv2="False"' in html

    cell_media = str(cell.media)
    assert 'js/tipi.js' in cell_media

    cell.regies = '1 regie1, 2- regie2,3 : regie3,4,5regie5 ,bad-format-regie 6'
    cell.save()
    html = cell.render({})
    assert 'Community identifier' in html
    assert '<select id="numcli">' in html
    assert '<option value="1">1 regie1</option>' in html
    assert '<option value="2">2- regie2</option>' in html
    assert '<option value="3">3 : regie3</option>' in html
    assert '<option value="4">4</option>' in html
    assert '<option value="5">5regie5</option>' in html

    # set reference default values and check they are filled and readonly
    cell.exer = '1234'
    cell.rolrec = '00'
    cell.save()
    html = cell.render({})
    assert 'value="1234" readonly' in html
    assert 'value="00" readonly' in html
