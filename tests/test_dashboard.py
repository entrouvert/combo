import datetime
import json
import os
import urllib.parse
from unittest import mock

import pytest
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db import connection
from django.template import Context, Template
from django.test import override_settings
from django.test.utils import CaptureQueriesContext
from django.urls import reverse

from combo.apps.dashboard.models import DashboardCell, Tile
from combo.data.models import Page, TextCell

from .test_manager import login

pytestmark = pytest.mark.django_db

ConfigJsonCell_TEMPLATE = '''
{% load dashboard %}
{% with tile=cell|as_dashboard_cell:request.user %}
    {% if tile %}checked{% endif %}
{% endwith %}
'''


@pytest.fixture()
def rest_framework_no_session(settings):
    rest_framework_conf = settings.REST_FRAMEWORK
    rest_framework_conf['DEFAULT_AUTHENTICATION_CLASSES'] = [
        'rest_framework.authentication.BasicAuthentication'
    ]
    with override_settings(REST_FRAMEWORK=rest_framework_conf):
        yield None


@pytest.fixture
def site(admin_user, rest_framework_no_session):
    page = Page(title='One', slug='index')
    page.save()
    # order will be useful to get to the cell later on
    for i in range(100, 110):
        cell = TextCell(page=page, order=i, placeholder='content', text='hello world (%s)' % i)
        cell.save()

    page = Page(title='Two', slug='two')
    page.save()
    dashboard_cell = DashboardCell(page=page, order=0, placeholder='content')
    dashboard_cell.save()


def test_empty_dashboard(app, site):
    resp = app.get('/', status=200)
    assert 'hello world' in resp.text
    resp = app.get('/two/', status=200)
    assert 'dashboardcell' not in resp.text
    app = login(app)
    resp = app.get('/two/', status=200)
    assert 'dashboardcell' in resp.text


def test_add_to_dashboard(app, site):
    app = login(app)
    cell = TextCell.objects.get(order=100)
    dashboard = DashboardCell.objects.all()[0]
    user = User.objects.all()[0]
    resp = app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert urllib.parse.urlparse(resp.location).path == dashboard.page.get_online_url()
    assert Tile.objects.count() == 1
    assert Tile.objects.all()[0].cell.id != cell.id
    assert Tile.objects.all()[0].cell.text == cell.text
    assert Tile.objects.all()[0].dashboard_id == dashboard.id
    assert Tile.objects.all()[0].user_id == user.id
    assert Tile.objects.all()[0].order == 0

    resp = app.get('/two/', status=200)
    assert 'hello world' in resp.text

    # retrieve tile using cell reference
    ctx = Context({'cell': cell, 'request': {'user': user}})
    t = Template(ConfigJsonCell_TEMPLATE)
    assert 'checked' in t.render(ctx)

    # missing dashboard cell
    dashboard.delete()
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}), status=404)


def test_add_to_dashboard_order(app, site):
    app = login(app)

    cell = TextCell.objects.get(order=100)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 1

    cell = TextCell.objects.get(order=101)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 2
    assert Tile.objects.all()[0].cell.text == 'hello world (100)'
    assert Tile.objects.all()[1].cell.text == 'hello world (101)'

    cell = TextCell.objects.get(order=102)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 3
    assert Tile.objects.all()[0].cell.text == 'hello world (100)'
    assert Tile.objects.all()[1].cell.text == 'hello world (101)'
    assert Tile.objects.all()[2].cell.text == 'hello world (102)'

    with override_settings(COMBO_DASHBOARD_NEW_TILE_POSITION='first'):
        cell = TextCell.objects.get(order=103)
        app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
        assert Tile.objects.count() == 4
        assert Tile.objects.all()[0].cell.text == 'hello world (103)'
        assert Tile.objects.all()[1].cell.text == 'hello world (100)'
        assert Tile.objects.all()[2].cell.text == 'hello world (101)'
        assert Tile.objects.all()[3].cell.text == 'hello world (102)'

    cell = TextCell.objects.get(order=104)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 5
    assert Tile.objects.all()[0].cell.text == 'hello world (103)'
    assert Tile.objects.all()[1].cell.text == 'hello world (100)'
    assert Tile.objects.all()[2].cell.text == 'hello world (101)'
    assert Tile.objects.all()[3].cell.text == 'hello world (102)'
    assert Tile.objects.all()[4].cell.text == 'hello world (104)'


def test_ajax_add_to_dashboard(app, site):
    cell = TextCell.objects.get(order=100)
    resp = app.get(
        reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}),
        headers={'x-requested-with': 'XMLHttpRequest'},
        status=403,
    )
    assert Tile.objects.count() == 0

    app = login(app)
    resp = app.get(
        reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}),
        headers={'x-requested-with': 'XMLHttpRequest'},
    )
    assert resp.json['err'] == 0
    assert resp.json['url'] == 'http://testserver/two/'
    assert Tile.objects.count() == 1


def test_ajax_render(app, site):
    app = login(app)
    cell = TextCell.objects.get(order=100)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 1

    app.reset()  # logout
    tile = Tile.objects.all()[0]
    page = Page.objects.get(slug='two')
    app.get(
        reverse(
            'combo-public-ajax-page-cell',
            kwargs={'page_pk': page.id, 'cell_reference': tile.cell.get_reference()},
        ),
        status=403,
    )

    app = login(app)
    app.get(
        reverse(
            'combo-public-ajax-page-cell',
            kwargs={'page_pk': page.id, 'cell_reference': tile.cell.get_reference()},
        ),
        status=200,
    )

    User.objects.create_user('plop', email=None, password='plop')
    app = login(app, username='plop', password='plop')
    app.get(
        reverse(
            'combo-public-ajax-page-cell',
            kwargs={'page_pk': page.id, 'cell_reference': tile.cell.get_reference()},
        ),
        status=403,
    )


def test_remove_from_dashboard(app, site):
    app = login(app)
    cell = TextCell.objects.get(order=100)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 1

    # retrieve tile using cell reference
    user = User.objects.all()[0]
    ctx = Context({'cell': cell, 'request': {'user': user}})
    t = Template(ConfigJsonCell_TEMPLATE)
    assert 'checked' in t.render(ctx)

    app.reset()  # logout
    tile = Tile.objects.all()[0]
    app.get(
        reverse('combo-dashboard-remove-tile', kwargs={'cell_reference': tile.cell.get_reference()}),
        status=403,
    )

    app = login(app)
    app.get(
        reverse('combo-dashboard-remove-tile', kwargs={'cell_reference': tile.cell.get_reference()}),
        status=302,
    )
    assert Tile.objects.count() == 0
    assert TextCell.objects.count() == 11  # the cell itself is kept

    # tile no more available using cell reference
    assert 'checked' not in t.render(ctx)


def test_ajax_remove_from_dashboard(app, site):
    app = login(app)
    cell = TextCell.objects.get(order=100)
    app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.count() == 1

    tile = Tile.objects.all()[0]
    resp = app.get(
        reverse('combo-dashboard-remove-tile', kwargs={'cell_reference': tile.cell.get_reference()}),
        headers={'x-requested-with': 'XMLHttpRequest'},
        status=200,
    )
    assert resp.json['err'] == 0
    assert resp.json['url'] == 'http://testserver/two/'
    assert Tile.objects.count() == 0


def test_reorder_dashboard(app, site):
    user = User.objects.all()[0]
    dashboard = DashboardCell.objects.all()[0]

    # add cells to dashboard
    app = login(app)
    for i in range(103, 108):
        app.get(
            reverse(
                'combo-dashboard-add-tile',
                kwargs={'cell_reference': TextCell.objects.get(order=i).get_reference()},
            )
        )
    assert Tile.objects.count() == 5

    # check reordering
    app.reset()  # logout
    tiles = Tile.objects.filter(user=user).order_by('?')
    new_order = ','.join([str(x.id) for x in tiles])
    app.get(
        reverse('combo-dashboard-reorder-tiles', kwargs={'dashboard_id': dashboard.id}),
        params={'order': new_order},
        status=403,
    )

    app = login(app)
    app.get(
        reverse('combo-dashboard-reorder-tiles', kwargs={'dashboard_id': dashboard.id}),
        params={'order': new_order},
    )

    tiles = Tile.objects.filter(user=user)
    assert new_order == ','.join([str(x.id) for x in tiles])

    app.get(
        reverse('combo-dashboard-reorder-tiles', kwargs={'dashboard_id': dashboard.id}),
        params={'order': new_order + ',2133'},
    )


def test_auto_tile(app, site):
    templates_settings = [settings.TEMPLATES[0].copy()]
    templates_settings[0]['DIRS'] = ['%s/templates-1' % os.path.abspath(os.path.dirname(__file__))]
    with override_settings(
        JSON_CELL_TYPES={
            'test-config-json-cell': {
                'name': 'Foobar',
                'url': 'http://test/',
                'form': [
                    {
                        'varname': 'var1',
                    }
                ],
            }
        },
        TEMPLATES=templates_settings,
    ):
        with mock.patch('combo.utils.requests.get') as requests_get:
            # logged out
            requests_get.return_value = mock.Mock(content='<div>HELLO</div>', status_code=200)
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params=json.dumps({'var1': 'one', 'var2': 'two'}),
                content_type='application/json',
            )
            assert resp.text.strip() == '/var1=one/var2=/'

            # and logged in
            app = login(app)
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params=json.dumps({'var1': 'one', 'var2': 'two'}),
                content_type='application/json',
            )
            assert resp.text.strip() == '/var1=one/var2=/'

            # with invalid cell key
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'missing'}),
                params=json.dumps({'var1': 'one', 'var2': 'two'}),
                content_type='application/json',
                status=400,
            )

            # with missing data
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params=json.dumps({'var2': 'two'}),
                content_type='application/json',
                status=400,
            )

            # and with a GET instead of POST
            resp = app.get(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}), status=405
            )
            # bad json
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params='',
                status=400,
            )

            # missing dashboard cell
            DashboardCell.objects.all().delete()
            app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params=json.dumps({'var1': 'one', 'var2': 'two'}),
                content_type='application/json',
                status=404,
            )


def test_clean_autotiles(app, site):
    appconfig = apps.get_app_config('dashboard')
    appconfig.clean_autotiles()


def test_dashboard_invalid_cells(app, site):
    app = login(app)
    cell = TextCell.objects.get(order=100)
    dashboard = DashboardCell.objects.all()[0]
    user = User.objects.all()[0]
    resp = app.get(reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}))
    assert Tile.objects.all()[0].cell.id != cell.id
    assert Tile.objects.all()[0].cell.text == cell.text
    assert Tile.objects.all()[0].dashboard_id == dashboard.id
    assert Tile.objects.all()[0].user_id == user.id
    assert Tile.objects.all()[0].order == 0

    app = login(app)
    resp = app.get('/two/', status=200)
    assert 'hello world' in resp.text

    cell = Tile.objects.all()[0].cell
    cell.mark_as_invalid(reason_code='plop')
    resp = app.get('/two/', status=200)
    assert 'hello world' in resp.text

    validity_info = cell.get_validity_info()
    validity_info.invalid_since = validity_info.invalid_since - datetime.timedelta(days=3)
    validity_info.save()

    resp = app.get('/two/', status=200)
    assert 'hello world' not in resp.text


def test_dashboard_tile_stats(app, site):
    templates_settings = [settings.TEMPLATES[0].copy()]
    templates_settings[0]['DIRS'] = ['%s/templates-1' % os.path.abspath(os.path.dirname(__file__))]
    with override_settings(
        JSON_CELL_TYPES={
            'test-config-json-cell': {
                'name': 'Foobar',
                'url': 'http://test/',
                'form': [
                    {
                        'varname': 'var1',
                    }
                ],
            }
        },
        TEMPLATES=templates_settings,
    ):
        app = login(app)

        with mock.patch('combo.utils.requests.get') as requests_get:
            requests_get.return_value = mock.Mock(content='<div>HELLO</div>', status_code=200)
            resp = app.post(
                reverse('combo-dashboard-auto-tile', kwargs={'key': 'test-config-json-cell'}),
                params=json.dumps({'var1': 'one', 'var2': 'two'}),
                content_type='application/json',
            )
            resp = app.get(resp.headers['X-Add-To-Dashboard-Url'])

            app.authorization = ('Basic', ('admin', 'admin'))
            stats = app.get(reverse('combo-dashboard-tile-stats'))
            assert stats.json['users']['have-tiles'] == 1


def test_manager_queries(app, site):
    app = login(app)

    cell = TextCell.objects.get(order=100)
    dashboard_cell = DashboardCell.objects.all().first()

    # add a non-dashboard cell to dashboard page to get the text cell
    # content type declared in dashboard page related_cells attribute.
    TextCell.objects.create(page=dashboard_cell.page, order=0, placeholder='content', text='hello world')

    with CaptureQueriesContext(connection) as ctx:
        app.get('/manage/pages/%s/' % dashboard_cell.page_id)
        no_tile_count = len(ctx.captured_queries)
        len_prefetch_groups = len(
            [
                x['sql']
                for x in ctx.captured_queries
                if 'data_textcell_groups' in x['sql'] and '_prefetch_related_val_textcell_id' in x['sql']
            ][0]
        )

    # add tiles
    for _ in range(10):
        app.get(
            reverse('combo-dashboard-add-tile', kwargs={'cell_reference': cell.get_reference()}),
            headers={'x-requested-with': 'XMLHttpRequest'},
        )
    assert Tile.objects.count() == 10

    # check there are no new queries and the prefetch query didn't grow.
    with CaptureQueriesContext(connection) as ctx:
        app.get('/manage/pages/%s/' % dashboard_cell.page_id)
        assert len(ctx.captured_queries) <= no_tile_count
        new_len_prefetch_groups = len(
            [
                x['sql']
                for x in ctx.captured_queries
                if 'data_textcell_groups' in x['sql'] and '_prefetch_related_val_textcell_id' in x['sql']
            ][0]
        )
        assert new_len_prefetch_groups <= len_prefetch_groups
