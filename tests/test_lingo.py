from django.core.management import call_command

from combo.apps.lingo.models import PaymentBackend


def test_post_migrate(db):
    '''manual_validation is a transaction scope parameter on the systempayv2
    backend, check it is transfered after a migration.'''

    payment_backend = PaymentBackend.objects.create(
        label='test', slug='test', service='systempayv2', service_options={'manual_validation': True}
    )
    regie = payment_backend.regie_set.create(label='test', slug='test', description='test')

    call_command('migrate', 'lingo')

    payment_backend.refresh_from_db()
    regie.refresh_from_db()
    assert 'manual_validation' not in payment_backend.service_options
    assert 'manual_validation' in regie.transaction_options
