from pyquery import PyQuery


def manager_submit_cell(form, expect_errors=False):
    # submit cell edit form and replace current page body with
    # new tab contents.
    resp = form.response
    action = form.action
    pq = PyQuery(resp.body)
    resp2 = form.submit()
    for tab in resp2.json['tabs']:
        pq.find('form[action="%s"] [data-tab-slug="%s"]' % (action, tab)).html(
            resp2.json['tabs'][tab]['form']
        )
    resp.text = pq.html()
    resp._forms_indexed = None
    assert expect_errors or not resp2.json['errorlist'], 'got unexpected errors'
    return resp2
