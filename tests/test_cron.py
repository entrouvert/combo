from io import StringIO
from unittest import mock

import pytest
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import override_settings

pytestmark = pytest.mark.django_db


@override_settings(STATISTICS_PROVIDERS=[])
def test_cron_run(app):
    with mock.patch('combo.apps.lingo.apps.AppConfig.hourly') as hourly_job:
        call_command('cron')
        assert hourly_job.call_count == 1

        call_command('cron', application='dashboard')
        assert hourly_job.call_count == 1

        call_command('cron', application='lingo')
        assert hourly_job.call_count == 2


def test_cron_error(app):
    with mock.patch('combo.apps.lingo.apps.AppConfig.hourly') as hourly_job:
        hourly_job.side_effect = Exception('test error')
        out = StringIO()
        with pytest.raises(CommandError):
            call_command('cron', application='lingo', verbosity=0, stdout=out)
        assert out.getvalue() == ''

        out = StringIO()
        with pytest.raises(CommandError):
            call_command('cron', application='lingo', verbosity=1, stdout=out)
        assert out.getvalue() == 'combo.apps.lingo: error: test error\n'

        out = StringIO()
        with pytest.raises(CommandError):
            call_command('cron', application='lingo', verbosity=2, stdout=out)
        assert out.getvalue().startswith('combo.apps.lingo: error: test error\n')
        assert 'Traceback' in out.getvalue()
