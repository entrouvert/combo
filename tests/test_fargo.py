import re
from unittest import mock

import pytest
from django.test import override_settings

from combo.apps.fargo.models import RecentDocumentsCell
from combo.data.models import Page

from .test_manager import login

pytestmark = pytest.mark.django_db


def test_manager_fargo_cell_setup(app, admin_user):
    Page.objects.all().delete()
    page = Page(title='One', slug='one', template_name='standard')
    page.save()
    app = login(app)
    resp = app.get('/manage/pages/%s/' % page.id)
    assert not resp.html.find('option', **{'data-add-url': re.compile('recentdoc')})

    with override_settings(
        KNOWN_SERVICES={
            'fargo': {
                'default': {
                    'title': 'test',
                    'url': 'http://example.org',
                    'secret': 'combo',
                    'orig': 'combo',
                    'backoffice-menu-url': 'http://example.org/manage/',
                }
            }
        }
    ):
        resp = app.get('/manage/pages/%s/' % page.id)
        assert resp.html.find('option', **{'data-add-url': re.compile('recentdoc')})
        resp = app.get(resp.html.find('option', **{'data-add-url': re.compile('recentdoc')})['data-add-url'])

        cells = Page.objects.get(id=page.id).get_cells()
        assert len(cells) == 1
        assert isinstance(cells[0], RecentDocumentsCell)
        resp = app.get('/manage/pages/%s/' % page.id)

    with override_settings(
        KNOWN_SERVICES={
            'fargo': {
                'default': {
                    'title': 'test',
                    'url': 'http://example.org',
                    'secret': 'combo',
                    'orig': 'combo',
                    'backoffice-menu-url': 'http://example.org/manage/',
                },
                'second': {
                    'title': 'test2',
                    'url': 'http://example.org',
                    'secret': 'combo',
                    'orig': 'combo',
                    'backoffice-menu-url': 'http://example.org/manage/',
                },
            }
        }
    ):
        resp = app.get('/manage/pages/%s/' % page.id)
        cells = Page.objects.get(id=page.id).get_cells()
        resp.forms[0]['c%s-fargo_site' % cells[0].get_reference()].value = 'second'
        resp = resp.forms[0].submit()
        cells = Page.objects.get(id=page.id).get_cells()
        assert cells[0].fargo_site == 'second'


def test_fargo_cell_display(app, admin_user):
    Page.objects.all().delete()
    page = Page(title='One', slug='one', template_name='standard')
    page.save()

    cell = RecentDocumentsCell(page=page, order=0)
    cell.save()

    with (
        override_settings(
            KNOWN_SERVICES={
                'fargo': {
                    'default': {
                        'title': 'test',
                        'url': 'http://example.org',
                        'secret': 'combo',
                        'orig': 'combo',
                        'backoffice-menu-url': 'http://example.org/manage/',
                    }
                }
            }
        ),
        mock.patch('combo.utils.requests.get') as requests_get,
    ):
        data = {'results': [{'url': 'http://example.net/', 'label': 'foobar'}]}
        requests_get.return_value = mock.Mock(json=lambda: data, status_code=200)
        result = cell.render({'synchronous': True})
        assert 'http://example.net/' in result
        assert 'foobar' in result
