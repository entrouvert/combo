Combo
=====

Combo is a simple content management system, tailored to create simple
websites, and with a specialization in aggregating contents from different
sources.

Installation
------------

Dependencies can be installed with pip,

 $ pip install -r requirements.txt

It's then required to get the database configured (./manage.py migrate); by
default it will create a postgresqsl DB.

You can then run the Django test server for a quick try (you should refer to
the Django documentation for production deployments).

 $ ./manage.py runserver


Architecture
------------

Combo manages content as a series of pages (objects of type 'Page'), that can
be sorted ('order' attribute) and hierarchically ordered ('parent' attribute).

Every pages have a title and a "slug", that is used as the page URL; a page
with 'index' as its slug will be served as the index page of the site.

The pages are set to use a template; the templates are defined in the settings
file (COMBO_PUBLIC_TEMPLATES) and are made of a name, a template file, and a
serie of "placeholders", identifying locations in the page (main content, side
bar, footer...).

Example:

    'standard': {
        'name': 'Standard',
        'template': 'combo/page_template.html',
        'placeholders': {
            'content': {
                'name': 'Content',
            },
            'footer': {
                'name': 'Footer',
                'acquired': True,
            },
        }
    }

The content of a page is defined as a serie of cells, of which there are
various types (they are all subclasses of CellBase); each cell is also
associated with a placeholder ('placeholder' attribute) and its order within
('order' attribute).

A placeholder can be marked as 'acquired' (see "footer" in the example above),
this way a cell of "same as parent" type will automatically be added.


Settings
--------

Default settings are loaded from settings.py, they can be overloaded by a
local_settings.py file set in the same directory, or by a file referenced
in the COMBO_SETTINGS_FILE environment variable.

SAML authentication can be enabled by adding 'mellon' to INSTALLED_APPS and
'mellon.backends.SAMLBackend' to AUTHENTICATION_BACKENDS, this requires
django-mellon to be installed, and further files and settings are required:

 - public and private keys (in cert.pem and key.cert in the current working
   directory, or from files defined in the MELLON_PUBLIC_KEYS and
   MELLON_PRIVATE_KEY settings)
 - metadata of the identity provider (in idp-metadata.xml, or defined using
   the MELLON_IDENTITY_PROVIDERS settings)

Details on these options and additional SAML settings are available in the
documentation of django-mellon.


Tests
-----

Unit tests are written using py.test, and its pytest-django support library.

  DJANGO_SETTINGS_MODULE=combo.settings COMBO_SETTINGS_FILE=tests/settings.py py.test


Code Style
----------

black is used to format the code, using thoses parameters:

    black --target-version py37 --skip-string-normalization --line-length 110

isort is used to format the imports, using those parameters:

    isort --profile black --line-length 110

pyupgrade is used to automatically upgrade syntax, using those parameters:

    pyupgrade --keep-percent-format --py37-plus

djhtml is used to automatically indent html files, using those parameters:

    djhtml --tabwidth 2

django-upgrade is used to automatically upgrade Django syntax, using those parameters:

    django-upgrade --target-version 3.2

There is .pre-commit-config.yaml to use pre-commit to automatically run these tools
before commits. (execute `pre-commit install` to install the git hook.)


License
-------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.


Combo embeds some other pieces of code, with their own authors and copyright
notices:

Gauge.js
  Files: combo/apps/dataviz/static/js/gauge.min.js
  License: MIT
  Comment:
   From http://bernii.github.io/gauge.js/

Pygal.tooltip.js
  Files: combo/apps/dataviz/static/js/pygal.tooltip.js
  Copyright: 2015, Florian Mounier Kozea
  License: LGPL-3+
  Comment:
   From https://github.com/Kozea/pygal.js/
