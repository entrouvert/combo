# This file is sourced by "execfile" from combo.settings

import os

PROJECT_NAME = 'combo'

#
# hobotization (multitenant)
#
exec(open('/usr/lib/hobo/debian_config_common.py').read())

# add custom hobo agent module
INSTALLED_APPS = ('hobo.agent.combo',) + INSTALLED_APPS

#
# serve assets using nginx X-Accel
#
COMBO_X_ACCEL_ASSETS = True

#
# local settings
#
exec(open(os.path.join(ETC_DIR, 'settings.py')).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
